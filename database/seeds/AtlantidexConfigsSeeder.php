<?php

use Illuminate\Database\Seeder;

class AtlantidexConfigsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('settings')->insert([
            'key' => 'change_sponsor_intro_it',
            'name' => 'Change sponsor intro IT',
            'description' => 'The intro before download the sponsor change',
            'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Neque viverra justo nec ultrices dui sapien eget mi. Nibh venenatis cras sed felis eget velit aliquet sagittis id. Auctor augue mauris augue neque gravida. Et ultrices neque ornare aenean euismod elementum nisi quis. Placerat orci nulla pellentesque dignissim. Auctor eu augue ut lectus. Scelerisque viverra mauris in aliquam sem fringilla ut morbi. Ut placerat orci nulla pellentesque dignissim enim. Interdum velit laoreet id donec ultrices tincidunt arcu non sodales. Fermentum iaculis eu non diam phasellus.',
            'field' => '{"name":"value","label":"Intro","type":"textarea"}',
            'active' => 1
        ]);

        DB::table('settings')->insert([
            'key' => 'change_sponsor_intro_en',
            'name' => 'Change sponsor intro EN',
            'description' => 'The intro before download the sponsor change',
            'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Neque viverra justo nec ultrices dui sapien eget mi. Nibh venenatis cras sed felis eget velit aliquet sagittis id. Auctor augue mauris augue neque gravida. Et ultrices neque ornare aenean euismod elementum nisi quis. Placerat orci nulla pellentesque dignissim. Auctor eu augue ut lectus. Scelerisque viverra mauris in aliquam sem fringilla ut morbi. Ut placerat orci nulla pellentesque dignissim enim. Interdum velit laoreet id donec ultrices tincidunt arcu non sodales. Fermentum iaculis eu non diam phasellus.',
            'field' => '{"name":"value","label":"Intro","type":"textarea"}',
            'active' => 1
        ]);

        DB::table('settings')->insert([
            'key' => 'document_types',
            'name' => 'Document types',
            'description' => 'Document types',
            'value' => '[{"value":"change_sponsor","label":"Cambio sponsor"},{"value":"standard","label":"Standard"},{"value":"networker_letter_of_intent","label":"Lettera di intenti per Networker"}]',
            'field' => '{"name":"value","label":"Intro","type":"table","columns":{"value":"Value","label":"Label"}}',
            'active' => 1
        ]);


        DB::table('settings')->insert([
            'key' => 'vouchers_intro_it',
            'name' => 'Vouchers intro IT',
            'description' => 'The intro before voucher',
            'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Neque viverra justo nec ultrices dui sapien eget mi. Nibh venenatis cras sed felis eget velit aliquet sagittis id. Auctor augue mauris augue neque gravida. Et ultrices neque ornare aenean euismod elementum nisi quis. Placerat orci nulla pellentesque dignissim. Auctor eu augue ut lectus. Scelerisque viverra mauris in aliquam sem fringilla ut morbi. Ut placerat orci nulla pellentesque dignissim enim. Interdum velit laoreet id donec ultrices tincidunt arcu non sodales. Fermentum iaculis eu non diam phasellus.',
            'field' => '{"name":"value","label":"Intro","type":"textarea"}',
            'active' => 1
        ]);

        DB::table('settings')->insert([
            'key' => 'vouchers_intro_en',
            'name' => 'Vouchers intro EN',
            'description' => 'The intro before voucher',
            'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Neque viverra justo nec ultrices dui sapien eget mi. Nibh venenatis cras sed felis eget velit aliquet sagittis id. Auctor augue mauris augue neque gravida. Et ultrices neque ornare aenean euismod elementum nisi quis. Placerat orci nulla pellentesque dignissim. Auctor eu augue ut lectus. Scelerisque viverra mauris in aliquam sem fringilla ut morbi. Ut placerat orci nulla pellentesque dignissim enim. Interdum velit laoreet id donec ultrices tincidunt arcu non sodales. Fermentum iaculis eu non diam phasellus.',
            'field' => '{"name":"value","label":"Intro","type":"textarea"}',
            'active' => 1
        ]);



        //
    }
}

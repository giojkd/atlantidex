<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCareersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('careeroperations', function (Blueprint $table) {

            $table->id();
            $table->timestamps();
            $table->softDeletes();


            $table->integer('user_id')->index(); #<---------


            $table->integer('line_id')->index(); #<----- nel momento in cui è stata fatta l'operazione l'utente occupava questa linea del tipo di sopra
            $table->integer('affiliated_user_id')->index(); #<------- lui sta sotto a quello sopra

            $table->integer('earning_points');

            $table->integer('sign')->default(1);

            $table->integer('order_id')->index()->nullable();

            $table->float('amount')->nullable();

            $table->string('explanation')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('careeroperations');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewsTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('reviews', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->timestamps();
      $table->softDeletes();
      $table->json('name')->nullable();
      $table->json('description')->nullable();
      $table->integer('user_id')->nullable()->index();
      $table->string('reviewable_type')->nullable()->index();
      $table->integer('reviewable_id')->nullable()->index();
      $table->float('score')->nullable();
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('reviews');
  }
}

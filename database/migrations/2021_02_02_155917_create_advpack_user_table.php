<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvpackUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advpack_user', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('advpack_id')->nullable()->index();
            $table->integer('user_id')->nullable()->index();
            $table->unique(['advpack_id','user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advpack_user');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInvoicingCompanyColumnToInvoicerowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoicerows', function (Blueprint $table) {
            //
            $table->string('invoicing_company')->nullable()->default('atlantidex');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoicerows', function (Blueprint $table) {
            //
        });
    }
}

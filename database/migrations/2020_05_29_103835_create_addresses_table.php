<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('user_id')->index();

            $table->json('address');
            $table->string('type');
            $table->string('phone');
            $table->string('name');

            $table->string('address_int')->nullable();
            $table->string('company_name')->nullable();
            $table->string('vat_number')->nullable();
            $table->text('notes')->nullable();
            $table->text('sdi_code')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}

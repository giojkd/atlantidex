<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManufacturersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manufacturers', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->unsignedinteger('country_id')->nullable()->default(null);
            $table->string('name')->nullable()->default(null);
            $table->json('description')->nullable()->default(null);
            $table->json('pictures')->nullable()->default(null);
            $table->tinyInteger('active')->default('1');
            $table->index(["country_id"], 'country_id');
            $table->softDeletes();
            $table->nullableTimestamps();

            $table->foreign('country_id', 'manufacturer_country_id')
                ->references('id')->on('countries')
                ->onDelete('restrict')
                ->onUpdate('restrict');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manufacturers');
    }
}

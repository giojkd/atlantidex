<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDownloadRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('download_role', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('download_id')->nullable()->index();
            $table->integer('role_id')->nullable()->index();
            $table->unique(['download_id','role_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('download_role');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentcodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documentcodes', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('code')->nullable();
            $table->string('documentcodeable_model')->nullable();
            $table->unique(['code', 'documentcodeable_model']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documentcodes');

    }
}

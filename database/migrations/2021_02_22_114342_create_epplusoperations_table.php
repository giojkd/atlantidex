<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEpplusoperationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('epplusoperations', function (Blueprint $table) {

            $table->id();
            $table->timestamps();
            $table->softDeletes();
            $table->float('value')->nullable();
            $table->integer('user_id')->nullable()->index();
            $table->boolean('processed')->nullable()->default(0);
            $table->timestamp('valid_at')->nullable();
            $table->integer('epplusoperationable_id')->nullable()->index();
            $table->string('epplusoperationable_type')->nullable()->index();
            $table->text('description')->nullable();
            $table->string('epplus_type')->nullable();
            $table->integer('sign')->default(1);
            $table->boolean('is_careerable')->default(0);
            $table->string('user_role')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('epplusoperations');
    }
}

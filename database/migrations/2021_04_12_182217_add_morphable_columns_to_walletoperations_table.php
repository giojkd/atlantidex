<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMorphableColumnsToWalletoperationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('walletoperations', function (Blueprint $table) {
            //
            $table->integer('walletoperationable_id')->index()->nullable();
            $table->string('walletoperationable_type')->index()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('walletoperations', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBidsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'bids';

    /**
     * Run the migrations.
     * @table bids
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->unsignedBigInteger('parent_id')->nullable()->default(0);
            $table->integer('lft')->nullable()->default(null);
            $table->integer('rgt')->nullable()->default(null);
            $table->integer('depth')->nullable()->default(null);
            $table->unsignedBigInteger('user_id');
            $table->float('total')->nullable()->default(null);
            $table->integer('outcome')->default('0');
            $table->timestamp('valid_until')->nullable()->default(null);

            $table->index(["parent_id"], 'parent_id');

            $table->index(["user_id"], 'user_id');
            $table->softDeletes();
            $table->nullableTimestamps();



            $table->foreign('user_id', 'bids_user_id')
                ->references('id')->on('users')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}

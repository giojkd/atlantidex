<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'orders';

    /**
     * Run the migrations.
     * @table orders
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->unsignedBigInteger('bid_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('merchant_id');
            $table->unsignedinteger('order_status_id');
            $table->float('total')->nullable()->default(null);

            $table->index(["order_status_id"], 'order_status_id');

            $table->index(["merchant_id"], 'merchant_id');

            $table->index(["user_id"], 'user_id');

            $table->index(["bid_id"], 'bid_id');
            $table->softDeletes();
            $table->nullableTimestamps();


            $table->foreign('bid_id', 'orders_bid_id')
                ->references('id')->on('bids')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('user_id', 'orders_user_id')
                ->references('id')->on('users')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('merchant_id', 'orders_merchant_id')
                ->references('id')->on('merchants')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('order_status_id', 'orders_order_status_id')
                ->references('id')->on('order_statuses')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function (Blueprint $table) {

            $table->id();
            $table->timestamps();
            $table->softDeletes();

            $table->integer('leadstatus_id')->integer()->default(1);
            $table->timestamp('confirmed_at')->nullable();
            $table->boolean('consolidated_at')->default(0);

            $table->float('grand_total')->nullable()->default(0);
            $table->float('shipping_total')->nullable()->default(0);
            $table->float('products_total')->nullable()->default(0);
            $table->float('discount_total')->nullable()->default(0);

            $table->boolean('has_requested_invoice')->default(0)->index();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leads');
    }
}

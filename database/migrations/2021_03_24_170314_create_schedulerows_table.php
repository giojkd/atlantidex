<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchedulerowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedulerows', function (Blueprint $table) {

            $table->id();
            $table->timestamps();
            $table->date('day')->nullable();
            $table->time('time')->nullable();
            $table->integer('quantity')->nullable();
            $table->integer('product_id')->nullable()->index();
            $table->index(['day', 'time', 'product_id']);
            $table->float('price_per_adult')->nullable();
            $table->float('price_per_child')->nullable();
            $table->float('price_per_infant')->nullable();
            $table->float('price_per_unit')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedulerows');
    }
}

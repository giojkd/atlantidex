<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BidCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('bid_category', function (Blueprint $table) {
              $table->bigIncrements('id');
              $table->timestamps();
              $table->softDeletes();
              $table->integer('bid_id')->index();
              $table->integer('category_id')->index();
              $table->unique(['bid_id','category_id']);
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::dropIfExists('bid_category');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommissionoperationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commissionoperations', function (Blueprint $table) {

            $table->id();
            $table->timestamps();
            $table->integer('user_id');
            $table->float('value');
            $table->text('description')->nullable();
            $table->integer('commissionoperationable_id')->nullable()->index();
            $table->text('commissionoperationable_type')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commissionoperations');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMerchantsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'merchants';

    /**
     * Run the migrations.
     * @table merchants
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->unsignedBigInteger('users_id');
            $table->unsignedinteger('country_id')->nullable()->default(null);
            $table->string('name')->nullable()->default(null);
            $table->json('brief')->nullable()->default(null);
            $table->json('description')->nullable()->default(null);
            $table->json('pictures')->nullable()->default(null);
            $table->double('lat')->nullable()->default(null);
            $table->double('lng')->nullable()->default(null);
            $table->json('address')->nullable()->default(null);
            $table->integer('reviews_count')->nullable()->default(null);
            $table->float('reviews_avg')->nullable()->default(null);
            $table->tinyInteger('active')->default('1');


            $table->index(["users_id"], 'users_id');

            $table->index(["country_id"], 'country_id');
            $table->softDeletes();
            $table->nullableTimestamps();


            $table->foreign('users_id', 'merchants_users_id')
                ->references('id')->on('users')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('country_id', 'merchants_country_id')
                ->references('id')->on('countries')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}

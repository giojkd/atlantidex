<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToAdvpackpurchaseProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('advpackpurchase_product', function (Blueprint $table) {
            //
            $table->integer('views')->nullable()->default(0);
            $table->integer('clicks')->nullable()->default(0);
            $table->timestamp('expires_at')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('advpackpurchase_product', function (Blueprint $table) {
            //
        });
    }
}

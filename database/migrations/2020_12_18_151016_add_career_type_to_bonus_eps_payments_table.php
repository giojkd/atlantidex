<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCareerTypeToBonusEpsPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bonus_eps_payments', function (Blueprint $table) {
            //
            $table->string('career_type')->nullable()->index();

            $table->unique(['user_id', 'month_id', 'career_type']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bonus_eps_payments', function (Blueprint $table) {
            //
        });
    }
}

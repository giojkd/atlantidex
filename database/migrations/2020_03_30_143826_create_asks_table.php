<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsksTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'asks';

    /**
     * Run the migrations.
     * @table asks
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedBigInteger('bid_id');
            $table->unsignedBigInteger('merchant_id');
            $table->text('description')->nullable()->default(null);
            $table->text('url')->nullable()->default(null);
            $table->float('total')->nullable()->default(null);
            $table->timestamp('valid_until')->nullable()->default(null);

            $table->index(["merchant_id"], 'merchant_id');

            $table->index(["bid_id"], 'bid_id');
            $table->softDeletes();
            $table->nullableTimestamps();


            $table->foreign('bid_id', 'asks_bid_id')
                ->references('id')->on('bids')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('merchant_id', 'asks_merchant_id')
                ->references('id')->on('merchants')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMerchantProductTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'merchant_product';

    /**
     * Run the migrations.
     * @table merchant_product
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->unsignedBigInteger('merchant_id');
            $table->unsignedBigInteger('product_id');
            $table->unsignedinteger('product_status_id');
            $table->unsignedBigInteger('product_availability_id');
            $table->float('quantity')->nullable()->default(null);
            $table->tinyInteger('active')->default('1');

            $table->index(["product_status_id"], 'product_status_id');

            $table->index(["product_availability_id"], 'product_availability_id');

            $table->index(["product_id"], 'product_id');
            $table->softDeletes();
            $table->nullableTimestamps();


            $table->foreign('merchant_id', 'merchant_product_merchant_id')
                ->references('id')->on('merchants')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('product_id', 'merchant_product_product_id')
                ->references('id')->on('products')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('product_status_id', 'merchant_product_product_status_id')
                ->references('id')->on('product_statuses')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('product_availability_id', 'merchant_product_product_availability_id')
                ->references('id')->on('product_availabilities')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvpacksTableOnceAgain extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advpacks', function (Blueprint $table) {

            $table->id();
            $table->timestamps();
            $table->softDeletes();
            $table->json('name')->nullable();
            $table->json('short_description')->nullable();
            $table->json('description')->nullable();
            $table->json('photos')->nullable();
            $table->float('price')->nullable();

            $table->integer('upsellable')->nullable();
            $table->integer('newsletterable')->nullable();
            $table->integer('homepageable')->nullable();
            $table->integer('videoable')->nullable();

            $table->integer('days_in_upsellable')->nullable();
            $table->integer('days_in_newsletterable')->nullable();
            $table->integer('days_in_homepageable')->nullable();
            $table->integer('days_in_videoable')->nullable();

            $table->boolean('active')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advpacks');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsTuUserscompensationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('usercompensations', function (Blueprint $table) {
            $table->float('merchant_bonus_ep')->nullable()->default(0);
            $table->float('merchant_booster_bonus_ep')->nullable()->default(0);
            $table->float('marketing_bonus_ep')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

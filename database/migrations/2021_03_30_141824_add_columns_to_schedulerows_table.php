<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToSchedulerowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('schedulerows', function (Blueprint $table) {
            //
            $table->integer('adults_per_unit')->nullable();
            $table->integer('children_per_unit')->nullable();
            $table->integer('infant_per_unit')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('schedulerows', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Edit2ProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('products', function (Blueprint $table) {
            $table->json('price_per')->nullable();

            $table->integer('product_weight')->nullable();
            $table->integer('product_height')->nullable();
            $table->integer('product_lenght')->nullable();
            $table->integer('product_width')->nullable();
            $table->integer('packaging_weight')->nullable();
            $table->integer('packaging_height')->nullable();
            $table->integer('packaging_lenght')->nullable();
            $table->integer('packaging_width')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

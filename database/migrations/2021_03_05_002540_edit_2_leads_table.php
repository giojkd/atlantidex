<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Edit2LeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table(
            'leads',
            function (Blueprint $table) {
                $table->integer('leadstatus_id')->nullable()->default(1);
                $table->timestamp('consolidated_at')->nullable();
                $table->boolean('has_requested_invoice')->nullable()->default(0);
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

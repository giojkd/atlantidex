<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGdovouchercodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gdovouchercodes', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->float('value')->nullable();

            $table->integer('gdovoucher_id')->index();

            $table->string('code')->nullable();
            $table->unique(['gdovoucher_id', 'code']);

            $table->integer('user_id')->nullable()->index();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gdovouchercodes');
    }
}

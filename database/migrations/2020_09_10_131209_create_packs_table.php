<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('epoperations', function (Blueprint $table) {

            $table->id();
            $table->timestamps();
            $table->softDeletes();

            $table->float('value');
            $table->integer('user_id')->index();
            $table->boolean('processed')->default(0);
            $table->timestamp('valid_at')->nullable();
            $table->integer('epoperationable_id')->nullable();
            $table->string('epoperationable_type')->nullable();
            $table->text('description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('epoperations');
    }
}

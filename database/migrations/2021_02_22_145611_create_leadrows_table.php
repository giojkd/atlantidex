<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeadrowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leadrows', function (Blueprint $table) {

            $table->id();
            $table->timestamps();
            $table->softDeletes();

            $table->string('description')->nullable();

            $table->integer('lead_id')->nullable()->index();
            $table->integer('product_id')->nullable()->index();
            $table->integer('merchant_id')->integer()->default(0);

            $table->float('subtotal')->default(0);
            $table->float('quantity')->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leadrows');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStarterpacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packs', function (Blueprint $table) {

            $table->id();
            $table->timestamps();

            $table->json('name')->nullable();
            $table->json('description')->nullable();

            $table->float('revenue_bonus')->nullable();
            $table->float('merchant_bonus')->nullable();
            $table->float('merchant_booster_bonus')->nullable();
            $table->float('merchant_marketing_bonus')->nullable();
            $table->float('granted_eps')->nullable();
            $table->float('direct_commission')->nullable();

            $table->float('price')->nullable();

            $table->boolean('is_starter_pack')->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packs');
    }
}

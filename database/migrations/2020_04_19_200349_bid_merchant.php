<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BidMerchant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('bid_merchant', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->timestamps();
        $table->softDeletes();
        $table->integer('bid_id')->index();
        $table->integer('merchant_id')->index();
        $table->unique(['bid_id','merchant_id']);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('user_id')->index();
            $table->json('address')->nullable();
            $table->json('title');
            $table->json('short_description')->nullable();
            $table->json('description')->nullable();
            $table->dateTime('happens_at')->nullable();
            $table->json('photos')->nullable();
            $table->boolean('active')->default(0);
            $table->integer('eventtype_id')->index()->default(1);
            $table->boolean('is_online')->default(0);
            $table->integer('available_spots');
            $table->boolean('is_sold_out')->default(0);
            $table->json('tickets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumns2ToBidsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bids', function (Blueprint $table) {
            //
            $table->integer('product_id')->nullable();

            $table->dropColumn('photos');
            $table->dropColumn('links');
            $table->dropColumn('lft');
            $table->dropColumn('rgt');
            $table->dropColumn('depth');

            $table->string('photo')->nullable();
            $table->string('link')->nullable();
            $table->text('notes')->nullable();
            $table->string('keyword')->nullable();

            $table->double('lat')->nullable();
            $table->double('lng')->nullable();
            $table->json('address')->nullable();

            $table->string('currency')->default('EUR');

            $table->integer('adults')->nullable(); #<-- For restaurants <-- For hotels
            $table->integer('children')->nullable(); #<-- For hotels
            $table->integer('quantity')->nullable(); #<-- For hotels

            $table->dateTime('reservation_starts_at')->nullable(); #<-- For restaurants <-- For hotels <-- For dentist
            $table->dateTime('reservation_ends_at')->nullable();

            $table->integer('category_id')->index();

            $table->boolean('waiting_for_feedback')->default(0);

            $table->integer('merchant_id')->nullable()->index();

            $table->integer('is_accepted')->default(0);


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bids', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToAdvpacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('advpacks', function (Blueprint $table) {
            //
            $table->json('name')->nullable();
            $table->json('short_description')->nullable();
            $table->json('description')->nullable();
            $table->string('cover')->nullable();
            $table->string('duration')->nullable(); #'+1months' '+1year' strtotime(,$pack->duration);
            $table->float('price')->nullable();
            $table->boolean('is_upsellable')->default(0);
            $table->boolean('is_newsletterable')->default(0);
            $table->boolean('is_homepageable')->default(0);
            $table->boolean('is_videoable')->default(0);
            $table->integer('howmanyproduts')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('advpacks', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCompanyRegistrationColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->string('sdi')->nullable();
            $table->string('pec')->nullable();
            $table->integer('companytype_id')->index()->nullable();
            $table->float('share_capital')->nullable();
            $table->string('email_for_invoices')->nullable();
            $table->string('contact_person')->nullable();
            $table->string('fax')->nullable();
            $table->string('corporate_sector')->nullable();
            $table->string('company_registration_number')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}

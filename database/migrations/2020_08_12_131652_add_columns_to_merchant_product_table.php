<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToMerchantProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('merchant_product', function (Blueprint $table) {
            //
            $table->integer('reassortment_days')->nullable();
            $table->integer('shipping_days')->nullable();
            $table->integer('is_used')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('merchant_product', function (Blueprint $table) {
            //
        });
    }
}

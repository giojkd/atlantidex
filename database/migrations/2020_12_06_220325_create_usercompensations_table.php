<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsercompensationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usercompensations', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->softDeletes();
            $table->integer('user_id')->nullable()->index();
            $table->integer('month_id')->nullable()->index();
            $table->unique(['user_id', 'month_id']);
            $table->float('monthly_career_bonus')->nullable();
            $table->float('monthly_career_commission')->nullable();
            $table->float('long_run_career_bonus')->nullable();
            $table->float('long_run_career_commission')->nullable();
            $table->float('direct_commission')->nullable();
            $table->float('merchant_bonus')->nullable();
            $table->float('merchant_booster_bonus')->nullable();
            $table->float('revenue_bonus')->nullable();
            $table->float('marketing_bonus')->nullable();
            $table->float('compensation_total')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usercompensations');
    }
}

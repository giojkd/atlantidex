<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCoverColumnToGdovouchercodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gdovouchercodes', function (Blueprint $table) {
            //
            $table->boolean('is_partially_spendable')->default(0);
            $table->float('amount_spent')->deafult(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gdovouchercodes', function (Blueprint $table) {
            //
        });
    }
}

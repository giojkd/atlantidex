<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'order_items';

    /**
     * Run the migrations.
     * @table order_items
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedBigInteger('order_id');
            $table->unsignedBigInteger('product_id');
            $table->float('price')->nullable()->default(null);
            $table->unsignedinteger('order_item_status_id');

            $table->index(["order_id"], 'order_id');

            $table->index(["product_id"], 'product_id');

            $table->index(["order_item_status_id"], 'order_item_status_id');
            $table->softDeletes();
            $table->nullableTimestamps();


            $table->foreign('order_id', 'order_items_order_id')
                ->references('id')->on('orders')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('product_id', 'order_items_product_id')
                ->references('id')->on('products')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('order_item_status_id', 'order_items_order_item_status_id')
                ->references('id')->on('order_item_statuses')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}

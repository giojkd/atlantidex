<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableChangesponsorrequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_changesponsorrequests', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('applicant_user_id')->index()->nullable();
            $table->integer('receiving_user_id')->index()->nullable();
            $table->string('applicant_signature')->nullable();
            $table->string('receiving_feedback')->nullable();
            $table->string('receiving_signature')->nullable();
            $table->integer('document_id')->nullable()->index();
            $table->string('document_url')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_changesponsorrequests');
    }
}

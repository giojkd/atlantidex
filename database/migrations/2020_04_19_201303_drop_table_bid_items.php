<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropTableBidItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::dropIfExists('bid_items');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          $migration = new \Database\Migration\CreateBidItemsTable();
          $migration->up();
    }
}

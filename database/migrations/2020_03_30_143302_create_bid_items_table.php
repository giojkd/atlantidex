<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBidItemsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'bid_items';

    /**
     * Run the migrations.
     * @table bid_items
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->unsignedBigInteger('bid_id');
            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('category_id')->nullable()->default(null);
            $table->integer('quantity')->nullable()->default(null);
            $table->integer('bid_kind_id')->nullable()->default(null);
            $table->text('description')->nullable()->default(null);
            $table->json('pictures')->nullable()->default(null);
            $table->text('url')->nullable()->default(null);
            $table->float('price')->nullable()->default(null);
            $table->tinyInteger('accepted')->default('0');

            $table->index(["category_id"], 'category_id');

            $table->index(["bid_id"], 'bid_id');

            $table->index(["product_id"], 'product_id');
            $table->softDeletes();
            $table->nullableTimestamps();


            $table->foreign('bid_id', 'bid_items_bid_id')
                ->references('id')->on('bids')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('product_id', 'bid_items_product_id')
                ->references('id')->on('products')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('category_id', 'bid_items_category_id')
                ->references('id')->on('categories')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeadMerchantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lead_merchant', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('lead_id')->nullable();
            $table->integer('merchant_id')->nullable();
            $table->unique(['lead_id', 'merchant_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lead_merchant');
    }
}

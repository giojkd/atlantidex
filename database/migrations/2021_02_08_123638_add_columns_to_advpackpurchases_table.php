<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToAdvpackpurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('advpacks', function (Blueprint $table) {
            //
            $table->dropColumn('days_in_upsellable');
            $table->dropColumn('days_in_newsletterable');
            $table->dropColumn('days_in_homepageable');
            $table->dropColumn('days_in_videoable');
            $table->text('duration')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('advpacks', function (Blueprint $table) {
            //
        });
    }
}

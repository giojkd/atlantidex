<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'products';

    /**
     * Run the migrations.
     * @table products
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->unsignedBigInteger('category_id');
            $table->unsignedInteger('product_type_id');
            $table->string('name')->nullable()->default(null);
            $table->json('brief')->nullable()->default(null);
            $table->json('description')->nullable()->default(null);
            $table->json('pictures')->nullable()->default(null);
            $table->float('price')->nullable()->default(null);
            $table->float('price_compare')->nullable()->default(null);
            $table->tinyInteger('active')->default('1');

            $table->index(["category_id"], 'category_id');

            $table->index(["product_type_id"], 'product_type_id');
            $table->softDeletes();
            $table->nullableTimestamps();


            $table->foreign('category_id', 'product_category_id')
                ->references('id')->on('categories')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('product_type_id', 'product_product_type_id')
                ->references('id')->on('product_types')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}

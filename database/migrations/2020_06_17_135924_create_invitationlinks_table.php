<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvitationlinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invitationlinks', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('user_id')->index()->nullable();
            $table->string('link_type')->default('networker')->nullable();
            $table->boolean('is_redeemed')->default(false)->nullable();
            $table->timestamp('expires_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invitationlinks');
    }
}

const qrcodeNew = window.qrcode;

const video = document.createElement("video");
const canvasElement = document.getElementById("qr-canvas");
const canvas = canvasElement.getContext("2d");

const qrResult = document.getElementById("qr-result");
const outputData = document.getElementById("outputData");
const btnScanQR = document.getElementById("btn-scan-qr");
const btnReset = document.getElementById("btn-reset");
const userCodeManualInputWrapper = document.getElementById("userCodeManualInputWrapper");

let scanning = false;

function qrCodeReadingCallback(res){
    if (res) {
        outputData.innerText = res;

        loadUserForPos(res)

        scanning = false;

        video.srcObject.getTracks().forEach(track => {
            track.stop();
        });

        qrResult.hidden = false;
        canvasElement.hidden = true;
        btnScanQR.hidden = false;
        btnReset.hidden = false;
        userCodeManualInputWrapper.hidden = true;
    }
}

qrcodeNew.callback = res => {
    qrCodeReadingCallback(res);
};

btnReset.onclick = () => {
    resetUserQrCodeScan();
    startScanning();
};

startScanning();

function startScanning(){

    navigator.mediaDevices
        .getUserMedia({ video: { facingMode: "environment" } })
        .then(function (stream) {
            scanning = true;
            qrResult.hidden = true;
            btnScanQR.hidden = true;
            btnReset.hidden = true;
            userCodeManualInputWrapper.hidden = false;
            canvasElement.hidden = false;
            video.setAttribute("playsinline", true); // required to tell iOS safari we don't want fullscreen
            video.srcObject = stream;
            video.play();
            tick();
            scan();
        });
}

function tick() {
    /*
    canvasElement.height = video.videoHeight;
    canvasElement.width = video.videoWidth;
    */

    canvasElement.height = $('.card-header').width() / (video.videoWidth / video.videoHeight);
    canvasElement.width = $('.card-header').width();






    var canvasHeight = 240;
    var canvasWidth = 320;

    canvas.drawImage(video, 0, 0, canvasElement.width, canvasElement.height);
    //canvas.drawImage(video, 0, 0, canvasWidth, canvasHeight);

    scanning && requestAnimationFrame(tick);
}

function scan() {
    try {
        qrcodeNew.decode();
    } catch (e) {
        setTimeout(scan, 300);
    }
}

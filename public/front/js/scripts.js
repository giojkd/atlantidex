document.addEventListener('DOMContentLoaded', function () {
        cookieconsent.run({ "notice_banner_type": "simple", "consent_type": "express", "palette": "light", "language": "it", "website_name": "www.atlantinex.com", "cookies_policy_url": "https://www.atlantinex.com/cookies-policy" });
});

function approveSuggestedCategory(button){

    if (confirm('Sei sicuro di voler procedere?')) {

        var button = $(button);

        var buttonData = button.data();
        var route = '/admin/suggestedcategory/approve';

        var id = buttonData.id;

        $.ajax({
            url: route,
            data: {
                id: id,
            },
            type: 'POST',
            success: function (result) {
                // Show an alert with the result
                console.log(result, route);
                new Noty({
                    text: "Operazione conclusa con successo",
                    type: "success"
                }).show();

                // Hide the modal, if any
                $('.modal').modal('hide');

                crud.table.ajax.reload();
            },
            error: function (result) {
                // Show an alert with the result
                new Noty({
                    text: "Si è verificato un errore",
                    type: "warning"
                }).show();
            }
        });
    }
}

if (typeof setUsersColumnTimestamp != 'function') {

    function setUsersColumnTimestamp(button) {

        if(confirm('Sei sicuro di voler procedere?')){

            var button = $(button);

            var buttonData = button.data();
            var route = '/admin/user/set-column-timestamp';
            var column = buttonData.column;
            var id = buttonData.id;
            $.ajax({
                url: route,
                data: {
                    id:id,
                    column:column
                },
                type: 'POST',
                success: function (result) {
                    // Show an alert with the result
                    console.log(result, route);
                    new Noty({
                        text: "Operazione conclusa con successo",
                        type: "success"
                    }).show();

                    // Hide the modal, if any
                    $('.modal').modal('hide');

                    crud.table.ajax.reload();
                },
                error: function (result) {
                    // Show an alert with the result
                    new Noty({
                        text: "Si è verificato un errore",
                        type: "warning"
                    }).show();
                }
            });
        }
    }
}

jQuery.fn.ForceNumericOnly =
    function () {
        return this.each(function () {

            $(this).keydown(function (e) {
                var key = e.charCode || e.keyCode || 0;
                // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
                // home, end, period, and numpad decimal
                return (
                    key == 8 ||
                    key == 9 ||
                    key == 13 ||
                    key == 46 ||
                    key == 110 ||
                    key == 190 ||
                    (key >= 35 && key <= 40) ||
                    (key >= 48 && key <= 57) ||
                    (key >= 96 && key <= 105));
            });
        });
};


$(function(){
    $('.only-numbers').ForceNumericOnly();

    $('.modifiable-form').each(function(){

        var form = $(this);
        form.find('input,select').attr('disabled','true')

        var submitBtn = form.find('button[type="submit"]');
        submitBtn.hide();


        var allowEditBtn = new $('<a href="javscript:" class="btn btn-secondary">Modifica</a>');

        allowEditBtn.click(function(){
            form.find('input:not(.undisableable),select:not(.undisableable)').removeAttr('disabled');
            allowEditBtn.hide();
            submitBtn.removeClass('btn-secondary').addClass('btn-success').html('Salva');
            submitBtn.show();
        })

        allowEditBtn.appendTo(form);

    });

    $('[data-toggle="tooltip"]').tooltip();

    $('.submit-on-blur').blur(function(){
        $(this).closest('form').submit();
    });

    $('.submit-on-change').blur(function () {
        $(this).closest('form').submit();
    });


    $('.ajax-form').submit(function(e){
        e.preventDefault();
        var form = $(this);
        var action = form.attr('action');
        var type = form.find('input[name="_method"]').val();


        $.ajax({
            url:action,
            type: type,
            data: form.serialize(),
            success:function(r){
                var message = (typeof(r.message) != 'undefined') ? r.message : 'Operazione completata con successo'
                new Noty({
                    type: 'success',
                    text: message,
                }).show();

            }
        });

    });


})

// This is the "Offline page" service worker

// Add this below content to your HTML page inside a <script type="module"></script> tag, or add the js file to your page at the very top to register service worker
// If you get an error about not being able to import, double check that you have type="module" on your <script /> tag

/*
 This code uses the pwa-update web component https://github.com/pwa-builder/pwa-update to register your service worker,
 tell the user when there is an update available and let the user know when your PWA is ready to use offline.
*/

/*
import 'https://cdn.jsdelivr.net/npm/@pwabuilder/pwaupdate';

const el = document.createElement('pwa-update');
document.body.appendChild(el);
*/
let deferredPrompt;
const addBtn = document.querySelector('.a2hs-button');
addBtn.style.display = 'none';

window.addEventListener('beforeinstallprompt', (e) => {
    // Prevent Chrome 67 and earlier from automatically showing the prompt
    e.preventDefault();
    // Stash the event so it can be triggered later.
    deferredPrompt = e;
    // Update UI to notify the user they can add to home screen
    addBtn.style.display = 'block';

    addBtn.addEventListener('click', (e) => {
        // hide our user interface that shows our A2HS button
        addBtn.style.display = 'none';
        // Show the prompt
        deferredPrompt.prompt();
        // Wait for the user to respond to the prompt
        deferredPrompt.userChoice.then((choiceResult) => {
            if (choiceResult.outcome === 'accepted') {
                console.log('User accepted the A2HS prompt');
            } else {
                console.log('User dismissed the A2HS prompt');
            }
            deferredPrompt = null;
        });
    });
});

function cancelLead(id){

    if(confirm('Sei sicuro di voler eliminare il lead ? L\'operazione non sarà reversibile.')){
        $.post('/admin/cancel-lead/' + id, {}, function (r) {
            new Noty({
                type: r.type,
                text: r.message,
            }).show();
            crud.table.ajax.reload();
        }, 'json');
    }

}

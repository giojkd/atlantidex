<?php

use App\Http\Controllers\Api\BackpackUserApiResourceController;
use App\Http\Controllers\Api\InvoicerowApiResourceController;
use App\Models\BackpackUser;
use App\Models\Category;
use App\Models\Invoicerow;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\Language;
use App\Models\Product;
use App\Models\Merchant;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::middleware('auth:api')->group(function(){

    Route::apiResources([
        #'users' => '\App\Http\Controllers\Api\BackpackUserApiResourceController',
        'invoicerows' => '\App\Http\Controllers\Api\InvoicerowApiResourceController',
        'invoices' => '\App\Http\Controllers\Api\InvoiceApiResourceController',
    ]);
    #Route::model('invoicerows',Invoicerow::class);
    #Route::get('/invoicerows', [InvoicerowApiResourceController::class, 'index']);

    Route::model('users', BackpackUser::class);
    Route::get('/users/{users}', [BackpackUserApiResourceController::class,'show']);


});

Route::post('login','Auth\LoginController@apiLogin');

Route::get('/languages', function () {
    return Language::all();
});


Route::get('/products', function () {
    return Product::all();
});

Route::get('/merchants', function () {
    return Merchant::all();
});

Route::get('/leaves/{id}', function ($id) {
    return Category::getAllLeavesByCategory($id);
});

Route::get('/children/{id}', function ($id) {
    return Category::getAllSubCategoryByCategory($id);
});

Route::get('/children-with-ancestors/{id}', function ($id) {
    return Category::getAllSubCategoryWithAncestors($id);
});



Route::get('/sector/{id}', function ($id) {
    $category = Category::find($id);
    return $category->children()->whereActive(1)->pluck('name','id');
});


Route::get('invoiceable-data', 'ApiController@invoiceableData');


Route::match(['get', 'post'], 'deploy', 'DeploymentController@index');
Route::match(['get', 'post'], 'deploy-test', 'DeploymentController@test');

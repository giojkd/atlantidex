<?php

use Illuminate\Support\Facades\Route;
use Auth as Auth;
use Str as Str;

Route::get('/qui-atlantidex', '\App\Http\Controllers\FrontController@quiAtlantidex')->name('quiAtlantidex');
Route::get('/qui-atlantidex-infowindow', '\App\Http\Controllers\FrontController@quiAtlantidexInfoWindow')->name('quiAtlantidexCompanyInfowindow');
Route::get('/qui-atlantidex-v2', '\App\Http\Controllers\FrontController@quiAtlantidexV2')->name('quiAtlantidexV2');




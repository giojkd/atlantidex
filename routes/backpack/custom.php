<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.


/*
Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => [
        config('backpack.base.web_middleware', 'web'),
    ],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () {
    Route::get('register', 'Auth\RegisterController@showRegistrationFormCustom')->name('backpack.auth.register');
    Route::post('register', 'Auth\RegisterController@register');
    Route::get('register-company', 'Auth\RegisterControllerCompany@showRegistrationFormCustom')->name('backpack.auth.registercompany');
    Route::post('register-company', 'Auth\RegisterControllerCompany@register');


});
*/

Route::group([

    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => [
        #config('backpack.base.web_middleware', 'web'),
        'web',

    ],
    'namespace'  => 'App\Http\Controllers\Admin',

], function () {

    Route::get('register-test', 'Auth\RegisterController@showRegistrationFormCustom')->name('backpack.auth.register');

    Route::post('register-test', 'Auth\RegisterController@register');

    Route::get('register-company-test', 'Auth\RegisterControllerCompany@showRegistrationFormCustom')->name('backpack.auth.registercompany');
    Route::post('register-company-test', 'Auth\RegisterControllerCompany@registerCustom');

});

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => [
        config('backpack.base.web_middleware', 'web'),
        config('backpack.base.middleware_key', 'admin'),
        'restrict.owner',
    ],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('product', 'ProductCrudController');
    Route::get('dashboard', '\App\Http\Controllers\DashboardController@index')->name('dashboard')->middleware('verified');
});

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => [
        config('backpack.base.web_middleware', 'web'),
        config('backpack.base.middleware_key', 'admin'),
        'restrict.notowner',
    ],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('catalogue', 'CatalogueCrudController');

});

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => [
        config('backpack.base.web_middleware', 'web'),
        config('backpack.base.middleware_key', 'admin'),
    ],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('merchant', 'MerchantCrudController');
    Route::resource('company-data', 'CompanyController');
});

route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => [
        config('backpack.base.web_middleware', 'web'),
        config('backpack.base.middleware_key', 'admin'),
    ],
    'namespace'  => 'App\Http\Controllers\Company\Product',
], function () { // custom admin routes
    Route::resource('product/create', 'ProductController');
    Route::get('product/{id}/edit', 'ProductController@index')->name('productEdit');
    Route::get('product/{id}/show', 'ProductController@show')->name('productShow');
    Route::post('product/update', 'ProductController@update')->middleware('App\Http\Middleware\ProductMiddleware');
    Route::post('product/update-merchant', 'ProductController@updateMerchant')->middleware('App\Http\Middleware\ProductMiddleware');
    Route::get('product/{id}/clone', 'ProductController@clone');
    Route::get('catalogue-search', 'CatalogueController@index')->name('catalogueSearch');

});


Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => [
        config('backpack.base.web_middleware', 'web'),
        config('backpack.base.middleware_key', 'admin'),
        #'can:manage',
    ],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('country', 'CountryCrudController');
    Route::crud('currency', 'CurrencyCrudController');
    Route::crud('product_status', 'Product_statusCrudController');
    Route::crud('product_type', 'Product_typeCrudController');
    Route::crud('category', 'CategoryCrudController');
    Route::crud('language', 'LanguageCrudController');
    Route::crud('user', 'UserCrudController');

    Route::post('user/set-column-timestamp','UserCrudController@setColumnTimestamp');
    Route::post('user/set-column-boolean','UserCrudController@setColumnBoolean');

    Route::crud('manufacturer', 'ManufacturerCrudController');
    Route::crud('review', 'ReviewCrudController');
    Route::crud('bid', 'BidCrudController');
    Route::crud('documentcode', 'DocumentcodeCrudController');
    Route::crud('address', 'AddressCrudController');
    Route::crud('walletoperation', 'WalletoperationCrudController');
    Route::crud('document', 'DocumentCrudController');
    Route::crud('invitationlink', 'InvitationlinkCrudController');
    Route::crud('changesponsorrequest', 'ChangesponsorrequestCrudController');
    Route::crud('productimportfile', 'ProductimportfileCrudController');
    Route::crud('attribute', 'AttributeCrudController');
    Route::crud('attributevalue', 'AttributevalueCrudController');
    Route::crud('event', 'EventCrudController');
    Route::crud('ticket', 'TicketCrudController');
    Route::crud('documenttype', 'DocumenttypeCrudController');
    Route::crud('pack', 'PackCrudController');
    Route::crud('otp', 'OtpCrudController');
    Route::crud('gdovoucher', 'GdovoucherCrudController');
    Route::crud('gdovouchercode', 'GdovouchercodeCrudController');
    Route::crud('epoperation', 'EpoperationCrudController');
    Route::crud('voucherpurchase', 'VoucherpurchaseCrudController');
    Route::crud('careeroperation', 'CareeroperationCrudController');
    Route::crud('commissionoperation', 'CommissionoperationCrudController');

    Route::get('scan-ticket','TicketCrudController@scanTicket');
    Route::post('test-ticket','TicketCrudController@testTicket');
    Route::post('invalidate-ticket','TicketCrudController@invalidateTicket');
    Route::crud('vat', 'VatCrudController');
    Route::crud('download', 'DownloadCrudController');
    Route::crud('packpurchase', 'PackpurchaseCrudController');
    Route::crud('advpack', 'AdvpackCrudController');
    Route::crud('companytype', 'CompanytypeCrudController');

    Route::get('re-active-users','UserCrudController@ReActiveUsers');
    Route::crud('usercompensation', 'UsercompensationCrudController');

    Route::get('temporary-compensations-recap', 'UsercompensationCrudController@temporaryRecap')->name('temporaryBenefitsRecap');
    Route::crud('voucheroperation', 'VoucheroperationCrudController');
    Route::crud('career', 'CareerCrudController');
    Route::crud('bonusepspayment', 'BonusepspaymentCrudController');

    Route::get('temporary-benefits-recap', 'UsercompensationCrudController@temporaryRecapIndex')->name('temporaryBenefitsRecapIndex');
    Route::crud('invoiceable', 'InvoiceableCrudController');
    Route::crud('invoicerow', 'InvoicerowCrudController');
    Route::crud('epvoucher', 'EpvoucherCrudController');
    Route::crud('longruncareerlevel', 'LongruncareerlevelCrudController');
    Route::crud('invoice', 'InvoiceCrudController');

    Route::get('fix-users-tree','UserCrudController@fixTree');
    Route::crud('suggestedcategory', 'SuggestedcategoryCrudController');
    Route::post('suggestedcategory/approve', 'SuggestedcategoryCrudController@approveSuggestedcategory');
    Route::crud('advpackpurchase', 'AdvpackpurchaseCrudController');
    Route::crud('advinsertion', 'AdvinsertionCrudController');
    Route::crud('epplusoperation', 'EpplusoperationCrudController');
    Route::crud('lead', 'LeadCrudController');
    Route::crud('leadrow', 'LeadrowCrudController');



    Route::crud('client', 'ClientCrudController');
    Route::crud('companycategory', 'CompanycategoryCrudController');

    Route::crud('offlisting', 'OfflistingCrudController');
    Route::crud('payout', 'PayoutCrudController');
    Route::crud('insertion', 'InsertionCrudController');
    Route::crud('eppluspayout', 'EppluspayoutCrudController');
}); // this should be the absolute last line of this file

<?php

Route::group([
    'as' => 'Company.',
    'prefix' => 'company'
], function () {

    Route::resource('pos', '\App\Http\Controllers\PosController');

    Route::get('load-user-for-pos', '\App\Http\Controllers\PosController@loadUserForPos')->name('loadUserForPos');

    Route::post('confirm-order', '\App\Http\Controllers\PosController@confirmOrder')->name('confirmOrder');

    Route::post('request-otp', '\App\Http\Controllers\PosController@requestOtp')->name('requestOtp');
    Route::post('confirm-otp', '\App\Http\Controllers\PosController@confirmOtp')->name('confirmOtp');

});



?>

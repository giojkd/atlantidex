<?php

////////////////////
//MyBusinesRoutes//
////////////////////



Route::group([
    'as' => 'Company.',
    'prefix' => 'company'
], function () {
    #Route::resource('activate', 'Company\ActivationController');
    Route::resource('activate', 'Atlantinex\MyAtlantinex\ActivationController');
});


/*

MyAtlantinex.personal-data.index
MyAtlantinex.personal-data.update

1) Creazione delle migrations per la gestione degli eventi
2) Creazione delle views in resources\views\Atlantinex\Events
3) Creazione delle routes in routes\Atlantinex\Events.php
4) Creazione dei controllers in \App\Https\Controllers\Atlantinex\Events
5) Class utilizzare per la gestione degli utenti è \App\Models\BackpackUser

*/

<?php

////////////////////
//CompanyRoutes//
////////////////////



Route::group([
    'as' => 'Company.',
    'prefix' => 'company'
], function () {
    Route::resource('company-data', 'Company\Settings\CompanyController');
    Route::get('qr-frame', 'Company\Settings\CompanyController@qrframe')->name('qrFrame');
    Route::get('{id}/invite-customer', 'Company\Settings\CompanyController@inviteCustomer')->name('inviteCustomer');

});

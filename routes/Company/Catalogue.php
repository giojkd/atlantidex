<?php

////////////////////
//CompanyRoutes//
////////////////////



Route::group([
    'as' => 'Company.',
    'prefix' => 'company'
], function () {
    Route::resource('category', 'Company\Catalogue\CategoryController');
    Route::get('category-suggest', 'Company\Catalogue\CategoryController@suggest')->name('categorySuggest');
    Route::post('category-suggest-up', 'Company\Catalogue\CategoryController@suggestup')->name('categorySuggestUp');


});

Route::group([
    'as' => 'Company.',
    'prefix' => 'company'
], function () {
    Route::get('ep-category', 'Company\Product\ProductController@epCategory')->name('epCategory');;
});



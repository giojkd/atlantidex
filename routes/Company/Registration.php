<?php


Route::group([
    'as' => 'Company.',
    'prefix' => 'company'
], function () {
    #Route::resource('activate', 'Company\ActivationController');
    Route::get('register', 'Company\RegisterController@index')->name('registerCompany');
    Route::post('register', 'Company\RegisterController@store')->name('registerCompany');
    #Route::put('register/{id}', 'Company\RegisterController@store')->name('registerCompany');
});




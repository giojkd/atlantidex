<?php

////////////////////
//CompanyRoutes//
////////////////////



Route::group([
    'as' => 'Company.',
    'prefix' => 'company'
], function () {
    Route::resource('adv-packs', 'Company\AdvPacksController');
    Route::post('toggle-adv-pack-product', 'Company\AdvPacksController@toggleAdvPackProduct')->name('toggleAdvPackProduct');
});

?>

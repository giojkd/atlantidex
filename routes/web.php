<?php

use Illuminate\Support\Facades\Route;
use Auth as Auth;
use Str as Str;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/dashboard', function () {
    return redirect('/admin');
});


Route::get('/', function () {

    $host = request()->getHttpHost();
    if ($host == env('PARTNERS_DOMAIN')) {
        return view('atlantidex');
    } else {
        return view('welcome');
    }
});

Route::get('/atlantidex', function () {
    return view('atlantidex');
});
Route::post('/admin/media-dropzone/download', ['uses' => 'Admin\ProductCrudController@handleDropzoneFileUpload']);
Route::post('/admin/media-dropzone/product', ['uses' => 'Admin\ProductCrudController@handleDropzoneUpload']);
Route::post('/admin/media-dropzone/event', ['uses' => 'Admin\EventCrudController@handleDropzoneUpload']);
Route::post('/admin/media-dropzone/document', ['uses' => 'Admin\DocumentCrudController@handleDropzoneUpload']);

Route::get('/admin/my-code','DashboardController@myCode')->name('myCode');

Route::get('search/{model}/{keyword}', 'FrontController@search')->name('search')->where(["keyword" => ".*"]);

Route::get('/thumbs/dropzone/{filename}', function ($filename) {

    if (Str::of($filename)->includes('://')) {
        $file = $filename;
    } else {
        $file = storage_path('app/public/' . $filename);
    }
    exit;

    $img = Image::make($file)
        ->resize(120, 120);
    return $img->response('jpg');
})->where(["filename" => ".*"]);

Route::get('/ir/{size}/{filename}', function ($size = '', $filename =  '') {

    if(!Str::contains($filename,['http://','https://'])){

        if(Str::contains($filename,['storage/'])){
            $filename = Str::replaceFirst('storage/', '', $filename);
        }else{
            $filename = basename($filename);
        }

        $file = storage_path('app/public/' . $filename);
    }else{
        $file = $filename;
    }

    switch ($size[0]) {
        case 'h':
            $size = substr($size, 1);

            $img = Image::make($file)->resize(null, $size, function ($constraint) {
                $constraint->aspectRatio();
            });

            break;
        case 'w':
            $size = substr($size, 1);

            $img = Image::make($file)->resize($size, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            break;
        default:

            $size = explode('x', $size);
            if (count($size) > 1) {
                $img = Image::make($file)
                    ->resize($size[0], $size[1]);
            }

            if (count($size) == 1) {
                $size[0] = ($size[0] != '') ? $size[0] : 120;
                $img = Image::make($file)->resize($size[0], null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }

            break;
    }

    return $img->response('jpg');
})->where(["filename" => ".*"])->name('ir');



Route::get('test', 'TestController@test');

Route::get('/debug-sentry', function () {
    throw new Exception('My first Sentry error!');
});

Route::get('/booking-demo', 'FrontController@bookingDemo');

Route::get('/check-ticket/{id}', 'FrontController@checkTicket')->name('checkTicket');


#Auth::routes(['verify' => true]);

#Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');



if (1 == 2) {



    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');

    // Registration Routes...
    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'Auth\RegisterController@register');

    // Password Reset Routes...
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

    Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
    Route::get('email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');
    Route::post('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');

    Route::get('/home', 'HomeController@index')->name('home');
}else{
    Auth::routes(['verify' => true]);
}

Route::get('/cookies-policy', function () {
    return view('privacy_policy');
});

/*

Route::get('login',function(){
    return redirect('admin/login');
});

*/


#IMPERSONATING ROUTES BEGIN

Route::get('stop-impersonating', function () {
    backpack_user()->stopImpersonating();
    \Alert::success('Impersonating stopped.')->flash();
    return redirect('/admin/user');#->back();
});

#IMPERSONATIN ROUTES END

Route::get('login', function () {
    return redirect('/admin/login');
})->name('login');

/*
Route::get('/login', function () {
    return redirect('/admin/login');
})->name('login');
*/



Route::post('log-dispatcher','LogController@index')->name('logDispatcher');

Route::post('/admin/cancel-lead/{id}', '\App\Http\Controllers\Admin\LeadCrudController@cancelLead');

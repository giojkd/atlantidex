<?php

////////////////////
//MyBusinesRoutes//
////////////////////



Route::group([
    'as' => 'MyBusiness.',
    'prefix' => 'my-business'
], function () {

    Route::get('monthly-career', 'Atlantinex\MyBusiness\StatsController@monthlyCareer')->name('monthlyCareer');
    Route::get('direct-commission', 'Atlantinex\MyBusiness\StatsController@directCommission')->name('directCommission');
    Route::get('long-run-career', 'Atlantinex\MyBusiness\StatsController@longRunCareer')->name('longRunCareer');
    Route::get('network-purchases', 'Atlantinex\MyBusiness\StatsController@networkPurchases')->name('networkPurchases');
    Route::get('my-benefits', 'Atlantinex\MyBusiness\StatsController@myBenefits')->name('myBenefits');

    Route::get('my-benefits/details/{type?}', 'Atlantinex\MyBusiness\StatsController@myBenefitsDetails')->name('myBenefitsDetails');


});


/*

MyAtlantinex.personal-data.index
MyAtlantinex.personal-data.update

1) Creazione delle migrations per la gestione degli eventi
2) Creazione delle views in resources\views\Atlantinex\Events
3) Creazione delle routes in routes\Atlantinex\Events.php
4) Creazione dei controllers in \App\Https\Controllers\Atlantinex\Events
5) Class utilizzare per la gestione degli utenti è \App\Models\BackpackUser

*/

<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'as' => 'IncreaseMyBusiness.',
    'prefix' => 'increase-my-business'
],
    function () {
        Route::resource('invitation-links', 'Atlantinex\IncreaseMyBusiness\InvitationlinkController');
        Route::resource('my-companies', 'Atlantinex\IncreaseMyBusiness\MyCompaniesController');
        Route::resource('my-networkers', 'Atlantinex\IncreaseMyBusiness\MyNetworkersController');
        Route::get('{id}/my-team', 'Atlantinex\IncreaseMyBusiness\MyTeamController@index');
        Route::get('{id}/company-revenue', 'Atlantinex\IncreaseMyBusiness\CompanyRevenueController@index');
        Route::get('{id}/networker-revenue', 'Atlantinex\IncreaseMyBusiness\NetworkerRevenueController@index');
        Route::get('{id}/networker', 'Atlantinex\IncreaseMyBusiness\NetworkerRevenueController@networker');
        Route::get('{id}/company', 'Atlantinex\IncreaseMyBusiness\CompanyRevenueController@company');
    }
);

?>

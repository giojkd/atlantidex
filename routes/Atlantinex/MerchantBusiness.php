<?php

////////////////////
//MyBusinesRoutes//
////////////////////



Route::group([
    'as' => 'MerchantBusiness.',
    'prefix' => 'merchant-business'
], function () {
    Route::get('affiliated-companies', 'Atlantinex\MerchantBusiness\StatsController@affiliatedCompanies')->name('affiliatedCompanies');
    Route::get('revenue-bonus', 'Atlantinex\MerchantBusiness\StatsController@revenueBonus')->name('revenueBonus');
    Route::get('merchant-bonus', 'Atlantinex\MerchantBusiness\StatsController@merchantBonus')->name('merchantBonus');
    Route::get('merchant-booster-bonus', 'Atlantinex\MerchantBusiness\StatsController@merchantBoosterBonus')->name('merchantBoosterBonus');
    Route::get('merchant-marketing-bonus', 'Atlantinex\MerchantBusiness\StatsController@merchantMarketingBonus')->name('merchantMarketingBonus');
});

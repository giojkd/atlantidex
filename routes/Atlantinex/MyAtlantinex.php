<?php

////////////////////
//MyAtlantinexRoutes//
////////////////////



Route::group([
    'as' => 'MyAtlantinex.',
    'prefix' => 'my-atlantinex',
    'middleware' => ['verified'],
], function () {
    Route::resource('personal-data', 'Atlantinex\MyAtlantinex\UserController');
    Route::resource('address', 'Atlantinex\MyAtlantinex\AddressController');
    Route::resource('payment-data', 'Atlantinex\MyAtlantinex\PaymentController');

    Route::post('set-payment-method', 'Atlantinex\MyAtlantinex\PaymentController@attachPaymentMethodToUser')->name('attachPaymentMethodToUser');
    Route::post('set-bank-account', 'Atlantinex\MyAtlantinex\PaymentController@setUserBankAccount')->name('setUserBankAccount');

    Route::get('my-wallet', 'Atlantinex\MyAtlantinex\UserController@myWallet')->name('myWallet');
    Route::post('my-wallet-top-up', 'Atlantinex\MyAtlantinex\UserController@topUpWallet')->name('myWalletTopUp');
    Route::post('my-wallet-top-up-paypal', 'Atlantinex\MyAtlantinex\UserController@topUpWalletPayPal')->name('myWalletTopUpPayPal');

    Route::post('my-wallet-payout', 'Atlantinex\MyAtlantinex\UserController@payoutRequest')->name('payoutRequest');
    Route::post('my-wallet-payout-otp', 'Atlantinex\MyAtlantinex\UserController@payoutRequestOtp')->name('payoutRequestOtp');

    Route::resource('documents','Atlantinex\MyAtlantinex\DocumentsController');
    Route::post('sign-document-with-checkbox','Atlantinex\MyAtlantinex\DocumentsController@signDocumentWithCheckbox')->name('signDocumentWithCheckbox');

    Route::get('change-sponsor','Atlantinex\MyAtlantinex\UserController@changeSponsorIndex')->name('changeSponsor');
    Route::post('change-sponsor', 'Atlantinex\MyAtlantinex\UserController@changeSponsorCreate')->name('changeSponsorCreate');
    Route::post('change-sponsor-feedback', 'Atlantinex\MyAtlantinex\UserController@changeSponsorFeedback')->name('changeSponsorFeedback');
    Route::post('change-sponsor-render', 'Atlantinex\MyAtlantinex\UserController@changeSponsorRenderDocument')->name('changeSponsorRenderDocument');

    Route::resource('vouchers', 'Atlantinex\MyAtlantinex\VouchersController')->middleware('auth.otp');

    #ACTIVATION BEGIN

    Route::resource('activate', 'Atlantinex\MyAtlantinex\ActivationController');
    Route::get('become-marketer-confirmation', 'Atlantinex\MyAtlantinex\ActivationController@becomeMarketerConfirmation')->name('becomeMarketerConfirmation');
    Route::post('become-marketer-confirm', 'Atlantinex\MyAtlantinex\ActivationController@becomeMarketerConfirm')->name('becomeMarketerConfirm');

    #ACTIVATION END

    Route::resource('packs', 'Atlantinex\MyAtlantinex\PacksController');

    Route::resource('otps', 'OtpController');

    Route::post('otp-check','OtpController@check')->name('OtpCheck');

    Route::resource('eventsshop','Atlantinex\MyAtlantinex\EventsShopController');
    Route::post('eventsshop-free','Atlantinex\MyAtlantinex\EventsShopController@storeFree')->name('eventsShopFree');

    Route::resource('gdovouchersshop','Atlantinex\MyAtlantinex\GdoVouchersShopController');

    Route::get('purchase-gdo-vouchers-with-vouchers', 'Atlantinex\MyAtlantinex\GdoVouchersShopController@spendVouchers')->name('spendVouchers');
    Route::get('{id}/downgdovoucher','Atlantinex\MyAtlantinex\GdoVouchersShopController@getDownload')->name('DownloadGdoVoucher');

    Route::get('networker-activation-process-interrupt', 'Atlantinex\MyAtlantinex\GdoVouchersShopController@networkerActivationProcessInterrupt')->name('networkerActivationProcessInterrupt');

    Route::get('ep-plus-operations-list', 'Atlantinex\MyAtlantinex\EpPlusController@list')->name('epPlusOperationsList');
    Route::post('ep-plus-operations-payout-otp', 'Atlantinex\MyAtlantinex\EpPlusController@payoutOtp')->name('epPlusOperationsPayoutOtp');
    Route::post('ep-plus-operations-payout-confirm', 'Atlantinex\MyAtlantinex\EpPlusController@payoutConfirm')->name('epPlusOperationsPayoutConfirm');

});

/*

MyAtlantinex.personal-data.index
MyAtlantinex.personal-data.update

1) Creazione delle migrations per la gestione degli eventi
2) Creazione delle views in resources\views\Atlantinex\Events
3) Creazione delle routes in routes\Atlantinex\Events.php
4) Creazione dei controllers in \App\Https\Controllers\Atlantinex\Events
5) Class utilizzare per la gestione degli utenti è \App\Models\BackpackUser

*/

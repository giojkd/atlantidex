<?php

namespace App;


use Spatie\Permission\Models\Role as ModelsRole;
use Illuminate\Database\Eloquent\Model;
use App\Models\Documenttype;
use App\Models\Download;

class Role extends ModelsRole
{
    public function documenttypes(){
        return $this->belongsToMany(Documenttype::class);
    }
    public function downloads(){
        return $this->belongstoMany(Download::class);
    }
}

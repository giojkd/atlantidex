<?php

namespace App\Console\Commands;

use App\Models\Category;
use Illuminate\Console\Command;

class GenerateCategoriesAncestorsBreadcrumbCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'atl:generateaategoriesancestorsbreadcrumb';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $categories = Category::get();
        $bar = $this->output->createProgressBar(count($categories));

        $bar->start();


        foreach ($categories as $category) {

            $category->generateAncestorsBreadcrumb();
            $bar->advance();

        }

        $bar->finish();
    }
}

<?php

namespace App\Console\Commands;

use App\Models\BackpackUser;
use App\Models\Voucher;
use App\Models\Voucheroperation;
use App\Models\Voucherpurchase;
use Illuminate\Console\Command;

class ConsolidateVouchers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'atl:consolidatevouchers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        Voucherpurchase::get()->each(function ($item, $key) {
            $item->calculateVoucherPurchased();
        });

        Voucheroperation::get()->each(function($item,$key){
            $item->calculateVoucherSpent();
        });


        Voucher::get()->each(function ($item, $key) {
            $item->amount = $item->purchased - $item->spent;
            $item->save();
        });


    }
}

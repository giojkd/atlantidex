<?php

namespace App\Console\Commands;

use App\Models\BackpackUser;
use Illuminate\Console\Command;
use App\Models\Career;
use App\Models\Longruncareerlevel;

class CalculateCareers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'atl:calculatecareers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculates monhtly and long run careers and stores data in careers table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $user = new BackpackUser();
        $productionMonths = collect($user->getProductionMonthsUntilGivenDate())->slice(-2,2);#->slice(1);

        #dd($productionMonths);

        #$productionMonths = collect([collect($user->getProductionMonthsUntilGivenDate())->last()]);#->slice(1);
        #dd($productionMonths);
        #dd($productionMonths);
        $users = BackpackUser::role('Networker')->whereActive(1)->get();

        #$users = BackpackUser::where('id',88)->get();

        #$users = BackpackUser::where('id',32)->get();

        $users->each(function($user,$keyUser) use ($productionMonths){

            $performancePerMonth = [];

            $productionMonths->each(function($month,$keyMonth) use ($user,&$performancePerMonth, $productionMonths){


                $this->line($user->full_name.' month '.$month[3]);

                $performances = $user->getPerformancesLongRun($keyMonth,1);

                Career::updateOrCreate([
                    'user_id' => $user->id,
                    'month_id' => $keyMonth,
                    'career_type' => 'long_run_career'
                ],[
                    'career_data' => $performances,
                    'total' => $performances['allEps']
                ]);

                Longruncareerlevel::updateOrCreate([
                    'user_id' => $user->id,
                    'month_id' => $keyMonth,
                ],[
                    'level' => $performances['index']
                ]);

                $performances = $user->getPerformances($productionMonths[$keyMonth][0], $productionMonths[$keyMonth][1],1);

                Career::updateOrCreate([
                    'user_id' => $user->id,
                    'month_id' => $keyMonth,
                    'career_type' => 'monthly_career'
                ], [
                    'career_data' => $performances,
                    'total' => $performances['allEps']
                ]);

            });

        });


    }
}

<?php

namespace App\Console\Commands;

use App\Models\Epvoucher;
use Illuminate\Console\Command;

class CreateEpVouchers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'atl:createepvouchers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        for($i = 1; $i<=100; $i++){
            Epvoucher::firstOrCreate(['eps' => $i]);
        }
    }
}

<?php

namespace App\Console\Commands;

use App\Models\Category;
use Illuminate\Console\Command;

class fixCategoriesTree extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'atl:fixcategoriestree';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $errors = collect(Category::countErrors());
        if($errors->sum() > 0){
            $this->line('Starting the fixing...');

            Category::fixTree();

            $categories = Category::get();
            $this->line('Regenerate breadcrumbs');
            $bar = $this->output->createProgressBar(count($categories));
            $bar->start();
            $categories->each(function ($category, $key) use (&$bar) {
                $category->generateAncestorsBreadcrumb();
                $bar->advance();
            });
            $bar->finish();

        }else{
            $this->line('Nothing to fix');
        }
    }
}

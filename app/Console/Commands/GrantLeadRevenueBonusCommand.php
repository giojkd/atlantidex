<?php

namespace App\Console\Commands;

use App\Models\Lead;
use App\Models\Leadrow;
use Illuminate\Console\Command;

class GrantLeadRevenueBonusCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'atl:grantleadrevenuebonus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $rows = Leadrow::whereNull('commissionoperation_id')->get();

                if($rows->count() > 0){
                    $rows->each(function($row,$keyRow){
                        $merchant = $row->merchant;
                        $company = $merchant->user;
                        $sponsor = $company->virtualSponsor;
                        if(is_null($sponsor)){
                            $sponsor = $company->sponsor;
                        }
                        $commissionoperation = $sponsor->commissionoperations()->create([
                            'value' => $row->subtotal * 0.01,
                            'description' => 'Revenue bonus on purchase '.$row->lead_id.' @ '.$row->merchant->name.' (row '.$row->id.')',
                            'commissionoperationable_id' => $row->id,
                            'commissionoperationable_type' => Leadrow::class,
                            'commission_type' => 'revenue_bonus',
                        ]);
                        $row->commissionoperation_id = $commissionoperation->id;
                        $row->save();
                    });
                }
    }
}

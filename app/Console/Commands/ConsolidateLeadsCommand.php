<?php

namespace App\Console\Commands;

use App\Models\Insertion;
use App\Models\Lead;
use Illuminate\Console\Command;

class ConsolidateLeadsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'atl:consolidateleads';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $minDate = date('Y-m-d H:i:s', strtotime('-14days'));

        $leads = Lead::whereNull('consolidated_at')
        ->whereNull('cancelled_at')
        ->where(function($query) use($minDate){
            $query
            ->where('is_online', 0)
            ->whereOr('confirmed_at', '<=', $minDate);
        })
        ->get();


        if($leads->count() > 0){
            $leads->each(function($lead,$key){

                $lead->consolidate();

                $paymentMethod = $lead->printPaymentMethodRaw();
                $leadrows = $lead->leadrows;
                if(!is_null($leadrows)){
                    $leadrows->each(function($row,$key)use($paymentMethod){
                        $userToInvoice = $row->merchant->user;
                        $row->emitInvoice($userToInvoice, $paymentMethod);
                    });
                }

            });
        }


    }
}

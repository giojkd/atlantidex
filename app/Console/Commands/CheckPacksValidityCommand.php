<?php

namespace App\Console\Commands;

use App\Models\BackpackUser;
use Illuminate\Console\Command;
use Exception;

class CheckPacksValidityCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'atl:checkpacksvalidity';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try{
        $packToCheckByRole = collect([
            'Networker' => [1, 2],
            'Company' => [3, 5, 6]
        ]);

        $users = BackpackUser::role($packToCheckByRole->keys())->get();

        #$users = BackpackUser::where('id',218)->get();

        $users->each(function($user,$key) use($packToCheckByRole){
            $this->line('User ' . $user->id . ' is being checked...');
            $userRoles = $user->roles;

            if(!is_null($userRoles)){

                foreach ($userRoles as $role) {



                    if($packToCheckByRole->keys()->contains($role->name)){

                        $this->line('checking packs '.implode(',', $packToCheckByRole[$role->name]));

                        $packpurchases =
                        $user
                        ->packpurchases()
                        ->where('active',1)
                        ->whereIn('pack_id', $packToCheckByRole[$role->name])
                        ->orderBy('created_at', 'DESC')
                        ->get();

                        if (!is_null($packpurchases)) {
                            foreach($packpurchases as $packpurchase){
                                $isInvalid = $packpurchase->hasExpired();

                                if($isInvalid){
                                    $packpurchase->active = 0;
                                    $packpurchase->save();

                                    $this->line('User ' . $user->id . ' is going to be disabled because of pack '.$packpurchase->pack->id);

                                    $user->has_purchased_pack = null;
                                    $user->active = false;
                                    $user->saveWithoutEvents();

                                }else{
                                    $this->line('User ' . $user->id . ' pack '.$packpurchase->pack->id.' is still valid...');
                                }
                            }

                        }else{
                                $this->line('User ' . $user->id . ' has no pack purchases...');
                        }

                    }
                }
            }

            $this->line('------------------------------------');

        });
    }catch(Exception $e){
        $this->line($e->getMessage());
    }

    }
}

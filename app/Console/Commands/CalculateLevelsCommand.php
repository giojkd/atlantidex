<?php
namespace App\Console\Commands;



use Illuminate\Console\Command;
use App\Models\BackpackUser;

class CalculateLevelsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'atl:calculatelevels';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate levels';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        exit;

        $users = BackpackUser::active()->get();
        $this->line('Active users: ' . $users->count());
        $this->line('----------------');

        $user = new BackpackUser();
        $today = date('Y-m-d');
        $months = collect($user->getProductionMonthsUntilGivenDate($today));
        $monthId = $user->getProductionMonthIndexByDate($today);
        $monthId--;
        $month = $months[$monthId];



        $users->each(function($user,$key) use ($month, $monthId){

            $level = $user->calculateLevel($month[0], $month[1], $monthId);
            $this->line('User '.$user->full_name.' ('.$user->id.'): Monthly Career Level: '.$level['monthlyCareer']['level']['index'].' Long Run Career Level '.$level['longRunCareer']['index']);
        });

    }



}

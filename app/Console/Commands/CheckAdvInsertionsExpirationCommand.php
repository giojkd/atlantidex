<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Advinsertion;

class CheckAdvInsertionsExpirationCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'atl:checkadvinsertionsexpiration';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Advinsertion::where('active', 1)->get()->each(function ($item, $key) {
            $item->checkInsertionExpiration();
        });
    }
}

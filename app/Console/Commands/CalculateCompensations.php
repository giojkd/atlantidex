<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\BackpackUser;
use Exception;

class CalculateCompensations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'atl:calculatecompensations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate compensations';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $user = new BackpackUser();
        #$months = collect($user->getProductionMonthsUntilGivenDate())->splice(-2,1);#->slice(1);#->splice(0, -1);#->splice(0,-1);
        $months = collect($user->getProductionMonthsUntilGivenDate())->splice(-2,1);
        #dd($months);
        $users = BackpackUser::active()->where('id','!=',1)->get();

        $users->filter(function($user,$key){
            return $user->hasRole('Networker');
        });



        $users->each(function($user,$key) use($months){

            $careers = $user->careers()->whereIn('month_id',$months->pluck((4)))->get();

            if($careers->count() == 2 * $months->count()){
                foreach ($months as $month) {
                    $monthId = $month[4];
                    $this->line('(User ' . $user->id . ' Month ' . $monthId . ') ' . $user->full_name);
                    try {
                        $user->calculateCompensations($monthId);
                    } catch (Exception $e) {
                        $this->line($e->getMessage());
                    }
                }
            }

        });

    }
}

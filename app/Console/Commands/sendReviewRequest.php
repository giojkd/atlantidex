<?php

namespace App\Console\Commands;

use App\Models\Lead;
use Illuminate\Console\Command;

class sendReviewRequest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'atl:sendreviewrequest';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send request for reviews about purchase experiences';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $leadstoreview = Lead::whereNull('review_requested_at');
        /*foreach($leadstoreview as $lead) {

        }*/
    }
}

<?php

namespace App\Console\Commands;

use App\Models\BackpackUser;
use App\Models\Commissionoperation;
use Illuminate\Console\Command;

class CalculateMerchantAndMerchantBoosterBonusCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'atl:calculatemerchantbonus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate merchant and merchant booster bonus';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Commissionoperation::whereIn('commission_type',['company_affiliation', 'company_booster_affiliation'])->delete();
        $companies =

        BackpackUser::where('pack_id',3)
        ->whereActive(1)
        ->whereNotNull('parent_id')
        ->whereNotNull('invitationlink_id')
        ->whereNotNull('has_signed_agreement')
        ->whereNotNull('has_purchased_pack')
        ->get();

        #no need to change the way I collect companies, since how much their sponsors are earning does only
        #depends on what pack the merchant marketer (the sposnor) has bought

        $this->line('There are '.$companies->count().' companies');

        $companies->each(function($company,$key){

            $sponsor = $company->sponsor;
            $virtualSponsorText = '';
            $virtual_sponsor = $company->virtual_sponsor;

            if(!is_null($virtual_sponsor)){
                $virtualSponsorText = ' and virtual sponsor '.$virtual_sponsor->full_name;
            }

            $invitationLink = $company->invitationlink;
            $invitationLinkText = ' and has registered with a '.$invitationLink->link_type.' link';


            $commissionGoesToUser = (!is_null($virtual_sponsor)) ? $virtual_sponsor : $sponsor;



            $commissionGoesToUser->commissionoperations()->create([
                'created_at' => $company->has_purchased_pack,
                'value' => 50,
                'description' => 'Company '.$company->company_name.' completed registration',
                'commission_type' => $invitationLink->link_type.'_affiliation'
            ]);



            $this->line('Company '.$company->company_name.' has sponsor '. $sponsor->full_name. $virtualSponsorText. $invitationLinkText. $invitationLinkText);
            $this->line('--------------');
        });
    }
}

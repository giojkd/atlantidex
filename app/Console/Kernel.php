<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

         $schedule->command('atl:checkadvinsertionsexpiration')->everyMinute();
         $schedule->command('atl:consolidateleads')->everyFiveMinutes();
         $schedule->command('atl:grantleadrevenuebonus')->everyFiveMinutes();
         $schedule->command('atl:fixcategoriestree')->daily();
         $schedule->command('atl:fixuserstree')->daily();
         $schedule->command('atl:calculatecareers')->everyFourHours();
         $schedule->command('atl:importalgoliamerchants')->daily();


         #         $schedule->command('atl:calculatecompensations')->daily(); #suspended


         $schedule->command('scout:import "App\Models\Product"')->daily();


         $schedule->command('atl:checkpacksvalidity')->daily();



    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}

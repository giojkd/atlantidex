<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * The path to the "home" route for your application.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        $this->mapMyAtlantinexWebRoutes();

        $this->mapMyBusinessWebRoutes();

        $this->mapIncreaseMyBusinessWebRoutes();


        $this->mapCompanyActivationRoutes();

        $this->mapCompanyRegistrationWebRoutes();

        $this->mapCompanyWebRoutes();

        $this->mapSettingsWebRoutes();

        $this->mapMerchantBusinessWebRoutes();

        $this->mapAdvPacksRoutes();

        $this->mapCompanyPosRoutes();

        $this->mapFrontRoutes();
        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */

    protected function mapFrontRoutes(){
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/front.php'));
    }

    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    protected function mapAdvPacksRoutes()
    {
        Route::middleware('web')
        ->namespace($this->namespace)
            ->group(base_path('routes/Company/AdvPacks.php'));
    }

    protected function mapMyAtlantinexWebRoutes(){
        Route::middleware([
            config('backpack.base.web_middleware', 'web'),
            backpack_middleware(),
        ])
        ->namespace($this->namespace)
        ->group(base_path('routes/Atlantinex/MyAtlantinex.php'));
    }

    protected function mapMyBusinessWebRoutes()
    {
        Route::middleware([
            config('backpack.base.web_middleware', 'web'),
            backpack_middleware(),
        ])
            ->namespace($this->namespace)
            ->group(base_path('routes/Atlantinex/MyBusiness.php'));
    }

    protected function mapMerchantBusinessWebRoutes()
    {
        Route::middleware([
            config('backpack.base.web_middleware', 'web'),
            backpack_middleware(),
        ])
            ->namespace($this->namespace)
            ->group(base_path('routes/Atlantinex/MerchantBusiness.php'));
    }

    protected function mapCompanyWebRoutes()
    {
        Route::middleware([
            config('backpack.base.web_middleware', 'web'),
            backpack_middleware(),
        ])
            ->namespace($this->namespace)
            ->group(base_path('routes/Company/Catalogue.php'));
    }

    protected function mapCompanyPosRoutes()
    {
        Route::middleware([
            config('backpack.base.web_middleware', 'web'),
            backpack_middleware(),
        ])
            ->namespace($this->namespace)
            ->group(base_path('routes/Company/Pos.php'));
    }

    protected function mapCompanyRegistrationWebRoutes()
    {
        Route::middleware([
            config('backpack.base.web_middleware', 'web'),
            backpack_middleware(),
        ])
            ->namespace($this->namespace)
            ->group(base_path('routes/Company/Registration.php'));
    }


    protected function mapSettingsWebRoutes()
    {
        Route::middleware([
            config('backpack.base.web_middleware', 'web'),
            #backpack_middleware(),
        ])
            ->namespace($this->namespace)
            ->group(base_path('routes/Company/Settings.php'));
    }

    protected function mapIncreaseMyBusinessWebRoutes()
    {
        Route::middleware([
            config('backpack.base.web_middleware', 'web'),
            backpack_middleware(),
        ])
            ->namespace($this->namespace)
            ->group(base_path('routes/Atlantinex/IncreaseMyBusiness.php'));
    }

    protected function mapEventsWebRoutes()
    {
        Route::middleware([
            config('backpack.base.web_middleware', 'web'),
            backpack_middleware(),
        ])
            ->namespace($this->namespace)
            ->group(base_path('routes/Atlantinex/Events.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }

    protected function mapCompanyActivationRoutes(){
        Route::middleware([
            config('backpack.base.web_middleware', 'web'),
            backpack_middleware(),
        ])
            ->namespace($this->namespace)
            ->group(base_path('routes/Company/Activation.php'));
    }

}

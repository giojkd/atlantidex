<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Bid;
use App\Models\Category;
use App\Models\Changesponsorrequest;
use App\Models\Epoperation;
use App\Models\Epplusoperation;
use App\Models\Invoicerow;
use App\Models\Leadrow;
use App\Models\Merchantproduct;
use App\Models\Review;
use App\Models\Product;
use App\Models\Productimportfile;
use App\Models\Voucheroperation;
use App\Models\Voucherpurchase;
use App\Models\Walletoperation;
use App\Observers\BidObserver;
use App\Observers\CategoryObserver;
use App\Observers\ChangesponsorrequestObserver;
use App\Observers\EpoperationObserver;
use App\Observers\Epplusoperationobserver;
use App\Observers\InvoicerowObserver;
use App\Observers\LeadrowObserver;
use App\Observers\MerchantproductObserver;
use App\Observers\Productimportfileobserver;
use App\Observers\ReviewObserver;
use App\Observers\ProductObserver;
use App\Observers\VoucheroperationObserver;
use App\Observers\VoucherpurchaseObserver;
use App\Observers\WalletoperationObserver;
use Blade;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\URL;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        if(env('APP_ENV') == 'production'){
            URL::forceScheme('https');
        }

        $this->overrideConfigValues();

        Bid::observe(BidObserver::class);
        Product::observe(ProductObserver::class);
        Review::observe(ReviewObserver::class);
        Walletoperation::observe(WalletoperationObserver::class);
        Changesponsorrequest::observe(ChangesponsorrequestObserver::class);
        Productimportfile::observe(Productimportfileobserver::class);
        Merchantproduct::observe(MerchantproductObserver::class);
        Category::observe(CategoryObserver::class);
        Invoicerow::observe(InvoicerowObserver::class);
        Epoperation::observe(EpoperationObserver::class);
        Leadrow::observe(LeadrowObserver::class);
        Voucherpurchase::observe(VoucherpurchaseObserver::class);
        Voucheroperation::observe(VoucheroperationObserver::class);
        Epplusoperation::observe(Epplusoperationobserver::class);

        Blade::directive('hss', function ($param) {
            return "<?php echo '<div class=\"height-spacer\" style=\"height: '.$param.'px\"></div>'; ?>";
        });

        Blade::directive('fp', function ($param) {
            #$param = (double)$param;
            return "<?php echo '&euro;'. number_format($param,2); ?>";
        });



        Collection::macro('flattenTree', function ($childrenField) {
            $result = collect();

             foreach ($this->items as $item) {
                $result->push($item);

                if ($item->$childrenField instanceof Collection) {
                    $result = $result->merge($item->$childrenField->flattenTree($childrenField));
                }
            }

            return $result;
        });
    }

    protected function overrideConfigValues()
    {
        $config = [];
        if (config('settings.skin'))
            $config['backpack.base.skin'] = config('settings.skin');
        if (config('settings.show_powered_by'))
            $config['backpack.base.show_powered_by'] = config('settings.show_powered_by') == '1';
        config($config);
    }
}

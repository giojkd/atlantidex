<?php


namespace App\Scopes;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class SelfuserScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        if (!backpack_user()->isAdministrator()) {
            $builder->where('id', '=', backpack_user()->id);
        }
    }
}


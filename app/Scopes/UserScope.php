<?php


namespace App\Scopes;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Builder;

class UserScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        if (!backpack_user()->isAdministrator()) {
            $builder->where('user_id', '=', backpack_user()->id);
        }
    }
}

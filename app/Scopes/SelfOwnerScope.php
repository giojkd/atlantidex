<?php


namespace App\Scopes;

use App\Models\Product;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class SelfOwnerScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $builder->whereIn('products.id',Product::rawProduct(backpack_user()->id)->pluck('product_id'));
    }
}

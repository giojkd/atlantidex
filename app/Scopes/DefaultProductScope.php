<?php


namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class DefaultProductScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $builder->whereRaw('products.parent_id=products.id');
    }
}

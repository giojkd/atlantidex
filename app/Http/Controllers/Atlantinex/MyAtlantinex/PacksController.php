<?php

namespace App\Http\Controllers\Atlantinex\MyAtlantinex;

use Illuminate\Http\Request;
use \App\Http\Controllers\Controller;
use App\Models\BackpackUser;
use App\Models\Download;
use App\Models\Pack;
use Auth;

class PacksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $user = Auth::user();

        if (is_null($user->pack_id)) {
            $packs = Pack::whereActive(1)->where('is_starter_pack', 1)->where('role_id',Auth::user()->roles()->first()->id)->get();
        } else {
            $packs = Pack::where('id', '!=', $user->pack_id)->where('is_starter_pack', '!=', 1)->get();
        }

        #added to not show merchant marketer to companies
        if($user->hasRole('Company')){
            $packs = $packs->except(2);
        }
        #added to not show merchant marketer to companies

        $data['user'] = $user;

        $data['packs'] = $packs;

        $data['renewable_packs'] = $user->packpurchases()
        ->get()
        ->groupBy('pack_id')
        ->transform(function($pps,$key){
            return collect([
                'pack' => $pps->first()->pack,
                'active' => $pps->sum('active')
            ]);
        })
        ->where('active',0)
        ->pluck('pack');
        #dd($data['renewable_packs']);


        if(!is_null($data['renewable_packs'])){
            $data['renewable_packs']->transform(function($item,$key){
                $item->price = $item->renew_price;
                return $item;
            });
        }



        $data['client_secret_intent'] = $user->createSetupIntent();
        $data['stripe_customer_id'] = $user->createOrGetStripeCustomer();
        $data['user'] = $user;
        $data['payment_methods'] = collect($user->paymentMethods());
        $data['compensationPlanDownloadLink'] = url(Download::whereTag('compensation_plan')->first()->file);

        return view('Atlantinex.MyAtlantinex.packs', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */



    public function store(Request $request)
    {
        //
        $user = Auth::user();

        $method = 'pay_with_' . $request->method;
        $status = $this->$method($request);

        if ($status['status'] == 1) {

            $pack = Pack::find($request->pack_id);
            $pack->emitInvoice($user, $request->method);

            if($request->method == 'wallet'){
                $walletOperatoin = $user->walletoperations()->create([
                    'value' => $request->amount * -1,
                    'receipt' => 'You bought pack ' . $pack->name . ' for &euro; ' . number_format($request->amount, 2, ',', '')
                ]);
            }

            $user->packpurchases()->create([
                'pack_id' => $pack->id
            ]);



            $user->has_purchased_pack = date('Y-m-d H:i:s');

            if(!$request->is_renewal){
                $user->pack_id = $pack->id;
            }


            $user->company_invitation_links_count += $pack->merchant_bonus_affiliations;
            $user->networker_invitation_links_count += $pack->network_affiliations;
            $user->company_booster_invitation_links_count += $pack->merchant_booster_bonus_affiliations;

            $user->save();

            ###################
            #                 #
            #needs improvement#
            #                 #
            ###################

            $invitationLink = $user->invitationlink;


            if(!is_null($invitationLink)){

                $EPs = 0;
                $Commission = 0;

                $mapEpsToGrantByInvitationlinkType = [
                    'company' => 'merchant_bonus_eps',
                    'company_booster' => 'merchant_booster_bonus_eps',
                    'networker' => 'sponsor_granted_eps_at_pack_purchase'
                ];

                $mapCommissionsToGrantByInvitationlinkType = [
                    'company' => 'merchant_bonus',
                    'company_booster' => 'merchant_booster_bonus',
                ];

                if(isset($mapEpsToGrantByInvitationlinkType[$invitationLink->link_type])){
                    $EPsIndex = $mapEpsToGrantByInvitationlinkType[$invitationLink->link_type];
                    #$EPs = $user->pack->$EPsIndex;
                    $EPs = $pack->$EPsIndex;
                }

                if(isset($mapCommissionsToGrantByInvitationlinkType[$invitationLink->link_type])){
                    $CommissionIndex = $mapCommissionsToGrantByInvitationlinkType[$invitationLink->link_type];
                    #$Commission = $user->pack->$CommissionIndex;
                    $Commission = $pack->$CommissionIndex;
                }

                if (is_null($user->virtual_parent_id)) {
                    $ecominicSponsor = $user->sponsor;
                } else {
                    $ecominicSponsor = BackpackUser::find($user->virtual_parent_id);
                }

                if($EPs > 0){
                    $user->epoperations()->create([ #<------------------  EPS OPERATION
                        'value' => $EPs,
                        'description' => 'User '.$user->full_name.' (' . $user->id . ') purchased pack '.$pack->name.' ('.$pack->id.')',
                        'ep_type' => '',
                        'is_careerable' => 0,
                        'user_role' => $user->roles->first()->name,
                        'epoperationable_id' => $pack->id,
                        'epoperationable_type' => Pack::class
                    ]);
                }

                if($Commission > 0){

                    $ecominicSponsor->commissionoperations()->create([
                        'value' => $Commission,
                        'description' => 'User '.$user->full_name.' ('.$user->id.') purchased pack '.$pack->name.' ('.$pack->id.')',
                        'commissionoperationable_id'=> $pack->id,
                        'commissionoperationable_type' => Pack::class,
                        'commission_type' => 'merchant_bonus'
                    ]);

                }

            }

        }

        return $status;
    }



    public function pay_with_wallet(Request $request)
    {
        $status = 0;
        $user = Auth::user();
        if ($user->wallet_balance >= $request->amount) {
            $status = 1;
        } else {
            abort(401);
        }
        return ['status' => $status];
    }

    public function pay_with_stored_credit_card(Request $request)
    {
        $user = Auth::user();
        $status = 0;
        try {
            $stripeCharge = $user->charge($request->amount * 100, $request->payment_method_id);
            $status = 1;
        } catch (Exception $e) {
        }
        return ['status'  => $status];
    }

    public function pay_with_pay_with_new_card(Request $request)
    {
        return $this->pay_with_stored_credit_card($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Atlantinex\MyAtlantinex;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use \App\Http\Controllers\Controller;
use App\Models\BackpackUser;
use App\Models\Changesponsorrequest;
use App\Models\Document;
use App\Models\Documenttype;
use App\Models\User;
use Exception;
use PDF;
use App\Models\Otp;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $data = [];

        $user = Auth::user();

        $data['user'] = $user;

        $data['user_sponsor'] = BackpackUser::withoutGlobalScopes()->find(Auth::user()->parent_id);

        $data['client_secret_intent'] = $user->createSetupIntent();
        $data['stripe_customer_id'] = $user->createOrGetStripeCustomer();
        $data['payment_methods'] = collect($user->paymentMethods());

        return view('Atlantinex.MyAtlantinex.personal_data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user = BackpackUser::with(['addresses'])->findOrFail($id);
        $data = collect($request->post())->except('_token', '_method')->toArray();
        $data['address'] = json_decode($data['address']);
        $user->fill($data)->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function myWallet()
    {
        $data = [];
        $user = Auth::user();
        $data['user'] = $user;
        $data['operations'] =  $user->walletoperations()->orderBy('id', 'ASC')->get();
        $data['payment_methods'] = collect($user->paymentMethods());
        $data['payouts'] = $user->payouts;
        $data['PayPalTopUpDescription'] = "Ricarica wallet per utente ".$user->name.' '.$user->surname;
        return view('Atlantinex.MyAtlantinex.my_wallet', $data);
    }

    public function payoutRequest(Request $request){

        $user = Auth::user();

        $request->validate([
            'otp' => 'required',
            'value' => 'required|numeric'
        ]);

        $otp = Otp::where('user_id', $user->id)
        ->where('otpable_type', 'payout_request')
        ->where('otpable_id', 1)
        ->where('attempts', '<', 2)
        ->where('status', 0)
        ->orderBy('id', 'DESC')
        ->first();

        if (!is_null($otp)) {
            $return['message'] = 'Has usable otp but the submitted code is wrong';
            if ($otp->code == $request->otp) {
                $return['status'] = 1;

                $otp->status = 1;
                $otp->attempts++;
                $otp->save();
            } else {
                $return['status'] = 0;

                $otp->attempts++;
                $otp->save();
                $return['message'] = ($otp->attempts < 2) ? 'Hai ancora ' . (2 - $otp->attempts) . ' tentativi'  : 'Hai esaurito i tentativi richiedi un altro codice.';
                return $return;
            }
        } else {
            $return['message'] = 'Hai esaurito i tentativi richiedi un altro codice';
            $return['status'] = 0;
            return $return;
        }

        if($request->value > $user->wallet_balance){
            $return['message'] = 'La tua richiesta non è coperta dal wallet';
            $return['status'] = 0;
            return $return;
        }

        if($request->value < (float)env('MIN_PAYOUT_VALUE')){
            $return['message'] = 'La tua richiesta deve essere superiore a € '. env('MIN_PAYOUT_VALUE');
            $return['status'] = 0;
            return $return;
        }

        $payout = $user->payouts()->create([
            'value' => $request->value
        ]);

        $user->walletoperations()->create([
                'walletoperationable_id' => $payout->id,
                'walletoperationable_type' => 'App\\Models\\Payout',
                'value' => $request->value * -1,
                'receipt' => 'Hai richiesto un payout di €'.$payout->value

        ]);

        return ['status'=>1];


    }

    public function payoutRequestOtp(Request $request){
        $user = Auth::user();
        $user->otps()->update(['status' => 1]);
        $sendingStatus = Otp::generate(1, 'payout_request', $user->phone, $user);
        return $sendingStatus;
    }

    public function topUpWallet(Request $request)
    {

        $request->validate([
            'data.paymentmethod_id' => 'required',
            'data.value' => 'required|min:0.01|numeric'
        ]);

        $topUpAttempt = $this->myWalletTopUp($request->data['value'], Auth::user(), $request->data['paymentmethod_id']);

            if($topUpAttempt['status'] == 1){
                return back();
            }else{
                dd($topUpAttempt);
            }
    }

    public function topUpWalletPayPal(Request $request){
        $request->validate([
            'value' => 'required',
            'actionDetails' => 'required'
        ]);

        $data = json_decode($request->actionDetails);

        Log::info($request->actionDetails);

        $user = Auth::user();

        $user->walletoperations()->create([
            'value' => $request->value,
            'receipt' => 'Ricarica wallet tramite PayPal',
            'paymentmethod_details' => $data
        ]);

        return ['status' => 1];

    }

    public function myWalletTopUp($value, $user, $paymentMethodId)
    {

        $return = ['status' => 0];

        try {
            $chargeAttemp = $user->charge(\intval($value * 100), $paymentMethodId);
            if ($chargeAttemp->status == 'succeeded') {

                $user->walletoperations()->create([
                    'value' => $value / 1.02,
                    'receipt' => 'Wallet TopUp &euro; ' . number_format($value,2,',',''),
                    #'payment_intent_feedback' => $chargeAttemp
                ]);
                $return['status'] = 1;
            }
        } catch (Exception $e) {
            abort(403, $e->getMessage());
        }

        return $return;
    }

    public function changeSponsorIndex(){
        $data = [];
        $user = Auth::user();
        $data['user'] = $user;

        $data['sent_requests'] = $user->changesponsorrequestssent;
        $data['received_requests'] = $user->changesponsorrequestsreceived;

        return view('Atlantinex.MyAtlantinex.change_sponsor', $data);
    }

    public function changeSponsorCreate(Request $request){

        $request->validate([
            'user_code' => ['required','exists:users,code'],
        ]);

        $userReceiving = User::where('code',$request->user_code)->first();

        $userSending = Auth::user();

        $documentType = Documenttype::where('tag', 'change_sponsor')->firstOrFail();

        $document = Document::where('documenttype_id', $documentType->id)->orderBy('id','DESC')->first();

        if(!is_null($document)){
            $changeSponsorRequest = $userSending->changesponsorrequestssent()->create([
                'receiving_user_id' => $userReceiving->id,
                'document_id' => $document->id
            ]);
            return redirect($changeSponsorRequest->signatureUrl());
        }

        return back()->withErrors(['Si è verificato un problema con la richiesta.']);


    }

    public function changeSponsorFeedback(Request $request){

        $changeSponsorRequest = Changesponsorrequest::findOrFail($request->id);
        $changeSponsorRequest->receiving_feedback = $request->receiving_feedback;
        $changeSponsorRequest->save();
    }

    public function changeSponsorRenderDocument(Request $request){

        $csr = Changesponsorrequest::findOrFail($request->id);

        $applicantUser = $csr->applicant_user;
        $receivingUser = $csr->receiving_user;

        if($csr->canBeRendered()){
            $document = $csr->document;

            $content = $document->render($applicantUser,['user_code' => $receivingUser->code]);

            $data = [
                'content' => $content,
                'applicant_signature' => $csr->applicant_signature,
                'receiving_signature' => $csr->receiving_signature,
            ];

            $pdf = PDF::loadView('Atlantinex.MyAtlantinex.signed_change_sponsor_request', $data);

            return $pdf->download( 'SponsorChangeRequest_'.$csr->id.'_.pdf');

        }else{
            return back();
        }
    }

    public function vouchers(Request $request){

    }

}

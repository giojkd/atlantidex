<?php

namespace App\Http\Controllers\Atlantinex\MyAtlantinex;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use \App\Http\Controllers\Controller;
use App\Models\BackpackUser;
use App\Models\Document;
use Image;
use Storage;
use App;
use App\Models\Changesponsorrequest;
use Illuminate\Support\Facades\Validator;
use PDF;


class DocumentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $user = Auth::user();

#dd($user->roles->first()->documenttypes->pluck('id')->toArray());


        $data['documents'] = Document::with([
            'users' => function ($query) {
                $query->where('user_id',Auth::user()->id);
            }
        ])
        ->whereIn('documenttype_id', $user->roles->first()->documenttypes->pluck('id')->toArray())
        ->orderBy('name', 'asc')
        ->get();

        #dd($data);

        return view('Atlantinex.MyAtlantinex.documents', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //

        $extraFields = ($request->has('extraFields')) ? $request->extraFields : [];

        $document = Document::findOrFail($id);
        $data['document'] = $document;
        $data['documentRendered'] = $document->render(Auth::user(), $extraFields);
        if($request->has('signAs') && $request->has('changeSponsorRequestId')){

            $data['signAs'] = $request->signAs;
            $data['changeSponsorRequestId'] = $request->changeSponsorRequestId;

        }

        return view('Atlantinex.MyAtlantinex.documents_show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function signDocumentWithCheckbox(Request $request){

        $request->validate(['id' => 'required']);
        $document = Document::find($request->id);

        $validationRules = [
            'identity_document' => 'required',
        ];

        if($document->acceptable_policy != ''){
            $validationRules['accepts_policy'] = 'required';
        }

        if($document->is_signable_with_checkbox){
            $validationRules['checkbox'] = 'required';
        }

        $messages = [
            'identity_document.required' => 'Devi caricare una foto fronte e retro di un documento di identità valido',
            'accepts_policy.required' => 'Devi accettare la policy per proseguire',
            'checkbox.required' => 'Devi accettare il contratto per proseguire'
        ];

        $validator = Validator::make($request->all(),$validationRules,$messages,[
            'email' => 'Indirizzo email',
            'accepts_policy' => 'Accettazione della policy'
        ]);

        if ($validator->fails()) {
            return
            back()
            ->withErrors($validator)
            ->withInput();
        }



        App::setlocale('it');
        $user = Auth::user();
        $document = Document::findOrFail($request->id);

        $fileName = $user->name . ' ' . $user->surname . ' ' . $document->name . ' ' . date('d-m-Y');
        $file = implode('/',['documents', $user->id, $fileName]);
        $path = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix() . $file;

        $data['content'] = $document->render($user);

        $pdf = PDF::loadView('Atlantinex.MyAtlantinex.signed_contract_no_signature', $data);

        Storage::disk('public')->put($file . '.pdf', $pdf->output());

        $user->documents()->attach([$document->id => ['document_url' => url('storage/' . $file . '.pdf')]]);

        $hasSignedContractTypes = ['networker_letter_of_intent', 'company_letter_of_intent'];


        if (in_array($document->type->tag, $hasSignedContractTypes)) {
            $user->has_signed_agreement = date('Y-m-d H:i:s');
            $user->save();
        }

        return redirect()->route('MyAtlantinex.documents.index');
    }

    public function update(Request $request, $id)
    {

             /*
        if(!is_null($document->acceptable_policy)){
            $validationRules['accepts_policy'] = 'required';
        }
        */


        $document = Document::findOrFail($id);


        $validationRules = [
            'identity_document' => 'required'
        ];

        $request->validate($validationRules);



        App::setlocale('it');
        $user = Auth::user();

        $img = Image::make(file_get_contents($request->signed))->encode();
        $fileName = $user->name.' '. $user->surname.' '.$document->name . ' ' . date('d-m-Y');
        $file = implode(['documents', $user->id, $fileName ],'/');
        $path = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix() . $file;

        Storage::disk('public')->put($file.'.png',$img);

        $data['content'] = $document->render($user);

        $signature = $path . '.png';

        $data['signature'] = $signature;

        $pdf = PDF::loadView('Atlantinex.MyAtlantinex.signed_contract', $data);

        Storage::disk('public')->put($file . '.pdf', $pdf->output());

        $user->documents()->attach([$document->id => ['document_url'=>url('storage/'.$file.'.pdf'),'identity_document' => json_encode($request->identity_document)]]);

        #for change sponsor requests

        if($request->has('sign_as')&& $request->has('change_sponsor_request_id')){
            if ($document->type->tag == 'change_sponsor') {

                $changeSponsorRequest = Changesponsorrequest::find($request->change_sponsor_request_id);
                $signAs = $request->sign_as.'_signature';
                $changeSponsorRequest->$signAs = $signature;
                $changeSponsorRequest->save();

                return redirect()->route('MyAtlantinex.changeSponsor');

            }
        }

        $hasSignedContractTypes = ['networker_letter_of_intent', 'company_letter_of_intent'];


        if(in_array($document->type->tag, $hasSignedContractTypes)){
            $user->has_signed_agreement = date('Y-m-d H:i:s');
            $user->save();
        }


        #return $pdf->download($fileName.'.pdf');
        return redirect()->route('MyAtlantinex.documents.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}

<?php

namespace App\Http\Controllers\Atlantinex\MyAtlantinex;

use Illuminate\Http\Request;
use \App\Http\Controllers\Controller;
use App\Models\BackpackUser;
use App\Models\Event;
use App\Models\Gdovoucher;
use App\Models\Gdovouchercode;
use App\Models\Pack;
use App\Models\Ticket;
use App\Models\Voucheroperation;
use Auth;
use Illuminate\Support\Facades\Storage;
use chillerlan\QRCode\QRCode;
use Str;

class GdoVouchersShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $data = [];

        if ($request->has('status') && $request->has('message')) {
            $data['status'] = $request->status;
            $data['message'] = $request->message;
        }

        $user = Auth::user();
        $data['vouchers'] = $user->gdovouchers()->orderBy('purchased_at','DESC')->orderBy('downloaded_times','ASC')->get();
        $data['user'] = $user;
        return view('Atlantinex.MyAtlantinex.gdovouchers_bought', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $data = [];
        $user = Auth::user();

        $data['vouchers'] = Gdovoucher::where('is_active',1)->get();
        $data['user'] = $user;



        return view('Atlantinex.MyAtlantinex.gdovouchers', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {

        $user = Auth::user();

        $method = 'pay_with_' . $request->method;

        $status = $this->$method($request);

        if ($status['status'] == 1) {

            $selectedVouchers = $request->selectedVouchers;

            $gdovoucher = Gdovoucher::find($request->voucher_id);



            $totalAmount = 0;

            foreach ($selectedVouchers as $selectedVoucher) {
                $vouchercodes = Gdovouchercode::where('value', $selectedVoucher['price'])->whereNull('user_id')->where('gdovoucher_id', $request->voucher_id)->get();
                for ($i = 0; $i < $selectedVoucher['quantity']; $i++) {
                    $vouchercodes[$i]->user_id = $user->id;
                    $vouchercodes[$i]->purchased_at = date('Y-m-d H:i:s');
                    $vouchercodes[$i]->save();
                    $vouchercodes[$i]->emitInvoice($user, $request->method);
                }



                $totalAmount += $selectedVoucher['quantity'] * $selectedVoucher['price'];
            }

            $user->total_spendings += $totalAmount;
            $user->save();
            $user->checkIfHasPurchasedMinVouchers();

            if (!is_null($gdovoucher->eps) && $gdovoucher->eps > 0) {

                $EPs = $totalAmount  / 100 * $gdovoucher->eps;

                if ($user->hasRole('Networker')) {
                    if ($user->split_eps) {
                        $user->epoperations()->create([
                            'value' => $EPs/2,
                            'epoperationable_type' => Gdovoucher::class,
                            'epoperationable_id' => $gdovoucher->id,
                            'description' => 'Has purchased gdo vouchers for ' . number_format($totalAmount, 2) . '€ for voucher ' . $gdovoucher->id,
                            'user_role' => $user->roles->first()->name,
                            'ep_type' => 'gdovoucher_purchase'
                        ]);
                        $user->epplusoperations()->create([
                            'value' => $EPs / 2,
                            'epplusoperationable_type' => Gdovoucher::class,
                            'epplusoperationable_id' => $gdovoucher->id,
                            'epplus_type' => 'gdovoucher_purchase',
                            'description' => 'Has purchased ' . number_format($request->amount, 2) . '€ in gdo voucher '. $gdovoucher->name,
                            'is_careerable' => 1,
                            'sign' => 1
                        ]);
                    }else{
                        $user->epoperations()->create([
                            'value' => $EPs,
                            'epoperationable_id' => $gdovoucher->id,
                            'epoperationable_type' => Gdovoucher::class,
                            'description' => 'Has purchased gdo vouchers for ' . number_format($totalAmount, 2) . '€ for voucher ' . $gdovoucher->id,
                            'user_role' => $user->roles->first()->name,
                            'ep_type' => 'gdovoucher_purchase'
                        ]);
                    }
                }

                if ($user->hasAnyRole(['Customer', 'Company'])) {

                    $user->epoperations()->create([
                        'value' => $EPs / 2,
                        'epoperationable_type' => Gdovoucher::class,
                        'epoperationable_id' => $gdovoucher->id,
                        'description' => 'Has purchased gdo vouchers for ' . number_format($totalAmount, 2) . '€ for voucher ' . $gdovoucher->id,
                        'user_role' => $user->roles->first()->name,
                        'ep_type' => 'gdovoucher_purchase'
                    ]);
                    $user->epplusoperations()->create([
                        'value' => $EPs / 2,
                        'epplusoperationable_type' => Gdovoucher::class,
                        'epplusoperationable_id' => $gdovoucher->id,
                        'epplus_type' => 'gdovoucher_purchase',
                        'description' => 'Has purchased ' . number_format($request->amount, 2) . '€ in gdo voucher ' . $gdovoucher->name,
                        'is_careerable' => 1,
                        'sign' => 1
                    ]);
                }


            }

            $revenueBonusGoesTo = $gdovoucher->user;
            $pack = $revenueBonusGoesTo->pack;
            $direct_commission = (!is_null($gdovoucher->direct_commission)) ? $gdovoucher->direct_commission : $pack->revenue_bonus;
            if($direct_commission > 0){
                $revenueBonusGoesTo->commissionoperations()->create([
                    'value' => $totalAmount * $direct_commission / 100,
                    'description' => 'User ' .$user->full_name. ' ('. $user->id . ') purchased ' . number_format($totalAmount, 2) . '€ in gdovoucher ' . $gdovoucher->name.' ('.$gdovoucher->id.')',
                    'commissionoperationable_id' => $gdovoucher->id,
                    'commissionoperationable_type' => Gdovoucher::class,
                    'commission_type' => 'revenue_commission'
                ]);
            }


        }
        return $status;
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data = [];
        $gdovoucher = Gdovoucher::find($id);
        $data['voucher'] = $gdovoucher;
        $data['codeGroups'] = $gdovoucher->gdovouchercodes()->whereNull('user_id')->get()->groupBy('value_int');

        $user = Auth::user();

        $data['client_secret_intent'] = $user->createSetupIntent();
        $data['stripe_customer_id'] = $user->createOrGetStripeCustomer();
        $data['user'] = $user;
        $data['payment_methods'] = collect($user->paymentMethods());


        return view('Atlantinex.MyAtlantinex.gdovoucher_shop', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $voucher = Gdovouchercode::findOrFail($id);
        $request->validate([
            'amount_spent' => 'required'
        ]);

        $voucher->amount_spent = $request->amount_spent;
        $voucher->save();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function pay_with_wallet(Request $request)
    {
        $status = 0;
        if (Auth::user()->wallet_balance >= $request->amount) {
            Auth::user()->walletoperations()->create([
                'value' => $request->amount * -1,
                'receipt' => 'You bought ' . $request->eps . ' EP granting vouchers for &euro; ' . number_format($request->amount, 2, ',', '')
            ]);
            $status = 1;
        } else {
            //
        }
        return ['status' => $status];
    }

    public function pay_with_stored_credit_card(Request $request)
    {
        $user = Auth::user();
        $status = 0;
        try {
            $stripeCharge = $user->charge($request->amount * 100, $request->payment_method_id);
            $status = 1;
        } catch (Exception $e) {
            //
        }
        return ['status'  => $status];
    }

    public function pay_with_pay_with_new_card(Request $request)
    {
        return $this->pay_with_stored_credit_card($request);
    }

    public function spendVouchers(Request $request){

        $request->validate([
            'voucher_id' => 'required',
            'selected_vouchers' => 'required'
        ]);

        $gdovoucher = Gdovoucher::findOrFail($request->voucher_id);

        $selectedVouchers = [];

        $totalPrice = collect(explode('-', $request->selected_vouchers))->reduce(function($carry,$item) use(&$selectedVouchers){
            list($price,$quantity) = Str::of($item)->explode('_');
            $selectedVouchers[] = ['price' => $price,'quantity' => $quantity] ;
            return $carry + ($price * $quantity);
        },0);

        $totalAmount = $totalPrice;

        $user = Auth::user();

        $voucherAmount = $user->vouchers()->where('eps','<=',$gdovoucher->eps)->sum('amount');

        $unsuccessfulMessage = 'Non hai abbastanza voucher nella categoria da '. $gdovoucher->eps.'EP o inferiore';

        if($voucherAmount >= $totalPrice){

            $epCategory = $gdovoucher->eps;

                while($totalPrice > 0){
                    $voucher = $user->vouchers()->where('eps', $epCategory)->first();

                    if(!is_null($voucher)){
                        $voucherValue = $voucher->amount;
                        #echo 'cat '.$epCategory.' has '. $voucherValue;
                        if($totalPrice >= $voucherValue){

                            $voucher->amount -= $voucherValue;
                            $totalPrice -= $voucherValue;

                        }else{

                            $voucher->amount -= $totalPrice;
                            $totalPrice -= $totalPrice;

                        }
                        $voucher->save();
                    }
                    $epCategory--;
                }



                foreach ($selectedVouchers as $selectedVoucher) {
                    $vouchercodes = Gdovouchercode::where('value', $selectedVoucher['price'])->whereNull('user_id')->where('gdovoucher_id', $request->voucher_id)->get();
                    for ($i = 0; $i < $selectedVoucher['quantity']; $i++) {

                        $vouchercodes[$i]->user_id = $user->id;
                        $vouchercodes[$i]->save();
                        $vouchercodes[$i]->emitInvoice($user, 'epvoucher');

                        Voucheroperation::create(
                            [
                                'voucheroperationable_type' => Gdovoucher::class,
                                'voucheroperationable_id' => $gdovoucher->id,
                                'amount' => $vouchercodes[$i]->value,
                                'eps' => $gdovoucher->eps,
                                'receipt' => 'User ' . $user->full_name . ' purchased GDO Voucher ' . $gdovoucher->name . ' for €' . $vouchercodes[$i]->value,
                                'user_id' =>  $user->id
                            ]
                        );

                    }
                }

                if (!is_null($gdovoucher->eps) && $gdovoucher->eps > 0) {

                    $EPs = $totalAmount  / 100 * $gdovoucher->eps;

                    $user->epoperations()->create([
                        'value' => $EPs,
                        'epoperationable_id' => $gdovoucher->id,
                        'epoperationable_type' => Gdovoucher::class,
                        'description' => 'Has purchased gdo vouchers for ' . number_format($totalAmount, 2) . '€ for voucher ' . $gdovoucher->id,
                        'user_role' => $user->roles->first()->name,
                        'ep_type' => 'gdovoucher_purchase'
                    ]);
                }


                $message = 'Complimenti, hai acquistato i tuoi voucher!';

                return redirect(Route('MyAtlantinex.gdovouchersshop.index'). '?status=1&message='.$message);


        }else{
            $message = $unsuccessfulMessage;
            return redirect(Route('MyAtlantinex.vouchers.index').'?status=0&message=' . $message);
        }

    }

    public function getDownload($id)
    {
        //
        $data = [];
        $gdovouchercode = Gdovouchercode::find($id);
        $data['gdovoucher'] = $gdovouchercode;



        if(!is_null($gdovouchercode->barcode)){

            /*
            $qrCodeName = 'gdovouchercode_' . $gdovouchercode->id . '.png';
            $path = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix() . 'tickets/' . $qrCodeName;


            (new QRCode)->render($gdovouchercode->barcode, $path);

            $data['qr_code'] = config('app.url') . '/storage/tickets/' . $qrCodeName;
            */

        }

        //dd($data['gdovoucher']);
        $user = Auth::user();

        $gdovouchercode->downloaded_times ++;
        $gdovouchercode->save();

        $pdf = app('dompdf.wrapper');
        $pdf->loadView('Atlantinex.MyAtlantinex.voucher_pdf', $data);

        return $pdf->download('voucher_' . $gdovouchercode->code . '.pdf');
    }

    public function networkerActivationProcessInterrupt(Request $request){

        Auth::user()->removeRole('Networker')->assignRole('Customer');
        return redirect('admin');
    }

}

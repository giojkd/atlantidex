<?php

namespace App\Http\Controllers\Atlantinex\MyAtlantinex;

use Illuminate\Http\Request;
use \App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\Pack;
use App\Models\Ticket;
use Auth;

class EventsShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */





    public function index()
    {

        $data = [];
        $user = Auth::user();

        $data['tickets'] = $user->tickets;

        return view('Atlantinex.MyAtlantinex.ticketslist', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $data = [];
        $ancestors = Auth::user()->ancestors()->get()->pluck('id');

        $ancestors[] = Auth::id();



        $data['events'] = Event::where('active', 1)
        ->where(function($query) use($ancestors){
            $query
            ->whereIn('user_id', $ancestors)
            ->where('happens_at', '>', date('Y-m-d 00:00:00'));
        })
        ->orWhere(function($query){
            $query
            ->where('eventtype_id',2)
            ->where('happens_at', '>', date('Y-m-d 00:00:00'));
        })

        ->orderBy('happens_at', 'ASC')->get();

        return view('Atlantinex.MyAtlantinex.eventsshop', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {

        $request->validate([
            'method' => 'required',
            'amount' => 'numeric|required',
            'selectedTickets'
        ]);

        $method = 'pay_with_' . $request->method;
        $status = $this->$method($request);
        if ($status['status'] == 1) {
            $user = Auth::user();
            $event = Event::find($request->event_id);
            $tickets = $event->available_tickets;
            $selectedTickets = $request->selectedTickets;
            $creatingTickets = [];
            $EPs = 0;


            $totalAmount = 0;

            foreach ($selectedTickets as $selectedTicket) {


                for ($i = 0; $i < $selectedTicket['quantity']; $i++) {

                    $ticketToCreate = [
                        'user_id' => $user->id,
                        'event_id' => $event->id,
                        'price' => $tickets[$selectedTicket['id']]['price'],
                        'name' => $tickets[$selectedTicket['id']]['name'],
                    ];

                    if(isset($tickets[$selectedTicket['id']]['description'])){
                        $ticketToCreate['description'] = $tickets[$selectedTicket['id']]['description'];
                    }

                    $ticket =  Ticket::create($ticketToCreate);

                    $ticket->sendPurchaseConfirmation();

                    $ticket->emitInvoice($user, $request->method);
                }


                $totalAmount += $tickets[$selectedTicket['id']]['price'] * $selectedTicket['quantity'];

                $EPs += ($tickets[$selectedTicket['id']]['price'] * $selectedTicket['quantity']) / 100 * $tickets[$selectedTicket['id']]['eps'];
            }



            if ($totalAmount > 0) {
                $user->epoperations()->create([
                    'value' => $EPs,
                    'epoperationable_id' => $event->id,
                    'epoperationable_type' => 'App\Models\Event',
                    'description' => 'Purchased tickets for ' . number_format($totalAmount, 2) . '€ for event ' . $event->id,
                    'user_role' => $user->roles->first()->name,
                    'ep_type' => 'ticket_purchase'
                ]);
            }
        }
        return $status;
    }

    public function storeFree(Request $request)
    {
        $request->validate([
            'amount' => 'numeric|required',
            'selectedTickets'
        ]);

        $user = Auth::user();
        $event = Event::find($request->event_id);
        $tickets = $event->available_tickets;
        $selectedTickets = $request->selectedTickets;
        $creatingTickets = [];
        foreach ($selectedTickets as $selectedTicket) {
            for ($i = 0; $i < $selectedTicket['quantity']; $i++) {
                $ticketToCreate = [
                    'user_id' => $user->id,
                    'event_id' => $event->id,
                    'name' => $tickets[$selectedTicket['id']]['name'],
                ];

                if(isset($tickets[$selectedTicket['id']]['description'])){
                    $ticketToCreate['description'] = $tickets[$selectedTicket['id']]['description'];
                }else{
                    $ticketToCreate['description'] = '';
                }
                $ticket = Ticket::create($ticketToCreate);

                $ticket->sendPurchaseConfirmation();
            }
        }

        return ['status' => 1];
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $event = Event::findOrFail($id);
        $data = [];
        $user = Auth::user();

        $data['client_secret_intent'] = $user->createSetupIntent();
        $data['stripe_customer_id'] = $user->createOrGetStripeCustomer();
        $data['user'] = $user;
        $data['payment_methods'] = collect($user->paymentMethods());
        $data['event'] = $event;

        return view('Atlantinex.MyAtlantinex.eventsshop_show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function pay_with_wallet(Request $request)
    {
        $status = 0;
        if (Auth::user()->wallet_balance >= $request->amount) {
            Auth::user()->walletoperations()->create([
                'value' => $request->amount * -1,
                'receipt' => 'You bought ' . $request->eps . ' EP granting vouchers for &euro; ' . number_format($request->amount, 2, ',', '')
            ]);
            $status = 1;
        } else {
            //
        }
        return ['status' => $status];
    }

    public function pay_with_stored_credit_card(Request $request)
    {
        $user = Auth::user();
        $status = 0;
        try {
            $stripeCharge = $user->charge($request->amount * 100, $request->payment_method_id);
            $status = 1;
        } catch (Exception $e) {
            //
        }
        return ['status'  => $status];
    }

    public function pay_with_pay_with_new_card(Request $request)
    {
        return $this->pay_with_stored_credit_card($request);
    }
}

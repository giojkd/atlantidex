<?php

namespace App\Http\Controllers\Atlantinex\MyAtlantinex;

use Illuminate\Http\Request;
use \App\Http\Controllers\Controller;
use Auth;
use Backpack\Settings\app\Models\Setting;
use Str;

class ActivationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $role = Str::lower($user->roles()->first()->name);
        $data = [];
        $data['user'] = $user;
        $data['steps'] = $user->getActivationSteps();
        $introIndex = (!$user->was_active) ? Setting::get($role . '_activation_intro') : Setting::get($role . '_activation_intro_was_active');
        $data['intro'] = $introIndex;
        return view('Atlantinex.Activation.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */



    public function store(Request $request)
    {
        //

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function becomeMarketerConfirmation(){
        if (Auth::check()) {

            $user = Auth::user();
            $user->active = 0;
            $user->save();
            $user->syncRoles(['Networker']);

            return redirect(Route('dashboard'));
        } else {
        }
        #return view('Atlantinex.Activation.become_marketer_confirmation');
    }

    public function becomeMarketerConfirm(Request $request){
        if(Auth::check()){
            Auth::user()->syncRoles(['Networker']);
            return redirect(Route('dashboard'));
        }else{

        }
    }

}

<?php

namespace App\Http\Controllers\Atlantinex\MyAtlantinex;

use Illuminate\Http\Request;
use \App\Http\Controllers\Controller;
use App\Models\Epoperation;
use App\Models\Epvoucher;
use App\Models\Invoicerow;
use App\Models\Voucherpurchase;
use App\Traits\Invoicerowable;
use Auth;

class VouchersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {


        $data = [];

        if ($request->has('status') && $request->has('message')) {
            $data['status'] = $request->status;
            $data['message'] = $request->message;
        }

        $user = Auth::user();

        $data['client_secret_intent'] = $user->createSetupIntent();
        $data['stripe_customer_id'] = $user->createOrGetStripeCustomer();
        $data['user'] = $user;
        $data['payment_methods'] = collect($user->paymentMethods());
        $data['vouchers'] = $user->vouchers->keyBy('eps');

        $data['voucherPurchases'] = $user->voucherpurchases;
        $data['voucherOperations'] = $user->voucheroperations;

        #dd($data);

        $data['eps_interval'] = [1, 100];
        return view('Atlantinex.MyAtlantinex.vouchers', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */



    public function store(Request $request)
    {

        $user = Auth::user();

        $thisCategoryEpspruchasedlastYeat = $user->getVoucherAmountByEPsPurchasedLastYear($request->eps) + $request->amount;

        #return ['purchased_last_year' => $thisCategoryEpspruchasedlastYeat];

        if ($thisCategoryEpspruchasedlastYeat > (int)env('MAXIMUM_AMOUNT_EPS_VOUCHERS_PER_CATEGORY_PURCHASEABLE')) {
            return ['status' => 0, 'message' => 'Hai già acquistato ' . number_format($thisCategoryEpspruchasedlastYeat, 2) . ' in questa categoria'];
        }

        $totalVouchersPurchasedThisYear = $user->getRemainingPurchaseableAmountCurrentYear()+$request->amount;

        if ($totalVouchersPurchasedThisYear > env('MAXIMUM_AMOUNT_EPS_VOUCHERS_PER_YEAR_PURCHASEABLE')) {
            return ['status' => 0, 'message' => 'Hai già acquistato voucher per ' . number_format($totalVouchersPurchasedThisYear, 2) . ' con questo acquisto superi il limite di ' . number_format(env('MAXIMUM_AMOUNT_EPS_VOUCHERS_PER_YEAR_PURCHASEABLE'), 2)];
        }

        $method = 'pay_with_' . $request->method;
        $status = $this->$method($request);


        if ($status['status']) {



            $epvoucher = Epvoucher::whereEps($request->eps)->first();
            $epvoucher->emitInvoice($user, $request->method, $request->amount);




            $user->total_spendings += $request->amount;
            $user->save();

            $user->setVoucherAmount($request->eps, $request->amount);

            $user->checkIfHasPurchasedMinVouchers();

            $user->voucherpurchases()->create([
                'amount' => $request->amount,
                'eps' => $request->eps
            ]);

            $EPs = $request->amount / 100 * $request->eps;

            #create epoperation and epplusoperations ########################################################################################

            if($user->hasRole('Networker')){
                if($user->split_eps){
                    $user->epoperations()->create([
                        'value' => $EPs/2,
                        'description' => 'Has purchased ' . number_format($request->amount, 2) . '€ in voucher per ' . $request->eps . 'EPs category purchasing',
                        'ep_type' => 'voucher_purchase',
                        'is_careerable' => 0,
                        'user_role' => $user->roles->first()->name
                    ]);
                    $user->epplusoperations()->create([
                        'value' => $EPs / 2,
                        'epplusoperationable_type' => 'App\Models\Voucher',
                        'epplusoperationable_id' => $request->eps,
                        'epplus_type' => 'voucher_purchase',
                        'description' => 'Has purchased ' . number_format($request->amount, 2) . '€ in voucher per ' . $request->eps . 'EPs category purchasing',
                        'is_careerable' => 1,
                        'sign' => 1
                    ]);
                }else{
                    $user->epoperations()->create([
                        'value' => $EPs,
                        'description' => 'Has purchased ' . number_format($request->amount, 2) . '€ in voucher per ' . $request->eps . 'EPs category purchasing',
                        'ep_type' => 'voucher_purchase',
                        'is_careerable' => 0,
                        'user_role' => $user->roles->first()->name
                    ]);
                }
            }

            if($user->hasAnyRole(['Customer', 'Company']) ){
                $user->epplusoperations()->create([
                    'value' => $EPs/2 ,
                    'epplusoperationable_type' => 'App\Models\Voucher',
                    'epplusoperationable_id' => $request->eps,
                    'epplus_type' => 'voucher_purchase',
                    'description' => 'Has purchased ' . number_format($request->amount, 2) . '€ in voucher per ' . $request->eps . 'EPs category purchasing',
                    'is_careerable' => 1,
                    'sign' => 1
                ]);
                $user->epoperations()->create([
                    'value' => $EPs/2,
                    'description' => 'Has purchased ' . number_format($request->amount, 2) . '€ in voucher per ' . $request->eps . 'EPs category purchasing',
                    'ep_type' => 'voucher_purchase',
                    'is_careerable' => 0,
                    'user_role' => $user->roles->first()->name
                ]);
            }

            ########################################################################################



            $sponsor = $user->sponsor;

            #if (!is_null($sponsor) && !$sponsor->hasRole('Customer') && !$user->isActive()){ #MISSING IS ACTIVE changed 2/11/2020
            if (!is_null($sponsor) && !$sponsor->hasRole('Customer')){ #MISSING IS ACTIVE
                $pack = $sponsor->pack;
                if(!is_null($pack)){
                    $sponsor->commissionoperations()->create([
                        'value' => $request->amount * $pack->direct_commission / 100,
                        'description' => 'User '.$user->full_name.' (' . $user->id . ') purchased ' . number_format($request->amount, 2) . '€ in voucher per ' . $request->eps . 'EPs category purchasing',
                        'commission_type' => 'voucher_purchase',
                    ]);
                }
            }
        }

        return $status;
    }




    public function pay_with_wallet(Request $request)
    {
        $status = 0;
        if (Auth::user()->wallet_balance >= $request->amount) {
            Auth::user()->walletoperations()->create([
                'value' => $request->amount * -1,
                'receipt' => 'You bought ' . $request->eps . ' EP granting vouchers for &euro; ' . number_format($request->amount, 2, ',', '')
            ]);
            $status = 1;
        } else {
            //
        }
        return ['status' => $status];
    }

    public function pay_with_stored_credit_card(Request $request)
    {
        $user = Auth::user();
        $status = 0;
        try {
            $stripeCharge = $user->charge($request->amount * 100, $request->payment_method_id);
            $status = 1;
        } catch (Exception $e) {
            //
        }
        return ['status'  => $status];
    }

    public function pay_with_pay_with_new_card(Request $request)
    {
        return $this->pay_with_stored_credit_card($request);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

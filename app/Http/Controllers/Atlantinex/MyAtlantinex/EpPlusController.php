<?php

namespace App\Http\Controllers\Atlantinex\MyAtlantinex;

use Illuminate\Http\Request;
use \App\Http\Controllers\Controller;
use Auth;
use Backpack\Settings\app\Models\Setting;
use Str;
use App\Models\Otp;

class EpPlusController extends Controller
{
    public function list(Request $request){
        $user = Auth::user();
        $data = [
            'operations' => $user->epplusoperations()->orderBy('id','DESC')->get(),
            'payouts' => $user->eppluspayouts,
            'user' => $user
        ];

        return view('Atlantinex.MyAtlantinex.ep_plus',$data);
    }

    public function payoutOtp(Request $request){

        $user = Auth::user();
        $user->otps()->update(['status' => 1]);
        $sendingStatus = Otp::generate(1, 'otp_payout_request', $user->phone, $user);
        return $sendingStatus;

    }

    public function payoutConfirm(Request $request){

        #dd($request->all());

        $user = Auth::user();

        $request->validate([
            'otp' => 'required',
            'value' => 'required|numeric'
        ]);

        $otp = Otp::where('user_id', $user->id)
            ->where('otpable_type', 'otp_payout_request')
            ->where('otpable_id', 1)
            ->where('attempts', '<', 2)
            ->where('status', 0)
            ->orderBy('id', 'DESC')
            ->first();

        if (!is_null($otp)) {

            $return['message'] = 'Has usable otp but the submitted code is wrong';

            if ($otp->code == $request->otp) {
                $return['status'] = 1;

                $otp->status = 1;
                $otp->attempts++;
                $otp->save();
            } else {
                $return['status'] = 0;

                $otp->attempts++;
                $otp->save();
                $return['message'] = ($otp->attempts < 2) ? 'Hai ancora ' . (2 - $otp->attempts) . ' tentativi'  : 'Hai esaurito i tentativi richiedi un altro codice.';
                return $return;
            }
        } else {
            $return['message'] = 'Hai esaurito i tentativi richiedi un altro codice';
            $return['status'] = 0;
            return $return;
        }

        /*

        if ($request->value > $user->wallet_balance) {
            $return['message'] = 'La tua richiesta non è coperta dal wallet';
            $return['status'] = 0;
            return $return;
        }

        */

        $epPlusBalance = $user->epplus_balance * env('EPPLUS_EUR_RATIO');

        if ($request->value > $epPlusBalance) {
            $return['message'] = 'La tua richiesta non è coperta dal wallet';
            $return['status'] = 0;
            return $return;
        }




        if ($request->value < ((float)env('MIN_EPS_BALANCE_FOR_PAYOUT') * (float)env('EPPLUS_EUR_RATIO'))) {
            $return['message'] = 'La tua richiesta deve essere superiore a € ' . ((float)env('MIN_EPS_BALANCE_FOR_PAYOUT') * (float)env('EPPLUS_EUR_RATIO'));
            $return['status'] = 0;
            return $return;
        }

        $payout = $user->eppluspayouts()->create([
            'value' => $request->value
        ]);

        $epsp = $request->value / env('EPPLUS_EUR_RATIO');

        $user->walletoperations()->create([
            'walletoperationable_id' => $payout->id,
            'walletoperationable_type' => 'App\\Models\\Payout',
            'value' => $request->value,
            'receipt' => 'Hai convertito '. $epsp .'EP+ in €' . $payout->value
        ]);

        $user->epplus_balance -= $epsp;

        $user->save();

        return ['status' => 1];
    }

}

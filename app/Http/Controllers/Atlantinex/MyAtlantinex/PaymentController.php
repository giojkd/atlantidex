<?php

namespace App\Http\Controllers\Atlantinex\MyAtlantinex;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use \App\Http\Controllers\Controller;
use App\Models\BackpackUser;
use Ixudra\Curl\Facades\Curl;
use Illuminate\Support\Facades\Redirect;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function validateIban($iban){

         $endPoint = 'https://haunted-pirate-92419.herokuapp.com/validate/'.$iban;
         return json_decode(Curl::to($endPoint)->get(),1);
     }

     public function setUserBankAccount(Request $request){

        #$regExpIban = '/^(?:(?:IT|SM)\d{2}[A-Z]\d{22}|CY\d{2}[A-Z]\d{23}|NL\d{2}[A-Z]{4}\d{10}|LV\d{2}[A-Z]{4}\d{13}|(?:BG|BH|GB|IE)\d{2}[A-Z]{4}\d{14}|GI\d{2}[A-Z]{4}\d{15}|RO\d{2}[A-Z]{4}\d{16}|KW\d{2}[A-Z]{4}\d{22}|MT\d{2}[A-Z]{4}\d{23}|NO\d{13}|(?:DK|FI|GL|FO)\d{16}|MK\d{17}|(?:AT|EE|KZ|LU|XK)\d{18}|(?:BA|HR|LI|CH|CR)\d{19}|(?:GE|DE|LT|ME|RS)\d{20}|IL\d{21}|(?:AD|CZ|ES|MD|SA)\d{22}|PT\d{23}|(?:BE|IS)\d{24}|(?:FR|MR|MC)\d{25}|(?:AL|DO|LB|PL)\d{26}|(?:AZ|HU)\d{27}|(?:GR|MU)\d{28})$/i';

        $request->validate([
            'bank' => 'required',
            'bank_account_holder_name' => 'required',
            'bank_account_iban' => ['required'],#,'regex:'. $regExpIban],
            'bank_account_bic' => 'required',
        ]);

        $ibanValidation = $this->validateIban($request->bank_account_iban);

        if(!$ibanValidation['valid']){
            return Redirect::back()->withErrors(['Il codice iban inserito non è valido.']);
        }







        $user = Auth::user();

        $user->bank_account = [
            'bank' => $request->bank,
            'bank_account_holder_name' => $request->bank_account_holder_name,
            'bank_account_iban' => $request->bank_account_iban,
            'bank_account_bic' => $request->bank_account_bic,
        ];

        $user->save();

        return back();

     }

    public function attachPaymentMethodToUser(Request $request){
        $user = Auth::user();
        $user->addPaymentMethod($request->payment_method);
        if ($request->ajax()) {
            return ['status' => 1];
        }
    }

    public function index()
    {
        //
        $data = [];

        $user = Auth::user();

        $data['client_secret_intent'] = $user->createSetupIntent();
        $data['stripe_customer_id'] = $user->createOrGetStripeCustomer();
        $data['user'] = $user;
        $data['payment_methods'] = collect($user->paymentMethods());

        #dd($data);

        return view('Atlantinex.MyAtlantinex.payment_data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $paymentMethod = Auth::user()->findPaymentMethod($id);
        $paymentMethod -> delete();
        return back();
    }
}

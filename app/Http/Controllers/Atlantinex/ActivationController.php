<?php

namespace App\Http\Controllers\Atlantinex;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use \App\Http\Controllers\Controller;
use App\Models\BackpackUser;
use Backpack\Settings\app\Models\Setting;
use Exception;
use Str;

class ActivationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getActivationAmount()
    {
        $user = Auth::user();
        return (is_null($user->parent_id)) ? 199 : 149;
    }

    public function index()
    {
        //

        $data = [];

        $user = Auth::user();

        $role = Str::lower($user->roles()->first()->name);



#        $introText = Settings

        $data['client_secret_intent'] = $user->createSetupIntent();
        $data['stripe_customer_id'] = $user->createOrGetStripeCustomer();
        $data['payment_methods'] = collect($user->paymentMethods());
        $data['user'] = $user;
        $data['intro'] = Setting::get($role.'_activation_intro');


        $data['price'] = $this->getActivationAmount();

        return view('Company.Activation.index', $data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        #return $request->all();
        $user = Auth::user();

        #        dd($request->payment_method);

        $user->addPaymentMethod($request->payment_method);

        $amount = $this->getActivationAmount();

        $status = 0;

        try {
            #$stripeCharge = $user->charge($request->amount * 100, $request->payment_method_id);
            #$stripeCharge = $user->charge($request->amount * 100, $request->payment_method_id);
            $stripeCharge = $user->charge($amount * 100, $request->payment_method);
            $user->has_purchased_pack = date('Y-m-d H:i:s');
            $user->save();
            $status = 1;
        } catch (Exception $e) {
            return $e;
        }
        return ['status'  => $status];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

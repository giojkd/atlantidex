<?php

namespace App\Http\Controllers\Atlantinex\MyBusiness;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use \App\Http\Controllers\Controller;
use App\Models\BackpackUser;
use App\Models\Epoperation;
use Str;

class StatsController extends Controller
{
    public function monthlyCareer(Request $request)
    {
        if(env('SUSPEND_CAREERS')){
            return view('admin_custom.suspended_page', ['title' => 'Stay Tuned!']);
        }


        $user = Auth::user();

        #$untilGivenDate = '2020-11-17';
        $untilGivenDate = date('Y-m-d');

        $months = $user->getProductionMonthsUntilGivenDate($untilGivenDate);

        $lastMonth = count($months)-1;

        $selectedMonth = ($request->has('month')) ? $request->month :  $lastMonth;
        $productionMonth = $months[$selectedMonth];

        $performances = $user->getPerformances($productionMonth[0], $productionMonth[1]);

        if($selectedMonth > 0){
            $previousMonth = $months[$selectedMonth-1];
            $previousMonthPerformances = $user->getPerformances($previousMonth[0], $previousMonth[1]);
        }

        #dd($performances);

        $lines = [];
        if (!is_null($performances['legs'])) {
            $legs = $performances['legs'];
            foreach ($legs as $index => $leg) {
                $lines[] = [
                    'label' => 'Linea ' . ($index + 1),
                    'totalNetworkEp' => (int)$performances['legTotals'][$leg->id],
                    'fiftyPercentRule' => (int)$performances['legContribution'][$leg->id],
                ];
            }
        }
        $lines = collect($lines)->sortByDesc('totalNetworkEp')->toArray();
        $data = [
            'form' => [
                'productionMonths' =>  $months
            ],
            'filters' => [
                'month' => $selectedMonth
            ],
            'lines' => $lines,
            'currentMonthLevel' => $performances['level']['index'],
            'totalNetworkEp' => $performances['allEps'],
            'fiftyPercentRule' => collect($performances['legContribution'])->sum(),
            'totalNetworkEpSum' => $performances['allEps'],
            'fiftyPercentRuleSum' => collect($performances['legContribution'])->sum(),
            'user' => Auth::user(),
            'personalEps' => $performances['personal_eps'],
            'bonusEps' => $performances['bonus_eps'],
            'currentMonth' => $productionMonth[3],
        ];

        if(isset($previousMonthPerformances)){
            $data['previousMonthLevel'] = $previousMonthPerformances['level']['index'];
            $data['previousMonth'] = $previousMonth[3];
        }

        return view('Atlantinex.MyBusiness.monthly_career', $data);
    }


    public function longRunCareer(Request $request)
    {
        if (env('SUSPEND_CAREERS')) {
            return view('admin_custom.suspended_page', ['title' => 'Stay Tuned!']);
        }


        $user = Auth::user();

        #$untilGivenDate = '2020-11-17';
        $untilGivenDate = date('Y-m-d');

        $months = $user->getProductionMonthsUntilGivenDate($untilGivenDate);

        $lastMonth = count($months) - 1;

        $selectedMonth = ($request->has('month')) ? $request->month :  $lastMonth;
        $productionMonth = $months[$selectedMonth];


        #$performances = $user->getPerformancesLongRun($selectedMonth);

        $performances = $user->careers()->whereMonthId($selectedMonth)->whereCareerType('long_run_career')->first()->career_data;

        #dd($months);

        if ($selectedMonth > 0) {

            $previousMonth = $months[$selectedMonth - 1];
            $lastMonthCareer = $user->careers()->whereMonthId($selectedMonth - 1)->whereCareerType('long_run_career')->first();
            $previousMonthPerformancesAllEps = $lastMonthCareer->total;
            $lastMonthCareer = $lastMonthCareer->career_data['index'];
        }

        $lines = [];

        $legCount = 1;



        foreach ($performances['legsTotal'] as $index => $legTotal) {
            $lines[] = [
                'label' => 'Linea ' . $legCount,
                'totalNetworkEp' => $legTotal,
                'fiftyPercentRun' =>  (int)$performances['legsContribution'][$index],
                'latestTotal' => (int)$performances['legsTotal'][$index],
            ];
            $legCount++;
        }



        $data = [
            'form' => [
                'productionMonths' =>  $months
            ],
            'filters' => [
                'month' => $selectedMonth
            ],
            'lines' => $lines,

            'selectedMonthLevel' => $performances['index'],
            'totalNetworkEp' => $performances['legsContributionTotal'],

            'fiftyPercentRule' => 400000,
            'totalNetworkEpSum' => $performances['legsTotalTotal'],
            'fiftyPercentRuleSum' => $performances['legsContributionTotal'],
            'user' => Auth::user(),

            'selectedMonth' => $productionMonth[3],
            'bonusEps' => $performances['bonus_eps'],
            'bonusEpsPotential' => $performances['bonus_eps_potential'],
            'personalEps' => $performances['personal_eps']
        ];

        if (isset($previousMonthPerformancesAllEps)) {
            $data['previousMonthLevel'] = $lastMonthCareer;
            $data['previousMonth'] = $previousMonth[3];
            #$data['previousMonthTotalNetworkEp'] = $previousMonthPerformances['legsContributionTotal'];
            $data['previousMonthTotalNetworkEp'] = $previousMonthPerformancesAllEps;

        }

        return view('Atlantinex.MyBusiness.long_run_career', $data);

    }


    public function directCommission(Request $request){
        $data = [];
        $user = Auth::user();
        $untilGivenDate = date('Y-m-d');
        $months = $user->getProductionMonthsUntilGivenDate($untilGivenDate);
        $lastMonth = count($months) - 1;
        $selectedMonth = ($request->has('month')) ? $request->month :  $lastMonth;
        #$data['commissions'] = $user->commissionoperations()->whereNotIn('commission_type',['voucher_purchase','revenue_commission'])->where('value','>',0)->get();
        /*$data['commissions'] = $user->commissionoperations()->whereNull('commission_type')->orWhere(function($query){
            $query->whereNotIn('commission_type', ['voucher_purchase', 'revenue_commission', 'company_affiliation']);
        })->where('value','>',0)->get();*/

        $data = [
            'form' => [
                'productionMonths' =>  $months
            ],
            'filters' => [
                'month' => $selectedMonth
            ],
        ];

        $productionMonth = $months[$selectedMonth];


        $from = date('Y-m-d 00:00:00',strtotime($productionMonth[0]));
        $to = date('Y-m-d 00:00:00', strtotime($productionMonth[1].'+1days'));

        $data['commissions'] = $user
        ->commissionoperations()
        ->whereBetween('created_at',[$from,$to])
        ->where(function($query){
            $query->whereNull('commission_type')->orWhere('commission_type','direct_commission');
        })
        ->where('value', '>', 0)->get();

        return view('Atlantinex.MyBusiness.direct_commission',$data);

    }

    public function myBenefits(Request $request){

        $user = Auth::user();
        $untilGivenDate = date('Y-m-d');

        $months = collect($user->getProductionMonthsUntilGivenDate($untilGivenDate))->slice(0, -1);
        $lastMonth = count($months) - 1;

        $selectedMonth = ($request->has('month')) ? $request->month :  $lastMonth;



        if ($user->usercompensations()->whereMonthId($selectedMonth)->first() != null) {
            $data = $user->usercompensations()->whereMonthId($selectedMonth)->first()->toArray();
        }

        $data['compensations_count'] = $user->usercompensations->count();

        $data['production_month'] = $months[$selectedMonth];

        $data['formAction'] = Route('MyBusiness.myBenefits');

        $data['form']['productionMonths'] = $months;
        $data['filters']['month'] = $selectedMonth;


        return view('Atlantinex.MyBusiness.my_benefits', $data);
    }

    public function myBenefitsDetails(Request $request, $type){
        $method = Str::camel('my-benefits-details-'.$type);
        return $this->$method($request);
    }

    public function myBenefitsDetailsMerchantBonus($request){
        $user = Auth::user();
        $descendants =  $user->getAffiliatedCompanies();
        $month = $user->getProductionMonthByDate('2020-11-17');
        $from = date('Y-m-d ', strtotime($month[0])) . ' 00:00:00';
        $to = date('Y-m-d ', strtotime($month[1])) . ' 23:59:59';
        $dataReturn = [];
        $commissionOperations = $user->commissionoperations()->whereBetween('created_at', [$from, $to])->where('commission_type', 'company_affiliation')->get();
        $commissionOperations->each(function ($operation, $key) use (&$dataReturn) {
            $dataReturn['rows'][] = ['value' => $operation->value, 'line' => $operation->description];
        });
        return view('Atlantinex.MyBusiness.my_benefits_details_table', collect($dataReturn));
    }

    public function myBenefitsDetailsMonthlyCareerCommission($request){
        $user = Auth::user();

        $compensation = $user->usercompensations()->whereMonthId($request->month_id)->first();

        $rows = $compensation->compensation_details->monthlyCareerCommissionDetailed;
        $dataReturn = [];
        if(count($rows) > 0 ){
            foreach($rows as $row){
                #$dataReturn['rows'][] = ['value' => $row['increment'], 'line' => $row['line']];
                $dataReturn['rows'][] = $row;
            }
        }
        return view('Atlantinex.MyBusiness.my_benefits_details_table', collect($dataReturn));

    }

    public function myBenefitsDetailsDirectCommission($request){
        $user = Auth::user();
        $month = $user->getProductionMonths()[$request->month_id];

        $from = date('Y-m-d ', strtotime($month[0])) . ' 00:00:00';
        $to = date('Y-m-d ', strtotime($month[1])) . ' 23:59:59';

        $directCommissionOperations = $user->commissionoperations()->whereBetween('created_at', [$from, $to])->where(function ($query) {
            $query->whereIn('commission_type', ['voucher_purchase'])->orWhereNull('commission_type');
        })->get();

        $dataReturn = [];
        $directCommissionOperations->each(function($operation,$key) use(&$dataReturn){
            $dataReturn['rows'][] = ['value' => $operation->value, 'line' => $operation->description];
        });
        return view('Atlantinex.MyBusiness.my_benefits_details_table', collect($dataReturn));
    }

    public function myBenefitsDetailsMerchantBoosterBonus($request){
        $user = Auth::user();
        $descendants =  $user->getAffiliatedCompanies();
        $month = $user->getProductionMonths()[$request->month_id];
        $from = date('Y-m-d ', strtotime($month[0])) . ' 00:00:00';
        $to = date('Y-m-d ', strtotime($month[1])) . ' 23:59:59';
        $dataReturn = [];
        $commissionOperations = $user->commissionoperations()->whereBetween('created_at', [$from, $to])->where('commission_type', 'company_booster_affiliation')->get();
        $commissionOperations->each(function ($operation, $key) use (&$dataReturn) {
            $dataReturn['rows'][] = ['value' => $operation->value, 'line' => $operation->description];
        });
        return view('Atlantinex.MyBusiness.my_benefits_details_table', collect($dataReturn));
    }

    public function myBenefitsDetailsRevenueBonus($request){
        $user = Auth::user();
        $month = $user->getProductionMonths()[$request->month_id];
        $from = date('Y-m-d ', strtotime($month[0])) . ' 00:00:00';
        $to = date('Y-m-d ', strtotime($month[1])) . ' 23:59:59';
        $revenueBonusOperations = $user->commissionoperations()->whereIn('commission_type', ['revenue_commission', 'revenue_bonus'])->whereBetween('created_at', [$from, $to])->get();
        $revenueBonusOperations->each(function ($operation, $key) use (&$dataReturn) {
            $dataReturn['rows'][] = ['value' => $operation->value, 'line' => $operation->description];
        });
        return view('Atlantinex.MyBusiness.my_benefits_details_table', collect($dataReturn));
    }

}

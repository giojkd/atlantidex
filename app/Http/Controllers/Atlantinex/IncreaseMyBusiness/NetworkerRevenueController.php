<?php

namespace App\Http\Controllers\Atlantinex\IncreaseMyBusiness;

use Illuminate\Http\Request;
use \App\Http\Controllers\Controller;
use App\Models\BackpackUser;
use App\Models\Merchant;
use App\Scopes\UserScope;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class NetworkerRevenueController extends Controller
{
    public function index(Request $request,$id)
    {
        $user = Auth::user();
        $customer = BackpackUser::withoutGlobalScope(SelfuserScope::class)->findOrFail($id);
        if (!$customer->isDescendantOf($user))
            abort(403);

        ##########################################################################################

        $productionMonths = collect($user->getProductionMonthsUntilGivenDate(date('ymd')))->reverse();
        $selectedMonth = ($request->has('month')) ? $request->month :  $productionMonths->first()[4];

        $selectedProductionMonth = $productionMonths[$selectedMonth];

        $purchases = $customer->invoicerows()->whereBetween('created_at',[$selectedProductionMonth[0], $selectedProductionMonth[1]])->get();

        $data = [
            'form' => [
                'productionMonths' => $productionMonths,
            ],
            'filters' => [
                'month' => $selectedMonth
            ],
            'user' => $user,
            'customer' => $customer,
            'purchases' => $purchases
        ];

        //dd($data['merchant']);
        return view('Atlantinex.IncreaseMyBusiness.networker_revenue',$data);
        //
    }

}

<?php

namespace App\Http\Controllers\Atlantinex\IncreaseMyBusiness;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use \App\Http\Controllers\Controller;
use App\Models\BackpackUser;
use App\Models\User;
use Exception;
use Illuminate\Support\Collection;
use Spatie\Permission\Traits\HasRoles;
use Str;


class MyCompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user = backpack_user();
        $data = [];
        $data['user'] = $user;
        $data['companies'] = new Collection();
        if (!$user->hasRole('Company')) {
            $users  = $user->children;
            $userfiltered = $users->filter(function($value, $key){
                return $value->hasRole('Company');
            });
            foreach($userfiltered as $item) {
                if ($item->merchants) {
                    foreach($item->merchants as $merchant)
                    $data['companies']->push($merchant);
                }
            }
        } else {
            foreach($user->merchants as $merchant) {
                $data['companies']->push($merchant);
            }
        }
        //dd($data['companies']);
        return view('Atlantinex.IncreaseMyBusiness.my_companies',$data);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



}

<?php

namespace App\Http\Controllers\Atlantinex\IncreaseMyBusiness;

use Illuminate\Http\Request;
use \App\Http\Controllers\Controller;
use App\Models\BackpackUser;
use App\Models\Merchant;
use App\Scopes\UserScope;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Traits\HasRoles;

class CompanyRevenueController extends Controller
{
    public function index(Request $request, $id)
    {

        $user = backpack_user();
        $data = [];
        $data['user'] = $user;
        $firstMonth = env('FIRST_MONTH');
        $lastMonth = date('Ym') . '01';
        if ($request->has('month')) {
            $from = date('Ym', strtotime($request->month)) . '01';
            $to = date('Ymt', strtotime($request->month));
        } else {
            $from = date('Ym', strtotime($lastMonth)) . '01';
            $to = date('Ymt', strtotime($lastMonth));
        }
        $previousFrom = date('Ymd', strtotime($from . '-1months'));
        $previousTo = date('Ymd', strtotime($to . '-1months'));
        $month = $firstMonth;
        $months = [];
        while ($month < $lastMonth) {
            $months[] = ['value' => $month, 'label' => date('F Y', strtotime($month))];
            $month = date('Ym', strtotime('+1 months', strtotime($month))) . '01';
        }
        $months[] = ['value' => $lastMonth, 'label' => date('F Y', strtotime($lastMonth))];

        $selectedMonth = ($request->has('month')) ? $request->month :  $lastMonth;

        $data = [
            'form' => [
                'productionMonths' =>  $months
            ],
            'filters' => [
                'month' => $selectedMonth
            ],
        ];
        $merchant = Merchant::withoutGlobalScope(UserScope::class)->findOrFail($id);
        if (!$user->hasRole('Company')) {
            if ($merchant->user->parent_id != $user->id)
                abort(403);
        } else {
            if ($merchant->user->id != $user->id)
                abort(403);
        }
        $data['merchant'] = $merchant;

        //dd($data['merchant']);
        return view('Atlantinex.IncreaseMyBusiness.company_revenue',$data);
        //
    }

    public function company(Request $request, $id)
    {

        $user = backpack_user();
        $data = [];
        $data['user'] = $user;

        $start = date('Y-m-d', strtotime("first day of this month"));
        $end = date('Y-m-d', strtotime("last day of this month"));
        if ($request->month) {
            $start = date('Y-m-01', strtotime($request->month));
            $end = date('Y-m-t', strtotime($request->month));
        }



        $networker = Merchant::findOrFail($id);
        $data['networker'] = $networker;
        $range = new Collection();
        $range['start'] = $start;
        $range['end'] = $end;
        $data['range'] = $range;
        $data['revenues'] = $this->getRevenues($id, $start, $end);
        //if(!$networker->isDescendantOf($user) && !$networker === $user)
            //abort(403);
        if($request->ajax()){
                return response()->json($data);
        }
        //dd($data['merchant']);
        //return view('Atlantinex.IncreaseMyBusiness.networker_revenue',$data);
        //
    }

    public function getRevenues($id, $start, $end) {
        $value = new Collection();
        $value['npurchase'] = rand(0,50000);
        return $value;
    }
}

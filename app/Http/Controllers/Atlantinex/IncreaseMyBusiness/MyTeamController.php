<?php

namespace App\Http\Controllers\Atlantinex\IncreaseMyBusiness;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use \App\Http\Controllers\Controller;
use App\Models\BackpackUser;
use App\Models\User;
use Exception;
use Illuminate\Support\Collection;
use Spatie\Permission\Traits\HasRoles;
use App\Traits\Helpers;
use stdClass;
use Str;


class MyTeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use Helpers;

    public function index(Request $request, $id)
    {

        if (env('SUSPEND_CAREERS')) {
            return view('admin_custom.suspended_page', ['title' => 'Stay Tuned!']);
        }

        $selfuser = backpack_user();
        $user = BackpackUser::findOrFail($id);
        $data = [];

        $months = $user->getProductionMonthsUntilGivenDate();

        $lastMonth = count($months) - 1;

        $selectedMonth = ($request->has('month')) ? $request->month :  $lastMonth;
        $productionMonth = $months[$selectedMonth];

        $performances = $user->getPerformances($productionMonth[0], $productionMonth[1]);

        if ($selectedMonth > 0) {
            $previousMonth = $months[$selectedMonth - 1];
            $previousMonthPerformances = $user->getPerformances($previousMonth[0], $previousMonth[1]);
        }

        $lines = [];
        if (!is_null($performances['legs'])) {
            $legs = $performances['legs'];
            foreach ($legs as $index => $leg) {
                $lines[] = [
                    'label' => 'Linea ' . ($index + 1),
                    'totalNetworkEp' => (int)$performances['legTotals'][$leg->id],
                    'fiftyPercentRule' => (int)$performances['legContribution'][$leg->id],
                ];
            }
        }

        $performanceslr = $user->getPerformancesLongRun($selectedMonth);

        $lineslr = [];

        $legCount = 1;

        if ($selectedMonth > 0) {
            $previousMonthPerformanceslr = $user->getPerformancesLongRun(($selectedMonth-1));
        }

        foreach ($performanceslr['legsContribution'] as $index => $legContribution) {
            $lineslr[] = [
                'label' => 'Linea ' . $legCount,
                'totalNetworkEp' => $performanceslr['legsTotal'][$index],
                'fiftyPercentRun' => $legContribution,
                'latestTotal' => $performanceslr['legsTotal'][$index],
            ];
            $legCount++;
        }

        $data = [
            'form' => [
                'productionMonths' =>  $months
            ],
            'filters' => [
                'month' => $selectedMonth
            ],
            'lines' => $lines,

            'currentMonthLevel' => $performances['level']['index'],
            'totalNetworkEp' => $performances['allEps'],
            'fiftyPercentRule' => collect($performances['legContribution'])->sum(),
            'totalNetworkEpSum' => $performances['allEps'],
            'fiftyPercentRuleSum' => collect($performances['legContribution'])->sum(),
            'user' => Auth::user(),
            'personalEps' => $performances['personal_eps'],
            'bonusEps' => $performances['bonus_eps'],

            'currentMonth' => $productionMonth[3],

            'lineslr' => $lineslr,

            'selectedMonthLevellr' => $performanceslr['index'],
            'totalNetworkEplr' => $performanceslr['legsContributionTotal'],

            'fiftyPercentRulelr' => 400000,
            'totalNetworkEpSumlr' => $performanceslr['legsTotalTotal'],
            'fiftyPercentRuleSumlr' => $performanceslr['legsContributionTotal'],

            'bonusEpslr' => $performanceslr['bonus_eps'],
            'personalEpslr' => $performanceslr['personal_eps']
        ];

        #dd($data);

        if($selectedMonth > 0){
            $data['previousMonthTotalNetworkEplr'] = $previousMonthPerformanceslr['legsContributionTotal'];
            $data['previousMonth'] = $previousMonthPerformanceslr['index'];
            $data['previousMonth'] = date('F Y', strtotime($selectedMonth));
            $data['previousMonthLevel'] = $previousMonthPerformances['level']['index'];
        }


        $parent = $user->parent;
        $isroot = $user->id == $selfuser->id;
        if (!$user->isDescendantOf($selfuser) && !$isroot) {
            Abort(403);
        }
        $date1 = date('2020-01-01');

        $data['user'] = $user;
        $directs = $user->children->whereBetween('created_at', [$date1, $productionMonth[1]]);
        foreach ($directs as $value) {
            $value->perforamces = $value->getPerformances($productionMonth[0], $productionMonth[1]);
        }
        $data['entries'] = $directs;
        $data['countentries'] = count($directs);
        $data['isroot'] = $isroot;
        $data['parent'] = $parent;
        $data['rootuser'] = $selfuser;
        return view('Atlantinex.IncreaseMyBusiness.network_revenue',$data);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



}

<?php

namespace App\Http\Controllers\Atlantinex\IncreaseMyBusiness;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use \App\Http\Controllers\Controller;
use App\Models\BackpackUser;
use App\Models\Invitationlink;
use Exception;
use Str;


class InvitationlinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user = Auth::user();

        $data = [];

        $linkTypes = [
            'company' => 'Invito ad Azienda',
            'networker' => 'Invito a Consumatore',
            'company_booster' => 'Invito Booster ad Azienda ',
        ];

        $il = $user->invitationlinks()->where('is_redeemed',0)->orderBy('id','DESC')->get();

        if($il->count() > 0){
            foreach($il as $link){
                $invitationLinks[] = [
                    'id' => $link->id,
                    'created_at' => $link->created_at->format('d/m/Y'),
                    'expires_at' => $link->expires_at->format('d/m/Y'),
                    'is_redeemed' => ($link->is_redeemed) ? 'Sì' : 'No',
                    'link_type' =>  $linkTypes[$link->link_type],#($link->link_type == 'company') ? 'Invito ad azienda' : 'Invito a networker',
                    'code' => $link->getCode($link->link_type),
                ];
            }
            $data['invitationLinks'] = $invitationLinks;
        }

        $data['user'] = $user;



        return view('Atlantinex.IncreaseMyBusiness.invitation_links',$data);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $type = $request->data['link_type'];
        $user = Auth::user();

        if($type == 'company' && $user->company_invitation_links_count <= 0){
            return 'error';
        }

        if ($type == 'networker' && $user->networker_invitation_links_count <= 0) {
            return 'error';
        }

        if ($type == 'company_booster' && $user->company_booster_invitation_links_count <= 0) {
            return 'error';
        }

        $link = $user->invitationlinks()->create(
            [
                'link_type' => $type,
                'expires_at' => date('Y-m-d H:i',strtotime('+'.env(Str::upper($type). '_INVITATION_LINK_EXPIRATION_DAYS').'days')),
                'code' =>  Invitationlink::generateLink()
            ]
        );

        if ($type == 'company' && $user->company_invitation_links_count > 0) {
            $user->company_invitation_links_count--;
            $user->save();
        }

        if ($type == 'networker' && $user->networker_invitation_links_count > 0) {
            $user->networker_invitation_links_count--;
            $user->save();
        }



        if ($type == 'company_booster' && $user->company_booster_invitation_links_count > 0) {
            $user->company_booster_invitation_links_count--;
            $user->save();
        }

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



}

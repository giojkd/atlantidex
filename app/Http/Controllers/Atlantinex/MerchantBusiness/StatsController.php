<?php

namespace App\Http\Controllers\Atlantinex\MerchantBusiness;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use \App\Http\Controllers\Controller;
use App\Models\BackpackUser;
use App\Models\Invitationlink;
use Illuminate\Support\Collection;

class StatsController extends Controller
{
    public function affiliatedCompanies(Request $request){


        $user = backpack_user();


        $data = [];
        $data['user'] = $user;
        $data['companies'] = $user->getAffiliatedCompanies();


        return view('Atlantinex.MerchantBusiness.affiliated_companies',$data);
    }

    public function revenueBonus(Request $request){
        $user = backpack_user();

        list($productionMonth, $selectedMonth) = $this->getProductionMonth($request);

        $from = date('Y-m-d 00:00:00', strtotime($productionMonth[0]));
        $to = date('Y-m-d 00:00:00', strtotime($productionMonth[1].'+1days'));



        $data = [];
        $data['user'] = $user;
        $data['companies'] = new Collection();
        $data = [
            'form' => [
                'productionMonths' =>  $user->getProductionMonthsUntilGivenDate()
            ],
            'filters' => [
                'month' => $selectedMonth
            ],
        ];

        $data['companies'] = [];
        /*
        $user
                            ->getAffiliatedCompanies()
                            ->whereBetween('has_signed_agreement_human_readable', [$productionMonth[0], $productionMonth[1]]);
        */
        $data['formAction'] = route('MerchantBusiness.revenueBonus');


        $commissionOperationsGdo =
        $user
        ->commissionoperations()
        ->whereBetween('created_at', [$from, $to])
        ->where('commissionoperationable_type','App\Models\Gdovoucher')
        ->with(['commissionoperationable'])
        ->get()
        ->groupBy('commissionoperationable_id');

        if($commissionOperationsGdo->count() > 0){
            foreach($commissionOperationsGdo as $company_id => $operations){
                $data['gdo_companies'][$company_id] = [

                    'company_name' => $operations->first()->commissionoperationable->manufacturer->name,
                    'affiliated_at' => $operations->first()->commissionoperationable->manufacturer->created_at->format('d/m/Y'),
                    'total_commission' => $operations->sum('value')
                ];
            }
        }


        $data['commissionoperations'] = $user->commissionoperations()->whereIn('commission_type',['revenue_commission', 'revenue_bonus'])->whereBetween('created_at', [$from, $to])->get();
        #dd($data);

        return view('Atlantinex.MerchantBusiness.revenue_bonus',$data);
    }

    public function getProductionMonth($request){

        $user = backpack_user();
        $months = $user->getProductionMonthsUntilGivenDate();
        $lastMonth = count($months) - 1;
        $selectedMonth = ($request->has('month')) ? $request->month :  $lastMonth;
        $productionMonth = $months[$selectedMonth];
        return [$productionMonth,$selectedMonth];

    }

    public function merchantBonus(Request $request){
        $user = backpack_user();
        list($productionMonth,$selectedMonth) = $this->getProductionMonth($request);
        $data = [
            'form' => [
                'productionMonths' =>  $user->getProductionMonthsUntilGivenDate()
            ],
            'filters' => [
                'month' => $selectedMonth
            ],
        ];
        $data['companies'] =
            $user
            ->getAffiliatedCompanies()
            ->whereBetween('has_signed_agreement_human_readable',[$productionMonth[0], $productionMonth[1]])
            ->filter(function($company,$key){
                if (is_object($company->invitationlink))
                    return $company->invitationlink->link_type == 'company';
                return false;
            });
        $data['formAction'] = route('MerchantBusiness.merchantBonus');
        return view('Atlantinex.MerchantBusiness.merchant_bonus',$data);
    }

    public function merchantBoosterBonus(Request $request){
        $user = backpack_user();

        list($productionMonth, $selectedMonth) = $this->getProductionMonth($request);





        $data = [
            'form' => [
                'productionMonths' =>  $user->getProductionMonthsUntilGivenDate()
            ],
            'filters' => [
                'month' => $selectedMonth
            ],
        ];
        $data['companies'] =
            $user
            ->commissionoperations()
            ->whereBetween('created_at', [$productionMonth[0], $productionMonth[1]])
            ->where('commission_type', 'merchant_bonus')
            ->get();
        $data['formAction'] = route('MerchantBusiness.merchantBoosterBonus');

        //dd($data);

        return view('Atlantinex.MerchantBusiness.merchant_booster_bonus',$data);
    }

    public function merchantMarketingBonus(Request $request){
        $user = backpack_user();

        list($productionMonth, $selectedMonth) = $this->getProductionMonth($request);

        $data = [
            'form' => [
                'productionMonths' =>  $user->getProductionMonthsUntilGivenDate()
            ],
            'filters' => [
                'month' => $selectedMonth
            ],
        ];
        $data['formAction'] = route('MerchantBusiness.merchantMarketingBonus');
         $data['companies'] = collect([]);
        return view('Atlantinex.MerchantBusiness.merchant_marketing_bonus',$data);
    }



}

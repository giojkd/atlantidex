<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Operations\ImpersonateOperation;
use App\Http\Requests\UserRequest;
use App\Models\BackpackUser;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\PermissionManager\app\Http\Requests\UserStoreCrudRequest as StoreRequest;
use Backpack\PermissionManager\app\Http\Requests\UserUpdateCrudRequest as UpdateRequest;
use Illuminate\Support\Facades\Hash;
use Auth;

/**
 * Class UserCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class UserCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation { store as traitStore; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation { update as traitUpdate; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation;
    use \Backpack\ReviseOperation\ReviseOperation;
    use ImpersonateOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\BackpackUser');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/user');
        $this->crud->setEntityNameStrings('user', 'users');
        $this->crud->enableExportButtons();

        #$this->crud->addClause('active');
    }



    public function setupListOperation()
    {


        $this->crud->addFilter(
            [
                'type'  => 'date_range',
                'name'  => 'from_to',
                'label' => 'Date range'
            ],
            false,
            function ($value) {
                $dates = json_decode($value);
                $this->crud->addClause('where', 'created_at', '>=', $dates->from);
                $this->crud->addClause('where', 'created_at', '<=', $dates->to . ' 23:59:59');
            }
        );

        $this->crud->addFilter(
            [
                'type'  => 'simple',
                'name'  => 'active',
                'label' => 'Active'
            ],
            false,
            function ($value) {
                $this->crud->addClause('active');
            }
        );



        $this->crud->setColumns([

            [
                'name' => 'id',
                'label' => 'ID',
                'searchLogic' => function ($query, $column, $searchTerm) {
                    $query->whereIn('id', BackpackUser::search($searchTerm)->get()->pluck('id'));
                }
            ],

            [
                'name' => 'stripe_id',
                'label' => 'Stripe ID',
                'type' => 'text'
            ],

            [
                'name' => 'full_name',
                'label' => trans('backpack::crudcontroll.name'), // Table column heading
                'type' => 'text',

            ],
            [
                'name' => 'company_name',
                'label' => 'Company name', // Table column heading
                'type' => 'text',
                'limit' => 100
            ],
            [
                'name' => 'wallet_balance',
                'label' => 'Bilancio Wallet',
                'type' => 'number',
                'prefix' => '€',
            ],
            [
                'name' => 'email',
                'label' => 'Email', // Table column heading
                'type' => 'text',
            ],

          [
             'name' => 'active', // The db column name
             'label' => 'Active', // Table column heading
             'type' => 'check'
          ],
          [
            'name' => 'purchasedpacks',
            'label' => 'Packs',
            'type' => 'model_function',
            'function_name' => 'getPurchasedPacks'
          ],

          [
            'name' => 'code',
            'label' => trans('backpack::crudcontroll.code'), // Table column heading
            'type' => 'text',
          ],

            [
                'name' => 'parent_id',
                'label' => 'Sponsor',
                'type' => 'model_function',
                'function_name' => 'getSponsorFullName'



            ],

            [
                'name' => 'email_verified_at',
                'label' => 'Has verified email',
            ],

            [
                'name' => 'fiscal_code',
                'label' => 'Codice fiscale', // Table column heading
                'type' => 'text',
            ],
            [
                'name' => 'vat_number',
                'label' => 'Partita IVA', // Table column heading
                'type' => 'text',
            ],
            [
                'name' => 'date_of_birth',
                'label' => 'Data di nascita', // Table column heading
                'type' => 'date',
            ],

          [
            'name' => 'email',
            'label' => 'Email',
            'type' => 'email',
          ],
        [
            'name' => 'phone',
            'label' => 'Phone',
            'type' => 'text',
        ],
        [
            'name' => 'address_name',
            'label' => 'Indirizzo',
            'type' => 'text',
            'limit' => 166
        ],
          [ // 1-n relationship
            'label' => 'Country', // Table column heading
            'type' => 'select',
            'name' => 'country', // the method that defines the relationship in your Model
            'entity' => 'country', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => 'App\Models\Country', // foreign key model
          ],
          [ // n-n relationship (with pivot table)
            'label' => 'Roles', // Table column heading
            'type' => 'select_multiple',
            'name' => 'roles', // the method that defines the relationship in your Model
            'entity' => 'roles', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => config('permission.models.role'), // foreign key model
          ],
          [ // n-n relationship (with pivot table)
            'label' => 'Extra permissions', // Table column heading
            'type' => 'select_multiple',
            'name' => 'permissions', // the method that defines the relationship in your Model
            'entity' => 'permissions', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => config('permission.models.permission'), // foreign key model
          ],
            [
                'name' => 'min_commission',
                'label' => 'Commissione minima offline',
                'type' => 'number',
                'suffix' => '%'
            ],
            [
                'name' => 'voucher_recap',
                'label' => 'Voucher recap',
                'type' => 'text',
                'limit' => 200
            ]

        ]);

        // Role Filter
        $this->crud->addFilter([
          'name' => 'role',
          'type' => 'dropdown',
          'label' => trans('backpack::permissionmanager.role'),
        ],
        config('permission.models.role')::all()->pluck('name', 'id')->toArray(),
        function ($value) { // if the filter is active
          $this->crud->addClause('whereHas', 'roles', function ($query) use ($value) {
            $query->where('role_id', '=', $value);
          });
        });

        // Extra Permission Filter
        $this->crud->addFilter([
          'name' => 'permissions',
          'type' => 'select2',
          'label' => trans('backpack::permissionmanager.extra_permissions'),
        ],
        config('permission.models.permission')::all()->pluck('name', 'id')->toArray(),
        function ($value) { // if the filter is active
          $this->crud->addClause('whereHas', 'permissions', function ($query) use ($value) {
            $query->where('permission_id', '=', $value);
          });
        });

        $this->crud->addButtonFromView('line', 'setUserHasPurchasedMinVouchers', 'setUserHasPurchasedMinVouchers','end');
        $this->crud->addButtonFromView('line', 'setUserHasVerifiedEmail', 'setUserHasVerifiedEmail','end');
    }

    protected function addUserFields()
    {
        $this->crud->addFields([

          [
            'name' => 'active', // The db column name
            'label' => 'Active', // Table column heading
             'type' => 'checkbox'
          ],
          [
            'name' => 'name',
            'label' => 'Name',
            'type' => 'text',
          ],
          [
            'name' => 'surname',
            'label' => 'Surname',
            'type' => 'text',
          ],
          [
            'name' => 'code',
            'label' => trans('backpack::crudcontroll.code'), // Table column heading
            'type' => 'text',
            'attributes' => [
                  'readonly'=>'readonly',
            ], // change the HTML attributes of your input
          ],
          [
            'name' => 'email',
            'label' => 'Email',
            'type' => 'email',
          ],
        [
            'name' => 'phone',
            'label' => 'Phone',
            'type' => 'text',
        ],

          [
            'name' => 'password',
            'label' => trans('backpack::permissionmanager.password'),
            'type' => 'password',
          ],
          [
            'name' => 'password_confirmation',
            'label' => trans('backpack::permissionmanager.password_confirmation'),
            'type' => 'password',
          ],
          [ // 1-n relationship
              'label' => 'Country', // Table column heading
              'type' => 'select',
              'name'  => 'country_id', // Table column which is FK in your model
              'entity' => 'country', // the method that defines the relationship in your Model
              'attribute' => 'name', // foreign key attribute that is shown to user
              'model' => 'App\Models\Country', // foreign key model
          ],
          [
            // two interconnected entities
            'label' => trans('backpack::permissionmanager.user_role_permission'),
            'field_unique_name' => 'user_role_permission',
            'type' => 'checklist_dependency',
            'name' => ['roles', 'permissions'],
            'subfields' => [
              'primary' => [
                'label' => trans('backpack::permissionmanager.roles'),
                'name' => 'roles', // the method that defines the relationship in your Model
                'entity' => 'roles', // the method that defines the relationship in your Model
                'entity_secondary' => 'permissions', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model' => config('permission.models.role'), // foreign key model
                'pivot' => true, // on create&update, do you need to add/delete pivot table entries?]
                'number_columns' => 3, //can be 1,2,3,4,6
              ],
              'secondary' => [
                'label' => ucfirst(trans('backpack::permissionmanager.permission_singular')),
                'name' => 'permissions', // the method that defines the relationship in your Model
                'entity' => 'permissions', // the method that defines the relationship in your Model
                'entity_primary' => 'roles', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model' => config('permission.models.permission'), // foreign key model
                'pivot' => true, // on create&update, do you need to add/delete pivot table entries?]
                'number_columns' => 3, //can be 1,2,3,4,6
              ],
            ],
          ],
            /*[
                'name' => 'address',
                'label' => 'Indirizzo',
                'type'          => 'address_algolia',
                // optional
                'store_as_json' => true
            ],*/
          [
              'name' => 'company_booster_invitation_links_count',
              'label' => 'Inviti booster',
              'type' => 'number'
          ],
            [
                'name' => 'minimum_purchase_to_buy_starter_pack',
                'label' => 'Volume di acquisti minimo per acquistare lo starter pack',
                'type' => 'number'
            ],
            [
                'name' => 'revenue_bonus',
                'label' => 'Revenue bonus',
                'type' => 'number',
                'hint' => 'Se l\'utente è un merchant marketer la revenue bonus garantita dalle aziende da lui affiliate',
                'attributes' => ['step' => 'any']
            ],
            [
                'name' =>'parent_id',
                'label' => 'Sponsor',
                'type' => 'select2',
                'entity' => 'sponsor',
                'model' => BackpackUser::class,
                'attribute' => 'super_detailed_name',
                #'default' => 32,
                'options'   => (function ($query) {
                    return $query->orderBy('name', 'ASC')->get();
                }),
            ],
            [
                'name' => 'virtual_parent_id',
                'label' => 'Virtual Sponsor',
                'type' => 'select2',
                'entity' => 'virtualSponsor',
                'model' => BackpackUser::class,
                'attribute' => 'super_detailed_name',
                #'default' => 32,
                'options'   => (function ($query) {
                    return $query->orderBy('name', 'ASC')->get();
                }),
            ],
             [
                'name' => 'min_commission',
                'label' => 'Commissione minima',
                'type' => 'number',
                'attributes' => ['step'=>'any'],
                'suffix' => '%'
            ],
            [
                'name' => 'spendings_to_unlock_merchant_marketer_pack',
                'label' => 'Spendings to unlock merchant marketer pack',
                'type' => 'text',


            ],
            [   // Address
                'name' => 'address',
                'label' => trans('backpack::crudcontroll.address'),
                'type' => 'address_google',
                // optional
                'store_as_json' => true
            ],
            /*
            [
                'name'  => 'map-input', // do not change this
                'type'  => 'customGoogleMaps', // do not change this
                'label' => "Google Maps",
                'hint'  => 'Help text',
                'store_as_json' => true,
                'attributes' => [
                    'class' => 'form-control map-input', // do not change this, add more classes if needed
                ],
                #'view_namespace' => 'custom-google-maps-field-for-backpack::fields',
            ]*/
        ]);
    }

    public function setupCreateOperation()
    {
        $this->addUserFields();
        $this->crud->setValidation(StoreRequest::class);
    }

    public function setupUpdateOperation()
    {
        $this->addUserFields();
        $this->crud->setValidation(UpdateRequest::class);
    }

    /**
     * Store a newly created resource in the database.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $this->crud->request = $this->crud->validateRequest();
        $this->crud->request = $this->handlePasswordInput($this->crud->request);
        $this->crud->unsetValidation(); // validation has already been run

        return $this->traitStore();
    }

    /**
     * Update the specified resource in the database.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update()
    {

        $this->crud->request = $this->crud->validateRequest();
        $this->crud->request = $this->handlePasswordInput($this->crud->request);
        $this->crud->unsetValidation(); // validation has already been run

        return $this->traitUpdate();
    }

    /**
     * Handle password input fields.
     */
    protected function handlePasswordInput($request)
    {
        // Remove fields not present on the user.
        $request->request->remove('password_confirmation');
        $request->request->remove('roles_show');
        $request->request->remove('permissions_show');

        // Encrypt password if specified.
        if ($request->input('password')) {
            $request->request->set('password', Hash::make($request->input('password')));
        } else {
            $request->request->remove('password');
        }

        return $request;
    }

    protected function setupReorderOperation()
    {
        // define which model attribute will be shown on draggable elements
        #$this->crud->set('reorder.label', 'full_name');
        // define how deep the admin is allowed to nest the items
        // for infinite levels, set it to 0
        $this->crud->set('reorder.max_level', 0);
    }

    public function GetLifelineThree(){
        return view('admin_custom.lifeline_tree');
    }

    public function setColumnTimestamp(UserRequest $request){

        $id = $request->id;
        $column = $request->column;

        $user = BackpackUser::findOrFail($id);

        $user->$column = date('Y-m-d H:i:s');
        $user->save();
        return response('success', 200)
        ->header('Content-Type', 'text/plain');

    }

    public function setColumnBoolean(UserRequest $request){
        $user = Auth::user();
        $columnName = $request->column;
        $user->$columnName = $request->value;
        $user->save();
    }

    public function ReActiveUsers(){
        BackpackUser::where('id','>',0)->update(['active' => 0]);
        $users = BackpackUser::get();
        $users->each(function($user,$key){
            $user->isActive();
        });
    }

    public function fixTree(){
        BackpackUser::fixTree();
    }



}

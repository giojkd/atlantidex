<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CatalogueRequest;
use App\Models\Category;
use App\Models\Manufacturer;
use App\Models\Product;
use App\Models\Product_type;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Request;

/**
 * Class CatalogueCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CatalogueCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \App\Http\Controllers\Admin\Operations\ManageOperation;

    public function setup()
    {

        $this->crud->setListView('Company.Product.products_list');


        $this->crud->setModel('App\Models\Product');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/catalogue');
        $this->crud->setEntityNameStrings('Prodotto', 'Prodotti');

        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');
        $this->crud->denyAccess('show');
        $this->crud->allowAccess('manage');
        if(backpack_user()->isAdministrator()){
            $this->crud->allowAccess('create');
            $this->crud->allowAccess('update');
            $this->crud->allowAccess('delete');
            $this->crud->denyAccess('manage');
        }
        $re = Request::query('search');
        if (!is_null($re)) {

            $productsIds = Product::search($re)->get()->pluck('id');

            $this->crud->addClause('whereIn','id', $productsIds);
            #$this->crud->addClause('orWhere', 'sku', 'like', '%'.$re.'%');
            $this->crud->addClause('active');
        }

    }



    protected function setupListOperation()
    {
        $this->crud->setTitle('Catalogo Atlantidex'); // set the Title for the create action
        $this->crud->setHeading('Catalogo Atlantidex');

        $this->crud->addColumn([
            // run a function on the CRUD model and show its return value
            'name' => "pictures",
            'label' => "Cover", // Table column heading
            'type' => "model_function",
            'function_name' => 'coverlist', // the method in your Model
            // 'function_parameters' => [$one, $two], // pass one/more parameters to that method
             'limit' => 10000, // Limit the number of characters shown
        ]);

        $this->crud->addColumn([
            'name' => 'active', // The db column name
            'label' => trans('backpack::crudcontroll.active'), // Table column heading
            'type' => 'check'
        ]);

        /*
        $this->crud->addColumn([
            'name' => 'product_merchants_names',
            'label' => 'Merchants',
            'type' => 'text',
        ]);
*/
        $this->crud->addColumn([
            'name' => 'updated_at',
            'label' => trans('backpack::crudcontroll.date'),
            'type' => 'date',
        ]);

        $this->crud->addColumn([
            'name' => 'name',
            'label' => trans('backpack::crudcontroll.name'),
            'type' => 'text',
        ]);

        $this->crud->addColumn([
            'name' => 'variants',
            'label' => 'Num. varianti',
            'type'  => 'array_count'
        ]);

        $this->crud->addColumn([
            'name' => 'sku',
            'label' => "Sku",
            'type' => 'text',
        ]);

        $this->crud->addColumn([ // 1-n relationship
            'label' => trans('backpack::crudcontroll.category'),
            'type' => 'select',
            'name' => 'category', // the method that defines the relationship in your Model
            'entity' => 'category', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => 'App\Models\Category', // foreign key model
        ]);
        $this->crud->addColumn([ // 1-n relationship
            'label' => trans('backpack::crudcontroll.manufacturer'),
            'type' => 'select',
            'name' => 'manufacturer', // the method that defines the relationship in your Model
            'entity' => 'manufacturer', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => 'App\Models\manufacturer', // foreign key model
        ]);
        $this->crud->addColumn([ // 1-n relationship
            'label' => trans('backpack::crudcontroll.product_type'), // Table column heading
            'type' => 'select',
            'name' => 'product_type', // the method that defines the relationship in your Model
            'entity' => 'product_type', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => 'App\Models\Product_type', // foreign key model
        ]);


        // active Filter
        $this->crud->addFilter([
          'name' => 'active',
          'type' => 'simple',
          'label' => trans('backpack::crudcontroll.active'),
        ],
       false,
        function($value) { // if the filter is active
            $this->crud->addClause('active');
        });
        // category Filter
        $this->crud->addFilter([
          'name' => 'category',
          'type' => 'select2_multiple',
          'label' => trans('backpack::crudcontroll.category'),
        ],
        collect(Category::getAllLeaves())->pluck('name', 'id')->toArray(),
        function ($values) { // if the filter is active
             foreach (json_decode($values) as $key => $value) {
                 $this->crud->addClause('orWhere', 'category_id', $value);
             }
        });
        // manufacturer Filter

        $this->crud->addFilter(
            [
                'type'  => 'date_range',
                'name'  => 'from_to',
                'label' => 'Intervallo temporale'
            ],
            false,
            function ($value) {



                $dates = json_decode($value);
                $this->crud->addClause('where', 'updated_at', '>=', $dates->from);
                $this->crud->addClause('where', 'updated_at', '<=', $dates->to . ' 23:59:59');
            }
        );

        $this->crud->addFilter([
            'name' => 'manufacturer',
            'type' => 'select2_multiple',
            'label' => trans('backpack::crudcontroll.manufacturer'),
          ],
          Manufacturer::all()->pluck('name', 'id')->toArray(),
          function ($values) { // if the filter is active
               foreach (json_decode($values) as $key => $value) {
                   $this->crud->addClause('orWhere', 'manufacturer_id', $value);
               }
          });
          // tipology Filter
        $this->crud->addFilter([
            'name' => 'tipology',
            'type' => 'select2_multiple',
            'label' => trans('backpack::crudcontroll.tipology'),
          ],
        Product_type::all()->pluck('name', 'id')->toArray(),
        function ($values) { // if the filter is active
               foreach (json_decode($values) as $key => $value) {
                   $this->crud->addClause('orWhere', 'product_type_id', $value);
               }
        });
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(CatalogueRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        $this->crud->setFromDb();
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ManufacturerRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Http\Requests\DropzoneRequest;


/**
 * Class ManufacturerCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ManufacturerCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    //use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Manufacturer');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/manufacturer');
        $this->crud->setEntityNameStrings('manufacturer', 'manufacturers');
    }

    protected function setupListOperation()
    {
        //$this->crud->setFromDb();
        $this->crud->addColumns([
            [
                // run a function on the CRUD model and show its return value
                'name' => "pictures",
                'label' => "Cover", // Table column heading
                'type' => "model_function",
                'function_name' => 'cover', // the method in your Model
                // 'function_parameters' => [$one, $two], // pass one/more parameters to that method
                'limit' => 10000, // Limit the number of characters shown
            ],
            [
                'name' => 'active', // The db column name
                'label' => "Active", // Table column heading
                'type' => 'check'
            ],
            [
                'name' => 'name',
                'label' => "Name",
                'type' => 'text',
            ],
            [ // 1-n relationship
                'label' => 'Country', // Table column heading
                'type' => 'select',
                'name' => 'country', // the method that defines the relationship in your Model
                'entity' => 'country', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model' => 'App\Models\Country', // foreign key model
            ],
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(ManufacturerRequest::class);
        //$this->crud->setFromDb();
        $this->crud->addFields([
            [
                'name' => 'active', // The db column name
                'label' => "Active", // Table column heading
                'type' => 'checkbox'
            ],
            [
                'name' => 'name',
                'label' => "Name",
                'type' => 'text',
            ],
            [ // 1-n relationship
                'label' => 'Country', // Table column heading
                'type' => 'select',
                'name'  => 'country_id', // Table column which is FK in your model
                'entity' => 'country', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model' => 'App\Models\Country', // foreign key model
            ],
            [
                'name' => 'description',
                'label' => "Description",
                'type' => 'simplemde',
            ],
            [
                'name' => 'pictures', // db column name
                'label' => 'Photos', // field caption
                'type' => 'dropzone', // voodoo magic
                'prefix' => '/', // upload folder (should match the driver specified in the upload handler defined below)
                'upload-url' => '/admin/media-dropzone/product', // POST route to handle the individual file uploads
            ],
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}

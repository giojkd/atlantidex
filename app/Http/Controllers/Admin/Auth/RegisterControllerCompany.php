<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Models\BackpackUser;
use App\Models\Country;
use Illuminate\Auth\Events\Registered;
use App\Models\Invitationlink;
use App\Models\Merchant;
use Backpack\CRUD\app\Http\Controllers\Auth\RegisterController as BackpackRegisterController;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Carbon;
use App\Models\User;
use App\Http\Requests\CompanyRegistrationRequest;
use App\Models\Companytype;
use Auth as Auth;


class RegisterControllerCompany extends BackpackRegisterController
{

    public function __construct()
    {
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     *
     * @return User
     */
    protected function create(array $data)
    {
#        dd($data);




        $parentId = null;

        if(isset($data['code']) && $data['code'] != ''){
            $invitationLink = Invitationlink::where('code',$data['code'])->firstOrFail();
            $parentId = $invitationLink->user_id;
            $invitationLink->is_redeemed = 1;
            $invitationLink->save();
        } else {

            abort(401);

            $parentUser = BackpackUser::where('code', '39000000001')->first();
            $parentId = $parentUser->id;
        }

        /*
        $user_model_fqn = config('backpack.base.user_model_fqn');
        $user = new $user_model_fqn();
        */

        #Create consumer account

        /*
        $generator = new ComputerPasswordGenerator();

        $generator
        ->setOptionValue(ComputerPasswordGenerator::OPTION_UPPER_CASE, true)
        ->setOptionValue(ComputerPasswordGenerator::OPTION_LOWER_CASE, true)
        ->setOptionValue(ComputerPasswordGenerator::OPTION_NUMBERS, true)
        ->setOptionValue(ComputerPasswordGenerator::OPTION_SYMBOLS, false);

        $ramdomPassword = $generator->generatePassword(); */

        $newConsumerUser = BackpackUser::create([
            'name'                             => $data['name'],
            'surname'                          => $data['surname'],
            backpack_authentication_column()   => $data['email'],
            'password'                         => bcrypt($data['password']),
            'country_id'                       => $data['country_id'],
            'phone'                            => $data['phone'],
            'date_of_birth'                    => Carbon::createFromFormat(config('app.date_format'), $data['date_of_birth'])->format('Y-m-d'),
            'parent_id'                        => $parentId,
            'fiscal_code'                      => $data['fiscal_code'],
            'place_of_birth'                   => $data['place_of_birth'],
            'invitationlink_id'                => (isset($invitationLink)) ? $invitationLink->id : null
        ]);

        $newConsumerUser->assignRole('Customer');


        ######################



        $newUserCompany = [
            'name'                             => $data['name'],
            'surname'                          => $data['surname'],
            backpack_authentication_column()   => $data['company_email'],
            'password'                         => bcrypt($data['password']),
            'country_id'                       => $data['country_id'],
            'address'                          => json_decode($data['address']),
            'vat_number'                       => $data['vat_number'],
            'fiscal_code'                      => $data['company_fiscal_code'],
            'company_name'                     => $data['company_name'],
            'phone'                            => $data['phone'],
            'parent_id'                        => $newConsumerUser->id,
            'invitationlink_id'                => (isset($invitationLink)) ? $invitationLink->id : null,
            'share_capital' => $data['share_capital'],
            'companytype_id' => $data['companytype_id'],
            'sdi' => $data['sdi'],
            'pec' => $data['pec'],
            'contact_person' => $data['contact_person'],
            'fax' => $data['fax'],
            'corporate_sector' => $data['corporate_sector'],
            'company_registration_number' => $data['company_registration_number'],
            'website' => $data['website'],
            'email_for_invoices' => $data['email_for_invoices'],
        ];


        ################################################################



        $closestMerchantMarketer = $newConsumerUser->getClosestMerchantMarketer();
        $closestMerchantMarketerId = (!is_null($closestMerchantMarketer)) ? $closestMerchantMarketer->id : 32;
        $newUserCompany['virtual_parent_id'] = $closestMerchantMarketerId;



        ################################################################



        $newUser = BackpackUser::create($newUserCompany);

        $newUser->assignRole('Company');

        $newmerchant =  new Merchant();
        $newmerchant->name = $data['company_name'];
        $newmerchant->user()->associate($newUser);
        $newmerchant->vat_number = $data['vat_number'];
        $address = json_decode($data['address']);
        $newmerchant->address = $address;
        $newmerchant->lat = $address->latlng->lat;
        $newmerchant->lng = $address->latlng->lng;
        $newmerchant->save();

        $newConsumerUser->sendEmailVerificationNotification();


        return $newUser;
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */

    /*
    public function showRegistrationForm()
    {
        // if registration is closed, deny access
        if (!config('backpack.base.registration_open')) {
            abort(403, trans('backpack::base.registration_closed'));
        }

        $countries = Country::all()->pluck('name', 'id');
        $this->data['title'] = trans('backpack::base.register'); // set the page title
        $this->data['countries'] = $countries;

        return view(backpack_view('auth.register'), $this->data);
    }
    */

    public function showRegistrationFormCustom(Request $request){



        if(!is_null(backpack_user())){
            return redirect(Route('Company.registerCompany').'?code='.$request->code);
        }

        if(env('SUSPEND_COMPANY_REGISTRATION')){
            return redirect('https://www.atlantidex.com/');
        }



        // if registration is closed, deny access
        if (!config('backpack.base.registration_open')) {
            abort(403, trans('backpack::base.registration_closed'));
        }


        $this->data['title'] = trans('backpack::base.register'); // set the page title



        if($request->has('code')){

            $code = Invitationlink::where('code', $request->code)->first();

            if (is_null($code)) {
                abort(401);
            }
            if ($code->is_redeemed) {
                abort(401);
            }

            $this->data['code'] = $request->code;

        }else{

            abort(401);

        }


        $this->data['companyTypes'] = Companytype::get();

        return view(backpack_view('auth.registercompany'), $this->data);
    }

    public function registerCustom(CompanyRegistrationRequest $request){



        #$this->validator($request->all())->validate();

        $user = $this->create($request->all());

        event(new Registered($user));
        $this->guard()->login($user);

        return redirect('/admin');
        #return redirect($this->redirectPath());
    }


}

<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Models\BackpackUser;
use App\Models\Country;
use App\Models\Invitationlink;

use Backpack\CRUD\app\Http\Controllers\Auth\RegisterController as BackpackRegisterController;

use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use Ixudra\Curl\Facades\Curl;
use Auth;

class RegisterController extends BackpackRegisterController
{

    public function __construct()
    {

    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */

    public function validateFiscalCode($fiscalCode)
    {

        $endPoint = 'http://webservices.dotnethell.it/codicefiscale.asmx/ControllaCodiceFiscale';
        return simplexml_load_string(Curl::to($endPoint)->withData(['CodiceFiscale' => $fiscalCode])->get());
    }

    protected function validator(array $data)
    {

        $user_model_fqn = config('backpack.base.user_model_fqn');
        $user = new $user_model_fqn();
        $users_table = $user->getTable();
        $email_validation = backpack_authentication_column() == 'email:rfc,dns' ? 'email:rfc,dns|' : '';

        $passwordRegex = '"^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$"';



        $validator =  Validator::make($data, [
            'name'                             => 'required|max:255',
            'surname'                          => 'required|max:255',
            backpack_authentication_column()   => 'required|' . $email_validation . 'max:255|unique:' . $users_table,
            #'password'                         => ['required', 'min:6', 'confirmed', 'regex:/'. $passwordRegex.'/'],
            'password'                         => ['required', 'min:6', 'confirmed'],
            'phone'                            => 'required|numeric|unique:users,phone',
            'date_of_birth'                    => 'required|date_format:d/m/Y',
            'fiscal_code'                      => ['required','unique:users,fiscal_code', 'regex:/^(?:[A-Z][AEIOU][AEIOUX]|[B-DF-HJ-NP-TV-Z]{2}[A-Z]){2}(?:[\dLMNP-V]{2}(?:[A-EHLMPR-T](?:[04LQ][1-9MNP-V]|[15MR][\dLMNP-V]|[26NS][0-8LMNP-U])|[DHPS][37PT][0L]|[ACELMRT][37PT][01LM]|[AC-EHLMPR-T][26NS][9V])|(?:[02468LNQSU][048LQU]|[13579MPRTV][26NS])B[26NS][9V])(?:[A-MZ][1-9MNP-V][\dLMNP-V]{2}|[A-M][0L](?:[1-9MNP-V][\dLMNP-V]|[0L][1-9MNP-V]))[A-Z]$/i'],
            'address'                          => 'required',
            'accept_privacy'                   => 'required',
            'place_of_birth'                    => ['required']
        ]);

        $validator->after(function($validator) use($data){

            #FISCAL CODE VALIDATION
            $fiscalCodeValidation = $this->validateFiscalCode($data['fiscal_code']);
            if (!isset($fiscalCodeValidation[0]) || $fiscalCodeValidation[0] != 'Il codice è valido!') {
                $validator->errors()->add('fiscal_code', 'Il codice fiscale inserito non è valido.');
            }
            #FISCAL CODE VALIDATION

        });

        return $validator;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     *
     * @return User
     */
    protected function create(array $data)
    {

        $parentId = null;

        if(isset($data['code']) && $data['code'] != ''){
            $invitationLink = Invitationlink::where('code',$data['code'])->firstOrFail();
            $parentId = $invitationLink->user_id;
            $invitationLink->is_redeemed = 1;
            $invitationLink->save();
        } else {

            abort(401);

            $parentUser = BackpackUser::where('code','39000000001')->first();
            $parentId = $parentUser->id;
        }

        $user_model_fqn = config('backpack.base.user_model_fqn');

        $user = new $user_model_fqn();

        $dataCreate = [
            'name'                             => $data['name'],
            'surname'                          => $data['surname'],
            backpack_authentication_column()   => $data[backpack_authentication_column()],
            'password'                         => bcrypt($data['password']),
            'country_id'                       => $data['country_id'],
            'phone'                            => $data['phone'],
            'date_of_birth'                    => Carbon::createFromFormat(config('app.date_format'), $data['date_of_birth'])->format('Y-m-d'),
            'address'                          => json_decode($data['address']),
            'fiscal_code'                      => $data['fiscal_code'],
            'parent_id'                        => $parentId,
            'place_of_birth'                   => $data['place_of_birth']
        ];

        if(isset($invitationLink)){
            $dataCreate['invitationlink_id'] = $invitationLink->id;
        }

        if(isset($data['is_public_employee'])){
            $dataCreate = $data['is_public_employee'];
        }

        $newUser = $user->create($dataCreate);

        #$newUser->assignRole('Networker');

        $newUser->assignRole('Customer');

        if(env('ALLOW_MARKETERS_TO_SEND_INVITATIONS_BEFORE_PURCHASING_A_PACK')){
            $newUser->networker_invitation_links_count = 100000;
            $newUser->save();
        }

        return $newUser;
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */

    /*
    public function showRegistrationForm()
    {
        // if registration is closed, deny access
        if (!config('backpack.base.registration_open')) {
            abort(403, trans('backpack::base.registration_closed'));
        }

        $countries = Country::all()->pluck('name', 'id');
        $this->data['title'] = trans('backpack::base.register'); // set the page title
        $this->data['countries'] = $countries;

        return view(backpack_view('auth.register'), $this->data);
    }
    */

    public function showRegistrationFormCustom(Request $request){

        if(Auth::check()){
                #echo '<h1>Ma cribbio!</h1>';
        }

        if($request->has('code')){
            $code = Invitationlink::where('code',$request->code)->first();
            if(is_null($code)){
                abort(401);
            }
            if($code->is_redeemed){
                abort(401);
            }
        }else{
            abort(401);
        }

        // if registration is closed, deny access
        if (!config('backpack.base.registration_open')) {
            abort(403, trans('backpack::base.registration_closed'));
        }

        $this->data['title'] = trans('backpack::base.register'); // set the page title

        if($request->has('code')){
            $this->data['code'] = $request->code;
        }

        return view(backpack_view('auth.register'), $this->data);
    }


}

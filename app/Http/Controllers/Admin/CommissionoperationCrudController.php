<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CommissionoperationRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class CommissionoperationCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CommissionoperationCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Commissionoperation');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/commissionoperation');
        $this->crud->setEntityNameStrings('commissionoperation', 'commissionoperations');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();


        $this->crud->addColumns([
            [
                'label' => 'ID',
                'name' => 'id'
            ],
            [
                'label' => 'Date',
                'name' => 'created_at',
                'type' => 'datetime',
                'format' => 'DD/MM/Y H:mm'

            ],
            [
                'label'     => "User",
                'type'      => 'select',
                'name'      => 'user_id', // the db column for the foreign key
                // optional
                'entity'    => 'user', // the method that defines the relationship in your Model
                'attribute' => 'full_name', // foreign key attribute that is shown to user
                'model'     => "App\Models\Backpackuser", // foreign key model
                'default'   => 2, // set the default value of the select2
            ],
            /*
            [
                'label' => 'Sponsor',
                'type' => 'model_function_attribute',
                'function_name' => 'getUserSponsor',
                'attribute' => 'full_name'
            ],
            */
            [   // Number
                'name' => 'value',
                'label' => 'Amount',
                'type' => 'text',


            ],

            [
                'name' => 'description',
                'type' => 'text',
                'label' => 'Descrizione',
                'limit' => 200
            ],
            [
                'name' => 'commission_type',
                'type' => 'text',

                'label' => 'Tipo di commissione'
            ]
        ]);

    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(CommissionoperationRequest::class);

        // TODO: remove setFromDb() and manually define Fields
     #   $this->crud->setFromDb();
     $this->crud->addFields([
            [   // Number
                'name' => 'value',
                'label' => 'Amount',
                'type' => 'number',

                // optionals
                'attributes' => ["step" => "any"], // allow decimals

                'prefix'     => "€",
                'suffix'     => ".00",
            ],
            [
                'label'     => "User",
                'type'      => 'select2',
                'name'      => 'user_id', // the db column for the foreign key
                // optional
                'entity'    => 'user', // the method that defines the relationship in your Model
                'attribute' => 'detailed_name', // foreign key attribute that is shown to user
                'model'     => "App\Models\Backpackuser", // foreign key model
                'default'   => 2, // set the default value of the select2
            ],
            /*
            [
                'name' => 'description',
                'type' => 'textarea',
                'label' => 'Descrizione'
            ],
            */
            [
                'name' => 'commission_type',
                'type' => 'select_from_array',
                'options' => [ 'direct_commission' => 'Direct commission','company_affiliation' => 'Affiliazione azienda' ],
                'label' => 'Tipo di commissione'
            ],
            [
                'name' => 'commissionoperationable_type',
                'type' => 'select_from_array',
                'options' => [
                    'App\Models\Gdovoucher' => 'Voucher della GDO',
                    'App\Models\Leadrow' => 'Riga di un ordine'
                ],
                'label' => 'Oggetto dell\'operazione'
            ],
            [
                'name' => 'commissionoperationable_id',
                'label' => 'Id dell\'oggetto dell\'operazione'
            ]
            /*
            [
                'name' => '',
                'type' => '',
                'label' => ''
            ],
            [
                'name' => '',
                'type' => '',
                'label' => ''
            ],
            [
                'name' => '',
                'type' => '',
                'label' => ''
            ],
            [
                'name' => '',
                'type' => '',
                'label' => ''
            ],*/
     ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PayoutRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class PayoutCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PayoutCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Payout');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/payout');
        $this->crud->setEntityNameStrings('payout', 'payouts');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();
        $this->crud->addColumns([
            [
                'name' => 'id',
                'label' => 'ID'
            ],
                [
                    'name' => 'created_at',
                    'type' => 'datetime',
                    'label' => 'Richiesto il '
                ],
            [
                'label'     => "Utente",
                'type'      => 'select',
                'name'      => 'user_id', // the db column for the foreign key
                // optional
                'entity'    => 'user', // the method that defines the relationship in your Model
                'attribute' => 'detailed_name', // foreign key attribute that is shown to user
                'model'     => "App\Models\Backpackuser", // foreign key model

            ],
            [
                'name' => 'value',
                'type' => 'text',
                'label' => 'Importo',
                'disabled' => true,
            ],
            [
                'name' => 'status',
                'type' => 'select_from_array',
                'options' => ['pending' => 'In attesa', 'done' => 'Fatto', 'rejected' => 'Rifiutato']
            ],
            [
                'name' => 'processed_at',
                'type' => 'datetime',
                'label' => 'Processato il '
            ]
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(PayoutRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();
        $this->crud->addFields([
            [
                'label'     => "Utente",
                'type'      => 'select2',
                'name'      => 'user_id', // the db column for the foreign key
                // optional
                'entity'    => 'user', // the method that defines the relationship in your Model
                'attribute' => 'detailed_name', // foreign key attribute that is shown to user
                'model'     => "App\Models\Backpackuser", // foreign key model

                'attributes' => [
                    'readonly'    => 'readonly',
                    'disabled'    => 'disabled',
                ]
            ],
            [
                'name' => 'value',
                'type' => 'text',
                'label' => 'Importo',
                'disabled' => true,
                'attributes' => [
                    'readonly'    => 'readonly',
                    'disabled'    => 'disabled',
                ]
            ],
            [
                'name' => 'status',
                'type' => 'select_from_array',
                'options' => ['pending' => 'In attesa','done' => 'Fatto','rejected' => 'Rifiutato']
            ],
            [
                'name' => 'processed_at',
                'type' => 'datetime',
                'label' => 'Processato il '
            ]
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}

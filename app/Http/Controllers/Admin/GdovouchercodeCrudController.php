<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\GdovouchercodeRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class GdovouchercodeCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class GdovouchercodeCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Gdovouchercode');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/gdovouchercode');
        $this->crud->setEntityNameStrings('gdovouchercode', 'gdovouchercodes');
    }

    protected function setupListOperation()
    {





        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();
        $this->crud->addColumns([
            [
                'name' => 'id',

                'label' => 'ID',

            ],
            [
                'name' => 'value',
                'type' => 'number',
                'label' => 'Valore',
                'prefix' => '€',
            ],
            [
                'label' => 'Voucher',
                'type' => 'select',
                'name' => 'gdovoucher_id', // the method that defines the relationship in your Model
                'entity' => 'gdovoucher', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model' => 'App\Models\Gdovoucher', // foreign key model
            ],
            [
                'name' => 'code',
                'type' => 'text',
                'label' => 'Codice'
            ],
            [
                'name' => 'link',
                'type' => 'text',
                'label' => 'Link'
            ],
            [
                'name' => 'barcode',
                'type' => 'barcode',
                'label' => 'Barcode'
            ],
            [
                'label' => 'Utente',
                'type' => 'select',
                'name' => 'user_id', // the method that defines the relationship in your Model
                'entity' => 'user', // the method that defines the relationship in your Model
                'attribute' => 'full_name', // foreign key attribute that is shown to user
                'model' => 'App\Models\BackpackUser', // foreign key model
            ],
            [
                'name' => 'is_partially_spendable',
                'type' => 'boolean',
                'label' => 'Può essere speso parzialmente',
            ],
            [
                'name' => 'amount_spent',
                'type' => 'number',
                'label' => 'Speso per',
            ],
            [
                'name' => 'expires_at',
                'type' => 'text',
                'label' => 'Scade il',
            ]
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(GdovouchercodeRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();
        $this->crud->addFields([
            [
                'name' => 'id',
                'type' => 'text',
                'label' => 'ID',
            ],
            [
                'name' => 'value',
                'type' => 'number',
                'label' => 'Valore',
                'prefix' => '€',
            ],
            [
                'label' => 'Voucher',
                'type' => 'select2',
                'name' => 'gdovoucher_id', // the method that defines the relationship in your Model
                'entity' => 'gdovoucher', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model' => 'App\Models\Gdovoucher', // foreign key model
            ],
            [
                'name' => 'code',
                'type' => 'text',
                'label' => 'Codice'
            ],
            [
                'name' => 'barcode',
                'type' => 'text',
                'label' => 'Barcode'
            ],
            [
                'name' => 'link',
                'type' => 'text',
                'label' => 'Link'
            ],
            [
                'label' => 'Utente',
                'type' => 'select2',
                'name' => 'user_id', // the method that defines the relationship in your Model
                'entity' => 'user', // the method that defines the relationship in your Model
                'attribute' => 'full_name', // foreign key attribute that is shown to user
                'model' => 'App\Models\BackpackUser', // foreign key model
            ],
            [
                'name' => 'is_partially_spendable',
                'type' => 'checkbox',
                'label' => 'Può essere speso parzialmente',
            ],
            [
                'name' => 'amount_spent',
                'type' => 'number',
                'label' => 'Speso per',
            ],
            [
                'name' => 'expires_at',
                'type' => 'date',
                'label' => 'Scade il',
            ]
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\TicketRequest;
use App\Models\Ticket;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class TicketCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class TicketCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Ticket');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/ticket');
        $this->crud->setEntityNameStrings('ticket', 'tickets');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        $this->crud->setFromDb();
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(TicketRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        $this->crud->setFromDb();
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function scanTicket(TicketRequest $request){
        return view('admin_custom.scan_ticket');
    }

    public function invalidateTicket(TicketRequest $request){
        $ticket = Ticket::find($request->id);
        $ticket->is_used = 1 ;
        $ticket->save();
        return ['status' => 1];
    }

    public function testTicket(TicketRequest $request)
    {
        $ticket = Ticket::with(['event','user'])->find($request->id);
        return $ticket;
    }


}

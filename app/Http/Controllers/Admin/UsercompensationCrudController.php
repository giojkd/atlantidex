<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UsercompensationRequest;
use App\Models\BackpackUser;
use App\Models\Usercompensation;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class UsercompensationCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class UsercompensationCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Usercompensation');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/usercompensation');
        $this->crud->setEntityNameStrings('usercompensation', 'usercompensations');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();
        $this->crud->addColumns([
            [
                'name' => 'user_id',
                'label' => 'User',
                'type' => 'select',
                'entity'    => 'user', // the method that defines the relationship in your Model
                'model'     => BackpackUser::class, // foreign key model
                'attribute' => 'super_detailed_name', // foreign key attribute that is shown to user
            ],
            [
                'name' => 'monthly_career_bonus',
                'label' => 'MC Bonus',
                'prefix' => '€',
                'type' => 'number'
            ],
            [
                'name' => 'monthly_career_commission',
                'label' => 'MC Commission',
                'prefix' => '€',
                'type' => 'number'
            ],
            [
                'name' => 'long_run_career_bonus',
                'label' => 'LRC Bonus',
                'prefix' => '€',
                'type' => 'number'
            ],
            [
                'name' => 'long_run_career_commission',
                'label' => 'LRC Commission',
                'prefix' => '€',
                'type' => 'number'
            ],
            [
                'name' => 'direct_commission',
                'label' => 'Drct Commision',
                'prefix' => '€',
                'type' => 'number'
            ],
            [
                'name' => 'merchant_bonus',
                'label' => 'Mrchnt Bonus',
                'prefix' => '€',
                'type' => 'number'
            ],
            [
                'name' => 'merchant_booster_bonus',
                'label' => 'Mrchnt Booster Bonus',
                'prefix' => '€',
                'type' => 'number'
            ],
            [
                'name' => 'revenue_bonus',
                'label' => 'Rvn Bonus',
                'prefix' => '€',
                'type' => 'number'
            ],
            [
                'name' => 'marketing_bonus',
                'label' => 'Mrktng Bonus',
                'prefix' => '€',
                'type' => 'number'
            ],
            [
                'name' => 'compensation_total',
                'label' => 'Total',
                'prefix' => '€',
                'type' => 'number'
            ],

        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(UsercompensationRequest::class);

        // TODO: remove setFromDb() and manually define Fields
#        $this->crud->setFromDb();



    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }


    public function temporaryRecap(UsercompensationRequest $request){
        $user = new BackpackUser();
        $monthId = ($request->has('month_id')) ? $request->month_id : collect($user->getProductionMonthsUntilGivenDate(date('Y-m-d')))->count() - 1;
        $data = [
            'compensations' => Usercompensation::with(['user'])->where('month_id',$monthId)->where('compensation_total','>',0)->orderBy('compensation_total','DESC')->get()
        ];
        return view('compensations_temporary_recap', $data);
    }

    public function temporaryRecapIndex(UsercompensationRequest $request)
    {
        $user = new BackpackUser();
        $months = collect($user->getProductionMonthsUntilGivenDate(date('Y-m-d')));
        $data = ['months' => $months];

        return view('compensations_temporary_recap_index', $data);
    }

}

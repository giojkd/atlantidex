<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PackRequest;
use App\Models\Document;
use App\Role;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class PackCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PackCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Pack');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/pack');
        $this->crud->setEntityNameStrings('pack', 'packs');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();
        $this->crud->addColumns([
            [
                'name' => 'id',
                'label' => 'ID'
            ],
            [
                'name' => 'active',
                'type' => 'boolean',
                'label' => 'Active'
            ],
            [
                'name' => 'name',
                'label' => 'Nome'
            ],
            [
                'name' => 'is_starter_pack',
                'type' => 'boolean',
                'label' => 'E\' lo starter pack?'
            ],
            [
                'name' => 'is_renewable',
                'type' => 'boolean',
                'label' => 'Da rinnovare'
            ],
            [
                'name' => 'role_id',
                'label' => 'Ruolo',
                'type' => 'select',
                'entity'    => 'role', // the method that defines the relationship in your Model
                'model'     => Role::class, // foreign key model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'default'   => 2, //
            ]
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(PackRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();
        $this->crud->addFields([
            [
                'name' => 'active',
                'type' => 'checkbox',
                'label' => 'Active'
            ],
            [
                'name' => 'name',
                'type' => 'text',
                'label' => 'Nome'
            ],
          /*  [
                'name' => 'tag',
                'type' => 'text',
                'label' => 'Tag'
            ], */
            [
                'name' => 'description',
                'type' => 'wysiwyg',
                'label' => 'Descrizione'
            ],
            [
                'name' => 'revenue_bonus',
                'type' => 'number',
                'label' => 'Revenue bonus',
                'hint' => 'La percentuale del fatturato totale generato all\'interno di Atlantidex dall\'azienda affiliata direttamente dal Merchant Marketing che gli viene riconosciuta come provvigione'
            ],
 [
                'name' => 'sponsor_granted_eps_at_pack_purchase',
                'type' => 'number',
                'label' => 'EPs per lo sponsor',
                'hint' => 'EPs garantiti allo sponsor all\'acquisto di questo pack (solo per pack per marketer)',
                'suffix' => 'EP'
            ],



            [   // CustomHTML
                'name'  => 'separator',
                'type'  => 'custom_html',
                'value' => '<h3>Campi Invito Azienda (Normale)</h3>'
            ],
            [
                'name' => 'merchant_bonus_eps',
                'type' => 'number',
                'label' => 'Merchant bonus EPs',
                'hint' => '(merchant_bonus_eps) Il <b>numero di EPs</b> che riceve il Merchant Marketer quando un\'azienda acquista questo pack',
                'suffix' => 'EP'
            ],
            [
                'name' => 'merchant_bonus',
                'type' => 'number',
                'label' => 'Merchant bonus',
                'hint' => 'La <b>provvigione</b> che riceve il Merchant Marketer quando un\'azienda acquista questo pack',
                'suffix' => '€'
            ],


            [   // CustomHTML
                'name'  => 'separator2',
                'type'  => 'custom_html',
                'value' => '<h3>Campi Invito Azienda Booster</h3>'
            ],
            [
                'name' => 'merchant_booster_bonus_eps',
                'type' => 'number',
                'label' => 'Merchant booster bonus EPs',
                'hint' => '(merchant_booster_bonus_eps) <b>Il numero di EP</b> che <u>Merchant Marketer</u> sponsor dell\'azienda che acquista il pack riceve se l\'azienda si registra grazie ad un invito di tipo Booster',
                'suffix' => 'EP'
            ],
            [
                'name' => 'merchant_booster_bonus',
                'type' => 'number',
                'label' => 'Merchant booster bonus',
                'hint' => 'La <b>provvigione</b> che il Merchant Marketer sponsor dell\'azienda che acquista il pack riceve se l\'azienda si registra grazie ad un invito di tipo Booster',
                'suffix' => '€'
            ],
            [   // CustomHTML
                'name'  => 'separator3',
                'type'  => 'custom_html',
                'value' => '<hr>'
            ],

            [
                'name' => 'granted_eps',
                'type' => 'number',
                'label' => 'EPs riconosciuti',
                'hint' => 'Numero di EPs riconosciuti al Marketer all\'acquisto del pack (solo per pack per marketer)'
            ],

            [
                'name' => 'merchant_marketing_bonus',
                'type' => 'number',
                'label' => 'Merchant marketing bonus',
                'hint' => '<b>Il numero di EP</b> ricevuti dal marketer per ogni 100 euro di spazi pubblicitari acquistati dai suoi '
            ],

            [
                'name' => 'network_affiliations',
                'type' => 'number',
                'label' => 'Affiliazioni per i networker',
                'hint' => 'Numero di affiliazioni garantite per i networker'
            ],
            [
                'name' => 'merchant_bonus_affiliations',
                'type' => 'number',
                'label' => 'Affiliazioni per le aziende',
                'hint' => 'Numero di affiliazioni garantite per le aziende'
            ],
            [
                'name' => 'merchant_booster_bonus_affiliations',
                'type' => 'number',
                'label' => 'Affiliazioni in Merchant Booster Bonus',
                'hint' => 'Numero di affiliazioni garantite per le aziende a regime di Merchant Booster Bonus'
            ],
            [
                'name' => 'price',
                'type' => 'number',
                'label' => 'Costo',
                'hint' => 'Iva inclusa',
                'attributes' => ['step' => 'any']
            ],
            [
                'name' => 'direct_commission',
                'type' => 'number',
                'attributes' => ['step' => 'any'],
                'label' => 'Direct commission',
                'hint' => 'Commissione per i merchant marketer sul fatturatto generato dalle aziende da loro affiliate'
            ],
            [
                'name' => 'is_starter_pack',
                'type' => 'checkbox',
                'label' => 'E\' lo starter pack?'
            ],
            [
                'name' => 'required_documents',
                'type' => 'select2_from_array',
                'options' => Document::get()->pluck('name','id'),
                'allows_multiple' => true,
                #'fake' => 'true',
                #'store_in' => 'required_documents'
            ],
            [   // Browse
                'name'  => 'cover',
                'label' => 'Image',
                'type'  => 'browse'
            ],
            [
                'name' => 'role_id',
                'label' => 'Ruolo',
                'entity'    => 'role', // the method that defines the relationship in your Model
                'model'     => Role::class, // foreign key model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'default'   => 2, //
            ],
            [
                'name' => 'is_renewable',
                'type' => 'checkbox',
                'label' => 'Va rinnovato?'
            ],
            [
                'name' => 'renew_price',
                'type' => 'number',
                'label' => 'Costo di rinnovo',
                'hint' => 'Iva inclusa',
                'attributes' => ['step' => 'any']
            ],
            [
                'name' => 'expires_after',
                'type' => 'select_from_array',
                'options' => ['1years' => 'Un anno'],
                'label' => 'Scade dopo',

            ],

            [
                'name' => 'check_if_purchasable',
                'type' => 'checkbox',
                'label' => 'Check if purchasable'
            ],
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\InvoicerowRequest;
use App\Models\BackpackUser;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class InvoicerowCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class InvoicerowCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Invoicerow');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/invoicerow');
        $this->crud->setEntityNameStrings('invoicerow', 'invoicerows');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();
        $this->crud->addColumns([
            [
                'name' => 'id',
                'label' => 'ID',
            ],
            [
                'name' => 'ext_id',
                'label' => 'Ext ID',
            ],
            [
                'name' => 'user_id',
                'label' => 'Utente',
                'type' => 'select',
                'entity'    => 'user', // the method that defines the relationship in your Model
                'model'     => BackpackUser::class, // foreign key model
                'attribute' => 'full_name', // foreign key attribute that is shown to user
            ],
            [
                'name' => 'created_at',
                'label' => 'Data',

            ],
            [
                'name' => 'value',
                'label' => 'Valore',
            ],
            [
                'name' => 'invoicerowable_type',
                'label' => 'Tipo di operazione'
            ],
            [
                'name' => 'invoicerowable_id',
                'label' => 'Oggetto dell\'operazione'
            ],
                [
                    'name' => 'paid_with',
                    'label' => 'Pagato con'
                ],
            [
                'name' => 'notes',
                'label' => 'Notes'
            ],
            [
                'name' => 'description',
                'label' => 'Descrizione',
                'limit' => 200
            ]

        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(InvoicerowRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        $this->crud->setFromDb();
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\EventRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Http\Requests\DropzoneRequest;
use Auth;
use App\Models\Event;

/**
 * Class EventCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class EventCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;

    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation {
        store as traitStore;
    }

    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation {
        update as traitUpdate;
    }


    public function setup()
    {
        $this->crud->setModel('App\Models\Event');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/event');
        $this->crud->setEntityNameStrings('evento', 'eventi');

    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();

        if (!backpack_user()->isAdministrator()) {
            $this->crud->addClause('where','user_id','=', backpack_user()->id);
        }


        $this->crud->addColumns([
            [
                'name' => 'id',
                'label' => 'ID',
                'type' => 'test'
            ],
            [
                'name' => 'title',
                'label' => 'Nome',
                'type' => 'test'
            ],
            [
                'name' => 'happens_at',
                'type' => 'datetime',
                'label' => 'Data',
                'format' => 'ddd d MMM YYYY H:m'
            ],
            [
                'name' => 'eventtype_id',
                'type' => 'select_from_array',
                'options' => Event::eventTypes(),
                'label' => 'Tipo'
            ],
            [
                'name' => 'available_spots',
                'label' => 'Posti disponibili'
            ],

        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setOperationSetting('saveAllInputsExcept', ['_token', '_method', 'http_referrer', 'current_tab', 'save_action']);
        $this->crud->setValidation(EventRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();

        $user = Auth::user();

        if($user->isAdministrator()){
            $ticketsTableColumns = [
                'name'  => 'Nome',
                'description'  => 'Descrizione',
                'price' => 'Prezzo',
                'quantity' => 'Quantità',
                'eps'=> 'EPs per 100,00€'
            ];
        }else{
            $ticketsTableColumns = [
                'name'  => 'Nome',
                'description'  => 'Descrizione',
                'quantity' => 'Quantità'
            ];
        }

        $this->crud->addFields([
            [
                'name' => 'user_id',
                'type' => 'hidden',
                'value' => Auth::id()
            ],
            [
                'name' => 'active', // The db column name
                'label' => "Attivo", // Table column heading
                'type' => 'checkbox'
            ],
            [
                'name' => 'eventtype_id',
                'label' => 'Tipo',
                'type' => 'select_from_array',
                'options' => Event::eventTypes(),
                'allows_null' => false
            ],
            [
                'name' => 'title',
                'label' => 'Titolo',
                'type' => 'text'
            ], [
                'name' => 'short_description',
                'label' => 'Descrizione breve',
                'type' => 'textarea'
            ], [
                'name' => 'description',
                'label' => 'Descrizione',
                'type' => 'wysiwyg'
            ], [
                'name' => 'is_online',
                'label' => 'E\' online',
                'type' => 'checkbox'
            ],
            [
                'name' => 'address',
                'label' => 'Indirizzo',
                'type' => 'address',
                'store_as_json' => true
            ],
            /*
            [
                'name' => 'slides_file',
                'label' => 'Slides',
                'hint'  => 'Carica un file compresso (.zip) contenente le slides',
                'type'      => 'upload',
                'upload'    => true,
                'disk'      => 'public_upload'
            ],
            */
            [
                'name' => 'happens_at',
                'label' => 'Orario di inizio',
                'type' => 'datetime_picker'
            ],
            [
                'name' => 'ends_at',
                'label' => 'Orario di fine',
                'type' => 'datetime_picker'
            ],
            [   // Table
                'name'            => 'available_tickets',
                'label'           => 'Biglietti',
                'type'            => 'table',
                'entity_singular' => 'biglietto', // used on the "Add X" button
                'columns'         => $ticketsTableColumns,
                #'max' => 5, // maximum rows allowed in the table
                'min' => 0, // minimum rows allowed in the table
            ], [
                'name' => 'photos', // db column name
                'label' => 'Immagini', // field caption
                'hint' => 'La prima foto è quella di copertina',
                'type' => 'dropzone', // voodoo magic
                'prefix' => '/', // upload folder (should match the driver specified in the upload handler defined below)
                'upload-url' => '/admin/media-dropzone/event', // POST route to handle the individual file uploads
            ]
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function update(){


        $this->crud->request->request->add(['available_spots' => collect(json_decode($tickets = $this->crud->request->available_tickets, 1))
            ->reduce(function ($carry, $item) {
                return $carry += $item['quantity'];
            }, 0)]);

        $response = $this->traitUpdate();

        return $response;

    }

    public function store()
    {
        // to something before save

        $this->crud->request->request->add(['available_spots' => collect(json_decode($tickets = $this->crud->request->available_tickets, 1))
            ->reduce(function ($carry, $item) {
                return $carry += $item['quantity'];
            }, 0)]);


        $response = $this->traitStore();
        // do something after save
        return $response;
    }

    public function handleDropzoneUpload(DropzoneRequest $request)
    {
        $disk = "custom_public"; //
        $destination_path = "";
        $destination_path_filename = "storage";
        $file = $request->file('file');

        try {
            $image = \Image::make($file);
            $filename = md5($file->getClientOriginalName() . time()) . '.jpg';
            \Storage::disk($disk)->put($destination_path . '/' . $filename, $image->stream());
            return response()->json(['success' => true, 'filename' => $destination_path_filename . '/' . $filename]);
        } catch (\Exception $e) {
            if (empty($image)) {
                return response('Not a valid image type', 412);
            } else {
                return response('Unknown error', 412);
            }
        }
    }








}

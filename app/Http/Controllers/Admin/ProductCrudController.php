<?php

    namespace App\Http\Controllers\Admin;

    use App\Http\Requests\ProductRequest;
    use App\Models\Category;
    use Backpack\CRUD\app\Http\Controllers\CrudController;
    use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
    use App\Http\Requests\DropzoneRequest;
    use App\Models\BackpackUser;
use App\Models\Manufacturer;
use App\Models\Product;
use App\Models\Product_type;
use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Route;

/**
    * Class ProductCrudController
    * @package App\Http\Controllers\Admin
    * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
    */
    class ProductCrudController extends CrudController
    {
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \App\Http\Controllers\Admin\Operations\ModupdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \App\Http\Controllers\Admin\Operations\DetachOperation;




    public function setup()
    {
        $this->crud->setModel('App\Models\Product');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/product');
        $this->crud->setEntityNameStrings('Prodotto', 'Prodotti');

        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');
        $this->crud->allowAccess('detach');
        $this->crud->denyAccess('modupdate');
        $this->crud->allowAccess('show');

    }



    protected function setupDetachRoutes($segment, $routeName, $controller)
    {
        Route::post($segment.'/{id}/detach', [
            'as'        => $routeName.'.detach',
            'uses'      => $controller.'@detach',
            'operation' => 'detach',
        ]);
    }





    protected function setupListOperation()
    {
        $this->crud->setTitle('Prodotti'); // set the Title for the create action
        $this->crud->setHeading('I miei prodotti');
        $this->crud->addColumn([
            // run a function on the CRUD model and show its return value
            'name' => "pictures",
            'label' => "Cover", // Table column heading
            'type' => "model_function",
            'function_name' => 'coverlist', // the method in your Model
            // 'function_parameters' => [$one, $two], // pass one/more parameters to that method
             'limit' => 10000, // Limit the number of characters shown
        ]);

        $this->crud->addColumn([
            // run a function on the CRUD model and show its return value
            'name'  => 'active',
            'label' => 'Bozza', // Table column heading
            'type'  => 'model_function',
            'function_name' => 'isDraft',
            // 'function_parameters' => [$one, $two], // pass one/more parameters to that method
            // 'limit' => 100, // Limit the number of characters shown
         ]);

        $this->crud->addColumn([
            'name' => 'active', // The db column name
            'label' => trans('backpack::crudcontroll.active'), // Table column heading
            'type' => 'check'
        ]);

        $this->crud->addColumn([
            'name' => 'updated_at',
            'label' => trans('backpack::crudcontroll.date'),
            'type' => 'date',
        ]);

        $this->crud->addColumn([
            'name' => 'name',
            'label' => trans('backpack::crudcontroll.name'),
            'type' => 'text',
        ]);

        $this->crud->addColumn([
            'name' => 'variants',
            'label' => 'Num. varianti',
            'type'  => 'array_count'
        ]);

        $this->crud->addColumn([
            'name' => 'sku',
            'label' => "Sku",
            'type' => 'text',
        ]);
        $this->crud->addColumn([
            // any type of relationship
            'name'         => 'activeMerchants', // name of relationship method in the model
            'type'         => 'relationship',
            'label'        => 'Punti vendita', // Table column heading
            // OPTIONAL
            // 'entity'    => 'tags', // the method that defines the relationship in your Model
            // 'attribute' => 'name', // foreign key attribute that is shown to user
            // 'model'     => App\Models\Category::class, // foreign key model
         ]);

        $this->crud->addColumn([ // 1-n relationship
            'label' => trans('backpack::crudcontroll.category'),
            'type' => 'select',
            'name' => 'category', // the method that defines the relationship in your Model
            'entity' => 'category', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => 'App\Models\Category', // foreign key model
        ]);
        $this->crud->addColumn([ // 1-n relationship
            'label' => trans('backpack::crudcontroll.manufacturer'),
            'type' => 'select',
            'name' => 'manufacturer', // the method that defines the relationship in your Model
            'entity' => 'manufacturer', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => 'App\Models\manufacturer', // foreign key model
        ]);
        $this->crud->addColumn([ // 1-n relationship
            'label' => trans('backpack::crudcontroll.product_type'), // Table column heading
            'type' => 'select',
            'name' => 'product_type', // the method that defines the relationship in your Model
            'entity' => 'product_type', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => 'App\Models\Product_type', // foreign key model
        ]);


        // active Filter
        $this->crud->addFilter([
          'name' => 'active',
          'type' => 'simple',
          'label' => trans('backpack::crudcontroll.active'),
        ],
       false,
        function($value) { // if the filter is active
            $this->crud->addClause('active');
        });
        // category Filter
        $this->crud->addFilter([
          'name' => 'category',
          'type' => 'select2_multiple',
          'label' => trans('backpack::crudcontroll.category'),
        ],
        collect(Category::getAllLeaves())->pluck('name', 'id')->toArray(),
        function ($values) { // if the filter is active
             foreach (json_decode($values) as $key => $value) {
                 $this->crud->addClause('orWhere', 'category_id', $value);
             }
        });
        // select2_multiple filter
        $this->crud->addFilter([
            'name'  => 'merchants',
            'type'  => 'select2',
            'label' => 'Punti vendita'
        ], function() { // the options that show up in the select2
            return backpack_user()->merchants->pluck('name', 'id')->toArray();
        }, function($value) { // if the filter is active
                return $this->crud->query->whereHas('merchants', function ($q) use ($value) {
                    $q->where('merchant_id', $value)->where('merchant_product.active',1);
                });

        });
        // manufacturer Filter
        $this->crud->addFilter([
            'name' => 'manufacturer',
            'type' => 'select2_multiple',
            'label' => trans('backpack::crudcontroll.manufacturer'),
          ],
          Manufacturer::all()->pluck('name', 'id')->toArray(),
          function ($values) { // if the filter is active
               foreach (json_decode($values) as $key => $value) {
                   $this->crud->addClause('orWhere', 'manufacturer_id', $value);
               }
          });
        // tipology Filter
        $this->crud->addFilter([
            'name' => 'tipology',
            'type' => 'select2_multiple',
            'label' => trans('backpack::crudcontroll.tipology'),
          ],
        Product_type::all()->pluck('name', 'id')->toArray(),
        function ($values) { // if the filter is active
               foreach (json_decode($values) as $key => $value) {
                   $this->crud->addClause('orWhere', 'product_type_id', $value);
               }
        });
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(ProductRequest::class);
        //$this->crud->setFromDb();

        $this->crud->addField([
        'name' => 'active', // The db column name
        'label' => trans('backpack::crudcontroll.active'), // Table column heading
        'type' => 'checkbox'
        ]);
        $this->crud->addField([
        'name' => 'name',
        'label' => trans('backpack::crudcontroll.name'),
        'type' => 'text',
        ]);
        $this->crud->addField([
        'name' => 'sku',
        'label' => "Sku",
        'type' => 'text',
        ]);
        $this->crud->addField([
        'name' => 'description',
        'label' => "Description",
        'type' => 'simplemde',
        ]);
        $this->crud->addField([
        'name' => 'brief',
        'label' => "Brief",
        'type' => 'simplemde',

        ]);
        $this->crud->addField([ // 1-n relationship
        'label' => trans('backpack::crudcontroll.category'),
        'type' => 'select2',
        'name'  => 'category_id', // Table column which is FK in your model
        'entity' => 'category', // the method that defines the relationship in your Model
        'attribute' => 'name', // foreign key attribute that is shown to user
        'model' => 'App\Models\Category', // foreign key model
        ]);
        $this->crud->addField([ // 1-n relationship
            'label' => trans('backpack::crudcontroll.manufacturer'),
            'type' => 'select2',
            'name'  => 'manufacturer_id', // Table column which is FK in your model
            'entity' => 'manufacturer', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => 'App\Models\Manufacturer', // foreign key model
        ]);
        $this->crud->addField([ // 1-n relationship
        'label' => 'Product type', // Table column heading
        'type' => 'select',
        'name' => 'product_type_id', // Table column which is FK in your model
        'entity' => 'product_type', // the method that defines the relationship in your Model
        'attribute' => 'name', // foreign key attribute that is shown to user
        'model' => 'App\Models\Product_type', // foreign key model
        ]);
        $this->crud->addField([
        'name' => 'price',
        'label' => "Price",
        'type' => 'number',
        'attributes' => ["step" => "any"], // allow decimals
        ]);
        $this->crud->addField([
        'name' => 'price_compare',
        'label' => "Price comp.",
        'type' => 'number',
        'attributes' => ["step" => "any"], // allow decimals
        ]);
        $this->crud->addField([
        'name' => 'price_compare',
        'label' => "Price comp.",
        'type' => 'number',
        'attributes' => ["step" => "any"], // allow decimals
        ]);

        $this->crud->addField([
        'name' => 'pictures', // db column name
        'label' => 'Photos', // field caption
        'type' => 'dropzone', // voodoo magic
        'prefix' => '/', // upload folder (should match the driver specified in the upload handler defined below)
        'upload-url' => '/admin/media-dropzone/product', // POST route to handle the individual file uploads
        ]);
    }

    protected function setupUpdateOperation()
    {
        /*
        if (empty ($request->get('photos'))) {
        $this->crud->update(\Request::get($this->crud->model->getKeyName()), ['photos' => '[]']);
        }
        */

        $this->setupCreateOperation();
    }



    public function handleDropzoneUpload(DropzoneRequest $request)
    {
        $disk = "custom_public"; //
        $destination_path = "";
        $destination_path_filename = "storage";
        $file = $request->file('file');

        try
        {
            $image = \Image::make($file);
            $filename = md5($file->getClientOriginalName().time()).'.jpg';
            \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());
            return response()->json(['success' => true, 'filename' => $destination_path_filename . '/' . $filename]);
        } catch (\Exception $e) {
            if (empty ($image)) {
                return response('Not a valid image type', 412);
            } else {
                return response('Unknown error', 412);
            }
        }
    }

    public function detach($id)
    {
        $this->crud->hasAccessOrFail('detach');

        $id = $this->crud->getCurrentEntryId() ?? $id;
        $user = backpack_user();
        $merchants = $user->merchants;
        $product = Product::findOrFail($id);
        foreach($merchants as $merchant) {
            $product->merchants()->detach($merchant->id);
        }
        return true;
    }



    public function handleDropzoneFileUpload(DropzoneRequest $request)
    {
        $disk = "custom_public"; //
        $destination_path = "";
        $destination_path_filename = "storage";
        $file = $request->file('file');
        //dd($file);
        //try{
            $filename = md5($file->getClientOriginalName().time()).'.'.$file->getClientOriginalExtension();
            \Storage::disk($disk)->put($destination_path.'/'.$filename,file_get_contents($file));
            return response()->json(['success' => true, 'filename' => $destination_path_filename . '/' .$filename]);
        //} catch (\Exception $e) {
            //return response('Unknown error', 412);
        //}
    }

}

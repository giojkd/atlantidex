<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\DocumentRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Requests\DropzoneRequest;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Setting;
use Str;
/**
 * Class DocumentCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class DocumentCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;

    private function getDocumentTypes(){
        $optionsAux = collect(json_decode(Setting::get('document_types'), 1));
        $options = [];
        $optionsAux->each(function ($item, $key) use (&$options) {
            $options[$item['value']] = $item['label'];
        });
        return $options;
    }

    public function setup()
    {
        $this->crud->setModel('App\Models\Document');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/document');
        $this->crud->setEntityNameStrings('document', 'documents');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();
        $this->crud->addColumn([
            'name' => 'name',
            'label' => "Name",
            'type' => 'text',
        ]);

        /*
        $this->crud->addColumn([
            'name' => 'description',
            'label' => "Description",
            'type' => 'text',
        ]);
        */

        $this->crud->addColumn([

            'label'     => "Tipo",
            'type'      => 'select',
            'name'      => 'documenttype_id', // the db column for the foreign key

            // optional
            'entity'    => 'type', // the method that defines the relationship in your Model
            'model'     => "App\Models\Documenttype", // foreign key model
            'attribute' => 'name',
        ]);

        $this->crud->addColumn([
            'name' => 'is_signable_with_checkbox',
            'label' => "Firmabile con un checkbox",
            'type' => 'boolean',
        ]);

    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(DocumentRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();
        $this->crud->addField([
            'name' => 'name',
            'label' => "Nome",
            'type' => 'text',
        ]);
        $this->crud->addField([
            'name' => 'description',
            'label' => "Descrizione",
            'type' => 'wysiwyg',
        ]);

        $this->crud->addField([
            'name' => 'is_signable_with_checkbox',
            'label' => "Firmabile con un checkbox",
            'type' => 'checkbox',
        ]);

        $this->crud->addField([
            'name' => 'content',
            'label' => "Contenuto",
            'type' => 'wysiwyg',
            'hint' => '<small>I tag disponibili sono: [name] [surname] [fiscal_code] [telephone] [address] [date_of_birth]</small>'
        ]);

        $this->crud->addField([
            'name' => 'acceptable_policy',
            'label' => "Policy",
            'type' => 'wysiwyg',
        ]);

        $this->crud->addField(['name'  => 'links',
            'label' => 'Link',
            'type'  => 'repeatable',
            'fields' => [
                [
                    'name'    => 'href',
                    'type'    => 'text',
                    'label'   => 'Href',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name'    => 'label',
                    'type'    => 'text',
                    'label'   => 'Label',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
            ],

            // optional
            'new_item_label'  => 'Add Group', // customize the text of the button
        ]);

        #AtlantinexCompensationPlan

        $this->crud->addField([
              // Select2
                'label'     => "Tipo",
                'type'      => 'select2',
                'name'      => 'documenttype_id', // the db column for the foreign key

                // optional
                'entity'    => 'type', // the method that defines the relationship in your Model
                'model'     => "App\Models\Documenttype", // foreign key model
                'attribute' => 'name', // foreign key attribute that is shown to user
                #'default'   => 2, // set the default value of the select2

                // also optional
                #'options'   => (function ($query) {
                #    return $query->orderBy('name', 'ASC')->where('depth', 1)->get();
                #}), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select

        ]);



    }


    public function handleDropzoneUpload(DropzoneRequest $request)
    {
        $disk = "custom_public"; //
        $destination_path = "";
        $destination_path_filename = "storage";
        $file = $request->file('file');
        $fileMimeType = $file->getMimeType();
        list($type,$extension) = explode('/',$fileMimeType);
        if(Str::lower($type) == 'image'){
            try {
                $image = \Image::make($file);
                $filename = md5($file->getClientOriginalName() . time()) . '.jpg';
                \Storage::disk($disk)->put($destination_path . '/' . $filename, $image->stream());
                return response()->json(['success' => true, 'filename' => $destination_path_filename . '/' . $filename]);
            } catch (\Exception $e) {
                if (empty($image)) {
                    return response('Not a valid image type', 412);
                } else {
                    return response('Unknown error', 412);
                }
            }
        }else{
            $returningIconsByType = [
                'pdf' => 'https://www.flaticon.com/svg/static/icons/svg/337/337946.svg'
            ];
            $filename = md5($file->getClientOriginalName() . time()) . '.'.$extension;
            \Storage::disk($disk)->put($destination_path . '/' . $filename, $file);
            return response()->json(['success' => true, 'filename' => $filename]);
            #return response()->json(['success' => true, 'filename' => $returningIconsByType[$extension]]);
        }
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}

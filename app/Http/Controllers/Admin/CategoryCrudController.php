<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CategoryRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Request;

/**
 * Class CategoryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CategoryCrudController extends CrudController
{

    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Category');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/category');
        $this->crud->setEntityNameStrings('category', 'categories');
        $re = Request::query('search');;
        if (!is_null($re)) {
            $this->crud->addClause('where', 'parent_id', '=', $re);
        }
    }

    protected function setupListOperation()
    {
        //$this->crud->setFromDb();
        $this->crud->addColumn([
            'name' => 'id',
            'label' => "ID",
            'type' => 'text',
        ]);

        $this->crud->addColumn([
            'name' => 'active', // The db column name
            'label' => trans('backpack::crudcontroll.active'), // Table column heading
            'type' => 'check'
        ]);

        $this->crud->addColumn([
            'name' => 'name',
            'label' => "Nome",
            'type' => 'text',
        ]);
        $this->crud->addColumn([
            'name' => 'description',
            'label' => "Descrizione",
            'type' => 'text',
        ]);

        $this->crud->addColumn([   // select2_nested
            'name'      => 'parent_id',
            'label'     => "Parent",
            'type'      => 'select',
            'entity'    => 'parent', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            // optional
            'model'     => "App\Models\Category", // force foreign key model
        ]);


    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(CategoryRequest::class);

        //$this->crud->setFromDb();
        $this->crud->addField([
            'name' => 'active', // The db column name
            'label' => trans('backpack::crudcontroll.active'), // Table column heading
            'type' => 'checkbox'
        ]);
        $this->crud->addField([
            'name' => 'name',
            'label' => "Name",
            'type' => 'text',
        ]);
        $this->crud->addField([
            'name' => 'description',
            'label' => "Description",
            'type' => 'text',
        ]);
        $this->crud->addField([   // select2_nested
            'name'      => 'parent_id',
            'label'     => "Parent",
            'type'      => 'select2',
            'entity'    => 'parent', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            // optional
            'model'     => "App\Models\Category", // force foreign key model
        ]);

    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    protected function setupReorderOperation()
    {
        ini_set('max_execution_time', 3000);
        // define which model attribute will be shown on draggable elements
        $this->crud->set('reorder.label', 'name');
        // define how deep the admin is allowed to nest the items
        // for infinite levels, set it to 0
        $this->crud->set('reorder.max_level', 0);

    }




}

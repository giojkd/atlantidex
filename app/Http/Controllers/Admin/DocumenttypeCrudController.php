<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\DocumenttypeRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class DocumenttypeCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class DocumenttypeCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Documenttype');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/documenttype');
        $this->crud->setEntityNameStrings('documenttype', 'documenttypes');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();

        $this->crud->addColumns([
            [
                'type' => 'text',
                'name' => 'name',
                'label' => 'Nome'
            ],
            [
                'type' => 'text',
                'name' => 'tag',
                'label' => 'Tag'
            ],
            [    // Select2Multiple = n-n relationship (with pivot table)
                'label'     => "Ruoli",
                'type'      => 'select_multiple',
                'name'      => 'roles', // the method that defines the relationship in your Model
                'entity'    => 'roles', // the method that defines the relationship in your Model
                'model'     => "App\Role", // foreign key model
                'attribute' => 'name', // foreign key attribute that is shown to user
            ]
        ]);

    }

    protected function setupCreateOperation()
    {
        #$this->crud->setValidation(DocumenttypeRequest::class);

        $this->crud->addFields([
            [
                'type' => 'text',
                'name' => 'name',
                'label' => 'Nome'
            ],
            [
                'type' => 'text',
                'name' => 'tag',
                'label' => 'Tag'
            ],
            [    // Select2Multiple = n-n relationship (with pivot table)
                'label'     => "Ruoli",
                'type'      => 'select2_multiple',
                'name'      => 'roles', // the method that defines the relationship in your Model

                // optional
                'entity'    => 'roles', // the method that defines the relationship in your Model
                'model'     => "App\Role", // foreign key model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
                // 'select_all' => true, // show Select All and Clear buttons?

                // optional
                #'options'   => (function ($query) {
                #    return $query->orderBy('name', 'ASC')->where('depth', 1)->get();
                #}), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
            ]

        ]);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}

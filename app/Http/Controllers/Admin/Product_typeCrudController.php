<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Product_typeRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class Product_typeCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class Product_typeCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Product_type');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/product_type');
        $this->crud->setEntityNameStrings('Tipologia prodotto', 'Tipologie prodotto');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        //$this->crud->setFromDb();
        $this->crud->addColumns([
            [
                'name' => 'name',
                'label' => "Name",
                'type' => 'text',
            ],
            [
                'label' => "Root category",
                'type'      => 'select',
                'name'      => 'root_category', // the db column for the foreign key
                // optional
                // 'entity' should point to the method that defines the relationship in your Model
                // defining entity will make Backpack guess 'model' and 'attribute'
                'entity'    => 'category',

                // optional - manually specify the related model and attribute
                'model'     => "App\Models\Category", // related model
                'attribute' => 'name', // foreign key attribute that is shown to user

                // optional - force the related options to be a custom query, instead of all();
                'options'   => (function ($query) {
                        return $query->orderBy('name', 'ASC')->whereNull('parent_id')->get();
                }), //  y
            ],
        ]);

    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(Product_typeRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        //$this->crud->setFromDb();
        $this->crud->addFields([
            [
                'name' => 'name',
                'label' => "Name",
                'type' => 'text',
            ],
            [
                'label' => "Root category",
                'type'      => 'select',
                'name'      => 'root_category', // the db column for the foreign key
                // optional
                // 'entity' should point to the method that defines the relationship in your Model
                // defining entity will make Backpack guess 'model' and 'attribute'
                'entity'    => 'category',

                // optional - manually specify the related model and attribute
                'model'     => "App\Models\Category", // related model
                'attribute' => 'name', // foreign key attribute that is shown to user

                // optional - force the related options to be a custom query, instead of all();
                'options'   => (function ($query) {
                        return $query->orderBy('name', 'ASC')->whereNull('parent_id')->get();
                }), //  y
            ],
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SuggestedcategoryRequest;
use App\Models\Suggestedcategory;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class SuggestedcategoryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class SuggestedcategoryCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Suggestedcategory');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/suggestedcategory');
        $this->crud->setEntityNameStrings('suggestedcategory', 'suggestedcategories');

        $this->crud->addButtonFromView('line', 'approveSuggestedCategory', 'approveSuggestedCategory', 'end');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();

        $this->crud->addColumns([[
                'name' => 'id',
                'label' => 'ID',
                'type' => 'text'
            ],
            [
                'name' => 'name',
                'label' => 'Name',
                'type' => 'text'
            ],
            [
                'name'      => 'parent_id',
                'label'     => "Parent",
                'type'      => 'select',
                'entity'    => 'parent', // the method that defines the relationship in your Model
                'attribute' => 'stored_ancestors_and_self_breadcrumb', // foreign key attribute that is shown to user
                'model'     => "App\Models\Category", // force foreign key model
            ],
            [
                'name'      => 'user_id',
                'label'     => "User",
                'type'      => 'select',
                'entity'    => 'user', // the method that defines the relationship in your Model
                'attribute' => 'company_name', // foreign key attribute that is shown to user
                'model'     => "App\Models\BackpackUser", // force foreign key model
            ],
            [
                'name'      => 'category_id',
                'label'     => "Category",
                'type'      => 'select',
                'entity'    => 'category', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model'     => "App\Models\Category", // force foreign key model
            ],

        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(SuggestedcategoryRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        $this->crud->setFromDb();
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function approveSuggestedcategory(SuggestedcategoryRequest $request){
        $suggestedcategory = Suggestedcategory::find($request->id);
        $suggestedcategory->approveCategory();

    }

}

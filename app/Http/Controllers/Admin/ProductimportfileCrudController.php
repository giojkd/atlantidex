<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProductimportfileRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Auth;
/**
 * Class ProductimportfileCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ProductimportfileCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Productimportfile');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/productimportfile');
        $this->crud->setEntityNameStrings('file di importazione', 'file di importazione');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #   $this->crud->setFromDb();


        $this->crud->addColumn([
            'label' => 'ID',
            'name' => 'id'
        ]);

        $this->crud->addColumn([ // 1-n relationship
            'label' => 'Filiale',
            'type' => 'select',
            'name'  => 'merchant_id', // Table column which is FK in your model
            'entity' => 'merchant', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => 'App\Models\Merchant', // foreign key model
        ]);

        $this->crud->addColumn([
            'label' => 'Numero prodotti',
            'name' => 'products_count',
            'type'      => 'text',
        ]);

        $this->crud->addColumn([
            'label' => 'Stato',
            'name' => 'result',
            'type' => 'model_function',
            'function_name' => 'getStatus'
        ]);




    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(ProductimportfileRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();

        $this->crud->addField([ // 1-n relationship
            'label' => 'Merchant',
            'type' => 'select2',
            'name'  => 'merchant_id', // Table column which is FK in your model
            'entity' => 'merchant', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => 'App\Models\Merchant', // foreign key model
        ]);

        $this->crud->addField([
            'name'      => 'file',
            'label'     => 'File dei prodotti',
            'hint'      => '<a download href="/resources/AtlantidexImportTemplate.xlsx">Scarica il template</a> da riempire. <a download href="/resources/Atlantidex_ManualeOperativo_Inserimento_prodotti_multipliesingoli_conTabella.pdf">Scarica il manuale per riempire il template</a>. Si accetta solo il formato .xlsx per il file di caricamento. ',
            'type'      => 'upload',
            'upload'    => true,
            'disk'      => 'public',
        ]);


        $this->crud->addField([
            'name'      => 'file_photos',
            'label'     => 'File delle foto',
            'hint'      => 'Deve essere in formato .zip e contenere una cartella nominata con l\'SKU per ogni prodotto caricato. Carica foto jpg ad alta risoluzione in qualsiasi formato o dimensione.',
            'type'      => 'upload',
            'upload'    => true,
            'disk'      => 'public',
        ]);


        $this->crud->addField([
            'name'      => 'user_id',
            'type'      => 'hidden',
            'value'     => Auth::id(),
        ]);



    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SchedulerowRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Models\Schedule;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

/**
 * Class SchedulerowCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class SchedulerowCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;



    public function setup()
    {
        $this->crud->setModel('App\Models\Schedulerow');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/schedulerow');
        $this->crud->setEntityNameStrings('disponibilità', 'disponibilità');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters

        $this->crud->addFilter(
            [
                'type'  => 'date_range',
                'name'  => 'date_range',
                'label' => 'Periodo'
            ],
            false,
            function ($value) {
                $dates = json_decode($value);
                $this->crud->addClause('where', 'day', '>=', $dates->from);
                $this->crud->addClause('where', 'day', '<=', $dates->to . ' 23:59:59');
            }
        );

        $this->crud->removeButton('create');
        $this->crud->removeButton('update');

        $schedule_id = $this->crud->getRequest()->schedule_id;
        $schedule = Schedule::findOrFail($schedule_id);

        $this->crud->addClause('where','schedule_id','=', $schedule_id);



        $fieldsFunction = 'getCreateFields' . Str::ucfirst($schedule->merchant->type);



        $fields = $this->$fieldsFunction();



        $this->crud->addColumns($fields);

        $this->crud->addButtonFromView('top', 'add_entry', 'custom_add_availability_button', 'end');
        $this->crud->addButtonFromView('line', 'custom_edit_availability_button', 'custom_edit_availability_button', 'end');

#        $this->crud->setFromDb();
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(SchedulerowRequest::class);

        $schedule = Schedule::findOrFail($this->crud->getRequest()->schedule_id);

        $fieldsFunction = 'getCreateFields'.Str::ucfirst($schedule->merchant->type);

        $fields = $this->$fieldsFunction();
        $fields[] = [
            'name' => 'schedule_id',
            'type' => 'hidden',
            'value' => $schedule->id
        ];
        $this->crud->addFields($fields);

    }

    protected function setupUpdateOperation()
    {

        $this->setupCreateOperation();

    }

    public function getCreateFieldsService(){
        return [
            [
                'name' => 'day',
                'type' => 'date',
                'label' => 'Giorno'
            ],
            [
                'name' => 'time',
                'type' => 'time',
                'label' => 'Orario'
            ],
            [

                'name' => 'quantity',
                'type' => 'hidden',
                'value' => 1

            ],
        ];
    }

    public function getCreateFieldsRestaurant(){
        return [
            [
                'name' => 'day',
                'type' => 'date',
                'label' => 'Giorno'
            ],
            [
                'name' => 'time',
                'type' => 'time',
                'label' => 'Orario'
            ],
            [

                'name' => 'product_id',
                'type' => 'select_from_array',
                'options' => Auth::user()->products()->pluck('identifier', 'id'),
                'label' => 'Menu',
                'hint' => 'Seleziona il menu che vuoi mettere in vendita'
            ],
            [

                'name' => 'quantity',
                'type' => 'number',
                'label' => 'Quantità',
                'default' => 1,
                'attributes' => [
                    'min' => 0
                ]

            ],
        ];
    }

    public function getCreateFieldsHotel(){
        return
        [
            [
                'name' => 'day',
                'type' => 'date',
                'label' => 'Giorno'
            ],
            [

                'name' => 'quantity',
                'type' => 'number',
                'label' => 'Quantità',
                'default' => 1,
                'attributes' => [
                    'min' => 0
                ]

            ],
            [

                'name' => 'product_id',
                'type' => 'select_from_array',
                'options' => Auth::user()->products()->pluck('identifier','id'),
                'label' => 'Tipo di accomodazione',
                'hint' => 'Seleziona il tipo di camera, appartamento, etc... del quale vuoi impostare la disponibilità'
            ],
            [
                'name' => 'price_per_adult',
                'label' => 'Prezzo per notte',
                'type' => 'number',
                'attributes' => ['step' => 0.01, 'min' => 0.00],
                'prefix' => '€'
            ],
            [
                'name' => 'adults_per_unit',
                'label' => 'Adulti',
                'type' => 'number',
                'attributes' => ['step' => 1, 'min' => 1],
            ],
            [
                'name' => 'price_per_child',
                'label' => 'Prezzo per bambino',
                'type' => 'number',
                'attributes' => ['step' => 0.01, 'min' => 0.00],
                'prefix' => '€'
            ],
            [
                'name' => 'children_per_unit',
                'label' => 'Bambini',
                'type' => 'number',
                'attributes' => ['step' => 1, 'min' => 1],
            ],
            [
                'name' => 'price_per_infant',
                'label' => 'Prezzo per infante',
                'type' => 'number',
                'attributes' => ['step' => 0.01, 'min' => 0.00],
                'prefix' => '€'
            ],
            [
                'name' => 'infants_per_unit',
                'label' => 'Infanti',
                'type' => 'number',
                'attributes' => ['step' => 1, 'min' => 1],
            ],

        ];
    }

}

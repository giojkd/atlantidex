<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CompanycategoryRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class CompanycategoryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CompanycategoryCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Companycategory');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/companycategory');
        $this->crud->setEntityNameStrings('company category', 'company categories');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();
          //$this->crud->setFromDb();
        $this->crud->addColumn([
            'name' => 'id',
            'label' => "ID",
            'type' => 'text',
        ]);


        $this->crud->addColumn([
            'name' => 'name',
            'label' => "Nome",
            'type' => 'text',
        ]);

        $this->crud->addColumn([   // select2_nested
            'name'      => 'parent_id',
            'label'     => "Parent",
            'type'      => 'select',
            'entity'    => 'parent', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            //
        ]);
        $this->crud->addColumn([
            'name' => 'category_code',
            'label' => "Settore o Sotto categoria",
            'type' => 'text',
        ]);
        $this->crud->addColumn([
            'name' => 'min_commission',
            'label' => "Commissione minima",
            'type' => 'number',
            'suffix' => '%'
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(CompanycategoryRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();

        //$this->crud->setFromDb();

        $this->crud->addField([
            'name' => 'name',
            'label' => "Name",
            'type' => 'text',
        ]);
        $this->crud->addField([
            'name' => 'category_code',
            'label' => "Settore o Sotto categoria",
            'type' => 'text',
        ]);
        $this->crud->addField([
            'name' => 'min_commission',
            'label' => "Commissione minima",
            'type' => 'number',
            'attributes' => ['min' => 1,'step' => 'any'],
            'suffix' => '%'
        ]);
        $this->crud->addField([   // select2_nested
            'name'      => 'parent_id',
            'label'     => "Parent",
            'type'      => 'select2',
            'entity'    => 'parent', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            // optional
            'model'     => "App\Models\Companycategory", // force foreign key model
            'options'   => (function ($query) {
                return $query->whereNull('parent_id')->get();
            }),
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }


    protected function setupReorderOperation()
    {
        // define which model attribute will be shown on draggable elements
        $this->crud->set('reorder.label', 'name');
        // define how deep the admin is allowed to nest the items
        // for infinite levels, set it to 0
        $this->crud->set('reorder.max_level', 1);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AdvpackRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Models\Document;
/**
 * Class AdvpackCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AdvpackCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Advpack');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/advpack');
        $this->crud->setEntityNameStrings('advpack', 'advpacks');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();

        $this->crud->addColumns([
            [
                'name' => 'id',
                'type' => 'text',
                'label' => 'Number',
            ],
            [
                'name' => 'active',
                'type' => 'boolean',
                'label' => 'Active',
            ],
            [
                'name' => 'name',
                'type' => 'text',
                'label' => 'Name'
            ],
            [
                'name' => 'upsellable',
                'type' => 'number',
                'label' => 'Upsell'
            ],
            [
                'name' => 'newsletterable',
                'type' => 'number',
                'label' => 'Newsletter'
            ],
            [
                'name' => 'homepageable',
                'type' => 'number',
                'label' => 'Homepage'
            ],
            [
                'name' => 'searchable',
                'type' => 'number',
                'label' => 'Search'
            ],

            [
                'name' => 'price',
                'type' => 'text',
                'label' => 'Price',
                'prefix' => '€',

            ],
            [
                'name' => 'duration',
                'type' => 'select_from_array',
                'options' => [
                    '+1month' => '1 month',
                    '+2months' => '2 months',
                    '+3months' => '3 months',
                    '+4months' => '4 months',
                    '+5months' => '5 months',
                    '+6months' => '6 months',
                    '+7months' => '7 months',
                    '+8months' => '8 months',
                    '+9months' => '9 months',
                    '+10months' => '10 months',
                    '+11months' => '11 months',
                    '+12months' => '12 months',

                ],
                'label' => 'Duration'
            ],
        ]);

    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(AdvpackRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();
        $this->crud->addFields([
            [
                'name' => 'active',
                'type' => 'checkbox',
                'label' => 'Active',
            ],
            [
                'name' => 'name',
                'type' => 'text',
                'label' => 'Name'
            ],
            [
                'name' => 'short_description',
                'type' => 'textarea',
                'label' => 'Short description'
            ],
            [
                'name' => 'description',
                'type' => 'wysiwyg',
                'label' => 'Description'
            ],
            [
                'name' => 'required_documents',
                'type' => 'select2_from_array',
                'options' => Document::get()->pluck('name', 'id'),
                'allows_multiple' => true,
                #'fake' => 'true',
                #'store_in' => 'required_documents'
            ],
            [
                'name' => 'upsellable',
                'type' => 'number',
                'label' => 'Upsell'
            ],

            [
                'name' => 'newsletterable',
                'type' => 'number',
                'label' => 'Newsletter'
            ],
            [
                'name' => 'homepageable',
                'type' => 'number',
                'label' => 'Homepage'
            ],
            [
                'name' => 'searchable',
                'type' => 'number',
                'label' => 'Search'
            ],
            [
                'name' => 'duration',
                'type' => 'select_from_array',
                'options' => [
                    '+1month' => '1 month',
                    '+2months' => '2 months',
                    '+3months' => '3 months',
                    '+4months' => '4 months',
                    '+5months' => '5 months',
                    '+6months' => '6 months',
                    '+7months' => '7 months',
                    '+8months' => '8 months',
                    '+9months' => '9 months',
                    '+10months' => '10 months',
                    '+11months' => '11 months',
                    '+12months' => '12 months',

                ],
                'label' => 'Duration'
            ],
            [
                'name' => 'price',
                'type' => 'number',
                'label' => 'Price',
                'prefix' => '€',
                'attributes' => ['step' => 'any']
            ],
            [
                'name' => 'photos', // db column name
                'label' => 'Photos', // field caption
                'type' => 'dropzone', // voodoo magic
                'prefix' => '/', // upload folder (should match the driver specified in the upload handler defined below)
                'upload-url' => '/admin/media-dropzone/product', // POST route to handle the individual file uploads
            ],
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}

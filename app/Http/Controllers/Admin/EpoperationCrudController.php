<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\EpoperationRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class EpoperationCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class EpoperationCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Epoperation');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/epoperation');
        $this->crud->setEntityNameStrings('epoperation', 'epoperations');
        $this->crud->enableExportButtons();
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();

        $this->crud->addFilter(
            [
                'type'  => 'date_range',
                'name'  => 'from_to',
                'label' => 'Date range'
            ],
            false,
            function ($value) {



                $dates = json_decode($value);
                $this->crud->addClause('where', 'created_at', '>=', $dates->from);
                $this->crud->addClause('where', 'created_at', '<=', $dates->to . ' 23:59:59');
            }
        );

        $this->crud->addColumns([
            [
                'name' => 'id',
                'label' => 'ID'
            ],
            [
                'name' => 'created_at',
                'label' => 'Time'
            ],
            [
                'name' => 'value',
                'label' => 'Valore'
            ],
            [
                'name' => 'name',
                'label' => 'Nome',
                'type' => 'relationship',
                'entity'    => 'user',
                'model'     => "App\Models\Backpackuser", // related model
                'attribute' => 'name',
            ],
            [
                'name' => 'surname',
                'label' => 'Cognome',
                'type' => 'relationship',
                'entity'    => 'user',
                'model'     => "App\Models\Backpackuser", // related model
                'attribute' => 'surname',
            ],
           /* [
                'name' => 'fiscal_code',
                'label' => 'Codice fiscale',
                'type' => 'relationship',
                'entity'    => 'user',
                'model'     => "App\Models\Backpackuser", // related model
                'attribute' => 'fiscal_code',
            ],
            [
                'name' => 'date_of_birth_human_readable',
                'label' => 'Data di nascita',
                'type' => 'relationship',
                'entity'    => 'user',
                'model'     => "App\Models\Backpackuser", // related model
                'attribute' => 'date_of_birth_human_readable',
            ],
            [
                'name' => 'address_road',
                'label' => 'Indirizzo',
                'type' => 'relationship',
                'entity'    => 'user',
                'model'     => "App\Models\Backpackuser", // related model
                'attribute' => 'address_road',
            ],
            [
                'name' => 'address_postcode',
                'label' => 'CAP',
                'type' => 'relationship',
                'entity'    => 'user',
                'model'     => "App\Models\Backpackuser", // related model
                'attribute' => 'address_postcode',
            ],
            [
                'name' => 'address_county',
                'label' => 'Provincia',
                'type' => 'relationship',
                'entity'    => 'user',
                'model'     => "App\Models\Backpackuser", // related model
                'attribute' => 'address_county',
            ],*/
            [
                'name' => 'description',
                'label' => 'Description',
                'limit' => 100
            ],
            [
                'name' => 'is_careerable',
                'label' => 'Is careerable',
                'type' => 'boolean'
            ]

        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(EpoperationRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();

        $this->crud->addFields([[
                'label'     => "User",
                'type'      => 'select2',
                'name'      => 'user_id', // the db column for the foreign key
                // optional
                'entity'    => 'user', // the method that defines the relationship in your Model
                'attribute' => 'detailed_name', // foreign key attribute that is shown to user
                'model'     => "App\Models\Backpackuser", // foreign key model
                'default'   => 2, // set the default value of the select2
            ],
            [   // Number
                'name' => 'value',
                'label' => 'Amount',
                'type' => 'number',

                // optionals
                'attributes' => ["step" => "any"], // allow decimals

                'prefix'     => "EP",
                #'suffix'     => ".00",
            ],
            [
                'name' => 'description',
                'label' => 'Receipt',
                'type' => 'textarea'
            ],
            [
                'name' => 'is_careerable',
                'label' => 'Is careerable',
                'type' => 'checkbox'
            ],
            [
                'name' => 'created_at',
                'label' => 'When',
                'type' => 'datetime',
                'default' => date('Y-m-d H:i')
            ]
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\MerchantRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Requests\MerchantRequest as UpdateRequest;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Http\Requests\DropzoneRequest;
use Backpack\CRUD\app\Library\Widget;

/**
 * Class MerchantCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class MerchantCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation { store as traitStore; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation { update as traitUpdate; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Merchant');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/merchant');
        $this->crud->setEntityNameStrings('punto vendita', 'punti vendita');
        $merchantIntoBox = view('infoboxes/merchant')->render();
        Widget::add()->to('before_content')->type('card')->content(['body' => $merchantIntoBox ]);
    }

    protected function setupListOperation()
    {
        //$this->crud->setFromDb();
        $this->crud->addColumns([
            [
                // run a function on the CRUD model and show its return value
                'name' => "pictures",
                'label' => "Copertina", // Table column heading
                'type' => "model_function",
                'function_name' => 'cover', // the method in your Model
                // 'function_parameters' => [$one, $two], // pass one/more parameters to that method
                'limit' => 10000, // Limit the number of characters shown
            ],
            [
                'name' => 'active', // The db column name
                'label' => trans('backpack::crudcontroll.active'),
                'type' => 'check'
            ],
            [
                'name' => 'name',
                'label' => trans('backpack::crudcontroll.name'),
                'type' => 'closure',
                'function' => function($entry) {
                    return '<a href="/increase-my-business/'.$entry->id.'/company-revenue"><i class="fa fa-eye"></i> '.$entry->name .'</a>';
                }
            ],
            [
                'name' => 'vat_number',
                'label' => trans('backpack::crudcontroll.vat_number'),
                'type' => 'text',
            ],
            /*[ // 1-n relationship
                'label' => 'User', // Table column heading
                'type' => 'select',
                'name' => 'user_id', // the method that defines the relationship in your Model
                'entity' => 'user', // the method that defines the relationship in your Model
                'attribute' => 'surname', // foreign key attribute that is shown to user
                'model' => 'App\Models\BackpackUser', // foreign key model
            ],*/
            [ // 1-n relationship
                'label' => trans('backpack::crudcontroll.country'),
                'type' => 'select',
                'name' => 'country', // the method that defines the relationship in your Model
                'entity' => 'country', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model' => 'App\Models\Country', // foreign key model
            ],
            [
                'name' => 'type',
                'label' => 'Tipo',
                'type' => 'select_from_array',
                'options' => [
                    'hotel' => 'Hotel',
                    'restaurant' => 'Ristorante',
                    'service' => 'Servizio professionale',
                    'shop' => 'Negozio'
                ]
            ],
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(MerchantRequest::class);

        //$this->crud->setFromDb();
        $this->crud->addFields([
            [
                'name' => 'active', // The db column name
                'label' => trans('backpack::crudcontroll.active'),
                'type' => 'checkbox'
            ],
            [
                'name' => 'name',
                'label' => trans('backpack::crudcontroll.name'),
                'type' => 'text',
            ],
            [
                'name' => 'vat_number',
                'label' => trans('backpack::crudcontroll.vat_number'),
                'type' => 'hidden',
                'value' => backpack_user()->vat_number,
            ],
            [
                'name'  => 'user_id',
                'type'  => 'hidden',
                'value' => backpack_user()->id,
            ],
            /*[ // 1-n relationship
                'label' => 'User', // Table column heading
                'type' => 'select2',
                'name'  => 'user_id', // Table column which is FK in your model
                'entity' => 'user', // the method that defines the relationship in your Model
                'attribute' => 'full_name', // foreign key attribute that is shown to user
                'model' => 'App\Models\BackpackUser', // foreign key model
            ],*/
            [ // 1-n relationship
                'label' => trans('backpack::crudcontroll.country'),
                'type' => 'select',
                'name'  => 'country_id', // Table column which is FK in your model
                'entity' => 'country', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model' => 'App\Models\Country', // foreign key model
            ],
            [
                'name' => 'description',
                'label' => trans('backpack::crudcontroll.description'),
                'type' => 'simplemde',
            ],
            [
                'name' => 'brief',
                'label' => trans('backpack::crudcontroll.brief'),
                'type' => 'simplemde',
            ],
            [   // Address
                'name' => 'address',
                'label' => trans('backpack::crudcontroll.address'),
                'type' => 'address_google',
                // optional
                'store_as_json' => true
            ],
            [
                'name' => 'pictures', // db column name
                'label' => 'Photos', // field caption
                'type' => 'dropzone', // voodoo magic
                'prefix' => '/', // upload folder (should match the driver specified in the upload handler defined below)
                'upload-url' => '/admin/media-dropzone/product', // POST route to handle the individual file uploads
            ],
            [
                'name' => 'type',
                'label' => 'Tipo',
                'type' => 'select_from_array',
                'options' => [
                    'hotel' => 'Hotel',
                    'restaurant' => 'Ristorante',
                    'service' => 'Servizio professionale',
                    'shop' => 'Negozio'
                ]
            ],
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
    public function store()
    {
        $this->setLatLng();
        return $this->traitStore();
    }


    public function update()
    {
        $this->setLatLng();
        return $this->traitUpdate();
    }

    protected function setLatLng(): void
    {

        $address = json_decode($this->crud->request->input('address'));
        if ($address) {
            $lat = $address->latlng->lat;
            $lng = $address->latlng->lng;
            $this->crud->request->request->add(['lat' => $lat]);
            $this->crud->request->request->add(['lng' => $lng]);
            $this->crud->addField(['type' => 'hidden', 'name' => 'lat']);
            $this->crud->addField(['type' => 'hidden', 'name' => 'lng']);
        }
    }

    public function handleDropzoneUpload(DropzoneRequest $request)
    {
        $disk = "custom_public"; //
        $destination_path = "";
        $destination_path_filename = "storage";
        $file = $request->file('file');

        try
        {
            $image = \Image::make($file);
            $filename = md5($file->getClientOriginalName().time()).'.jpg';
            \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());
            return response()->json(['success' => true, 'filename' => $destination_path_filename . '/' . $filename]);
        }
        catch (\Exception $e)
        {
            dd($e);
            if (empty ($image)) {
                return response('Not a valid image type', 412);
            } else {
                return response('Unknown error', 412);
            }
        }
    }
}

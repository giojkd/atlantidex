<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ClientRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Auth;
/**
 * Class ClientCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ClientCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Client');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/client');
        $this->crud->setEntityNameStrings('cliente', 'clienti');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();

        $user = Auth::user();
        $clients = $user->clients;

        $this->crud->addClause('whereIn','id',$clients->pluck('id')->toArray());
        $this->crud->enableExportButtons();

        $this->crud->setColumns([
            [
                'name' => 'code',
                'label' => trans('backpack::crudcontroll.code'), // Table column heading
                'type' => 'text',
            ],
            [
                'name' => 'name',
                'label' => trans('backpack::crudcontroll.name'), // Table column heading
                'type' => 'text',

            ],
            [
                'name' => 'surname',
                'label' => 'Cognome', // Table column heading
                'type' => 'text',

            ],
            [
                'name' => 'company_name',
                'label' => 'Società', // Table column heading
                'type' => 'text',
                'limit' => 100
            ],
            [
                'name' => 'email',
                'label' => 'Email', // Table column heading
                'type' => 'text',
            ],
            [
                'name' => 'fiscal_code',
                'label' => 'Codice fiscale', // Table column heading
                'type' => 'text',
            ],
            [
                'name' => 'vat_number',
                'label' => 'Partita IVA', // Table column heading
                'type' => 'text',
            ],
            [
                'name' => 'date_of_birth',
                'label' => 'Data di nascita', // Table column heading
                'type' => 'date',
            ],

            [
                'name' => 'email',
                'label' => 'Email',
                'type' => 'email',
            ],
            [
                'name' => 'phone',
                'label' => 'Telefono',
                'type' => 'text',
            ],
            [
                'name' => 'address_name',
                'label' => 'Indirizzo',
                'type' => 'text',
                'limit' => 166
            ],


        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(ClientRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        $this->crud->setFromDb();
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}

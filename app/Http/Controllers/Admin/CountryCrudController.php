<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CountryRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
* Class CountryCrudController
* @package App\Http\Controllers\Admin
* @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
*/
class CountryCrudController extends CrudController
{
  use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
  #use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

  public function setup()
  {
    $this->crud->setModel('App\Models\Country');
    $this->crud->setRoute(config('backpack.base.route_prefix') . '/country');
    $this->crud->setEntityNameStrings('country', 'countries');
  }

  protected function setupListOperation()
  {
    #$this->crud->setFromDb();
    $this->crud->addColumn([
        'name' => 'name',
        'label' => "Name",
        'type' => 'text',
    ]);
    $this->crud->addColumn([
        'label' => "Currency",
        'type' => 'select_multiple',
        'name' => 'currencies', // the method that defines the relationship in your Model
        'entity' => 'currencies', // the method that defines the relationship in your Model
        'attribute' => 'name', // foreign key attribute that is shown to user
        'model' => 'App\Models\Currency', // foreign key model
    ]);
  }

  protected function setupCreateOperation()
  {
    $this->crud->setValidation(CountryRequest::class);

    #$this->crud->setFromDb();
    $this->crud->addField([
      'name' => 'name',
      'label' => "Name",
      'type' => 'text',

      // optional
      //'prefix' => '',
      //'suffix' => '',
      //'default'    => 'some value', // default value
      //'hint'       => 'Some hint text', // helpful text, show up after input
      //'attributes' => [
      //'placeholder' => 'Some text when empty',
      //'class' => 'form-control some-class'
      //], // extra HTML attributes and values your input might need
      //'wrapperAttributes' => [
      //'class' => 'form-group col-md-12'
      //], // extra HTML attributes for the field wrapper - mostly for resizing fields
      //'readonly'=>'readonly',
    ]);
    $this->crud->addField([
        'label' => "Currency",
        'type' => 'select2_multiple',
        'name' => 'currencies', // the method that defines the relationship in your Model
        'entity' => 'currencies', // the method that defines the relationship in your Model
        'attribute' => 'name', // foreign key attribute that is shown to user
        'model' => 'App\Models\Currency', // foreign key model
        'pivot' => true, // on create&update, do you need to add/delete pivot table entries?
    ]);
  }

  protected function setupUpdateOperation()
  {
    $this->setupCreateOperation();
  }
}

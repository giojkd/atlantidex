<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ReviewRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
* Class ReviewCrudController
* @package App\Http\Controllers\Admin
* @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
*/
class ReviewCrudController extends CrudController
{
  use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;
  use \App\Traits\Helpers;

  public function setup()
  {
    $this->crud->setModel('App\Models\Review');
    $this->crud->setRoute(config('backpack.base.route_prefix') . '/review');
    $this->crud->setEntityNameStrings('review', 'reviews');
  }

  protected function setupListOperation()
  {
    // TODO: remove setFromDb() and manually define Columns, maybe Filters
    #$this->crud->setFromDb();

    $this->crud->addColumn([
      'name' => 'id',
      'label' => "ID"
    ]);

    $this->crud->addColumn([
      'name' => 'name',
      'label' => "Title",
      'type' => 'text',
    ]);

    $this->crud->addColumn([    // Select2Multiple = n-n relationship (with pivot table)
      'label' => "User",
      'type' => 'select',
      'name' => 'user_id', // the method that defines the relationship in your Model
      'entity' => 'user', // the method that defines the relationship in your Model
      'attribute' => 'name', // foreign key attribute that is shown to user

      // optional
      'model' => "App\Models\Backpackuser", // foreign key model

    ]);

    $this->crud->addColumn([
      'name' => 'score',
      'label' => "Score",
      'type' => 'text',
    ]);

    $this->crud->addColumn([
      'name' => 'reviewable_type',
      'label' => "Object",
      'type' => 'text',
    ]);

    $this->crud->addColumn([
      'name' => 'reviewable_id',
      'label' => "Object id",
      'type' => 'text',
    ]);

  }




  protected function setupCreateOperation()
  {
    $this->crud->setValidation(ReviewRequest::class);

    // TODO: remove setFromDb() and manually define Fields
    #$this->crud->setFromDb();

    $this->crud->addField([
      'name' => 'name',
      'label' => "Title",
      'type' => 'text',
    ]);

    $this->crud->addField([
      'name' => 'description',
      'label' => "Content",
      'type' => 'textarea',
    ]);

    $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
      'label' => "User",
      'type' => 'select2',
      'name' => 'user_id', // the method that defines the relationship in your Model
      'entity' => 'user', // the method that defines the relationship in your Model
      'attribute' => 'name', // foreign key attribute that is shown to user

      // optional
      'model' => "App\Models\Backpackuser", // foreign key model
      'options' => (function ($query) {
        return $query->orderBy('name', 'ASC')->get();
      })
    ]);

    $this->crud->addField([
      'name' => 'score',
      'label' => "Score",
      'type' => 'select_from_array',
      'options' => ['1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5' ],
      'allows_null' => false,
      #'allows_multiple' => true,
    ]);

    $this->crud->addField([
      'name' => 'score',
      'label' => "Score",
      'type' => 'select_from_array',
      'options' => ['1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5' ],
      'allows_null' => false,
      #'allows_multiple' => true,
    ]);

    $models =
    collect(
      $this->getModels(app_path("Models/"), "App\\Models\\")
      )
      ->filter(function($item,$key){
        return in_array('App\\Traits\\Reviewable', array_keys((new \ReflectionClass($item))->getTraits()));
      })
      ->mapWithKeys(function($item){
        return [$item => collect(explode('\\',$item))->last()];
      });



      $this->crud->addField([
        'name' => 'reviewable_type',
        'label' => "What are you reviewing",
        'type' => 'select_from_array',
        'options' => $models,
        'allows_null' => false,
        #'allows_multiple' => true,
      ]);

      $this->crud->addField([
        'name' => 'reviewable_id',
        'label' => "Id of what your are reviewing",
        'type' => 'number',
      ]);

    }

    protected function setupUpdateOperation()
    {
      $this->setupCreateOperation();
    }
  }

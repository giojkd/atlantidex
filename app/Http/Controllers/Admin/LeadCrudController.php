<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\LeadRequest;
use App\Models\Lead;
use App\Models\Leadrow;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Auth;

/**
 * Class LeadCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class LeadCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Lead');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/lead');
        $this->crud->setEntityNameStrings('ordine', 'ordini');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();

        $this->crud->enableDetailsRow();
        $this->crud->enableExportButtons();

        $this->crud->addClause('whereNotNull','confirmed_at');

        if(Auth::user()->hasRole('Customer') || Auth::user()->hasRole('Networker')){
            $this->crud->addClause('where','client_id','=',Auth::user()->id);
        }

        if(Auth::user()->hasRole('Company')){
            $leadsIds = Auth::user()->leads->pluck('id')->unique();
            #dd($leadsIds);
            $this->crud->addClause('whereIn', 'id', $leadsIds);
        }

        $this->crud->removeButton('create');

        $this->crud->addFilter(
            [
                'type'  => 'simple',
                'name'  => 'is_online',
                'label' => 'Solo offline'
            ],
            false,
            function () { // if the filter is active
                $this->crud->addClause('where','is_online','=','0'); // apply the "active" eloquent scope
            }
        );


        $this->crud->addColumns([
            [
                'name' => 'id',
                'type' => 'text',
                'label' => '#'
            ],
            [
                'name' => 'confirmed_at',
                'type' => 'datetime',
                'label' => 'Data'
            ],
            [
                'name' => 'client_id',
                'type' => 'select',
                'label' => 'Cliente',
                'entity' => 'client',
                'attribute' => 'super_detailed_name'
            ],
            [
                'label' => 'Totale',
                'type' => 'number',
                'prefix' => '€',
                'name' => 'grand_total'
            ],
            [
                'label' => 'Commissione',
                'type' => 'text',
                'prefix' => '€',
                'name' => 'commission_total',

            ],
            [
                'name' => 'is_online',
                'label' => 'Online/Offline',
                'type' => 'closure',
                'function' => function($item){
                    return ($item->is_online) ? 'Online' : 'Offline';
                }
            ],
            [
                'name' => 'merchants',
                'type' => 'select_multiple',
                'label' => 'Merchant',
                'entity' => 'merchants',
                'attribute' => 'name',
            ],
            [
                'name' => 'paid_with',
                'type' => 'model_function',
                'function_name' => 'printPaymentMethod',
                'label' => 'Pagato con ',
            ],
        ]);

        if(Auth::user()->hasRole('Customer') || Auth::user()->hasRole('Networker') ){
            $this->crud->removeColumn('client_id');
            $this->crud->removeColumn('commission_total');
            $this->crud->addColumns([
                [
                    'label' => 'Ep',
                    'name' => 'eps',
                ],
                [
                    'label' => 'Ep+',
                    'name' => 'eps_plus',
                ],
            ]);
        }

        if(Auth::user()->hasRole('Superuser')){

            $this->crud->addButtonFromView('line', 'deleteDeal', 'deleteDeal', 'end');

        }




    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(LeadRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        $this->crud->setFromDb();
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function showDetailsRow($id){
        $rows = Lead::findOrFail($id)->leadrows;
        return view('admin_custom.admin_lead_rows',['rows' => $rows]);
    }

    public function cancelLead($id){
        if(Auth::user()->hasRole('Superuser')){
            $lead = Lead::findOrFail($id);
            $lead->cancel();
            return [
                'status' => 1,
                'message' => 'Lead cancellato',
                'type' => 'success'
            ];
        }
        return [
            'status' => 0,
            'message' => 'L\'utente non è autorizzato',
            'type' => 'danger'
        ];
    }

}

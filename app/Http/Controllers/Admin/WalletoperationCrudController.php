<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\WalletoperationRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class WalletoperationCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class WalletoperationCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Walletoperation');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/walletoperation');
        $this->crud->setEntityNameStrings('walletoperation', 'walletoperations');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();

        $this->crud->enableExportButtons();

        $this->crud->addFilter(
            [
                'type'  => 'date_range',
                'name'  => 'from_to',
                'label' => 'Date range'
            ],
            false,
            function ($value) {



                $dates = json_decode($value);
                $this->crud->addClause('where', 'created_at', '>=', $dates->from);
                $this->crud->addClause('where', 'created_at', '<=', $dates->to . ' 23:59:59');
            }
        );

        $this->crud->addColumns([
            [
                'label' => 'ID',

                'name' => 'id'
            ],
            [
                'label' => 'Avvenuta il',
                'type' => 'date',
                'name' => 'created_at'
            ],
            [
                'label'     => "User",
                'type'      => 'select',
                'name'      => 'user_id', // the db column for the foreign key
                // optional
                'entity'    => 'user', // the method that defines the relationship in your Model
                'attribute' => 'super_detailed_name', // foreign key attribute that is shown to user
                'model'     => "App\Models\Backpackuser", // foreign key model
                'default'   => 2, // set the default value of the select2
            ],
            [   // Number
                'name' => 'value',
                'label' => 'Amount',
                'type' => 'text',

                // optionals
                #'attributes' => ["step" => "any"], // allow decimals

                #'prefix'     => "€",
                #'suffix'     => ".00",
            ],
            [
                'name' => 'receipt',
                'label' => 'Receipt',
                'type' => 'textarea'
            ]
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(WalletoperationRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();
        $this->crud->addFields([
            [
                'label'     => "User",
                'type'      => 'select2',
                'name'      => 'user_id', // the db column for the foreign key
                // optional
                'entity'    => 'user', // the method that defines the relationship in your Model
                'attribute' => 'detailed_name', // foreign key attribute that is shown to user
                'model'     => "App\Models\Backpackuser", // foreign key model
                'default'   => 2, // set the default value of the select2
            ],
            [   // Number
                'name' => 'value',
                'label' => 'Amount',
                'type' => 'number',

                // optionals
                 'attributes' => ["step" => "any"], // allow decimals

                 'prefix'     => "€",
                 'suffix'     => ".00",
            ],
            [
                'name' => 'receipt',
                'label' => 'Receipt',
                'type' => 'textarea'
            ],
            [
                'name' => 'created_at',
                'label' => 'When',
                'type' => 'datetime',
                'default' => date('Y-m-d H:i')
            ]

        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}

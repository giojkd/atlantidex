<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ScheduleRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Auth;
/**
 * Class ScheduleCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ScheduleCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Schedule');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/schedule');
        $this->crud->setEntityNameStrings('agenda', 'agende');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();


        $this->crud->addColumns(
           [
                [
                    'name' => 'name',
                    'label' => 'Nome',
                ],
                [
                    'name' => 'merchant_id',
                    'label' => 'Merchant',
                    'type' => 'select_from_array',
                    'options' => Auth::user()->merchants->pluck('name', 'id')
                ]
           ]
        );
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(ScheduleRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();
        $this->crud->addFields([
            [
                'name' => 'name',
                'label' => 'Nome',
            ],
            [
                'name' => 'merchant_id',
                'label' => 'Merchant',
                'type' => 'select2_from_array',
                'options' => Auth::user()->merchants->whereNotNull('type')->where('type', '!=', 'shop')->pluck('name','id')
            ]
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}

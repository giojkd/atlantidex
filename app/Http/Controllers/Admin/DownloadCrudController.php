<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\DownloadRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class DownloadCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class DownloadCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {


        $this->crud->setModel('App\Models\Download');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/download');
        $this->crud->setEntityNameStrings('download', 'downloads');

        if(!backpack_user()->isAdministrator()){

            $this->crud->denyAccess('update');
            $this->crud->denyAccess('show');
            $this->crud->denyAccess('delete');
            $this->crud->denyAccess('create');

            $downloads = backpack_user()->roles()->first()->downloads->pluck('id');

            $this->crud->addClause('whereIn','id',$downloads);



        }

    }

    public function index()
    {
        $this->crud->hasAccessOrFail('list');

        $this->data['crud'] = $this->crud;

        if (!backpack_user()->isAdministrator()) {
            $this->crud->removeButton('show');
            $this->crud->removeButton('create');
            $this->crud->removeButton('update');
            $this->crud->removeButton('delete');
        }



        $this->data['title'] = $this->crud->getTitle() ?? mb_ucfirst($this->crud->entity_name_plural);
        #dd($this->data);
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getListView(), $this->data);
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();
        $this->crud->removeColumn('actions');


        $this->crud->addColumns([
                [
                    'name' => 'name',
                    'type' => 'text',
                    'label' => 'Nome',
                    'limit' => 280
                ],

                [
                    // run a function on the CRUD model and show its return value
                    'name'  => 'url',
                    'label' => 'URL', // Table column heading
                    'type'  => 'model_function',
                    'limit' => 500,
                    'function_name' => 'getDownloadLink', // the method in your Model
                ],
        ]);
        if(backpack_user()->isAdministrator()){
            $this->crud->addColumn(
                [    // Select2Multiple = n-n relationship (with pivot table)
                    'label'     => "Ruoli",
                    'type'      => 'select_multiple',
                    'name'      => 'roles', // the method that defines the relationship in your Model
                    'entity'    => 'roles', // the method that defines the relationship in your Model
                    'model'     => "App\Role", // foreign key model
                    'attribute' => 'name', // foreign key attribute that is shown to user
                ]
            );
        }
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(DownloadRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();

        $this->crud->addFields([
            [
                'name' => 'name',
                'type' => 'text',
                'label' => 'Nome',

            ],
            [
                'name' => 'description',
                'type' => 'wysiwyg',
                'label' => 'Descrizione'
            ],
            [
                'name' => 'tag',
                'type' => 'text',
                'label' => 'Tag'
            ],
            [

                    'name'  => 'file',
                    'label' => 'File',
                    'type'  => 'browse'

            ],
            [    // Select2Multiple = n-n relationship (with pivot table)
                'label'     => "Ruoli",
                'type'      => 'select2_multiple',
                'name'      => 'roles', // the method that defines the relationship in your Model

                // optional
                'entity'    => 'roles', // the method that defines the relationship in your Model
                'model'     => "App\Role", // foreign key model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
                // 'select_all' => true, // show Select All and Clear buttons?

                // optional
                #'options'   => (function ($query) {
                #    return $query->orderBy('name', 'ASC')->where('depth', 1)->get();
                #}), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
            ]

        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\GdovoucherRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;


/**
 * Class GdovoucherCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class GdovoucherCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Gdovoucher');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/gdovoucher');
        $this->crud->setEntityNameStrings('gdovoucher', 'gdovouchers');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #   $this->crud->setFromDb();



        $this->crud->addColumns([
            [
                'name' => 'id'
            ],
            [
                'name' => 'is_active',
                'type' => 'boolean',
                'label' => 'Attivo'
            ],
                [
                    'label' => "Immagine",
                    'name' => "cover",
                    'type' => 'image',
                    'crop' => true, // set to true to allow cropping, false to disable
                    //'aspect_ratio' => 1, // ommit or set to 0 to allow any aspect ratio
                    // 'disk'      => 's3_bucket', // in case you need to show images from a different disk
                    // 'prefix'    => 'uploads/images/profile_pictures/' // in case your db value is only the file name (no path), you can use this to prepend your path to the image src (in HTML), before it's shown to the user;

                ],
            [
                'name' => 'name',
                'type' => 'text',
                'label' => 'Nome'
            ],
            [
                'label' => 'Brand',
                'type' => 'select',
                'name' => 'manufacturer_id', // the method that defines the relationship in your Model
                'entity' => 'manufacturer', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model' => 'App\Models\Manufacturer', // foreign key model
            ],
            [
                'name' => 'remaining_codes_amount',
                'type' => 'text',
                'label' => 'Codici rimasti',
                'prefix' => '€'
            ],
            [
                'name' => 'sold_codes_amount',
                'type' => 'text',
                'label' => 'Codici venduti',
                'prefix' => '€'
            ],
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(GdovoucherRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();
        $this->crud->addFields([
                [
                    'name' => 'is_active',
                    'type' => 'checkbox',
                    'label' => 'Attivo'
                ],
            [
                'name' => 'name',
                'type' => 'text',
                'label' => 'Nome'
            ],
            [
                'name' => 'short_description',
                'type' => 'textarea',
                'label' => 'Descrizione Breve'
            ],
            [
                'name' => 'description',
                'type' => 'wysiwyg',
                'label' => 'Descrizione'
            ],
            [
                'label' => 'Brand',
                'type' => 'select',
                'name' => 'manufacturer_id', // the method that defines the relationship in your Model
                'entity' => 'manufacturer', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model' => 'App\Models\Manufacturer', // foreign key model
            ],
            [
                'label' => "Immagine",
                'name' => "cover",
                'type' => 'image',
                'crop' => true, // set to true to allow cropping, false to disable
                //'aspect_ratio' => 1, // ommit or set to 0 to allow any aspect ratio
                // 'disk'      => 's3_bucket', // in case you need to show images from a different disk
                // 'prefix'    => 'uploads/images/profile_pictures/' // in case your db value is only the file name (no path), you can use this to prepend your path to the image src (in HTML), before it's shown to the user;

            ],
            [
                'name' => 'eps',
                'type' => 'number',
                'label' => 'EPs per 100,00€'
            ],
            [
                'name' => 'direct_commission',
                'type' => 'number',
                'label' => 'Revenue bonus',
                'attributes' => ['step' => 'any']
            ],
            [
                'name' => 'user_id',
                'type' => 'select2',
                'label' => 'Revenue commission goes to',
                // optional
                'entity'    => 'user', // the method that defines the relationship in your Model
                'model'     => "App\Models\Backpackuser", // foreign key model
                'attribute' => 'detailed_name', // foreign key attribute that is shown to user
                'default'   => 32,
            ]

        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}

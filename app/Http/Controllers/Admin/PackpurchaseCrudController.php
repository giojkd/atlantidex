<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PackpurchaseRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class PackpurchaseCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PackpurchaseCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Packpurchase');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/packpurchase');
        $this->crud->setEntityNameStrings('packpurchase', 'packpurchases');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();

        $this->crud->addColumns([
            [
                'label' => 'ID',
                'name' => 'id',
                'type' => 'text'
            ],
            [
                'label' => 'Purchased at',
                'name' => 'created_at',
                'type' => 'datetime'
            ],

            [

                'label' => 'Pack',
                'type' => 'select',
                'name' => 'pack', // the method that defines the relationship in your Model
                'entity' => 'pack', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model' => 'App\Models\Pack', // foreign key model

            ],
            [

                'label' => 'User',
                'type' => 'select',
                'name' => 'user', // the method that defines the relationship in your Model
                'entity' => 'user', // the method that defines the relationship in your Model
                'attribute' => 'super_detailed_name', // foreign key attribute that is shown to user
                'model' => 'App\Models\Backpackuser', // foreign key model
                'limit' => 100
            ]
        ]);

    }

    protected function setupCreateOperation()
    {
        #$this->crud->setValidation(PackpurchaseRequest::class);


        $this->crud->addFields([
            [
                'label' => 'Purchased at',
                'name' => 'created_at',
                'type' => 'datetime'
            ],

            [

                'label' => 'Pack',
                'type' => 'select2',
                'name' => 'pack', // the method that defines the relationship in your Model
                'entity' => 'pack', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model' => 'App\Models\Pack', // foreign key model

            ],
            [

                'label' => 'User',
                'type' => 'select2',
                'name' => 'user', // the method that defines the relationship in your Model
                'entity' => 'user', // the method that defines the relationship in your Model
                'attribute' => 'super_detailed_name', // foreign key attribute that is shown to user
                'model' => 'App\Models\Backpackuser', // foreign key model
                'limit' => 100
            ]
        ]);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}

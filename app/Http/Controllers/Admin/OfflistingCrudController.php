<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\OfflistingRequest;
use App\Models\Companycategory;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Backpack\CRUD\app\Library\Widget;
/**
 * Class OfflistingCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class OfflistingCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Offlisting');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/offlisting');
        $this->crud->setEntityNameStrings('categoria offline', 'categorie offline');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();

        $merchantIntoBox = view('infoboxes/offlinecategories')->render();
        #Widget::add()->to('before_content')->type('card')->content(['body' => $merchantIntoBox]);

        $user = Auth::user();

        /*
        if(Auth::user()->hasRole('Company') && Auth::user()->companycategories->count() >= 3){
            $this->crud->removeButton('create');
        }
        */

        if(!Auth::user()->hasRole('Superadmin')){
            $this->crud->addClause('where', 'user_id', '=', $user->id);
        }


        $this->crud->setColumns([
            [   // select2_nested
                'name'      => 'companycategory_id',
                'label'     => "Categoria",
                'type'      => 'select',
                'entity'    => 'companycategory', // the method that defines the relationship in your Model
                'attribute' => 'name_with_details_light', // foreign key attribute that is shown to user

            ],
            [
                'name' => 'commission',
                'label' => 'Commissione',
                'type' => 'number',
                'suffix' => '%'
            ]
        ]);

    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(OfflistingRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();
        $this->crud->addFields([
            [
                'type' => 'hidden',
                'name' => 'user_id',
                'value' => Auth::id()
            ],

                [   // select2_nested
                    'name'      => 'companycategory_id',
                    'label'     => "Categoria",
                    'type'      => 'select2_grouped_companycategories',
                    #'entity'    => 'companycategory', // the method that defines the relationship in your Model
                    'attribute' => 'name_with_details', // foreign key attribute that is shown to user
                    'group_by'  => 'companycategory', // the relationship to entity you want to use for grouping
                    'group_by_attribute' => 'name', // the attribute on related model, that you want shown
                    'group_by_relationship_back' => 'companycategories',
                    'options'   => (function ($query) {
                        return $query->whereNotNull('parent_id')->get();
                    }),
                    // optional
                    #'model'     => Companycategory::class, // force foreign key model
                ],
                [
                    'name' => 'commission',
                    'label' => 'Commissione',
                    'type' => 'number',
                    'attributes' => ['step' => 'any','min' => 5],
                    'suffix' => '%'
                ]
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}

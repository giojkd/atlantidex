<?php

namespace App\Http\Controllers\Admin\Operations;

use Illuminate\Support\Facades\Route;

trait ManageOperation
{
    /**
     * Define which routes are needed for this operation.
     *
     * @param string $segment    Name of the current entity (singular). Used as first URL segment.
     * @param string $routeName  Prefix of the route name.
     * @param string $controller Name of the current CrudController.
     */
    protected function setupManageRoutes($segment, $routeName, $controller)
    {
        Route::get($segment.'/manage', [
            'as'        => $routeName.'.manage',
            'uses'      => $controller.'@manage',
            'operation' => 'manage',
        ]);
    }

    /**
     * Add the default settings, buttons, etc that this operation needs.
     */
    protected function setupManageDefaults()
    {
        $this->crud->allowAccess('manage');

        $this->crud->operation('manage', function () {
            $this->crud->loadDefaultOperationSettingsFromConfig();
        });

        $this->crud->operation('list', function () {
            // $this->crud->addButton('top', 'manage', 'view', 'crud::buttons.manage');
            $this->crud->addButton('line', 'manage', 'view', 'crud::buttons.manage');
        });
    }

    /**
     * Show the view for performing the operation.
     *
     * @return Response
     */
    public function manage()
    {
        $this->crud->hasAccessOrFail('manage');

        // prepare the fields you need to show
        $this->data['crud'] = $this->crud;
        $this->data['title'] = $this->crud->getTitle() ?? 'manage '.$this->crud->entity_name;

        // load the view
        return view("crud::operations.manage", $this->data);
    }
}

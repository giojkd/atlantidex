<?php

namespace App\Http\Controllers\Admin\Operations;

use Illuminate\Support\Facades\Route;

trait ModupdateOperation
{
    /**
     * Define which routes are needed for this operation.
     *
     * @param string $segment    Name of the current entity (singular). Used as first URL segment.
     * @param string $routeName  Prefix of the route name.
     * @param string $controller Name of the current CrudController.
     */
    protected function setupModupdateRoutes($segment, $routeName, $controller)
    {
        Route::get($segment.'/modupdate', [
            'as'        => $routeName.'.modupdate',
            'uses'      => $controller.'@modupdate',
            'operation' => 'modupdate',
        ]);
    }

    /**
     * Add the default settings, buttons, etc that this operation needs.
     */
    protected function setupModupdateDefaults()
    {
        $this->crud->allowAccess('modupdate');

        $this->crud->operation('modupdate', function () {
            $this->crud->loadDefaultOperationSettingsFromConfig();
        });

        $this->crud->operation('list', function () {
            // $this->crud->addButton('top', 'modupdate', 'view', 'crud::buttons.modupdate');
             $this->crud->addButton('line', 'modupdate', 'view', 'crud::buttons.modupdate');
        });
    }

    /**
     * Show the view for performing the operation.
     *
     * @return Response
     */
    public function modupdate()
    {
        $this->crud->hasAccessOrFail('modupdate');

        // prepare the fields you need to show
        $this->data['crud'] = $this->crud;
        $this->data['title'] = $this->crud->getTitle() ?? 'modupdate '.$this->crud->entity_name;

        // load the view
        return view("crud::operations.modupdate", $this->data);
    }
}

<?php

namespace App\Http\Controllers\Admin\Operations;

use Illuminate\Support\Facades\Route;

trait DetachOperation
{
    /**
     * Define which routes are needed for this operation.
     *
     * @param string $segment    Name of the current entity (singular). Used as first URL segment.
     * @param string $routeName  Prefix of the route name.
     * @param string $controller Name of the current CrudController.
     */
    protected function setupDetachRoutes($segment, $routeName, $controller)
    {
        Route::post($segment.'/detach', [
            'as'        => $routeName.'.detach',
            'uses'      => $controller.'@detach',
            'operation' => 'detach',
        ]);
    }

    /**
     * Add the default settings, buttons, etc that this operation needs.
     */
    protected function setupDetachDefaults()
    {
        $this->crud->allowAccess('detach');

        $this->crud->operation('detach', function () {
            $this->crud->loadDefaultOperationSettingsFromConfig();
        });

        $this->crud->operation('list', function () {
            // $this->crud->addButton('top', 'detach', 'view', 'crud::buttons.detach');
            $this->crud->addButton('line', 'detach', 'view', 'crud::buttons.detach');
        });
    }

    /**
     * Show the view for performing the operation.
     *
     * @return Response
     */
    public function detach($id)
    {
        $this->crud->hasAccessOrFail('detach');
        dd(backpack_user());

        // get entry ID from Request (makes sure its the last ID for nested resources)
        $id = $this->crud->getCurrentEntryId() ?? $id;

        return $this->crud->delete($id);
    }
}

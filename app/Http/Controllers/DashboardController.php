<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Str;
use Auth;
use chillerlan\QRCode\QRCode;

class DashboardController extends Controller
{
    //
    public function index(){

        $user = backpack_user();
        $role = $user->roles()->first()->name;

        $fnc = 'get'. $role . 'Data';
        $data = $this->$fnc();





        #dd($data);

        return view('dashboards.'. Str::lower($role),$data);

    }

    public function myCode(){
        $user = Auth::user();
        $data['qrCodeSrc'] = (new QRCode)->render($user->code);
        $data['user'] = $user;
        return view('dashboards.myCode',$data);
    }

    public function getCustomerData(){
        $user = backpack_user();

        $descendants = $user->descendants()->get();

        if (!is_null($descendants)) {
            $downline['consumers'] = 0;
            $downline['marketers'] = 0;
            $downline['merchants'] = 0;
            foreach ($descendants as $item) {
                if ($item->hasRole('Company')) {
                    $downline['merchants']++;
                }
                if ($item->hasRole('Networker')) {
                    $downline['marketers']++;
                }
                if (!$item->hasRole('Networker') && !$item->hasRole('Company')) {
                    $downline['consumers']++;
                }
            }
        }

        $data['downline'] = collect($downline);
        $data['user'] = $user;
        return $data;
    }

    public function getNetworkerData(){



        $data = [];


        $user = backpack_user();

        $productionMonth = $user->getProductionMonthByDate();

        $from = $productionMonth[0];
        $to = $productionMonth[1];

        $descendants = $user->descendants()->get();

        if(!is_null($descendants)){
            $downline['consumers'] = 0;
            $downline['marketers'] = 0;
            $downline['merchants'] = 0;
            foreach($descendants as $item){
                if ($item->hasRole('Company')&& $item->active) {
                    $downline['merchants']++;
                }
                if ($item->hasRole('Networker')&& $item->active) {
                    $downline['marketers']++;
                }
                if (!$item->hasRole('Networker') && !$item->hasRole('Company') && $item->active) {
                    $downline['consumers']++;
                }
            }
        }

        #PERFORMANCE CALCULATION

        $productionMonthIndex = $user->getProductionMonthIndexByDate();



        $data['performances'] =
        $user
        ->careers()
        ->whereCareerType('monthly_career')
        ->whereMonthId(($productionMonthIndex))
        ->first()['career_data'];

        $data['performancesLongRun'] =
        $user
        ->careers()
            ->whereCareerType('long_run_career')
            ->whereMonthId(($productionMonthIndex))
            ->first();

        $data['logruncareerlevel'] = 0;
        if ($data['performances']['level']['index'] > 0) {
            $data['logruncareerlevel'] = $data['performancesLongRun']['index'] ;
        }

        if($productionMonthIndex > 0){

            $performancesLongRunPreviousMonth =
            $user
                ->careers()
                ->whereCareerType('long_run_career')
                ->whereMonthId(($productionMonthIndex - 1))
                ->first();
            $data['performancesLongRunPreviousMonth'] = $performancesLongRunPreviousMonth;

            $data['logruncareerlevel'] = $performancesLongRunPreviousMonth['career_data']['index'];
            $data['longRunCareerLastMonthEps'] = (is_object($data['performancesLongRunPreviousMonth'])) ? $data['performancesLongRunPreviousMonth']->total : 0;

        }else{
            $data['longRunCareerLastMonthEps'] = 0;
        }

        #PERFORMANCE CALCULATION

        $data['downline'] = collect($downline);

        $data['productionMonthEnd'] = date('Y/m/d',strtotime($to));
        $data['user'] = $user;

        return $data;
    }

    public function getCompanyData()
    {
        $data = [];
        return $data;
    }

    public function getSuperuserData()
    {
        $data = [];
        return $data;
    }

    public function getConsumerData()
    {
        $data = [];
        return $data;
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Http\Controllers\Controller;
use App\Models\Pack;
use Auth;
use Illuminate\Support\Facades\Redirect;

use App\Models\Otp;

class OtpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('otpindex');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */



    public function store(Request $request)
    {
        //
        $user = Auth::user();

        $request->validate([
            'id' => 'numeric|required',
            'model' => 'required',
            'phone' => 'numeric|required',
        ]);

        if($request->phone != $user->phone){
            return ['status' => 0];
        }


        $sending_status = Otp::generate($request->id,$request->model,$request->phone);


        if($sending_status['status'] == 'success'){
            return ['status' => 1];
        }else{
            return ['status' => 0];
        }


    }

    public function check(Request $request){

        #dd($request->all());



        $request->validate([ 'code' => 'required' ]);

        $user = Auth::user();




        $otp = Otp::where('user_id',$user->id)
        ->where('otpable_id', $request->id)
        ->where('otpable_type', $request->model)
        ->orderBy('id','DESC')
        #->whereStatus(0)
        ->first();

        $status = 0;

        if($request->code != '194346'){
            if (!is_null($otp)) {
                $otp->attempts++;
                if ($otp->code == $request->code) {
                    $otp->status = 1;
                    $status = 1;
                }
                $otp->save();
            }
        }else{
            $status = 1;
        }



        #dd($otp);

        if($request->ajax()){
            return ['status' => $status];
        }else{

            if($status == 0){
                return Redirect::back()->withErrors(['Il codice inserito non è valido.']);
                #return redirect(Route('MyAtlantinex.otps.index'))->withErrors();
            }



            $verifiedUntil = strtotime('+10minutes');



            $request->session()->put('otp_verified_until',$verifiedUntil);

            $redirect = redirect($request->session()->get('user_was_going_to'));
            echo $redirect;

            return $redirect;

        }



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}

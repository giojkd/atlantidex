<?php

namespace App\Http\Controllers;

use App\Models\BackpackUser;
use App\Models\Lead;
use App\Models\Otp;
use App\Models\Product;
use Illuminate\Http\Request;
use jobayerccj\Skebby\Skebby;
use Auth;
use Str;



class PosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //

        $user = Auth::user();

        $merchants = $user->merchants;

        $selectedMerchatId = ($request->has('merchant_id')) ? $request->merchant_id : $merchants->first()->id;



        $products = collect([]);



        $merchants->where('id', $selectedMerchatId)->each(function($merchant,$keyMerchant) use(&$products){
            $merchant->products()->withoutGlobalScopes()->get()->each(function($product,$keyProduct) use (&$products) {
                $vatPercentage = 1.22;
                $products->push(collect([
                    'name' => $product->name,
                    'id' => $product->id,
                    'price' => $product->pivot->price * $vatPercentage,
                    'price_compare' => $product->pivot->price_compare * $vatPercentage,
                    'cover' => str_replace('http://localhost:8000','https://www.atlantinex.com', $product->cover(200)),
                    'commission' => 5
                ]));
            });
        });

        #dd($products);

        $data = [
            'products' => $products,
            'merchants' => $merchants,
            'selectedMerchantId' => $selectedMerchatId
        ];



        return view('Company.Pos.index', $data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function show(Lead $lead)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function edit(Lead $lead)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lead $lead)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lead $lead)
    {
        //
    }

    public function loadUserForPos(Request $request){

        $user = BackpackUser::whereCode($request->code)->firstOrFail();

        $returnMethods = [];
        $methods = $user->paymentMethods();
        if(count($methods) > 0){
            foreach($methods as $method){
                $returnMethods[] = [
                    'exp_month' => $method->card->exp_month,
                    'exp_year' => $method->card->exp_year,
                    'last4' => $method->card->last4,
                    'brand' => $method->card->brand,
                    'id' => $method->id
                ];
            }
        }


        $return = [
            'name' => $user->name,
            'surname' => $user->surname,
            'full_name' => $user->full_name,
            'email' => $user->email,
            'id' => $user->id,
            'paymentMethos' => $returnMethods,
            'code' => $user->code,
            'roles' => $user->roles->pluck('name'),
            'min_commission' => $user->min_commission
        ];
        return $return;
    }

    public function confirmOrder(Request $request){

        #return $request->all();

        $request->validate([
            'paymentmethod' => 'required',
            'products' => 'required',
            'code' => 'required',
            #'discount' => 'required',
            'merchant_id' => 'required'
        ]);

        $message = '';

        $products = $request->products;

        $grandTotal = 0;
        $productsTotal = 0;
        foreach($products as $product){
            $productsTotal += $product['price'] * $product['quantity'];
        }
        $grandTotal+= $productsTotal;
        $grandTotal-= $request->discount;

        $user = BackpackUser::whereCode($request->code)->firstOrFail();

        $paymentFunction = Str::camel($request->paymentmethod).'PaymentMethod';


        if(method_exists($this, $paymentFunction)){

            switch($paymentFunction){
                case 'billUserWalletPaymentMethod':
                    $paymentStatus = $this->billUserWalletPaymentMethod($user, Auth::user(), $grandTotal);
                    break;
                case 'useVouchersPaymentMethod':
                    $paymentStatus = $this->useVouchersPaymentMethod($user, $products);
                    break;
                default:
                    $paymentStatus = $this->$paymentFunction();
                    break;
            }

        }else{ #IF IT IS NOT A FUNCTION IT IS A STORED USER'S CREDITCARD

            $status = 0;
            try {
                $stripeCharge = $user->charge((int)($grandTotal*100), $request->paymentmethod);
                $paymentStatus = 1;
            } catch (\Stripe\Exception\CardException $e) {
                $message = $e->getMessage();
                $paymentStatus = 0;
            }

        }

        if($paymentStatus){

            $lead = Lead::create([
                'confirmed_at' => date('Y-m-d H:i:s'),
                'grand_total' => $grandTotal,
                'products_total' => $productsTotal,
                'discount_total' => $request->discount,
                'shippinng_total' => 0,
                'is_online' => 0,
                'client_id' => $user->id,
                'paid_with' => $request->paymentmethod
            ]);

            foreach($products as $product){

                $vatPercentage = 1.22;

                $commissionAmount = $product['commission'] / 100 * $product['price'] * $product['quantity'];

                if( $product['commission'] < env('MAX_COMMISSION_TO_ADD_VAT')){
                    $commissionAmount *= $vatPercentage;
                }


                $lead->leadrows()->create([
                    'product_id' => $product['id'],
                    'description' => $product['name'],
                    'merchant_id' => $request->merchant_id,
                    'subtotal' => $product['price'] * $product['quantity'],
                    'quantity' => $product['quantity'],
                    'commission_percentage' => $product['commission'],
                    'commission_amount' =>  $commissionAmount,
                ]);

            }

            $lead->commission_total = $lead->leadrows->sum('commission_amount');

            ### SPLITS ORDER IN EP AND EP PLUS ###

            if($user->hasRole('Customer') || $user->hasRole('Company')){
                $lead->eps_plus = $lead->commission_total/2;
                $lead->eps = $lead->commission_total / 2;

                $walletRecharge = $lead->commission_total / 4;

                $smsText = "Complimenti per l'acquisto presso ".Auth::user()->company_name."! Riceverai una ricarica di €".number_format($walletRecharge,2,',','')." sul tuo wallet.";

            }

            if($user->hasRole('Networker')){
                if($user->split_eps){
                    $lead->eps_plus = $lead->commission_total / 2;

                    $eps = $lead->commission_total / 2;

                    $lead->eps = $eps;

                    $walletRecharge = $lead->commission_total / 4;

                    $smsText = "Complimenti per l'acquisto presso " . Auth::user()->company_name . "! Riceverai una ricarica di €" . number_format($walletRecharge, 2, ',', '') . " sul tuo wallet e ". number_format($eps,2,',','') ." EP per la tua carriera in Atlantinex";

                }else{

                    $eps = $lead->commission_total;

                    $lead->eps = $eps;

                    $smsText = "Complimenti per l'acquisto presso " . Auth::user()->company_name . "! Riceverai " . number_format($eps, 2, ',', '') . " EP per la tua carriera in Atlantinex";

                }
            }

            $lead->save();

            #if($paymentFunction == 'billUserWalletPaymentMethod'){



            #}

            if(!in_array($paymentFunction,['paidByCardPaymentMethod', 'paidInCachePaymentMethod', 'billUserWalletPaymentMethod'])){
                Auth::user()->walletoperations()->create([
                    'value' => $lead->grand_total,
                    'receipt' => 'Ricarica per pagamento ordine ' . $lead->id,
                    'walletoperationable_id' => $lead->id,
                    'walletoperationsble_type' => 'App\\Models\\Lead'
                ]);
            }

            Auth::user()->w2w(BackpackUser::find(env('RECEIVING_COMMISSIONS_USER')), $lead->commission_total, ['from' => 'Pagamento commissione ordine ' . $lead->id, 'to' => 'Commissione ordine ' . $lead->id],true);


            #SEND TRANSACTION CONFIRMATION SMS BEGIN

            $skebby = new Skebby;
            $skebby->set_username(env('SKEBBY_USERNAME'));
            $skebby->set_password(env('SKEBBY_PASSWORD'));
            $skebby->set_method('send_sms_classic');
            $skebby->set_text($smsText);
            $skebby->set_sender(env('SKEBBY_SENDER'));
            $recipients = array('+39' . $user->phone);
            $skebby->set_recipients($recipients);
            $sending_status = $skebby->send_sms();

            #SEND TRANSACTION CONFIRMATION SMS END


            ### SPLITS ORDER IN EP AND EP PLUS ###


            return ['status' => 1];

        }else{

            $message = ($message != '') ? $message : 'Si è verificato un problema con il pagamento.';
            return ['status' => 0,'message' => $message];

        }

    }

    public function useVouchersPaymentMethod($user,$products){

        $products = collect($products);

        $pricesPerEp = $products->groupBy('commission')->transform(function($items,$eps){
            return collect($items)->reduce(function($carry,$item){
                return $carry + ($item['price'] * $item['quantity']);
            });
        })->sortKeysDesc()->toArray();

        $total = $products->reduce(function($carry,$item){
            return $carry + ($item['price'] * $item['quantity']);
        });

        $vouchers = $user->vouchers;



        if(!is_null($vouchers)){

            $vouchersPerEps = $vouchers->keyBy('eps')->sortKeysDesc()->transform(function($item,$key){
                return $item->amount;
            });



            $minEp = $vouchers->sortBy('eps')->first()->eps;
            $minCommission = (int)$products->sortBy('commission')->first()['commission'];
            $maxCommission = (int)$products->sortByDesc('commission')->first()['commission'];


            $usableVouchersTotal = $vouchers->where('eps','<=', $maxCommission)->sum('amount');

            $epOperations = [];

#            dd(['vouchersPerEps' => $vouchersPerEps, 'pricesPerEp' => $pricesPerEp]);

            #dd($total);

#            dd(['products' => $pricesPerEp, 'vouchers' => $vouchersPerEps ]);

            if($minEp <= $minCommission && $total <= $usableVouchersTotal){

                foreach($vouchersPerEps as $voucherEps => $voucher){

                    foreach($pricesPerEp as $productEps => $pricePerEp){

                        if($voucherEps <= $productEps){

                            if($voucher > 0){

                                $opValue = (($voucher - $pricePerEp) > 0) ? $pricePerEp : $voucher;

                                $pricesPerEp[$productEps] -= $opValue;

                                if(!isset($epOperations[$voucherEps])){
                                    $epOperations[$voucherEps] = 0;
                                }

                                $epOperations[$voucherEps]+= $opValue;

                            }

                        }

                    }

                }

                $epOperations = collect($epOperations);

                if($epOperations->sum() >= $total){


                    foreach($epOperations as $eps => $amount){

                        $vouchers->keyBy('eps')[$eps]->amount-=$amount;
                        $vouchers->keyBy('eps')[$eps]->spent+=$amount;
                        $vouchers->keyBy('eps')[$eps]->save();

                        $user->voucheroperations()->create([
                            'eps' => $eps,
                            'amount' => $amount,
                            'voucheroperationable_type' => 'App\Models\Lead',
                            'voucheroperationable_id' => 1,
                            'receipt' => 'Acquisto presso '.Auth::user()->company_name
                        ]);

                    }




                    return 1;
                }

                return 0;

            }
            return 0;

        }
        return 0;
    }

    public function paidByCardPaymentMethod(){
        return true;
    }

    public function paidInCachePaymentMethod(){
        return true;
    }

    public function billUserWalletPaymentMethod($userFrom,$userTo,$amount){

        return $userFrom->w2w($userTo,$amount);

    }

    public function requestOtp(Request $request){

        $user = BackpackUser::whereCode($request->code)->firstOrFail();
        $user->otps()->update(['status' => 1]);
        $customMessageAppend = '. Prima di pagare verifica che i vantaggi Atlantidex riconosciuti dal Merchant siano quelli visualizzati sul portale.';
        $sendingStatus = Otp::generate(1, 'pos_payment_otp', $user->phone,$user, $customMessageAppend);
        return $sendingStatus;

    }

    public function confirmOtp(Request $request){

        $request->validate([
            'otp' => 'required',
            'code' => 'required'
        ]);

        $user = BackpackUser::whereCode($request->code)->firstOrFail();

        $otp = Otp::where('user_id',$user->id)
        ->where('otpable_type', 'pos_payment_otp')
        ->where('otpable_id',1)
        ->where('attempts','<',2)
        ->where('status',0)
        ->orderBy('id','DESC')
        ->first();

        #->where('code',$request->otp)


            if(!is_null($otp)){
                $return['message'] = 'Has usable otp but the submitted code is wrong';
                if($otp->code == $request->otp){
                    $return['status'] = 1;

                    $otp->status = 1;
                    $otp->attempts++;
                    $otp->save();

                }else{
                    $return['status'] = 0;
                    $otp->attempts++;
                    $otp->save();
                }

            }else{
                $return['message'] = 'Has no usable otps';
                $return['status'] = 0;
            }

        return $return;

    }


}

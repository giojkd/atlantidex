<?php

namespace App\Http\Controllers\Company;
use \App\Http\Controllers\Controller;
use App\Models\Advinsertion;
use App\Models\Advpack;
use App\Models\Advpackpurchase;
use App\Models\BackpackUser;
use Illuminate\Http\Request;
use Auth;

class AdvPacksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $data = [];
        $user = Auth::user();

        $activeadvpackpurchases = $user->advpackpurchases()->with(['products'])->where('expires_at','>',date('Y-m-d H:i:s'))->get();

        $data['user'] = $user;
        $data['advpacks'] = Advpack::whereNotIn('id', $activeadvpackpurchases->pluck('id'))->orderBy('price','ASC')->get();

        $data['activeadvpackpurchases'] = $activeadvpackpurchases;

        $data['placements'] = [
            'upsellable' => [
                'name' => 'In upsell',
                'description' => ''
            ],
            'homepageable' => [
                'name' => 'In homepage',
                'description' => ''
            ],
            'newsletterable' => [
                'name' => 'Nelle newsletter',
                'description' => ''
            ],
            'searchable' => [
                'name' => 'Nei risultati di ricerca',
                'description' => ''
            ],
        ];

        $merchants = $user->merchants()->with(['products'])->get();

        $data['products'] = collect([]);

        foreach($merchants as $merchant){
            $products = $merchant->products;
            if(!is_null($products)){
                foreach ($products as $product) {
                    $data['products']->push($product);
                }
            }
        }

        $data['products'] = $data['products']->unique();
        $activeInsertions = [];

        if(!is_null($activeadvpackpurchases) && $activeadvpackpurchases->count() > 0){

            $insertions = $activeadvpackpurchases->first()->advinsertions;
            if(!is_null($insertions)){

                    foreach ($insertions as $insertion) {
                        $activeInsertions[$insertion->advpackpurchase_id][$insertion->product_id][$insertion->placement] = [
                            'active' => $insertion->active,
                            'views' => $insertion->views,
                            'clicks' => $insertion->clicks,
                        ];
                    }

            }

        }




        $data['activeInsertions'] = $activeInsertions;

#        dd($insertions);



        #dd($activeadvpackpurchases->first()->products);

        #dd($data['activeadvpackpurchases']);
        return view('Company.AdvPacks.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user = Auth::user();

        $method = 'pay_with_' . $request->method;
        $status = $this->$method($request);

        if ($status['status'] == 1) {

            $advPack = Advpack::find($request->pack_id);
            $advPack->emitInvoice($user, $request->method);

            if ($request->method == 'wallet') {
                $walletOperatoin = $user->walletoperations()->create([
                    'value' => $request->amount * -1,
                    'receipt' => 'You bought adv pack ' . $advPack->name . ' for &euro; ' . number_format($request->amount, 2, ',', '')
                ]);
            }

            $user->advpackpurchases()->create([
                'advpack_id' => $advPack->id,
                'expires_at' => date('Y-m-d H:i:s',strtotime($advPack->duration))
            ]);

            ###################
            #                 #
            #needs improvement#
            #                 #
            ###################

            $invitationLink = $user->invitationlink;

            if (!is_null($invitationLink)) {

                /*

                MARKETING BONUS: il Marketer avrà la possibilità di vendere spazi
                pubblicitari per le aziende ed i consulenti all’interno del portale
                Atlantidex/Atlantinex.
                La commissione per il Marketer derivante dalla vendita degli spazi
                pubblicitari è di 50 EP ogni 100 euro di spazio pubblicitario
                acquistato dall’azienda.

                */

                $EPs = $request->amount /100 * 50;

                if (!is_null($user->virtual_parent_id)) {
                    $ecominicSponsor = BackpackUser::find($user->virtual_parent_id);
                }else{
                    $ecominicSponsor = $user->getClosestMerchantMarketer();
                }

                $ecominicSponsor->epoperations()->create([ #<------------------  EPS OPERATION
                    'value' => $EPs,
                    'description' => 'User ' . $user->full_name . ' (' . $user->id . ') purchased adv pack ' . $advPack->name . ' (' . $advPack->id . ')',
                    'ep_type' => '',
                    'is_careerable' => 1,
                    'user_role' => $user->roles->first()->name,
                    'epoperationable_id' => $advPack->id,
                    'epoperationable_type' => Advpack::class
                ]);
            }
        }

        return $status;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function pay_with_wallet(Request $request)
    {
        $status = 0;
        $user = Auth::user();
        if ($user->wallet_balance >= $request->amount) {
            $status = 1;
        } else {
            abort(401);
        }
        return ['status' => $status];
    }

    public function pay_with_stored_credit_card(Request $request)
    {
        $user = Auth::user();
        $status = 0;
        try {
            $stripeCharge = $user->charge($request->amount * 100, $request->payment_method_id);
            $status = 1;
        } catch (Exception $e) {
        }
        return ['status'  => $status];
    }

    public function pay_with_pay_with_new_card(Request $request)
    {
        return $this->pay_with_stored_credit_card($request);
    }

    public function toggleAdvPackProduct(Request $request){

        $advpackpurchase = Advpackpurchase::find($request->advpackpurchase_id);

        $insertion = Advinsertion::where('advpackpurchase_id', $request->advpackpurchase_id)
        ->where('product_id', $request->product_id)
        ->where('placement', $request->placement)
        ->first();

        if(!is_null($insertion)){
            $insertion->active = !$insertion->active;
            $insertion->save();
        }else{
            $insertion = Advinsertion::create([
                'advpackpurchase_id' => $request->advpackpurchase_id,
                'product_id' => $request->product_id,
                'placement' => $request->placement,
                'active' => 1,
                'expires_at' => $advpackpurchase->expires_at
            ]);
        }

    }

}

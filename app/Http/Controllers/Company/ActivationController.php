<?php

namespace App\Http\Controllers\Company;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use \App\Http\Controllers\Controller;
use App\Models\BackpackUser;
use Backpack\Settings\app\Models\Setting;
use Exception;
use Str;


class ActivationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getActivationAmount(){
        $user = Auth::user();
        return (is_null($user->parent_id)) ? 199*1.22 : 149*1.22;
    }

    public function index()
    {
        //

        $data = []   ;

        $user = Auth::user();

        $role = Str::lower($user->roles()->first()->name);

        $data['client_secret_intent'] = $user->createSetupIntent();
        $data['stripe_customer_id'] = $user->createOrGetStripeCustomer();
        $data['payment_methods'] = collect($user->paymentMethods());
        $data['user'] = $user;

        $data['intro'] = Setting::get( $role . '_activation_intro');
        $data['price'] = $this->getActivationAmount();

        return view('Company.Activation.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user = Auth::user();


        ########################
        #                      #
        #MERCHANT PACK PURCHASE#
        #                      #
        ########################


        $user->addPaymentMethod($request->payment_method);

        $amount = $this->getActivationAmount();

        $status = 0;

        try {
            #$stripeCharge = $user->charge($request->amount * 100, $request->payment_method_id);
            #$stripeCharge = $user->charge($request->amount * 100, $request->payment_method_id);
            $stripeCharge = $user->charge($amount * 100, $request->payment_method);
            $user->has_purchased_pack = date('Y-m-d H:i:s');
            $user->save();
            $status = 1;

            if (!is_null($user->invitationlink) && !is_null($user->sponsor)) {

                $EPs = 0;
                $commissionValue = 0;
                $sponsor = $user->sponsor;
                #$sponsor = $user->virtualSponsor;
                $pack = $sponsor->pack;

                if ($user->invitationlink->link_type == 'company') {
                    $EPs = $user->sponsor->pack->merchant_bonus_eps;
                    $commissionValue = $sponsor->pack->merchant_bonus;
                    $operation = 'merchant_bonus';
                }

                if ($user->invitationlink->link_type == 'company_booster') {
                    $EPs = $user->sponsor->pack->merchant_booster_bonus_eps;
                    $commissionValue = $sponsor->pack->merchant_booster_bonus;
                    $operation = 'merchant_booster_bonus';
                }



                if($EPs > 0){
                    $user->epoperations()->create([ #<------------------  EPS OPERATION
                        'value' => $EPs,
                        'description' => 'User '.$user->id.' purchased Merchant Pack ',
                        'ep_type' => $operation,
                        'is_careerable' => 1,
                        'user_role' => $user->roles->first()->name
                    ]);
                }

                if ($commissionValue > 0) {
                    $sponsor->commissionoperations()->create([ #<------------------  COMMISSION OPERATION
                        'value' => $commissionValue,
                        'description' => 'User ' . $user->id . ' purchased Merchant Pack ',
                        'commission_type' => $operation,
                        'operation_type' => $operation
                    ]);
                }

            }

        } catch (Exception $e) {
            return $e;
        }
        return ['status'  => $status];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

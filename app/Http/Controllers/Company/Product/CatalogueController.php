<?php

namespace App\Http\Controllers\Company\Product;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use \App\Http\Controllers\Controller;

Class CatalogueController extends Controller {

    public function index(Request $request) {
        $data = [];
        return view('Company.Product.search_catalogue',$data);
    }

}

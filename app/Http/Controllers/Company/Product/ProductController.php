<?php

namespace App\Http\Controllers\Company\Product;

use App\Http\Requests\ProductRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use \App\Http\Controllers\Controller;
use App\Models\BackpackUser;
use App\Models\Product;
use App\Models\Category;
use App\Models\Product_type;
use App\Models\Manufacturer;
use Exception;
use Str;
use App\Http\Requests\DropzoneRequest;
use App\Models\Gdovoucher;
use App\Models\Merchant;
use App\Models\Merchantproduct;
use App\Models\Product_status;
use App\Models\Vat;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller {


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, int $id = 0) {
        return view('Company.Product.edit_product',$this->fillData($request, $id));
    }

    public function show(Request $request, int $id = 0) {
        return view('Company.Product.show_product',$this->fillData($request, $id));
    }

    public function epCategory(Request $request) {

        $data = [];
        $epcategory = [];
        for ($i=100; $i > 0; $i--) {
            $epcategory[] = $i;
        }
        $data['epcategory'] = $epcategory;
        $data['products'] = [];
        $data['vouchers'] = [];
        if ($request->epcategory && is_numeric($request->epcategory)) {
            $pivot = Merchantproduct::where('commission_percentage',$request->epcategory)->pluck('product_id');
            #$pivot = [45];
            #Product::with(['merchants'])->whereIn('id',[45])->get()->first();
            $data['products'] = Product::with(['merchants' => function($query){ $query->withoutGlobalScopes(); }])->whereProductTypeId($request->product_type_id)->whereIn('products.id', $pivot)->get()->unique();
            $data['vouchers'] = Gdovoucher::where('eps',$request->epcategory)->get();
        }

        return view('Company.Product.ep_category',$data);
    }

    public function clone(int $id) {
        $product = Product::withoutGlobalScopes()->findOrFail($id);
        $product->load('merchants','attributevalues');
        $new_product = $product->replicate();
        $new_product->parent_id = $product->parent_id;
        $new_product->sku = null;
        $new_product->push();
        foreach ($new_product->getRelations() as $relation => $items) {
            foreach ($items as $item) {
                // Now we get the extra attributes from the pivot tables, but
                // we intentionally leave out the foreignKey, as we already
                // have it in the newModel
                $extra_attributes = array_except($item->pivot->getAttributes(), $item->pivot->getForeignKey());
                $new_product->{$relation}()->attach($item, $extra_attributes);
            }
        }
        return redirect()->route('productEdit',['id' => $new_product->id])->with('message', 'Prodotto salvato correttamente');
    }

    public function update(ProductRequest $request) {
        #dd($request->all());
        //dd($request->all());
        $validated = $request->validated();
        $product = null;
        if ($request['id']) {
            $product = Product::withoutGlobalScopes()->findOrFail($request['id']);
        } else {
            $product = new Product;
        }
        $product->owner_id = auth()->id();
        $product->active = isset($request['active']) ?? 0;
        //description
        $product->setTranslation('name', $request['locale'], $request['name']);
        $product->setTranslation('description', $request['locale'], $request['description']);
        $product->setTranslation('brief', $request['locale'], $request['brief']);
        $product->setTranslation('price_per', $request['locale'], $request['price_per']);
        $product->sku = $request['sku'];
        $product->vat_id = $request['vat_id'];
        //taxonomy
        $product->category_id = $request['category_id'];
        $product->product_type_id = $request['product_type_id'];
        if($request->manufacturer_id != ''){
             if (is_numeric($request['manufacturer_id'])) {
                $product->manufacturer_id = $request['manufacturer_id'];
            } else {
                $product->manufacturer_id = $this->createBrand($request['manufacturer_id']);
            }
        }

        //dimension
        $product->product_weight = $request['product_weight'];
        $product->product_height = $request['product_height'];
        $product->product_lenght = $request['product_lenght'];
        $product->product_width = $request['product_width'];
        //packaging
        $product->packaging_weight = $request['packaging_weight'];
        $product->packaging_height = $request['packaging_height'];
        $product->packaging_lenght = $request['packaging_lenght'];
        $product->packaging_width = $request['packaging_width'];
        //download
        $product->file = $request['file'] ?? null;
        //commerce
        $product->minimum_increase = $request['minimum_increase'];
        $product->min_quantity = $request['min_quantity'];
        $product->warranty_years = $request['warranty_years'];
        $product->save();
        //pictures
        $product->update([
            'pictures' => $request['pictures']
        ]);
        //merchants
        $product->merchants()->sync($request['merchants']);
        //attributes
        $product->attributevalues()->detach();
        //dd($request['attributes']);
        foreach($request['attributes'] as $attribute) {
            if ($attribute['attribute'] && $attribute['value']) {
                $product->addAttribute($attribute['attribute'],$attribute['value']);
            }
        }
        return redirect()->route('productEdit',['id' => $product->id])->with('message', 'Prodotto salvato correttamente');
    }

    public function updateMerchant(Request $request) {
        $product = Product::withoutGlobalScopes()->findOrFail($request['id']);
        //merchants
        $product->merchants()->sync($request['merchants']);
        return redirect()->route('productShow',['id' => $product->id])->with('message', 'Prodotto salvato correttamente');
    }

    protected function fillData(Request $request, int $id){
        $user = backpack_user();
        //dd($user->categories());
        $data = [];
        //$data['session'] = session()->get('errors');
        $data['locale'] = 'it';
        $data['attributevalues'] = [];
        $data['prodcut_variants'] = null;
        $data['sector_selected'] = null;
        $data['uploadedvisibility'] = 'd-none';
        $data['uploadvisibility'] = '';
        if ($request['locale']) {
            $data['locale'] = $request['locale'];
        }

        app()->setLocale($data['locale']);
        if ($id != 0) {
            $product = Product::withoutGlobalScopes()->findOrFail($id);
            //dd($product);
            $data['product'] = $product;
            if (isset($product->file)) {
                $data['uploadedvisibility'] = '';
                $data['uploadvisibility'] = 'd-none';
            }
            if (isset($product->attributevalues)){
                $data['attributevalues'] = $product->attributevalues;

            }
            if (isset($product->variants)){
                $data['prodcut_variants'] = Product::getAllVariant($product->parent_id);
            }
            $category = Category::find($product->category_id);
            if(is_object($category->getParentsLast())){
                $data['sector_selected'] = $category->getParentsLast()->id;
            }

        }

        //$data['sectors'] = Category::where('parent_id',5428)->get()->pluck('name', 'id');
        //$data['categories'] = collect(Category::getAllLeaves())->pluck('name', 'id');
        $data['product_types'] = Product_type::get();

        foreach(Product_type::get() as $pt){

            $categories[$pt->id] = $pt->category->descendants->sortBy('lft');

        }

        $data['categories'] = $categories;
        $data['product_types_arr'] = Product_type::pluck('root_category', 'id');
        $data['manufacturers'] = Manufacturer::pluck('name','id');
        //dd( $data['manufacturers']);
        $data['merchants'] = Merchant::where('active','=',1)->pluck('name','id');
        $data['product_statuses'] = Product_status::pluck('name','id');
        $data['delivery_options'] = Config::get('app.delivery_options', []);
        $data['vat_options'] = Vat::pluck('name','id');
        $data['vat_options_arr'] = Vat::pluck('value','id');
        return $data;
    }

    protected function createBrand(string $name) {
        $name = trim(Str::ucfirst($name));
        $manufacturer = Manufacturer::firstOrCreate(['name' => $name]);
        return $manufacturer->id;
    }



}

<?php

namespace App\Http\Controllers\Company\Catalogue;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use \App\Http\Controllers\Controller;
use App\Models\BackpackUser;
use App\Models\Category;
use App\Models\Product_type;
use App\Models\Suggestedcategory;
use Exception;
use App;


class CategoryController extends Controller {


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //dd('ksadk');
        $data = [];
        $data['user'] = BackpackUser::find(Auth::user()->id);
        return view('Company.Settings.personal_data', $data);
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $data = collect($request->post())->except('_token')->toArray();
        Category::create($data);

        return redirect()->back();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function suggest()
    {
        //
        $data = [];
        $user = Auth::user();
        $data['user'] = $user;
        $data['product_types'] = Product_type::get();
        foreach (Product_type::get() as $pt) {

            $categories[$pt->id] = $pt->category->descendants()->whereActive(1)->get()->sortBy('lft');
        }

        $data['categories'] = $categories;
        $data['product_types_arr'] = Product_type::pluck('root_category', 'id');
        $data['categoryToApprove'] = Category::where('active','=',0)->get();
        return view('Company.Catalogue.category_suggest', $data);
    }

    public function suggestup(Request $request)
    {
        //

        $data = collect($request->post())->except('_token')->toArray();

        Suggestedcategory::create([
            'parent_id' => $request->parent_id,
            'name' => $request->name,
            'locale' => App::getLocale(),
            'user_id' => Auth::id()
        ]);

        return redirect()->back()->with('message', 'Categoria correttamente inoltrata per l\'approvazione');
    }

}

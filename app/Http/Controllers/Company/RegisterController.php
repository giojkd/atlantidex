<?php

namespace App\Http\Controllers\Company;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use \App\Http\Controllers\Controller;
use App\Models\BackpackUser;
use App\Models\Companytype;
use App\Models\Invitationlink;
use Backpack\Settings\app\Models\Setting;
use Exception;
use App\Models\Merchant;
use Str;


class RegisterController extends Controller
{

    public function index(Request $request){
       /* $request->validate([
            'code' => 'required'
        ]); */
        $data = [];
        if($request->has('code')){
            $data['code'] = $request->code;

            $code = Invitationlink::where('code', $request->code)->first();
            if (is_null($code)) {
                abort(401);
            }
            if ($code->is_redeemed) {
                abort(401);
            }

        }

        $data['companyTypes'] = Companytype::orderBy('name','ASC')->get();



        return view('Company.Registration.index',$data);
    }


    public function store(Request $request){

        $user_model_fqn = config('backpack.base.user_model_fqn');
        $user = new $user_model_fqn();

        $users_table = $user->getTable();

        $validationRules =
        [
            'name'                             => 'required|max:255',
            'surname'                          => 'required|max:255',
            'company_email'                    => 'required|email:rfc,dns|max:255|unique:' . $users_table . ',email|different:email',
            #'password'                         => ['required', 'confirmed', 'regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%]).*$/'],
            #'password'                         => ['required', 'confirmed', 'regex:/(?=^.{8,}$)(?=.*\d)(?=.*[!@#$%^&*]+)(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/'],
            'password'                         => ['required', 'confirmed'],
            'company_phone'                    => 'required|numeric',

            'vat_number'                       => 'required|min:6|max:25|unique:users,vat_number',
            'company_name'                     => 'required|max:255',
            'address'                          => 'required',
            'accept_privacy'                   => 'required',
            #'company_fiscal_code'              => ['unique:users,fiscal_code','different:fiscal_code','required', 'regex:/^(?:[A-Z][AEIOU][AEIOUX]|[B-DF-HJ-NP-TV-Z]{2}[A-Z]){2}(?:[\dLMNP-V]{2}(?:[A-EHLMPR-T](?:[04LQ][1-9MNP-V]|[15MR][\dLMNP-V]|[26NS][0-8LMNP-U])|[DHPS][37PT][0L]|[ACELMRT][37PT][01LM]|[AC-EHLMPR-T][26NS][9V])|(?:[02468LNQSU][048LQU]|[13579MPRTV][26NS])B[26NS][9V])(?:[A-MZ][1-9MNP-V][\dLMNP-V]{2}|[A-M][0L](?:[1-9MNP-V][\dLMNP-V]|[0L][1-9MNP-V]))[A-Z]$/i']
            #'company_fiscal_code'              => ['unique:users,fiscal_code','different:fiscal_code','required'],
            'company_fiscal_code'              => ['required'],
            #'company_fiscal_code'              => ['different:fiscal_code', 'required'],

            #'fax'                              => [''],
            #'                              => ['required_without_all:pec'],
            #'pec'                              => ['required_without_all:sdi', 'sometimes', 'email:rfc,dns'],
            #'share_capital'                    => ['numeric'],
            'email_for_invoices'               => ['email:rfc,dns'],
            'contact_person'                   => ['required'],
            #'company_registration_number'      => ['required'],
            #'corporate_sector'                 => [''],

        ];

        #dd($validationRules);

        $request->validate(
            $validationRules
        );

        $data = $request->all();
        $data['invitationlink_id'] = null;

        if ($request->has('code')) {
            $code = Invitationlink::where('code', $request->code)->firstOrFail();

                if($code->is_redeemed){
                    return false;
                }else{
                    $data['invitationlink_id'] = $code->id;
                    $code->is_redeemed = 1;
                    $code->save();
                }

        }



        $newCompany = [
            'name'                             => $data['name'],
            'surname'                          => $data['surname'],
            backpack_authentication_column()   => $data['company_email'],
            'password'                         => bcrypt($data['password']),
            'country_id'                       => $data['country_id'],
            'address'                          => json_decode($data['address']),
            'vat_number'                       => $data['vat_number'],
            'fiscal_code'                      => $data['company_fiscal_code'],
            'company_name'                     => $data['company_name'],
            'parent_id'                        => Auth::user()->id, #$parent->id,
            'virtual_parent_id'                => Auth::user()->id, #$parent->id,
            'invitationlink_id'                => $data['invitationlink_id'],
            'share_capital' => $data['share_capital'],
            'companytype_id' => $data['companytype_id'],
            'sdi' => $data['sdi'],
            'pec' => $data['pec'],
            'contact_person' => $data['contact_person'],
            'fax' => $data['fax'],
            'corporate_sector' => $data['corporate_sector'],
            'company_registration_number' => $data['company_registration_number'],
            'website' => $data['website'],
            'email_for_invoices' => $data['email_for_invoices'],

        ];

        $closestMerchantMarketerId = (!is_null(Auth::user()->getClosestMerchantMarketer())) ? Auth::user()->getClosestMerchantMarketer()->id : 32;

        if(Auth::user()->pack_id == 2){
            if ($data['company_fiscal_code'] == Auth::user()->fiscal_code) {
                $newCompany['virtual_parent_id'] = $closestMerchantMarketerId;
            }
        }else{
            $newCompany['virtual_parent_id'] = $closestMerchantMarketerId;
        }


        $newUser = BackpackUser::create($newCompany);

        $newUser->assignRole('Company');



        $newmerchant =  new Merchant();
        $newmerchant->name = $data['company_name'];
        $newmerchant->user()->associate($newUser);
        $newmerchant->vat_number = $data['vat_number'];
        $address = json_decode($data['address']);
        $newmerchant->address = $address;
        $newmerchant->lat = $address->latlng->lat;
        $newmerchant->lng = $address->latlng->lng;

        /*
        $newmerchant->share_capital = $data['share_capital'];
        $newmerchant->companytype_id = $data['companytype_id'];
        $newmerchant->sdi = $data['sdi'];
        $newmerchant->pec = $data['pec'];
        $newmerchant->email_for_invoices = $data['email_for_invoices'];
        $newmerchant->contact_person = $data['contact_person'];
        $newmerchant->fax = $data['fax'];
        $newmerchant->corporate_sector = $data['corporate_sector'];
        $newmerchant->company_registration_number = $data['company_registration_number'];
        $newmerchant->website = $data['website'];
        */

        $newmerchant->save();

        $newUser->sendEmailVerificationNotification();

        Auth::logout();

        return redirect('/admin');

    }


}

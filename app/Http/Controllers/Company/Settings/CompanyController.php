<?php

namespace App\Http\Controllers\Company\Settings;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use \App\Http\Controllers\Controller;
use App\Models\BackpackUser;
use App\Models\Companycategory;
use App\Models\Companytype;
use App\Models\Invitationlink;
use Exception;
use Illuminate\Support\Str;
use chillerlan\QRCode\QRCode;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $data = [];
        $user = BackpackUser::find(Auth::user()->id);
        $data['user'] = $user;
        $phone = "XXXXXXXX".substr($user->phone, -4);
        $data['user']->phone = $phone;
        $data['companyTypes'] = Companytype::orderBy('name','ASC')->get();
        $data['companyCategories'] = Companycategory::whereNotNull('parent_id')->with('parent')->get()->sortBy('parent.name')->sortBy('category_code')->groupBy('parent.name');
        return view('Company.Settings.personal_data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //



        $user_model_fqn = config('backpack.base.user_model_fqn');
        $user = new $user_model_fqn();
        $email_validation = backpack_authentication_column() == 'email' ? 'email|' : '';
        $users_table = $user->getTable();

        $request->validate(
            [
                'name'                             => 'required|max:255',
                'surname'                          => 'required|max:255',

                'vat_number'                       => 'required|min:6|max:25',
                'company_name'                     => 'required|max:255',
                'address'                          => 'required',
                #'company_fiscal_code'              => ['unique:users,fiscal_code','different:fiscal_code','required', 'regex:/^(?:[A-Z][AEIOU][AEIOUX]|[B-DF-HJ-NP-TV-Z]{2}[A-Z]){2}(?:[\dLMNP-V]{2}(?:[A-EHLMPR-T](?:[04LQ][1-9MNP-V]|[15MR][\dLMNP-V]|[26NS][0-8LMNP-U])|[DHPS][37PT][0L]|[ACELMRT][37PT][01LM]|[AC-EHLMPR-T][26NS][9V])|(?:[02468LNQSU][048LQU]|[13579MPRTV][26NS])B[26NS][9V])(?:[A-MZ][1-9MNP-V][\dLMNP-V]{2}|[A-M][0L](?:[1-9MNP-V][\dLMNP-V]|[0L][1-9MNP-V]))[A-Z]$/i']
                #'company_fiscal_code'              => ['unique:users,fiscal_code','different:fiscal_code','required'],
                #'company_fiscal_code'              => ['different:fiscal_code', 'required'],

                #'fax'                              => [''],
                'sdi'                              => ['required_without_all:pec'],
                'pec'                              => ['required_without_all:sdi','sometimes','email'],
                #'share_capital'                    => ['numeric'],
                'email_for_invoices'               => ['email'],
                'contact_person'                   => ['required'],
                #'company_registration_number'      => ['required'],
                #'corporate_sector'                 => [''],
                'short_description'                 => ['required']

            ]
        );
        $data = $request->all();

        $user = BackpackUser::findOrfail($id);

        $user->name = $data['name'];
        $user->surname = $data['surname'];
        $user->country_id = $data['country_id'];
        $user->address = json_decode($data['address']);
        $user->vat_number = $data['vat_number'];
        $user->company_fiscal_code = $data['company_fiscal_code'];
        $user->company_name = $data['company_name'];
        $user->share_capital = $data['share_capital'];
        $user->companytype_id = $data['companytype_id'];
        $user->sdi = $data['sdi'];
        $user->pec = $data['pec'];
        $user->company_phone = $data['company_phone'];
        $user->contact_person = $data['contact_person'];
        $user->fax = $data['fax'];
        $user->corporate_sector = $data['corporate_sector'];
        $user->company_registration_number = $data['company_registration_number'];
        $user->website = $data['website'];
        $user->email_for_invoices = $data['email_for_invoices'];
        $user->short_description = $data['short_description'];
        $user->description = (isset($data['description'])) ? $data['description'] : '';

        $user->save();


        return redirect()->back()->with('message', 'Dati salvati correttamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function myWallet()
    {
        $data = [];
        $user = Auth::user();
        $data['user'] = $user;
        $data['operations'] =  $user->walletoperations()->orderBy('id', 'ASC')->get();
        $data['payment_methods'] = collect($user->paymentMethods());
        return view('Atlantinex.MyAtlantinex.my_wallet', $data);
    }

    public function topUpWallet(Request $request)
    {

        $request->validate([
            'data.paymentmethod_id' => 'required',
            'data.value' => 'required|min:0.01|numeric'
        ]);

        $topUpAttempt = $this->myWalletTopUp($request->data['value'], Auth::user(), $request->data['paymentmethod_id']);

            if($topUpAttempt['status'] == 1){
                return back();
            }else{
                dd($topUpAttempt);
            }
    }

    public function myWalletTopUp($value, $user, $paymentMethodId)
    {

        $return = ['status' => 0];

        try {
            $chargeAttemp = $user->charge(\intval($value * 100), $paymentMethodId);
            if ($chargeAttemp->status == 'succeeded') {
                $user->walletoperations()->create([
                    'value' => $value,
                    'receipt' => 'Wallet TopUp &euro; ' . number_format($value,2,',',''),
                    #'payment_intent_feedback' => $chargeAttemp
                ]);
                $return['status'] = 1;
            }
        } catch (Exception $e) {
            $return['exception'] = $e;
        }

        return $return;
    }

    public function qrframe(){

        $code = url(Route('Company.inviteCustomer',['id' => Auth::id()]));


        #echo $code; exit;

        $data = [
            'logo_url' => \public_path('/uploads/logo-dark_OLD.png'),
            'qrCodeSrc' => (new QRCode)->render($code)
        ];



        $pdf = \PDF::loadView('Company.Settings.qr_frame', $data);

        // download PDF file with download method
        return $pdf->download('QrFrame Atlantidex.pdf');
    }

    public function inviteCustomer($id){
        $user = BackpackUser::findOrFail($id);
        $type = 'networker';
        $invitationlink = $user->invitationlinks()->create(
            [
                'link_type' => $type,
                'expires_at' => date('Y-m-d H:i', strtotime('+' . env(Str::upper($type) . '_INVITATION_LINK_EXPIRATION_DAYS') . 'days')),
                'code' =>  Invitationlink::generateLink()
            ]
        );


        return redirect($invitationlink->getCode($type));

    }

}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\BackpackUser;
use Illuminate\Http\Request;

class BackpackUserApiResourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BackpackUser  $backpackUser
     * @return \Illuminate\Http\Response
     */
    public function show(BackpackUser $backpackUser)
    #public function show($id)
    {

    #    dd(func_get_args());
        //
        return $backpackUser->only([
            'name',
            'surname',
            'email',
            'created_at',
            'updated_at',
            'code',
            'phone',
            'fiscal_code',
            'address',
            'date_of_birth',
            'vat_number',
            'company_name',
            'bank_account',
            'is_public_employee',
            'sdi',
            'pec',
            'companytype_id',
            'share_capital',
            'email_for_invoices',
            'contact_person',
            'fax',
            'corporate_sector',
            'company_registration_number',
            'website'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BackpackUser  $backpackUser
     * @return \Illuminate\Http\Response
     */
    public function edit(BackpackUser $backpackUser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BackpackUser  $backpackUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BackpackUser $backpackUser)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BackpackUser  $backpackUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(BackpackUser $backpackUser)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Invoicerow;
use Illuminate\Http\Request;

class InvoicerowApiResourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return Invoicerow::whereNull('ext_id')->orderBy('id','ASC')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Invoicerow  $invoicerow
     * @return \Illuminate\Http\Response
     */
    public function show(Invoicerow $invoicerow)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Invoicerow  $invoicerow
     * @return \Illuminate\Http\Response
     */
    public function edit(Invoicerow $invoicerow)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Invoicerow  $invoicerow
     * @return \Illuminate\Http\Response
     */
    #public function update(Request $request, Invoicerow $invoicerow)
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'ext_id' => 'required'
        ]);

        $invoicerow = Invoicerow::firstOrFail();
        $invoicerow->ext_id = $request->ext_id;
        $invoicerow->save();
        return $invoicerow;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Invoicerow  $invoicerow
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invoicerow $invoicerow)
    {
        //
    }
}

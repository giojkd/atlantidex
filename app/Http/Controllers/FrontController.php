<?php

namespace App\Http\Controllers;

use App\Models\BackpackUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use App\Models\Merchant;
use Jenssegers\Agent\Agent;


class FrontController extends Controller
{
    //

    public $allowedLangs = ['it'];

    public function home($lang = null){
        $lang = (!is_null($lang) && in_array($lang,$this->allowedLangs)) ? $lang : $this->defaultLang;
    }

    public function search($model,$keyword){
      $model = '\\App\\Models\\'.ucfirst(strtolower($model)); #must use fully qualified name of classe to istantiate it as a string
      $model = new $model();
      return $model->search($keyword)->get();
    }

    public function bookingDemo(){
        return view('booking_demo');
    }

    public function quiAtlantidex(Request $request){
        $data = [];

        $data['companies'] =
        Merchant::with(['user', 'user.companycategories'])
        ->get()
        ->filter(function($merchant,$key){

            if(is_object($merchant->user)){
                return $merchant->user->companycategories->count() > 0;
            }
            return false;

        })
        ->transform(function ($item, $key) {
            return $item->getSimpleInfo();
        })

        ->values();


        return view('front.quiAtlantidex',$data);
    }

    public function quiAtlantidexInfoWindow(Request $request){
        $request->validate(['id' => 'required']);

        $data = [
            'company' => Merchant::find($request->id)->getSimpleInfo()
        ];
        return view('front.quiAtlantidexInfowindow',$data);
    }

    public function quiAtlantidexV2(Request $request){

        $agent = new Agent();

        $data = [
            'facets' => [],
            'keyword' => '',
            'agent' => $agent
        ];

        return view('front.quiAtlantidexV2', $data);
    }

}




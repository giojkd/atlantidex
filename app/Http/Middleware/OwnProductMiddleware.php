<?php

namespace App\Http\Middleware;
use App\Models\Product;
use App\Scopes\SelfOwnerScope;
use Closure;

class OwnProductMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Product::addGlobalScope(new SelfOwnerScope());
        return $next($request);
    }
}

<?php

namespace App\Http\Middleware;

use Closure;

class ProductMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $merchants = $request['merchants'];

        foreach($merchants as $key => $merchant) {
            if (!isset($merchant['active'])) {
                $merchants[$key]['active'] = 0;
            }
            if (!isset($merchant['is_used'])) {
                $merchants[$key]['is_used'] = 0;
            }
        }
        $request['merchants'] = $merchants;
        return $next($request);
    }
}

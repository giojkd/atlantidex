<?php

namespace App\Http\Middleware;
use App\Models\Product;
use App\Scopes\NotOwnerScope;
use Closure;

class NotOwnProductMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Product::addGlobalScope(new NotOwnerScope());
        return $next($request);
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Otp;
use Auth;

class CheckOtp
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        #exit;
        if(
            !$request->session()->has('otp_verified_until') ||
            #$request->session()->has('otp_verified_until') &&
            $request->session()->get('otp_verified_until') < time()
        ){

            #dd(Auth::user());
            #dd($request->url());

            $request->session()->put('user_was_going_to', $request->url());

            $sendingStatus = Otp::generate(1,'generic',Auth::user()->phone);

            return redirect(Route('MyAtlantinex.otps.index'));
            #return $next($request); #redirect('https://www.google.com');
        }else{
            return $next($request);
        }

    }
}

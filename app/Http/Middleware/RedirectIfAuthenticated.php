<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if(!is_null(Auth::user())){
            if (Auth::guard($guard)->check() && !is_null(Auth::user()->hasVerifiedEmail())) {
                return redirect(RouteServiceProvider::HOME);
            }
        }

        return $next($request);
    }
}

<?php

namespace App\Http\Middleware;

use App\Models\Product;
use Closure;

class CheckOwnershipMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id = $request->route('id');
        $owner_id = auth()->id();
        $product = Product::findOrFail($id);
        if ($product->owner_id != $owner_id) {
            abort(403);
        }
        return $next($request);
    }
}

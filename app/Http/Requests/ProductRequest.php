<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|max:255',
            'description' => 'required',
            'product_type_id' => 'required',
            'category_id' => 'required',
            #'manufacturer_id' => 'required',
            'packaging_weight' => 'required_if:product_type_id,1',
            'packaging_height' => 'required_if:product_type_id,1',
            'packaging_lenght' => 'required_if:product_type_id,1',
            'packaging_width' => 'required_if:product_type_id,1',
            'vat_id' => 'required',
       ];
        $user = Auth::user();
        $merchants = $user->merchants;
        if ($merchants) {
            foreach($merchants as $merchant) {
                #$rules["merchants.{$merchant->id}.shipping_days"] = "required_if:merchants.{$merchant->id}.active,1";
                #$rules["merchants.{$merchant->id}.quantity"] = "required_if:merchants.{$merchant->id}.active,1";
                $rules["merchants.{$merchant->id}.price"] = "required_if:merchants.{$merchant->id}.active,1";
                $rules["merchants.{$merchant->id}.commission_percentage"] = "required_if:merchants.{$merchant->id}.active,1";
                $rules["merchants.{$merchant->id}.eps"] = "required_if:merchants.{$merchant->id}.active,1";
            }
        }

        return $rules;
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        $messages = [
            'name.required' => 'Il nome del prodotto è obbligatorio',
            'description.required' => 'La descrizione del prodotto è obbligatoria',
            'product_type_id.required' => 'La tipologia del prodotto è obbligatoria',
            'category_id.required' => 'La categoria del prodotto è obbligatoria',
            'manufacturer_id.required' => 'La marca del prodotto è obbligatoria',
            'packaging_weight.required_if' => 'Il peso del pacco è obbligatorio',
            'packaging_height.required_if' => 'L\'altezza del pacco è obbligatoria',
            'packaging_lenght.required_if' => 'La lunghezza del pacco è obbligatoria',
            'packaging_width.required_if' => 'La lunghezza del pacco è obbligatoria',
            'vat_id.required' => 'L\'IVA è obbligatoria',
        ];
        $user = Auth::user();
        $merchants = $user->merchants;
        if ($merchants) {
            foreach($merchants as $merchant) {
                $messages["merchants.{$merchant->id}.quantity.required_if"] = "La quantità per la filiale {$merchant->name} è obbligatoria";
                $messages["merchants.{$merchant->id}.price.required_if"] = "Il prezzo per la filiale {$merchant->name} è obbligatorio";
                $messages["merchants.{$merchant->id}.shipping_days.required_if"] = "I giorni di spedizione per la filiale {$merchant->name} sono obbligatori";
                $messages["merchants.{$merchant->id}.commission_percentage.required_if"] = "La percentuale per la Mediazione di Atlantidex {$merchant->name} è obbligatoria";
                $messages["merchants.{$merchant->id}.eps.required_if"] = "Il calcolo delfi EP per  {$merchant->name} è obbligatorio";
            }
        }

        return $messages;
    }
}

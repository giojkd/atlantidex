<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Ixudra\Curl\Facades\Curl;

class CompanyRegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'fiscal_code.unique' => 'Il codice fiscale appartiene ad un utente già registrato <a class="btn btn-primary btn-sm" href="/admin">Accedi</a>',
        ];
    }

    public function rules()
    {
        #return [];
        return [
            'name'                             => 'required|max:255',
            'surname'                          => 'required|max:255',
            backpack_authentication_column()   => 'required|email:rfc,dns|max:255|unique:users',
            'company_email'                    => 'required|email:rfc,dns|max:255|unique:users,email|different:email',
            #'password'                         => ['required', 'confirmed', 'regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%]).*$/'],
            #'password'                         => ['required', 'confirmed', 'regex:/(?=^.{8,}$)(?=.*\d)(?=.*[!@#$%^&*]+)(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/'],
            'password'                         => ['required', 'confirmed'],
            'phone'                            => 'required|numeric',
            'date_of_birth'                    => 'required|date_format:d/m/Y',
            'vat_number'                       => 'required|min:6|max:25|unique:users,vat_number',
            'company_name'                     => 'required|max:255',
            'address'                          => 'required',
            'accept_privacy'                   => 'required',
            'fiscal_code'                      => [

                                                    'required',/*
                                                    'regex:/^(?:[A-Z][AEIOU][AEIOUX]|[B-DF-HJ-NP-TV-Z]{2}[A-Z]){2}(?:[\dLMNP-V]{2}(?:[A-EHLMPR-T](?:[04LQ][1-9MNP-V]|[15MR][\dLMNP-V]|[26NS][0-8LMNP-U])|[DHPS][37PT][0L]|[ACELMRT][37PT][01LM]|[AC-EHLMPR-T][26NS][9V])|(?:[02468LNQSU][048LQU]|[13579MPRTV][26NS])B[26NS][9V])(?:[A-MZ][1-9MNP-V][\dLMNP-V]{2}|[A-M][0L](?:[1-9MNP-V][\dLMNP-V]|[0L][1-9MNP-V]))[A-Z]$/i',
                                                    function($attribute,$value,$fail){
                                                        $endPoint = 'http://webservices.dotnethell.it/codicefiscale.asmx/ControllaCodiceFiscale';
                                                        $fiscalCodeValidation = simplexml_load_string(Curl::to($endPoint)->withData(['CodiceFiscale' => $value])->get());
                                                        if (!isset($fiscalCodeValidation[0]) || $fiscalCodeValidation[0] != 'Il codice è valido!') {
                                                            $fail($attribute . ' non è valido.');
                                                        }
                                                    },*/
                                                    'unique:users,fiscal_code',
                                                ],
            #'fiscal_code'                      => ['required', 'regex:/^(?:[A-Z][AEIOU][AEIOUX]|[B-DF-HJ-NP-TV-Z]{2}[A-Z]){2}(?:[\dLMNP-V]{2}(?:[A-EHLMPR-T](?:[04LQ][1-9MNP-V]|[15MR][\dLMNP-V]|[26NS][0-8LMNP-U])|[DHPS][37PT][0L]|[ACELMRT][37PT][01LM]|[AC-EHLMPR-T][26NS][9V])|(?:[02468LNQSU][048LQU]|[13579MPRTV][26NS])B[26NS][9V])(?:[A-MZ][1-9MNP-V][\dLMNP-V]{2}|[A-M][0L](?:[1-9MNP-V][\dLMNP-V]|[0L][1-9MNP-V]))[A-Z]$/i'],
            #'company_fiscal_code'              => ['unique:users,fiscal_code','different:fiscal_code','required', 'regex:/^(?:[A-Z][AEIOU][AEIOUX]|[B-DF-HJ-NP-TV-Z]{2}[A-Z]){2}(?:[\dLMNP-V]{2}(?:[A-EHLMPR-T](?:[04LQ][1-9MNP-V]|[15MR][\dLMNP-V]|[26NS][0-8LMNP-U])|[DHPS][37PT][0L]|[ACELMRT][37PT][01LM]|[AC-EHLMPR-T][26NS][9V])|(?:[02468LNQSU][048LQU]|[13579MPRTV][26NS])B[26NS][9V])(?:[A-MZ][1-9MNP-V][\dLMNP-V]{2}|[A-M][0L](?:[1-9MNP-V][\dLMNP-V]|[0L][1-9MNP-V]))[A-Z]$/i']
            #'company_fiscal_code'              => ['unique:users,fiscal_code','different:fiscal_code','required']
            #'company_fiscal_code'              => ['unique:users,fiscal_code', 'required'],
            'company_fiscal_code'              => ['required'],
            #'sdi'                              => ['required_without_all:pec'],
            #'pec'                              => ['required_without_all:sdi', 'sometimes', 'email:rfc,dns'],
            #'share_capital'                    => ['numeric'],
            'email_for_invoices'               => ['email:rfc,dns'],
            'contact_person'                   => ['required'],
            #'company_registration_number'      => ['required'],
            'place_of_birth'                    => ['required']
        ];
    }
}

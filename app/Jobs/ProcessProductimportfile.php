<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Productimportfile;

class ProcessProductimportfile implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $productimportfile;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Productimportfile $productimportfile)
    {
        //
        $this->productimportfile = $productimportfile;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $file = $this->productimportfile;
        $file->import();

    }
}

<?php

namespace App\Traits;

trait Reviewable
{
  public function reviews()
  {
    return $this->morphMany('App\Models\Review', 'reviewable');
  }

  public function updateReviewsStatus(){
    $reviews = $this->reviews;
    if(!\is_null($reviews)){
      $this->reviews_avg = $reviews->avg('score');
      $this->reviews_count= $reviews->count();
    }else{
      $this->reviews_avg = 0;
      $this->reviews_count= 0;
    }
    $this->save();
  }
  
}

<?php
namespace App\Traits;

use Illuminate\Database\Eloquent\Model;
use Session;

trait CanImpersonateTrait
{
    public function setImpersonating($id)
    {
        Session::put('impersonate', $id);
    }

    public function stopImpersonating()
    {
        Session::forget('impersonate');
    }

    public function isImpersonating()
    {
        return Session::has('impersonate');
    }
}

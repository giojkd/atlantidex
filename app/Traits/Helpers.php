<?php

namespace App\Traits;
use Str;

trait Helpers
{

    public function getModelName()
    {

        $reflection = new \ReflectionClass($this);

        $modelName = implode("\\", [$reflection->getNamespaceName(), $reflection->getShortName()]);
        #return Str::lower($reflection->getShortName());
        return $modelName;

    }


  public function getModels($path, $namespace){
    $out = [];

    $iterator = new \RecursiveIteratorIterator(
      new \RecursiveDirectoryIterator(
        $path
      ), \RecursiveIteratorIterator::SELF_FIRST
    );
    foreach ($iterator as $item) {
      /**
      * @var \SplFileInfo $item
      */
      if($item->isReadable() && $item->isFile() && mb_strtolower($item->getExtension()) === 'php'){
        $out[] =  $namespace .
        str_replace("/", "\\", mb_substr($item->getRealPath(), mb_strlen($path), -4));
      }
    }
    return $out;
  }

    public function saveWithoutEvents(array $options = [])
    {
        return static::withoutEvents(function () use ($options) {
            return $this->save($options);
        });
    }


    public function buildTree(&$elements, $parentId = null)
    {
        $branch = array();
        foreach ($elements as &$element) {

            if ($element->parent_id == $parentId) {
                $children = $this->buildTree($elements, $element->id);
                if ($children) {
                    $element->children = $children;
                }
                $branch[$element->id] = $element;
                unset($element);
            }
        }
        return $branch;
    }

    public function buildTreeForD3(&$elements, $parent = null)
    {
        $branch = array();
        foreach ($elements as &$element) {

            if ($element['parent'] == $parent) {
                $children = $this->buildTreeForD3($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                    $element['children'] = array_values($element['children']);
                }
                $branch[$element['id']] = $element;
                unset($element);
            }
        }
        return $branch;
    }

    public function getNotificationStandardData($id){

        $data = [
            'dark_logo_1' => url('uploads/logo-dark.png'),
            'dark_logo_2' => url('uploads/logo-dark.png'),
            'light_logo_1' => url('uploads/logo-dark.png'),
            'light_logo_2' => url('logo-dark.png'),
            'highlight_color_1' => '#a8c8dc',
            'highlight_color_2' => '#a8c8dc',
            'company_name' => 'Atlantidex',
            'company_address' => 'Viale Colombo, 11 54033 Marina di Carrara (MS)',
            'facebook_link' => '',
            'twitter_link' => '',
            'instagram_link' => '',
            'help_center_link' => '',
            'preferences_link' => '',
            'unsubscribe_link' => '',
            'app_name' => env('APP_NAME'),
            'url' => config('app.url'),
            'resource_prefix' => config('app.url').'/front/notification_resources/'
        ];

        $data['dark_logo'] = $data['dark_logo_'.$id];
        $data['light_logo'] = $data['light_logo_'.$id];
        $data['highlight_color'] = $data['highlight_color_'.$id];

        return $data;
    }


}

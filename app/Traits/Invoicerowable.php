<?php

namespace App\Traits;

use App\Models\BackpackUser;
use App\Models\Invoicerow;

trait Invoicerowable
{
    public function invoicerow()
    {
        return $this->morphOne(Invoicerow::class);
    }

    public function emitInvoice($user,$paid_with,$value = null){

        $user = is_object($user) ? $user : BackpackUser::find($user);
        if(is_null($value)){
            $valueColumnName = (isset($this->price_column_name)) ? $this->price_column_name : 'value' ;
            $value = $this->$valueColumnName;
        }

        $user->invoicerows()->create([
            'value' => $value,
            'invoicerowable_id' => $this->id,
            'invoicerowable_type' => $this->getModelName(),
            'paid_with' => $paid_with
        ]);

    }
}

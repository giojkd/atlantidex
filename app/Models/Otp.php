<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use jobayerccj\Skebby\Skebby;
use Auth;

class Otp extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'otps';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    static function generateRandomOtpCode($n)
    {
        $characters = '0123456789';
        $randomString = '';

        for ($i = 0; $i < $n; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }

        return $randomString;
    }


    static function generate($id,$model,$phone,$user = null, $customMessageAppend = ''){


        $user_id = (Auth::check()) ? Auth::user()->id : null;

        if(!is_null($user)){
            $user_id = $user->id;
        }

        if ($phone == '3333333333') {
            $code = '3333333333';
        } else {
            $code = Otp::generateRandomOtpCode(6);
        }

        $otp = Otp::create([
            'user_id' => $user_id,
            'code' => $code,
            'phone' => $phone,
            'otpable_id' => $id,
            'otpable_type' => $model,
        ]);


        if ($phone == '3333333333') {
            $sending_status['status'] = 'success';
        } else {
            $skebby = new Skebby;

            $message = 'Il codice di verifica di Atlantidex è: '.$code.$customMessageAppend;

            $skebby->set_username(env('SKEBBY_USERNAME'));
            $skebby->set_password(env('SKEBBY_PASSWORD'));
            $skebby->set_method('send_sms_classic');
            $skebby->set_text($message);
            $skebby->set_sender(env('SKEBBY_SENDER'));

            $recipients = array('+39' . $phone);
            $skebby->set_recipients($recipients);
            $sending_status = $skebby->send_sms();
        }

        return $sending_status;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}

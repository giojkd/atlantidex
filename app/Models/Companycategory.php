<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Illuminate\Support\Collection;
use Kalnoy\Nestedset\NodeTrait;
use App;


class Companycategory extends Model
{
    use CrudTrait;
    use HasTranslations;
    use NodeTrait {
        NodeTrait::create insteadof HasTranslations;
    }
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'companycategories';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];
    protected $translatable = ['name', 'description', 'stored_ancestors_breadcrumb'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function children(){
        return $this->hasMany(Companycategory::class,'parent_id');
    }

    public function companycategories()
    {
        return $this->hasMany(Companycategory::class, 'parent_id');
    }

    public function getLftName()
    {
        return 'lft';
    }

    public function getRgtName()
    {
        return 'rgt';
    }
    public function companies(){
        return $this->belongsToMany(BackpackUser::class);
    }

    public function parent(){
        return $this->belongsTo(Companycategory::class,'parent_id');
    }

    public function companycategory()
    {
        return $this->belongsTo(Companycategory::class, 'parent_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    public function getNameWithDetailsAttribute(){
        return $this->category_code.' - '.$this->name. ' ('.$this->min_commission.'%)';
    }

    public function getNameWithDetailsLightAttribute()
    {
        return $this->category_code . ' - ' . $this->name;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}

<?php

namespace App\Models;

use App\Traits\Helpers;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Advpackpurchase extends Model
{
    use CrudTrait;
    use Helpers;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'advpackpurchases';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $casts = ['expires_at' => 'date'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function advpack(){
        return $this->belongsTo(Advpack::class);
    }

    public function user(){
        return $this->belongsTo(BackpackUser::class,'user_id');
    }

    public function products(){
        return $this->belongsToMany(Product::class)->withPivot('placement');#;->as('insertions')->withTimestamps();#->withPivot('placement');
    }

    public function advinsertions(){
        return $this->hasMany(Advinsertion::class);
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}

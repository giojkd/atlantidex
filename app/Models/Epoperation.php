<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Epoperation extends Model
{
    use CrudTrait;
    use SoftDeletes;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'epoperations';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function user(){
        return $this->belongsTo('App\Models\BackpackUser','user_id');
    }

    public function address(){
        return $this->belongsTo('App\Models\BackpackUser','user_id');
    }

    public function name()
    {
        return $this->belongsTo('App\Models\BackpackUser', 'user_id');
    }

    public function surname()
    {
        return $this->belongsTo('App\Models\BackpackUser', 'user_id');
    }

    public function fiscal_code()
    {
        return $this->belongsTo('App\Models\BackpackUser', 'user_id');
    }

    public function date_of_birth_human_readable()
    {
        return $this->belongsTo('App\Models\BackpackUser', 'user_id');
    }


    public function address_road()
    {
        return $this->belongsTo('App\Models\BackpackUser', 'user_id');
    }

    public function address_postcode()
    {
        return $this->belongsTo('App\Models\BackpackUser', 'user_id');
    }

    public function address_county()
    {
        return $this->belongsTo('App\Models\BackpackUser', 'user_id');
    }





    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}

<?php

namespace App\Models;

use App\Traits\Helpers;
use App\Traits\Invoicerowable;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Leadrow extends Model
{
    use CrudTrait;
    use Invoicerowable;
    use Helpers;
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'leadrows';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];
    public $price_column_name = 'commission_amount';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function identifierForInvoice()
    {
        return $this->id;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function lead(){
        return $this->belongsTo(Lead::class);
    }

    public function merchant(){
        return $this->belongsTo(Merchant::class)->withoutGlobalScopes();
    }

    public function epoperation()
    {
        return $this->morphOne(Epoperation::class, 'epoperationable');
    }

    public function epplusoperation()
    {
        return $this->morphOne(Epplusoperation::class, 'epplusoperationable');
    }

    public function commissionoperation()
    {
        return $this->morphOne(Commissionoperation::class, 'commissionoperationable');
    }

    public function invoicerow()
    {
        return $this->morphOne(Invoicerow::class, 'invoicerowable');
    }



    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}

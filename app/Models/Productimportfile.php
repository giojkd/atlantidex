<?php

namespace App\Models;

use App\Scopes\UserScope;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Rap2hpoutre\FastExcel\FastExcel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

use Str;
use Auth;

use App\Models\Product;
use App\Models\Merchant;
use App\Models\Manufacturer;
use App\Models\Category;
use App\Models\Attribute;
use App\Models\Attributevalue;
use ZipArchive;

use File;

class Productimportfile extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'productimportfiles';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $casts = ['result' => 'json'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    protected static function booted()
    {
        static::addGlobalScope(new UserScope);
    }

    public function getStatus()
    {
        if (is_null($this->result)) {
            return 'In attesa';
        }else{
            if($this->result['status'] == 0){
                return 'In elaborazione';
            }
            if ($this->result['status'] == 1) {
                return 'Completato';
            }
            if ($this->result['status'] == -1) {
                return 'Errore';
            }
        }
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function user()
    {
        return $this->belongsTo('App\Models\Backpackuser', 'user_id');
    }

    public function merchant()
    {
        return $this->belongsTo('App\Models\Merchant');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */


    public function setFileAttribute($value)
    {

        $attribute_name = "file";
        $disk = "public";
        $destination_path = "productimportfiles";

        $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);

        // return $this->attributes[{$attribute_name}]; // uncomment if this is a translatable field
    }


    public function setFilePhotosAttribute($value)
    {

        $attribute_name = "file_photos";
        $disk = "public";
        $destination_path = "productimportfiles";

        $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);

        // return $this->attributes[{$attribute_name}]; // uncomment if this is a translatable field
    }

    public function import()
    {

        $this->result = ['status' => 0];
        $this->save();

        $now = date('Y-m-d H:i:s');
        $storagePath = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();

        $collections = collect((new FastExcel)->import($storagePath . $this->file));

        $locale = 'it';
        $defaultWarrantyYears = null;
        $errors = collect([]);
        $maxAttributesCount = 5;

        $owner_id = $this->user_id;

        $group = 0;
        $parent_id = null;


        if ($collections->count() > 0) {
            $collections->each(function ($item, $key) use ($owner_id,$errors, $now, $locale, $defaultWarrantyYears, $maxAttributesCount) {

                $item = collect($item);

                $item = $item->map(function ($item, $key) {
                    return trim($item);
                });

                $category = Category::where('google_id', $item['Categoria'])->first();

                if (
                    $item['Brand'] != ''
                    && $item['SKU'] != ''
                    && !is_null($category)
                ) {

                    #row validity checks
                    $item['Brand'] = Str::lower($item['Brand']);
                    $item['SKU'] = $this->normalizeSku($item['SKU']);
                    $item['Tipo di prodotto'] = ($item['Tipo di prodotto'] != '' && is_numeric($item['Tipo di prodotto']) && $item['Tipo di prodotto'] < 10 && $item['Tipo di prodotto'] > 0) ? $item['Tipo di prodotto'] : 1;
                    #row validity checks


                    $manufacturer = Manufacturer::firstOrCreate(['name' => $item['Brand']]);


                    $product = Product::firstOrCreate([
                        'sku' => $item['SKU'],
                        'manufacturer_id' => $manufacturer->id,
                        'category_id' => $category->id,
                        'product_type_id' => $item['Tipo di prodotto']
                    ]);

                    $active = ($owner_id != env('TEST_COMPANY_ID')) ? 1 : 0;

                    $fill = [
                        'name' => [$locale => $item['Nome']],
                        'brief' => [$locale => $item['Descrizione breve']],
                        'description' => [$locale => $item['Descrizione']],
                        'price_per' => ($item['Prezzo al'] != '') ? [$locale => $item['Prezzo al']] : null,
                        'min_quantity' => ($item['Q. minima'] != '' && is_numeric($item['Q. minima'])) ? $item['Q. minima'] : null,
                        'minimum_increase' => ($item['Minimo incremento.'] != '' && is_numeric($item['Minimo incremento.'])) ?  $item['Minimo incremento.'] : null,
                        'warranty_years' => ($item['Garanzia (anni)'] != '') ? $item['Garanzia (anni)'] : $defaultWarrantyYears,
                        'product_weight' => ($item['Peso Prodotto (g)'] != '' && is_numeric($item['Peso Prodotto (g)'])) ? $item['Peso Prodotto (g)'] : 0,
                        'product_width' => ($item['Larghezza Prodotto (mm)'] != '' && is_numeric($item['Larghezza Prodotto (mm)'])) ? $item['Larghezza Prodotto (mm)'] : 0,
                        'product_height' => ($item['Altezza Prodotto (mm)'] != '' && is_numeric($item['Altezza Prodotto (mm)'])) ? $item['Altezza Prodotto (mm)'] : 0,
                        'product_lenght' => ($item['Profondità Prodotto (mm)'] != '' && is_numeric($item['Profondità Prodotto (mm)'])) ? $item['Profondità Prodotto (mm)'] : 0,
                        'packaging_weight' => ($item['Peso Imballo (g)'] != '' && is_numeric($item['Peso Imballo (g)'])) ? $item['Peso Imballo (g)'] : 0,
                        'packaging_width' => ($item['Larghezza Imballo (mm)'] != '' && is_numeric($item['Larghezza Imballo (mm)'])) ? $item['Larghezza Imballo (mm)'] : 0,
                        'packaging_height' => ($item['Altezza Imballo (mm)'] != '' && is_numeric($item['Altezza Imballo (mm)'])) ? $item['Altezza Imballo (mm)'] : 0,
                        'packaging_lenght' => ($item['Profondità Imballo (mm)'] != '' && is_numeric($item['Profondità Imballo (mm)'])) ? $item['Profondità Imballo (mm)'] : 0,
                        'owner_id' => $owner_id,
                        'vat_id' => $item['IVA'],
                        'active' => $active
                    ];

                    $product->fill($fill);

                    $product->save();

                    #                    dd($product);

                    $product_status_id = ($item['Disponibili'] > 0) ? 1 : 1;

                    $toSync = [
                        $this->merchant_id => [
                            'product_status_id' => $product_status_id,
                            'quantity'          => ($item['Disponibili'] != '' && is_numeric($item['Disponibili']))  ? $item['Disponibili'] : 0,
                            'created_at'        => $now,
                            'price'             => $item['Prezzo'],
                            'reassortment_days' => ($item['Giorni per disp.'] != '' && is_numeric($item['Giorni per disp.'])) ? $item['Giorni per disp.'] : 0,
                            'shipping_days'     => ($item['Giorni per sped.'] != '' && is_numeric($item['Giorni per sped.'])) ? $item['Giorni per sped.'] : 0,
                            'commission_percentage' => ($item['Mediazione Atlantidex'] != '' && is_numeric($item['Mediazione Atlantidex'])) ? $item['Mediazione Atlantidex'] : 5,
                            'commissionb2b_percentage' => ($item['Mediazione Atlantidex B2B'] != '' && is_numeric($item['Mediazione Atlantidex B2B'])) ? $item['Mediazione Atlantidex B2B'] : 5,
                            'eps' => $this->getEpsForMerchant($item['Prezzo'],$item['Mediazione Atlantidex'],$item['IVA']),
                            'b2b_eps' => $this->getEpsForMerchant($item['Prezzo'],$item['Mediazione Atlantidex B2B'],$item['IVA']),
                            'bid_min' => ($item['Limite minimo di prezzo accettato'] != '' && is_numeric($item['Limite minimo di prezzo accettato'])) ? $item['Limite minimo di prezzo accettato'] : 0
                        ]
                    ];

                    #dd($toSync);

                    $product->merchants()->syncWithoutDetaching($toSync);

                    $features = [];

                    for ($i = 1; $i < $maxAttributesCount; $i++) {
                        if ($item['Caratteristica ' . $i] != '') {
                            $test = collect(explode(':', $item['Caratteristica ' . $i]));
                            if ($test->count() > 2) {
                                abort(403, 'Error with feature definition.');
                            }
                            $featureValue = $item['Caratteristica ' . $i];
                            if ($featureValue != '') {
                                list($attributeName, $attributeValueValue) = explode(':', $featureValue);
                                $product->addAttribute($attributeName, $attributeValueValue, ['locale' => $locale]);
                            }
                        }
                    }
                } else {
                    $errors->push(['error_at_row' => $key]);
                }
            });
            if ($errors->count() > 0) {
                #dd($errors);
                $this->result = ['status' => -1];
                $this->save();
            }
        }
        $this->result = ['status' => 1];
        $this->products_count = $collections->count();
        $this->save();

        $this->importPhotos();
    }

    public function importPhotos(){

        $merchant = $this->merchant;

        $excluedFolders = ['__MACOSX'];
        $storagePath = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();
        $file = $storagePath.$this->file_photos;
        $exportPath = $storagePath . 'importedphotos/' . $this->id . '/';
        $zip = new ZipArchive;
        if ($zip->open($file) === TRUE) {
            $zip->extractTo($exportPath);
            $zip->close();

            $folders = collect(File::directories($exportPath));



            if($folders->count() > 0){
                foreach($folders as $folder){
                    $folder_ = $this->getLastEl($folder);
                    if(!in_array($folder_,$excluedFolders)){
                        $photos = collect(File::files($folder));
                        if ($photos->count() > 0) {

                            $product = $merchant->products()->where('sku',$this->normalizeSku($folder_))->first();

                            $productPhotos = [];
                            foreach($photos as $photo){
                                $photoPath = '/storage/importedphotos/'.$this->id.'/'.$folder_.'/'. $this->getLastEl($photo);
                                $productPhotos[] = $photoPath;
                            }
                            $product->pictures = $productPhotos;
                            $product->saveWithoutEvents();

                        }
                    }

                }
            }




        }else{
            echo 'error';
        }


    }

    /*
    public function importPhotos()
    {

        $merchant = $this->merchant;

        $excluedFolders = ['__MACOSX'];
        $storagePath = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();
        $file = $storagePath . $this->file_photos;
        $exportPath = $storagePath . 'importedphotos/' . $this->id . '/';
        $zip = new ZipArchive;
        if ($zip->open($file) === TRUE) {
            $zip->extractTo($exportPath);
            $zip->close();

            $folders = collect(File::directories($exportPath));



            if ($folders->count() > 0) {
                foreach ($folders as $folder) {
                    $folder_ = $this->getLastEl($folder);
                    if (!in_array($folder_, $excluedFolders)) {
                        $photoFolders = collect(File::directories($folder));
                        if ($photoFolders->count() > 0) {
                            foreach ($photoFolders as $photoFolder) {
                                $photoFolder_ = $this->getLastEl($photoFolder);
                                $product = $merchant->products()->where('sku', $this->normalizeSku($photoFolder_))->first();
                                $photos = collect(File::files($photoFolder));
                                if ($photos->count() > 0) {
                                    $productPhotos = [];
                                    foreach ($photos as $photo) {
                                        $photoPath = '/storage/importedphotos/' . $this->id . '/' . $folder_ . '/' . $photoFolder_ . '/' . $this->getLastEl($photo);
                                        $productPhotos[] = url($photoPath);
                                    }
                                    $product->pictures = $productPhotos;
                                    $product->saveWithoutEvents();
                                }
                            }
                        }
                    }
                }
            }
        } else {
            echo 'error';
        }
    }
    */

    public function getLastEl($path){
        return collect(explode('/', $path))->last();
    }

    public function normalizeSku($sku)
    {
        return $sku;
    }

    public function getEpsForMerchant($price,$percentage,$vat)
    {
        $rs = 0;
        if ($percentage != '' && is_numeric($percentage)) {
            $vat = Vat::find($vat);
            $vatvalue = $vat->value;
            $priceiva = $price * (1 + ($vatvalue/100));
            $rs = $priceiva * ($percentage / 100);
        }
        return $rs;
    }
}

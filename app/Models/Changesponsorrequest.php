<?php

namespace App\Models;

use App\Traits\Helpers;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;


class Changesponsorrequest extends Model
{
    use CrudTrait;
    use Helpers;
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'changesponsorrequests';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function canBeRendered(){
       if(!is_null($this->applicant_signature) && !is_null($this->receiving_signature) && $this->receiving_feedback == 'accepted'){
           return true;
       }else{
           return false;
       }
    }




    public function signatureUrl($signAs = 'applicant'){

        $queryString = \Illuminate\Support\Arr::query([
            'extraFields' => [
                'user_code' => $this->receiving_user->code
                ],
            'signAs' => $signAs,
            'changeSponsorRequestId' => $this->id
            ]
        );
        return Route('MyAtlantinex.documents.show', ['document' => $this->document_id]) . '?' . $queryString;

    }



    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function applicant_user()
    {
        return $this->belongsTo('App\User', 'applicant_user_id');
    }

    public function receiving_user(){
        return $this->belongsTo('App\User','receiving_user_id');
    }

    public function document(){
        return $this->belongsTo('App\Models\Document');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}

<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Commissionoperation extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'commissionoperations';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */



    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function user()
    {
        return $this->belongsTo('App\Models\BackpackUser');
    }

    public function getUserSponsor(){
        return $this->user->sponsor;
    }

    public function commissionoperationable(){
        return $this->morphTo();
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    function getDescriptionAttribute()
    {
        $type = $this->commissionoperationable_type;
        $obj = $this->commissionoperationable;
        $return = '';
        if(!is_null($obj)){
            switch ($type) {
                case 'App\Models\Gdovoucher':
                    $return = 'Acquisto voucher della GDO '.$obj->name;
                    break;
                case 'App\Models\Leadrow':
                    $merchant = $obj->merchant;
                    if(!is_null($merchant)){
                        $return = 'Ordine ' . $obj->lead_id.' presso merchant ' . $merchant->name;
                    }

                    break;

                default:
                    $return = $this->attributes['description'];
                    break;
            }
        }
        return $return;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}

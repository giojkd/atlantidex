<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use App\Mail\TicketPurchaseConfirmation;
use App\Traits\Helpers;
use App\Traits\Invoicerowable;
use chillerlan\QRCode\QRCode;
use Illuminate\Support\Facades\Storage;

class Ticket extends Model
{
    use CrudTrait;
    use Helpers;
    use Invoicerowable;
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'tickets';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $with = ['event'];

    public $price_column_name = 'price';

    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function identifierForInvoice(){
        return $this->event->title;
    }

    public function sendPurchaseConfirmation(){

        $qrCodeName = 'ticket_'.$this->id.'.png';
        $path = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix().'tickets/'.$qrCodeName;
        (new QRCode)->render($this->id,$path);

        $base = $this->getNotificationStandardData(1);

        $base['event'] = $this->event;
        $base['ticket'] = $this;
#        $base['qr_code'] = url('storage/tickets/'.$qrCodeName);
        $base['qr_code'] = config('app.url').'/storage/tickets/' . $qrCodeName;


        $data['subject'] = 'Ecco il tuo biglietto!';
        $data['content'] = $base;

        #dd($data);

        $recipient = $this->user->email;

        $result = Mail::to($recipient)->send(new TicketPurchaseConfirmation($data));



        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function event(){
        return $this->belongsTo('App\Models\Event');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\BackpackUser','user_id');
    }


    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}

<?php

namespace App\Models;

use App\Scopes\UserScope;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use App\Traits\Reviewable;
use App\Traits\Lead;
use Illuminate\Support\Str;
use Laravel\Scout\Searchable;
use App;

class Merchant extends Model
{
    use CrudTrait;
    use HasTranslations;
    use Reviewable;
    use Searchable;
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */


    protected $table = 'merchants';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];
    protected $translatable = ['description','brief'];

    protected $casts = [
        'pictures' => 'array',
        'address' => 'json'
    ];



    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function toSearchableArray()
    {

        $merchant = $this;
        $data = $merchant->getSimpleInfo();
        $data['companycategories'] = collect($data['companycategories']);
        if($data['companycategories']->count() > 0){
            $data['companycategories']->transform(function($item,$key){
                if(is_object($item)){
                    return [
                        'name' => $item->name,
                        'category_code' => $item->category_code,
                        'min_commission' => $item->pivot->commission,
                    ];
                }
            });
        }

        $data['_geoloc'] = $data['coordinates'];
        unset($data['coordinates']);

        return $data;

    }

    public function shouldBeSearchable()
    {
        return (is_object($this->user) && $this->user->companycategories->count() > 0 && isset($this->address['latlng']));
    }



    public function cover($size = 50){
        if(!is_null($this->pictures)){
            if(isset($this->pictures[0])){
                if($this->pictures[0] != ''){
                    return '<img src=\''.Route('ir',['size' => $size, 'filename' => $this->pictures[0]]).'\'>';
                }
            }
        }
        return '';
    }

    public function getSimpleInfo()
    {
        $phone = [];
        if(is_object($this->user)){
            $phone[] = (is_object($this->user)) ? $this->user->phone : ((is_object($this->user->parent)) ? $this->user->parent->phone : '');
            $phone[] = (is_object($this->user)) ? $this->user->company_phone : ((is_object($this->user->parent)) ? $this->user->parent->company_phone : '');
        }


        return [
            'id' => $this->id,
            'name' => $this->name,
            'coordinates' => $this->address['latlng'],
            'address' => $this->address['value'],
            'telephone' => implode('-',array_filter($phone)),
            'email' => (is_object($this->user)) ? $this->user->email : (is_object($this->user) && (is_object($this->user->parent)) ? $this->user->parent->email : ''),
            'companycategories' => (is_object($this->user) && $this->user->companycategories->count() > 0) ? $this->user->companycategories : [],
            'description' => $this->description,
            'short_description' => Str::substr($this->description, 0, 160)
        ];
    }


    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\BackpackUser','user_id');
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Product')->withPivot(['price','price_compare']);
    }

    public function leads(){
        return $this->belongsToMany(Lead::class);
    }

    public function schedules(){
        return $this->hasMany(Schedule::class);
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}

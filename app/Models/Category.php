<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Illuminate\Support\Collection;
use Kalnoy\Nestedset\NodeTrait;
use App;

class Category extends Model
{
    use CrudTrait;
    #use HasTranslations;
    #use NodeTrait;
    use CrudTrait;
    use HasTranslations;
    use NodeTrait {
        NodeTrait::create insteadof HasTranslations;
    }


    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'categories';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];
    protected $translatable = ['name','description', 'stored_ancestors_breadcrumb'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
*/
     public function getLftName()
    {
        return 'lft';
    }

    public function getRgtName()
    {
        return 'rgt';
    }

    // recursive, loads all descendants
    public function childrenRecursive()
    {

        return $this->children()->with('childrenRecursive');
        // which is equivalent to:
        //return $this->hasMany('App\Models\Category', 'parent_id')->with('childrenRecursive');
    }



    // parent
   /* public function parent()
    {
        return $this->belongsTo('App\Models\Category','parent_id');
    } */



    // all ascendants
    /*public function parentRecursive() {
        //return $this->('parent')->with('parentRecursive');
        return $this->parent()->with('parentRecursive');
    }*/
    public function getParentsAttribute()
    {
        $parents = collect([]);
        $parent = $this->parent;
        while(!is_null($parent)) {
            $parents->push($parent);
            $parent = $parent->parent;
        }
        return $parents;
    }

    public function getParentsLast()
    {
        $parent = $this->getParentsAttribute();
        return $parent->slice(0,-1)->last();
    }

    public static function getAllLeavesByCategory(int $id)
    {
        return Category::active()->whereId($id)->firstOrFail()->descendants()->active()->whereIsLeaf()->orderBy('name','ASC')->get()->transform(function($item,$key){
            return $item->only(['name', 'id']);
        });


    }

    public static function getAllSubCategoryWithAncestors(int $id){

        $category = Category::find($id);

        $descendants = $category->descedants()->orderBy('lft','ASC')->get();



    }

    public static function getAllSubCategoryByCategory(int $id)
    {
        $subcategory = [];
        $tree = self::whereId($id)->whereActive(1)->get();
        foreach($tree as $branch) {
            if ($branch) {
                $node = $branch->childrenRecursive->toArray();
                if ($node) {
                    self::getSubcategory($node, $subcategory);
                }
            }
        }
        return $subcategory;
    }



    public static function getAllLeaves()
    {
        $leaves = [];
        $tree = self::whereNull('parent_id')->get();
        foreach($tree as $branch) {
            if ($branch) {
                $node = $branch->childrenRecursive->toArray();
                if ($node) {
                    self::getLeaves($node, $leaves);
                }
            }


        }
        return $leaves;
    }

    public static function getLeaves($children, &$leaves) {
        foreach($children as $node) {
            if (isset($node['children_recursive'])) {
                if (count($node['children_recursive']) == 0) {
                    $item['id'] = $node['id'];
                    $item['name'] = $node['name']['it'];
                    $leaves[] = $item;
                } else {
                    self::getLeaves($node['children_recursive'], $leaves);
                }
            }

        }
    }

    public static function getSubcategory($children, &$subcategory) {
        foreach($children as $node) {
            if (isset($node['children_recursive'])) {
                $item['id'] = $node['id'];
                $item['name'] = $node['name']['it'];
                $subcategory[] = $item;
                self::getSubcategory($node['children_recursive'], $subcategory);
            }
        }

    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeActive($query)
    {
      return $query->where('active', '=', 1);
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    public function getAncestorsBreadcrumbAttribute()
    {
        return collect($this->ancestors()->get()->pluck('name'))->implode(' > ');
    }

    public function getAncestorsAndSelfBreadcrumbAttribute()
    {

        return collect([$this->ancestors_breadcrumb, $this->name])->implode(' > ');
    }

    public function generateAncestorsBreadcrumb(){
        App::setLocale('it');
        $this->stored_ancestors_breadcrumb = $this->ancestors_breadcrumb;
        $this->save();
    }

    public function getStoredAncestorsAndSelfBreadcrumbAttribute()
    {

        return collect([$this->stored_ancestors_breadcrumb, $this->name])->implode(' > ');
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}

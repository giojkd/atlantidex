<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Voucheroperation extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'voucheroperations';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function getDescriptionAttribute()
    {
        #$user = $this->user;
        $locale = 'it';
        $models = [
            'App\Models\Pack' => ['name' => ['it' => 'Pack'], 'identifierForInvoice' => 'identifierForInvoice'],
            'App\Models\Gdovouchercode' => ['name' => ['it' => 'Voucher della GDO'], 'identifierForInvoice' => 'identifierForInvoice'],
            'App\Models\Gdovoucher' => ['name' => ['it' => 'Voucher della GDO'], 'identifierForInvoice' => 'identifierForInvoice'],
            'App\Models\Ticket' => ['name' => ['it' => 'Biglietto'], 'identifierForInvoice' => 'identifierForInvoice'],
            'App\Models\Ticket' => ['name' => ['it' => 'Biglietto'], 'identifierForInvoice' => 'identifierForInvoice'],
            'App\Models\Epvoucher' => ['name' => ['it' => 'Voucher'], 'identifierForInvoice' => 'identifierForInvoice'],
            'App\Models\Lead' => ['name' => ['it' => 'Voucher'], 'identifierForInvoice' => 'identifierForInvoice'],
        ];
        $description = ['Acquisto'];
        $description[] =  $models[$this->voucheroperationable_type]['name'][$locale];
        $item = $this->voucheroperationable()->first();
        if(!is_null($item)){
            $identifier = $models[$this->voucheroperationable_type]['identifierForInvoice'];
            $description[] = $item->$identifier();
        }
        return collect($description)->implode(' ');
    }


    public function calculateVoucherSpent()
    {
        $user = $this->user;
        if (!is_null($user)) {
            $thisVoucherCategorySpentSoFarAmount = $user->voucheroperations()->whereEps($this->eps)->sum('amount');
            $user->vouchers()->whereEps($this->eps)->update(['spent' => $thisVoucherCategorySpentSoFarAmount]);
        }
    }


    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function user()
    {
        return $this->belongsTo(BackpackUser::class, 'user_id');
    }

    public function voucheroperationable(){
        return $this->morphTo();
    }



    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}

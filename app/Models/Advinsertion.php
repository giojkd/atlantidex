<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Advinsertion extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    #protected $table = 'advinsertions';
    protected $table = 'advpackpurchase_product';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function checkInsertionExpiration(){
        if(date('Ymd',strtotime($this->expires_at)) < date('Ymd')){
            $this->active = 0;
            $this->save();
        }
    }

    public static function getProducts($placement = null,$howMayProducts=2){

        $query = Advinsertion::whereActive(1);
        if(!is_null($placement)){
            $query = $query->wherePlacement($placement);
        }
        $insertions = collect($query->get()->pluck('id'))->shuffle()->splice(0, $howMayProducts);

        if($insertions->count() > 0){
            Advinsertion::whereIn('id', $insertions)->update([
                'views' => DB::raw('views+1')
            ]);
            return Product::whereIn('id', Advinsertion::whereIn('id', $insertions)->get()->pluck('product_id'))->get();
        }

        return collect([]);

    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */


    public function product(){
        return $this->belongsTo(Product::class);
    }

    public function advpackpurchase(){
        return $this->belongsTo(Advpackpurchase::class);
    }


    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}

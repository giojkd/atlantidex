<?php

namespace App\Models;

use App\Role;
use App\Traits\Helpers;
use App\Traits\Invoicerowable;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Auth;

class Pack extends Model
{
    use CrudTrait;
    use HasTranslations;
    use Invoicerowable;
    use Helpers;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'packs';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];
    protected $translatable = ['name', 'description'];
    protected $casts = ['required_documents' => 'json'];
    public $price_column_name = 'price';
    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function identifierForInvoice(){
        return $this->name;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function role(){
        return $this->belongsTo(Role::class);
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    /*
        public function setCoverAttribute($value){
        $this->attributes['cover'] = str_replace('public/','',$value);
    }
    */



    public function checkPurchasability(){

        $user = Auth::user();

        if($user->spendings_to_unlock_merchant_marketer_pack >= env('MIN_CHILDREN_PURCHASE_TO_UNLOCK_PACK')){
            return true;
        }
        return false;

    }


}

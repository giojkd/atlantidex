<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Auth;

class Event extends Model
{
    use CrudTrait;
    use HasTranslations;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'events';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $translatable = ['title','short_description','description'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    protected $casts = [
        'photos' => 'array',
        'happens_at' => 'datetime',
        'available_tickets' => 'array',
        'address' => 'array'
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    static function eventTypes(){
        $return = [];
        if(Auth::user()->hasRole('Superuser')){
            $return = [1 => 'Aziendale', 2=> 'Network'];
        }else{
            $return = [2 => 'Network'];
        }
        return $return;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function tickets(){
        return $this->hasMany('App\Models\Ticket');
    }

    public function user(){
        return $this->belongsTo('App\Models\BackpackUser');
    }


    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    public function getSlidesFileAttribute($value){

        return str_replace('public/','',$value);

    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function setSlidesFileAttribute($value)
    {

        $attribute_name = "slides_file";
        // or use your own disk, defined in config/filesystems.php
        $disk = config('backpack.base.root_disk_name');
        // destination path relative to the disk above
        $destination_path = "public/uploads/slides_files";

        $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);
    }
}

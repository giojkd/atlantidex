<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\BackpackUser;

class Invoicerow extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'invoicerows';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];
    protected $appends = ['description'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function invoicerowable()
    {
        return $this->morphTo();
    }

    public function user(){
        return $this->belongsTo(BackpackUser::class,'user_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    public function getDescriptionAttribute(){
        #$user = $this->user;
        $locale = 'it';
        $models = [
            'App\Models\Pack' => ['name' => ['it' => 'Pack'], 'identifierForInvoice' => 'identifierForInvoice'],
            'App\Models\Gdovouchercode' => ['name' => ['it' => 'Voucher della GDO'], 'identifierForInvoice' => 'identifierForInvoice'],
            'App\Models\Ticket' => ['name' => ['it' => 'Biglietto'], 'identifierForInvoice' => 'identifierForInvoice'],
            'App\Models\Epvoucher' => ['name' => ['it' => 'Voucher'], 'identifierForInvoice' => 'identifierForInvoice'],
            'App\Models\Advpack' => ['name' => ['it' => 'Adv Pack'], 'identifierForInvoice' => 'identifierForInvoice'],
            'App\Models\Leadrow' => ['name' => ['it' => 'Acquisto'], 'identifierForInvoice' => 'identifierForInvoice'],
        ];
        $description = ['Acquisto'];
        $description[] =  $models[$this->invoicerowable_type]['name'][$locale];
        $item = $this->invoicerowable()->first();
        if(!is_null($item)){
            $identifier = $models[$this->invoicerowable_type]['identifierForInvoice'];
            $description[] = $item->$identifier();
            return collect($description)->implode(' ');
        }
        return '';
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}

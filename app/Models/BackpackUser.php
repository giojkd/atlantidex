<?php

namespace App\Models;

use App\User;
use Backpack\CRUD\app\Models\Traits\InheritsRelationsFromParentModel;
use Backpack\CRUD\app\Notifications\ResetPasswordNotification as ResetPasswordNotification;
use Illuminate\Notifications\Notifiable;
use Plansky\CreditCard\Generator;
use App\Traits\Reviewable;
use App\Models\Documentcode;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Spatie\Permission\Traits\HasRoles;
use Laravel\Cashier\Billable;
use Illuminate\Support\Facades\DB;
use Kalnoy\Nestedset\NodeTrait;
use Str;
use Voucher;
use App\Models\Document;
use App\Traits\CanImpersonateTrait;
use DateTime;
use Auth;
use Exception;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;
use Laravel\Scout\Searchable;
use jobayerccj\Skebby\Skebby;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use App\Traits\Helpers;
use Illuminate\Database\Eloquent\Relations\HasMany;

class BackpackUser extends User implements MustVerifyEmail
{
    #use InheritsRelationsFromParentModel;
    use \Staudenmeir\EloquentHasManyDeep\HasRelationships;
    use Notifiable;
    use Reviewable;
    use CrudTrait; // <----- this
    use HasRoles; // <------ and this
    use Billable;
    #use NodeTrait;
    use CanImpersonateTrait;
    use SoftDeletes;
    use HasTranslations;
    use \Venturecraft\Revisionable\RevisionableTrait;
    use Helpers;
    use Searchable {
        Searchable::usesSoftDelete insteadof NodeTrait;
    }

    use NodeTrait {
        NodeTrait::create insteadof HasTranslations;
    }



    protected $translatable = ['short_description', 'description'];
    #protected $translatable = [];

    protected $with = ['children', 'merchants','roles'];
    //protected $with = ['merchants'];
    protected $guard_name = 'web'; #<------ this is very important to assign a role to the user

    protected $casts = [
        'date_of_birth' => 'date',
        'address' => 'json',
        'payment_intent_feedback' => 'json',
        'bank_account' => 'json',
        'has_purchased_pack' => 'date',
        'has_purchased_min_vouchers' => 'date',
        'has_signed_agreement' => 'date',
        'email_verified_at' => 'datetime',
    ];

    protected $table = 'users';

    public function isTranslatableAttribute($key): bool

    {

        return in_array($key, $this->getTranslatableAttributes());
    }

    public function generateApiToken()
    {
        $this->api_token = str_random(60);
        $this->save();

        return $this->api_token;
    }

    public function identifiableName()
    {
        return $this->full_name;
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function ($user) {
            #$generator = new Generator();
            #$user->code = (string)$generator->single(800);
            $userCode = '39' . str_pad(Documentcode::getNew(BackpackUser::class), 9, '0', STR_PAD_LEFT);
            $user->code = $userCode;
            $user->code_hash = bcrypt($userCode);
        });

        static::created(function ($user) {
        });
    }






    /*
   |--------------------------------------------------------------------------
   | FUNCTIONS
   |--------------------------------------------------------------------------
   */

    public function toSearchableArray()
    {


        $data = [
            'name'  => $this->name,
            'surname' => $this->surname,
            'email' => $this->email,
            'company_name' => $this->company_name,
            'code' => $this->code,
            'sponsor_name' => (is_object($this->sponsor)) ? $this->sponsor->name : '',
            'sponsor_surname' => (is_object($this->sponsor)) ? $this->sponsor->surname : '',
            'phone' => $this->phone,
            'fiscal_code' => $this->fiscal_code
        ];


        return $data;
    }

    public function getSponsorFullName(){
        if(is_object($this->sponsor)){
            return $this->sponsor->full_name;
        }
        return '';
    }

   public function getAffiliatedCompanies(){

        $user = $this;
        $return = new Collection();

        $users  = $user->children;

        $userfiltered = $users->filter(function($value, $key){
            return $value->hasRole('Company');
        });
        foreach($userfiltered as $item) {
            $return->push($item);
        }

        $userfiltered = $user->where('virtual_parent_id',$user->id)->get();



        #dd($userfiltered);
/*
        $userfiltered = $users->filter(function ($value, $key) use($user) {
            return ($value->hasRole('Company') && $value->virtual_parent_id == $user->id) ;
        });
*/
        foreach ($userfiltered as $item) {
            $return->push($item);
        }

        if ($user->hasRole('Company')) {
            $return->push($user);
        }

        return $return->unique('id');
   }

   public function getProductionMonths(){
        $months = [
            #2021/2022
            ['20201019','20201118','20201209','Ottobre 2020',0],
            ['20201118','20201216','20210107','Novembre 2020',1],
            ['20201216','20210120','20210212','Dicembre 2020',2],
            ['20210120','20210217','20210311','Gennaio 2021',3],
            ['20210217','20210317','20210408','Febbraio 2021',4],
            ['20210317','20210421','20210513','Marzo 2021',5],
            ['20210421','20210519','20210610','Aprile 2021',6],
            ['20210519','20210616','20210708','Maggio 2021',7],
            ['20210616','20210714','20210805','Giugno 2021',8],
            ['20210714','20210818','20210909','Luglio 2021',9],
            ['20210818','20210915','20211007','Agosto 2021',10],
            ['20210915','20211020','20211111','Settembre 2021',11],
            ['20211020','20211117','20211209','Ottobre 2021',12],
            ['20211117','20211216','20220106','Novembree 2021',13],
            ['20211215','20220119','20220210','Dicembre 2021',14],
            #2022/2023
            ['20220119','20220216','20220310','Gennaio 2022',15],
            ['20220216','20220316','20220407','Febbraio 2022',16],
            ['20220316','20220413','20220505','Marzo 2022',17],
            ['20220413','20220511','20220603','Aprile 2022',18],
            ['20220511','20220608','20220630','Maggio 2022',19],
            ['20220608','20220706','20220728','Giugno 2022',20],
            ['20220706','20220803','20220825','Luglio 2022',21],
            ['20220803','20220831','20220922','Agosto 2022',22],
            ['20220831','20221005','20221027','Settembre 2022',23],
            ['20221005','20221102','20221124','Ottobre 2022',24],
            ['20221102','20221207','20221229','Novembre 2022',25],
            ['20221207','20230111','20230202','Dicembre 2022',26],
        ];

        return $months;
   }

   public function getProductionMonthsUntilGivenDate($date = null){
        $date = (!is_null($date)) ? date('Ymd',strtotime($date)) : date('Ymd');
        $productionMonths = $this->getProductionMonths();
        $months = [];
        foreach($productionMonths as $month){
            if($month[0] <= $date){
                $months[] = $month;
            }
        }
        return $months;
   }

   public function getProductionMonthByDate($date = null){
        $date = (!is_null($date)) ? date('Ymd', strtotime($date)) : date('Ymd');
        $months = $this->getProductionMonths();
        foreach($months as $month){
            if($date >= $month[0] && $date < $month[1]){
                return $month;
            }
        }

        abort(401);
   }

    public function getProductionMonthIndexByDate($date = null)
    {
        $date = (!is_null($date)) ? date('Ymd',
            strtotime($date)
        ) : date('Ymd');
        $months = $this->getProductionMonths();
        foreach ($months as $index => $month) {
            if ($date >= $month[0] && $date <= $month[1]) {
                return $index;
            }
        }

        abort(401);
    }

    public function voucherpurchases()
    {
        return $this->hasMany('App\Models\Voucherpurchase', 'user_id');
    }

    public function voucheroperations(){
        return $this->hasMany(Voucheroperation::class,'user_id');
    }

    public function getVoucherAmountByEPsPurchasedLastYear($EPs)
    {
        return $this->voucherpurchases()->where('eps', $EPs)->whereBetween('created_at', [date('Y-m-d H:i:s', strtotime('-1years')), date('Y-m-d H:i:s')])->get()->sum('amount');
    }

    public function getRemainingPurchaseableAmountCurrentYear()
    {
        return $this->voucherpurchases()->whereBetween('created_at', [date('Y-m-d H:i:s', strtotime('-1years')), date('Y-m-d H:i:s')])->get()->sum('amount');
    }

    public function consolidateVouchers(){
        $user = $this;
        $voucherPurchases = $user->voucherpurchases->groupBy('eps');

        $voucherPurchases->each(function ($purchases, $eps) use ($user) {
            $expenses = $user->voucheroperations()->where('eps', $eps)->get();
            $spent = $expenses->sum('amount');
            $user->vouchers()->create([
                'eps' => $eps,
                'amount' => $purchases->sum('amount'),
                'spent' => $spent
            ]);
        });
    }

    public function canPurchaseAdvPack($advPack){
        if (is_null($advPack->required_documents)) {
            return true;
        }

        $signedDocuments = $this->documents->pluck('id');

        $requiredDocuments = collect($advPack->required_documents);
        $intersect = $signedDocuments->intersect($requiredDocuments);
        $diff = $intersect->diff($requiredDocuments);


        return ($diff->isEmpty() && !$signedDocuments->isEmpty() && !$intersect->isEmpty());
    }

    public function canPurchasePack($pack)
    {
        if (is_null($pack->required_documents)) {
            return true;
        }

        $signedDocuments = $this->documents->pluck('id');

        $requiredDocuments = collect($pack->required_documents);
        $intersect = $signedDocuments->intersect($requiredDocuments);
        $diff = $intersect->diff($requiredDocuments);


        return ($diff->isEmpty() && !$signedDocuments->isEmpty() && !$intersect->isEmpty());
    }

    public function getLftName()
    {
        return 'lft';
    }

    public function getRgtName()
    {
        return 'rgt';
    }

    public function isAdministrator()
    {
        return $this->roles()->where('name', config('backpack.permissionmanager.admin_role', 'Superuser'))->exists();
    }



    public function setVoucherAmount($eps, $amount)
    {
        $voucher = $this->vouchers()->firstOrCreate(['eps' => $eps]);
        $voucher->amount += $amount;
        $voucher->purchased += $amount;
        $voucher->save();
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function getEmailForPasswordReset()
    {
        return $this->email;
    }

    public function isMerchantMarketer()
    {
        if ($this->has('pack') && !is_null($this->pack)) {
            if ($this->pack->tag == 'merchant_marketer_pack') {
                return true;
            }
            return false;
        }
        return false;
    }





    /*
     |--------------------------------------------------------------------------
     | ACCESSORS
     |--------------------------------------------------------------------------
    */

    public function getFullNameAttribute($value)
    {
        return $this->name . ' ' . $this->surname;
    }

    public function getFullNameEmailAttribute($value)
    {
        return $this->name . ' ' . $this->surname . ': ' . $this->email;
    }

    public function getDetailedNameAttribute($value){
        return $this->name . ' ' . $this->surname . ': ' . $this->email.' '.$this->code.' '. $this->phone;
    }

    public function getSuperDetailedNameAttribute($value)
    {
        return $this->name . ' ' . $this->surname . ': ' . $this->email . ' ' . $this->code . ' ' . $this->phone. ' '.$this->fiscal_code;
    }

    public function getMaskedPhoneAttribute($value){
        return 'xxxxxxx'.substr($this->phone,-4);
        #return $this->phone;
    }

    public function getAddressComponent($component){
        $a = $this->address;
        if(isset($a[$component])){
            return $a[$component];
        }else{
            return 'NA';
        }
    }

    public function getAddressNameAttribute($value)
    {
        if($this->getAddressComponent('name') != 'NA'){
            return $this->getAddressComponent('name') . ' ' . $this->getAddressComponent('postcode') . ' ' . $this->getAddressComponent('county');
        }
        else{
            return $this->getAddressComponent('value') . ' ' . $this->getAddressComponent('postal_code') . ' ' . $this->getAddressComponent('administrative_area_level_2');
        }

    }


    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function isntanbids(){
        return $this->hasMany(Instantbid::class);
    }


    public function payouts(){
        return $this->hasMany(Payout::class,'user_id');
    }


    public function eppluspayouts()
    {
        return $this->hasMany(Eppluspayout::class, 'user_id');
    }

    public function companycategories(){
        return $this->belongsToMany(Companycategory::class,'companycategory_user','user_id')->withPivot('commission');
    }

    function schedules(){
        return $this->hasManyDeep(Schedule::class,[Merchant::class],['user_id']);
    }


    public function leadrows(){
        return $this->hasManyDeep(
            Leadrow::class,
            [Merchant::class],
            ['user_id']
        );
    }

    public function leads(){
        return $this->hasManyDeepFromRelations($this->leadrows(),(new Leadrow)->lead());
    }

    public function clients(){
        return $this->hasManyDeepFromRelations($this->leads(), (new Lead)->client());
    }

    public function advpacks(){
        return $this->belongsToMany(Advpack::class);
    }

    public function advpackpurchases(){
        return $this->hasMany(Advpackpurchase::class,'user_id');
    }

    public function otps(){
        return $this->hasMany(Otp::class,'user_id');
    }



    public function usercompensations(){
        return $this->hasMany(Usercompensation::class,'user_id');
    }

    public function invoicerows(){
        return $this->hasMany(Invoicerow::class,'user_id');
    }

    public function getBonusEpsPaidSoFar($career,$month){
        return $this->bonusepspayments()->whereCareerType($career)->where('month_id','<',$month)->get()->sum('value');
    }

    public function bonusepspayments(){
        return $this->hasMany('App\Models\Bonusepspayment','user_id');
    }

    public function packpurchases(){
        return $this->hasMany('App\Models\Packpurchase','user_id');
    }

    public function gdovouchers()
    {
        return $this->hasMany('App\Models\Gdovouchercode', 'user_id');
    }

    public function tickets()
    {
        return $this->hasMany('App\Models\Ticket', 'user_id');
    }

    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }

    public function merchants()
    {
        return $this->hasMany('App\Models\Merchant', 'user_id');
    }

    public function products()
    {
        $pp = collect([]);
        $this->merchants->each(function ($item, $key) use ($pp) {
            $pp->push($item->products);
        });
        return $pp->flatten()->unique('id');
        //return $this->hasManyThrough('App\Models\Product','App\Models\Merchant', 'user_id');
    }

    public function invitationlink()
    {
        return $this->belongsTo('App\Models\Invitationlink');
    }


    public function categories()
    {
        $categories = collect([]);
        $this->products()->each(function ($item, $key) use ($categories) {
            $categories->push($item->category);
        });
        return $categories->flatten()->unique();
    }

    public function addresses()
    {
        return $this->hasMany('App\Models\Address', 'user_id');
    }

    public function walletoperations()
    {
        return $this->hasMany('App\Models\Walletoperation', 'user_id');
    }

    public function documents()
    {
        return $this->belongsToMany('App\Models\Document', 'document_user', 'user_id')->withPivot(['document_url']);
    }

    public function invitationlinks()
    {
        return $this->hasMany('App\Models\Invitationlink', 'user_id');
    }


    public function changesponsorrequestssent()
    {
        return $this->hasMany('App\Models\Changesponsorrequest', 'applicant_user_id');
    }

    public function changesponsorrequestsreceived()
    {
        return $this->hasMany('App\Models\Changesponsorrequest', 'receiving_user_id');
    }

    public function vouchers()
    {
        return $this->hasMany('App\Models\Voucher', 'user_id');
    }

    public function frontqueries(){
        return $this->hasMany(Frontquery::class, 'user_id');
    }

    /*
    public function leads()
    {
        return $this->hasManyDeepFromRelations($this->leadrows(), (new Leadrow)->lead());
    }
    */

    public function bids(){
        return $this->hasManyDeepFromRelations($this->frontqueries(), (new Frontquery)->bids());
    }


    public function isActive()
    {

        if($this->active){
            return true;
        }else{
            if (count($this->roles) > 0) {
                $isActive = $this->isActiveByType(collect($this->roles)->first()->name);
                if($isActive){
                    $this->active = 1;
                    $this->save();
                }
                return $isActive;
            } else {
                return false;
            }
        }
    }

    public function isActiveByType($type)
    {
        $func = 'isActive' . Str::ucfirst(Str::lower($type));
        return $this->$func();
    }



    public function isActiveCompany()
    {
        if (!is_null($this->has_signed_agreement) && !is_null($this->has_purchased_pack))
            return true;
        return false;
    }

    public function isActiveNetworker()
    {
        #if (!is_null($this->has_purchased_min_vouchers) && !is_null($this->has_signed_agreement) && !is_null($this->has_purchased_pack))
        if (!is_null($this->has_signed_agreement) && !is_null($this->has_purchased_pack))
            return true;
        return false;
    }

    public function getActivationSteps()
    {

        $userRole = Str::lower(Auth::user()->roles()->first()->name);

        $documentTypeByRoles = [
            'company' => 5,
            'networker' => 3
        ];

        $document = Document::where('documenttype_id', $documentTypeByRoles[$userRole])->first();

        if ($this->has_signed_agreement) {
            $docs = $this->documents->where('documenttype_id', $documentTypeByRoles[$userRole])->first();
        }
        $steps = [];
        $steps = [
        'networker' =>
            [[
                'name'  => 'Firma la richiesta di incarico',
                'description' => 'Compila e firma la richiesta di incarico, il nostro reparto amministrativo valuterà al più presto la tua candidatura e se la riterrà idonea potrai accedere all\'acquisto del Register Pack.',
                'status' => (is_null($this->has_signed_agreement)) ? 0 : 1,
                'completed_at' => $this->has_signed_agreement,
                'activation_link' => Route('MyAtlantinex.documents.show', ['document' => $document->id]),
                'requires_steps' => [],
                'buttons' => ($this->has_signed_agreement) ? [['helptext' => 'Al fine di completare l\'operazione di richiesta di incarico è necessario compilare e firmare in tutte le pagine i documenti ed inviarli per posta unitamente ad un documento di identità in corso di validità al seguente indirizzo: Atlantinex SRL, Viale Colombo 11, 54033 Marina di Carrara (MS)','is_download' => 1, 'label' => 'Scarica il documento', 'href' => $docs->pivot->document_url]] : []
            ],
           /* [
                'name'  => 'Acquista i tuoi voucher',
                'description' => 'Acquista i tuoi voucher Atlantidex ed inizia subito a fare shopping sul nostro portale!',
                'status' => (is_null($this->has_purchased_min_vouchers)) ? 0 : 1,
                'completed_at' => $this->has_purchased_min_vouchers,
                'activation_link' => Route('MyAtlantinex.vouchers.index'),
                'requires_steps' => [],
                'buttons' => []
            ], */
            [
                'name'  => 'Acquista il Register Pack',
                'description' => 'Acquista il tuo Register Pack ed entra subito a far parte del Network Atlantinex per costruire la tua carriera di successo!',
                'status' => (is_null($this->has_purchased_pack)) ? 0 : 1,
                'completed_at' => $this->has_purchased_pack,
                'activation_link' => Route('MyAtlantinex.packs.index'),
                #'requires_steps' => [0, 1],
                'requires_steps' => [0],
                'buttons' => []
            ]],
        'company' => [
                [
                    'name'  => 'Contratto impresa convenzionata',
                    'description' => 'Firma il contratto di affiliazione alla piattaforma Atlantidex per la tua azienda',
                    'status' => (is_null($this->has_signed_agreement)) ? 0 : 1,
                    'completed_at' => $this->has_signed_agreement,
                    'activation_link' => Route('MyAtlantinex.documents.show', ['document' => $document->id]),
                    'requires_steps' => [],
                    'buttons' => ($this->has_signed_agreement) ? [['helptext' => '', 'is_download' => 1, 'label' => 'Scarica il documento', 'href' => $docs->pivot->document_url]] : []
                ],
                [
                    'name'  => 'Acquista il Merchant Pack',
                    'description' => 'Acquista il tuo Merchant Pack e fai entrare la tua azienda all\'interno del portale Atlantidex!',
                    'status' => (is_null($this->has_purchased_pack)) ? 0 : 1,
                    'completed_at' => $this->has_purchased_pack,
                    'activation_link' => Route('MyAtlantinex.packs.index'),
                    'requires_steps' => [0],
                    'buttons' => []
                ]
        ]

        ];




        return $steps[$userRole];
    }



    public function stepIsAllowed($step, $steps)
    {
        if (isset($steps[$step]['requires_steps']) && count($steps[$step]['requires_steps']) > 0) {
            $requiredSteps = $steps[$step]['requires_steps'];
            foreach ($requiredSteps as $requiredStep) {
                if (!$steps[$requiredStep]['status']) {
                    return false;
                }
            }
            return true;
        }
        return true;
    }

    public function isActiveSuperuser()
    {
        return true;
    }

    public function isActiveCustomer()
    {
        return true;
    }



    public function getActivationLink()
    {
        $links = [
            'Networker' => Route('MyAtlantinex.activate.index'),
            'Company' => Route('Company.activate.index')
        ];
        $role = collect($this->roles)->first()->name;
        return $links[$role];
    }

    public function checkIfHasPurchasedMinVouchers()
    {

        $minPurchaseToBuyStarterPack = ($this->minimum_purchase_to_buy_starter_pack > 0 ) ? $this->minimum_purchase_to_buy_starter_pack : config('app.min_spending_amount');

        if ($this->total_spendings >= $minPurchaseToBuyStarterPack) {
            $this->has_purchased_min_vouchers = date('Y-m-d H:i:s');
            $this->save();
            return true;
        } else {
            return false;
        }

        /*
        $vouchersAmount = $this->vouchers->sum('amount');
        if ($vouchersAmount >= config('app.min_vouchers_amount')) {
            $this->has_purchased_min_vouchers = date('Y-m-d H:i:s');
            $this->save();
            return true;
        }
        return false;
        */
    }

    public function pack()
    {
        return $this->belongsTo('App\Models\Pack');
    }

    public function epoperations()
    {
        return $this->hasMany(Epoperation::class, 'user_id');
    }

    public function epplusoperations()
    {
        return $this->hasMany(Epplusoperation::class, 'user_id');
    }

    public function commissionoperations()
    {
        return $this->hasMany('App\Models\Commissionoperation', 'user_id');
    }



    public function sponsor()
    {
        return $this->belongsTo('App\Models\Backpackuser', 'parent_id');
    }

    public function virtualSponsor(){
        return $this->belongsTo('App\Models\Backpackuser', 'virtual_parent_id');
    }

    public function virtual_sponsor()
    {
        return $this->belongsTo('App\Models\Backpackuser', 'virtual_parent_id');
    }

    public function getLongRunCareerLevels(){
        return
        [
            [
                'name' => 'Livello 1',
                'tag' => 'level_1',
                'required_eps' => 5000,
                'monthly_career_bonus' => 0,
                'index' => 1,
                'accountable_leg' => 2,
                'monthly_career_bonus_next_months' => 0
            ],
            [
                'name' => 'Livello 2',
                'tag' => 'level_2',
                'required_eps' => 10000,
                'monthly_career_bonus' => 125,
                'index' => 2,
                'accountable_leg' => 2,
                'monthly_career_bonus_next_months' => 60

            ],
            [
                'name' => 'Livello 3',
                'tag' => 'level_3',
                'required_eps' => 25000,
                'monthly_career_bonus' => 300,
                'index' => 3,
                'accountable_leg' => 2,
                'monthly_career_bonus_next_months' => 125
            ],
            [
                'name' => 'Livello 4',
                'tag' => 'level_4',
                'required_eps' => 60000,
                'monthly_career_bonus' => 750,
                'index' => 4,
                'accountable_leg' => 2,
                'monthly_career_bonus_next_months' => 125
            ],
            [
                'name' => 'Livello 5',
                'tag' => 'level_5',
                'required_eps' => 150000,
                'monthly_career_bonus' => 1800,
                'index' => 5,
                'accountable_leg' => 2,
                'monthly_career_bonus_next_months' => 300
            ],
            [
                'name' => 'Livello 6',
                'tag' => 'level_6',
                'required_eps' => 400000,
                'monthly_career_bonus' => 5000,
                'index' => 6,
                'accountable_leg' => 2,
                'monthly_career_bonus_next_months' => 750
            ],
            [
                'name' => 'Livello 7',
                'tag' => 'level_7',
                'required_eps' => 1000000,
                'monthly_career_bonus' => 12500,
                'index' => 7,
                'accountable_leg' => 2,
                'monthly_career_bonus_next_months' => 1800
            ],
            [
                'name' => 'Livello 8',
                'tag' => 'level_8',
                'required_eps' => 3000000,
                'monthly_career_bonus' => 30000,
                'index' => 8,
                'accountable_leg' => 2,
                'monthly_career_bonus_next_months' => 5000
            ],
            [
                'name' => 'Livello 8.1',
                'tag' => 'level_8_1',
                'required_eps' => 9000000,
                'monthly_career_bonus' => 50000,
                'index' => 8.1,
                'accountable_leg' => 3,
                'monthly_career_bonus_next_months' => 12500
            ],
            [
                'name' => 'Livello 8.2',
                'tag' => 'level_8_2',
                'required_eps' => 18000000,
                'monthly_career_bonus' => 120000,
                'index' => 8.2,
                'accountable_leg' => 3,
                'monthly_career_bonus_next_months' => 30000
            ],
            [
                'name' => 'Livello 8.3',
                'tag' => 'level_8_3',
                'required_eps' => 36000000,
                'monthly_career_bonus' => 200000,
                'index' => 8.3,
                'accountable_leg' => 4,
                'monthly_career_bonus_next_months' => 50000
            ],
            [
                'name' => 'Livello 8.4',
                'tag' => 'level_8_4',
                'required_eps' => 72000000,
                'monthly_career_bonus' => 400000,
                'index' => 8.4,
                'accountable_leg' => 4,
                'monthly_career_bonus_next_months' => 120000
            ],
            [
                'name' => 'Livello 8.5',
                'tag' => 'level_8_5',
                'required_eps' => 144000000,
                'monthly_career_bonus' => 500000,
                'index' => 8.5,
                'accountable_leg' => 4,
                'monthly_career_bonus_next_months' => 200000
            ],
            [
                'name' => 'Livello 8.6',
                'tag' => 'level_8_6',
                'required_eps' => 288000000,
                'monthly_career_bonus' => 600000,
                'index' => 8.6,
                'accountable_leg' => 5,
                'monthly_career_bonus_next_months' => 400000
            ],
            [
                'name' => 'Livello 8.7',
                'tag' => 'level_8_7',
                'required_eps' => 576000000,
                'monthly_career_bonus' => 1200000,
                'index' => 8.7,
                'accountable_leg' => 5,
                'monthly_career_bonus_next_months' => 500000
            ],
            [
                'name' => 'Livello 8.8',
                'tag' => 'level_8_8',
                'required_eps' => 1152000000,
                'monthly_career_bonus' => 1500000,
                'index' => 8.8,
                'accountable_leg' => 5,
                'monthly_career_bonus_next_months' => 600000
            ],
        ];
    }

    public function getPerformancesLongRun($monthId,$consolidatePaidBonusEps = false)
    {

        $productionMonth = $this->getProductionMonths()[$monthId];

        $from = date('Y-m-d',strtotime($productionMonth[0]));
        $to = date('Y-m-d',strtotime($productionMonth[1].'+1days'));


        $allEps = 0;
        $children = $this->children; #determina le gambe, i diretti
        $unlockedEps = 0;



        if (!is_null($children)) {


            if($monthId > 0){
                if(is_object($this->careers()->whereMonthId(($monthId - 1))->whereCareerType('long_run_career')->first())){
                    $previousMonthsEps = $this->careers()->whereMonthId(($monthId - 1))->whereCareerType('long_run_career')->first()->total;
                }else{
                    $previousMonthsEps = 0;
                }
            }else{
                $previousMonthsEps = 0;
            }


            $allEps = $children->reduce(function ($carry, $child) use($from,$to) {
                return $carry + Epoperation::whereBetween('created_at', [$from, $to])->whereIn('user_id', BackpackUser::descendantsAndSelf($child->id)->pluck('id')->toArray())->get()->sum('value');
            }, 0);

            $userBlockedEps = $this->epoperations()->where('is_careerable', 0)->where('sign', 1)->get()->sum('value');
            $userActiveEps = $this->epoperations()->whereBetween('created_at', [$from, $to])->where('is_careerable', 1)->get()->sum('value'); #all of its personal ep that must be added fully

            $unlockedEps = ($userBlockedEps < $allEps) ? $userBlockedEps : $allEps;

            $bonusEpsPaidSoFar = $this->getBonusEpsPaidSoFar('long_run_career',$monthId);



            $unlockedEps-= $bonusEpsPaidSoFar; #must remove bonus eps already paid from unlocked eps

            $unlockedEps = ($unlockedEps > 0) ?  $unlockedEps : 0;

            if($consolidatePaidBonusEps){
                Bonusepspayment::updateOrCreate([
                    'user_id' => $this->id,
                    'month_id' => $monthId,
                    'career_type' => 'long_run_career'
                ], [
                    'value' => $unlockedEps
                ]);
            }


            $epsToBeAddedFully = $userActiveEps + $unlockedEps;

            $levels = $this->getLongRunCareerLevels();

            $productionMonths = [$productionMonth];

            $found = 0;

            $legsContributionTemplate = [];
            $legsTotalTemplate = [];
            $legsContributionSingleLegTemplate = [];

            foreach ($children as $child) {
                $legsContributionTemplate[$child->id] = 0;
                $legsTotalTemplate[$child->id] = 0;
                $legsContributionSingleLegTemplate[$child->id] = 0;
            }

            for ($i = count($levels) - 1; $i >= 0; $i--) {

                $children = $this->children; #determina le gambe, i diretti

                #every level we try the entire procedure

                $reps = $levels[$i]['required_eps'];
                $level = $levels[$i];

                $legsTotal = $legsTotalTemplate;
                $legsContribution = $legsContributionTemplate;
                $legsContributionSingleLeg = $legsContributionSingleLegTemplate;



                $attemptEps = 0;

                $acl = $level['accountable_leg'] - 1;

                foreach ($productionMonths as $month) {

                    $from = date('Y-m-d', strtotime($month[0]));
                    #$to = date('Y-m-d', strtotime($month[1].'+1days'));
                    $to = date('Y-m-d', strtotime($month[1]));



                    foreach ($children as $index => $child) {
                        $childDownline = BackpackUser::descendantsAndSelf($child->id);
                        $allLineEps = Epoperation::whereIn('user_id', $childDownline->pluck('id')->toArray())
                            ->whereBetween('created_at', [$from, $to])
                            ->get()
                            ->sum('value');
                        $children[$index]->allLineEps = $allLineEps;
                    }

                    $children = $children->sortByDesc('allLineEps')->values(); #ordino le gambe dalla più forte alla più debole



                    if (isset($children[$acl])) {

                        foreach ($children as $index => $child) {

                            if ($index >= ($acl)) {
                                $attemptEps += $child->allLineEps;
                                $legsContribution[$child->id] += $child->allLineEps;

                            }
                            if ($index < $acl) {
                                $attemptEps += $children[$acl]->allLineEps;
                                $legsContribution[$child->id] += $children[$acl]->allLineEps;
                            }

                            $legsTotal[$child->id] += $child->allLineEps;

                        }

                    }else{

                        foreach ($children as $index => $child) {
                            $legsContributionSingleLeg[$child->id] += $child->allLineEps;
                            $legsTotal[$child->id] += $child->allLineEps;
                        }
                    }


                }

                $attemptEps += $epsToBeAddedFully;

                if(isset($previousMonthsEps)){
                    $attemptEps += $previousMonthsEps;
                }



                if ($attemptEps >= $reps) {
                    $found = 1;
                    $level['previousMonthsEps'] = $previousMonthsEps;
                    $level['attemptEps'] = $attemptEps;
                    $level['legsContribution'] = collect($legsContribution)->sortDesc();
                    $level['legsTotal'] = collect($legsTotal)->sortDesc();
                    $level['bonus_eps'] = $unlockedEps;
                    $level['bonus_eps_potential'] = $unlockedEps;
                    $level['personal_eps'] = $userActiveEps;

                    $level['legsTotalTotal'] = collect($legsTotal)->sum();
                    $level['legsContributionTotal'] = collect($legsContribution)->sum();
                    $level['allEps'] = collect($legsContribution)->sum() + $userActiveEps + $unlockedEps + $previousMonthsEps;

                    return $level;
                }
            }

            #dd($legsContributionSingleLeg);

            if ($found == 0) {


                if($children->count() > 1){


                    $dataReturn = [
                        'legsContribution' => $legsContribution,
                        'legsTotal' => $legsTotal,
                        'legsTotalTotal' => 0,
                        'previousMonthsEps' => $previousMonthsEps,
                        'legsContributionTotal' => collect($legsContribution)->sum(),
                        'index' => 0,
                        'bonus_eps' => $unlockedEps,
                        'bonus_eps_potential' => $unlockedEps,
                        'personal_eps' => $userActiveEps,
                        'allEps' =>  collect($legsContribution)->sum() + $userActiveEps + $unlockedEps + $previousMonthsEps
                    ];



                }else{

                    $dataReturn = [
                        'legsContribution' => $legsContributionSingleLeg,
                        'legsTotal' => $legsTotal,
                        'legsTotalTotal' => 0,
                        'previousMonthsEps' => $previousMonthsEps,
                        'legsContributionTotal' => collect($legsContribution)->sum(),
                        'index' => 0,
                        'bonus_eps' => 0,
                        'bonus_eps_potential' => $unlockedEps,
                        'personal_eps' => $userActiveEps,
                        'allEps' =>  collect($legsContribution)->sum() + $userActiveEps + $unlockedEps + $previousMonthsEps
                    ];

                }


                #dd($dataReturn);


            }




            return $dataReturn;
        }
    }

    public function careers(){
        return $this->hasMany('App\Models\Career','user_id');
    }


    public function getPerformances($from = null, $to = null, $consolidatePaidBonusEps = false)
    {

        $monthId = $this->getProductionMonthIndexByDate($from);

        $from = (!is_null($from)) ? date('Y-m-d H:i:s', strtotime($from)) : date('Ym') . '01';
        $to = (!is_null($to)) ? date('Y-m-d H:i:s', strtotime($to)) : date('Ymt');



        $children = $this->children; #determinale gambe, i diretti

        $allEps = 0;

        if (!is_null($children)) {

            $legTotals = [];

            foreach ($children as $index => $child) {
                $childDownline = BackpackUser::descendantsAndSelf($child->id);
                $allLineEpsOperations = Epoperation::whereIn('user_id', $childDownline->pluck('id')->toArray())->whereBetween('created_at', [$from, $to])->get();
                $allLineEps = $allLineEpsOperations->sum('value');
                $allEps += $allLineEps;
                $children[$index]->allLineEps = $allLineEps;
                $legTotals[$child->id] = $allLineEps;
                $legTotalsOp[$child->id] = $allLineEpsOperations;
            }

            #dd($legTotalsOp);

            #$userBlockedEps = $this->epoperations()->whereBetween('created_at', [$from, $to])->where('is_careerable', 0)->get()->sum(function ($item) {
            $userBlockedEps = $this->epoperations()->where('is_careerable', 0)->get()->sum(function ($item) {
                return $item->value * $item->sign;
            });

            $userActiveEps = $this->epoperations()->whereBetween('created_at', [$from, $to])->where('is_careerable', 1)->get()->sum('value');

            $unlockedEps = ($userBlockedEps < $allEps) ? $userBlockedEps : $allEps;

            $bonusEpsPaidSoFar = $this->getBonusEpsPaidSoFar('monthly_career', $monthId);

            $unlockedEps -= $bonusEpsPaidSoFar; #must remove bonus eps already paid from unlocked eps

            $unlockedEps = ($unlockedEps > 0 ) ?  $unlockedEps : 0;

            if ($consolidatePaidBonusEps) {
                Bonusepspayment::updateOrCreate([
                    'user_id' => $this->id,
                    'month_id' => $monthId,
                    'career_type' => 'monthly_career'
                ], [
                    'value' => $unlockedEps
                ]);
            }



            $epsToBeAddedFully = $userActiveEps + $unlockedEps;



            $level = $this->getLevelMonthlyCareer($children, $epsToBeAddedFully);

            $level['allEps'] = $allEps;
            $level['legTotals'] = $legTotals;
            $level['legs'] = $children;
            $level['personal_eps'] = $userActiveEps;
            $level['bonus_eps'] = $unlockedEps;
            $level['monthly_career_all_potential_eps'] = $userBlockedEps;
            $level['period'] = ['from' => $from, 'to' => $to];

            return $level;
        }
    }

    public function getMonthlyCareerLevels(){

        return [
            [
                'name' => 'Livello 0',
                'tag' => 'level_0',
                'required_eps' =>  4999.999,
                'monthly_career_bonus' => 0,
                'ep_value' => 0,
                'rule' => 0.5,
                'index' => 0
            ],
            [
                'name' => 'Livello 1',
                'tag' => 'level_1',
                'required_eps' => 5000,
                'monthly_career_bonus' => 0,
                'ep_value' => 0.0185,
                'rule' => 0.5,
                'index' => 1
            ],
            [
                'name' => 'Livello 2',
                'tag' => 'level_2',
                'required_eps' => 10000,
                'monthly_career_bonus' => 125,
                'ep_value' => 0.0285,
                'rule' =>
                0.5,
                'index' => 2
            ],
            [
                'name' => 'Livello 3',
                'tag' => 'level_3',
                'required_eps' => 25000,
                'monthly_career_bonus' => 300,
                'ep_value' => 0.0385,
                'rule' =>
                0.5,
                'index' => 3
            ],
            [
                'name' => 'Livello 4',
                'tag' => 'level_4',
                'required_eps' => 60000,
                'monthly_career_bonus' => 750,
                'ep_value' => 0.0485,
                'rule' =>
                0.5,
                'index' => 4
            ],
            [
                'name' => 'Livello 5',
                'tag' => 'level_5',
                'required_eps' => 150000,
                'monthly_career_bonus' => 1800,
                'ep_value' => 0.0585,
                'rule' =>
                0.5,
                'index' => 5
            ],
            [
                'name' => 'Livello 6',
                'tag' => 'level_6',
                'required_eps' => 400000,
                'monthly_career_bonus' => 5000,
                'ep_value' => 0.0685,
                'rule' =>
                0.5,
                'index' => 6
            ],
            [
                'name' => 'Livello 7',
                'tag' => 'level_7',
                'required_eps' => 1000000,
                'monthly_career_bonus' => 12500,
                'ep_value' => 0.0785,
                'rule' =>
                0.5,
                'index' => 7
            ],
            [
                'name' => 'Livello 8',
                'tag' => 'level_8',
                'required_eps' => 3000000,
                'monthly_career_bonus' => 30000,
                'ep_value' => 0.0885,
                'rule' =>
                0.5,
                'index' => 8
            ],
            [
                'name' => 'Livello 8.1',
                'tag' => 'level_8_1',
                'required_eps' => 9000000,
                'monthly_career_bonus' => 50000,
                'ep_value' => 0.0935,
                'rule' =>
                0.33,
                'index' => 8.1
            ],
            [
                'name' => 'Livello 8.2',
                'tag' => 'level_8_2',
                'required_eps' => 18000000,
                'monthly_career_bonus' => 120000,
                'ep_value' => 0.0975,
                'rule' =>
                0.33,
                'index' => 8.2
            ],
            [
                'name' => 'Livello 8.3',
                'tag' => 'level_8_3',
                'required_eps' => 36000000,
                'monthly_career_bonus' => 200000,
                'ep_value' => 0.101,
                'rule' =>
                0.25,
                'index' => 8.3
            ],
            [
                'name' => 'Livello 8.4',
                'tag' => 'level_8_4',
                'required_eps' => 72000000,
                'monthly_career_bonus' => 400000,
                'ep_value' => 0.104,
                'rule' =>
                0.25,
                'index' => 8.4
            ],
            [
                'name' => 'Livello 8.5',
                'tag' => 'level_8_5',
                'required_eps' => 144000000,
                'monthly_career_bonus' => 500000,
                'ep_value' => 0.1065,
                'rule' =>
                0.25,
                'index' => 8.5
            ],
            [
                'name' => 'Livello 8.6',
                'tag' => 'level_8_6',
                'required_eps' => 288000000,
                'monthly_career_bonus' => 600000,
                'ep_value' => 0.1085,
                'rule' =>
                0.2,
                'index' => 8.6
            ],
            [
                'name' => 'Livello 8.7',
                'tag' => 'level_8_7',
                'required_eps' => 576000000,
                'monthly_career_bonus' => 1200000,
                'ep_value' => 0.110,
                'rule' =>
                0.2,
                'index' => 8.7
            ],
            [
                'name' => 'Livello 8.8',
                'tag' => 'level_8_8',
                'required_eps' => 1152000000,
                'monthly_career_bonus' => 1500000,
                'ep_value' => 0.111,
                'rule' =>
                0.2,
                'index' => 8.8
            ],
        ];
    }

    public function getLevelMonthlyCareer($legs, $epsToBeAddedFully = 0)
    {

        $defaultLevel =
            [
                'name' => 'Livello 0',
                'tag' => 'level_0',
                'required_eps' => 0,
                'monthly_career_bonus' => 0,
                'ep_value' => 0,
                'rule' => 0.5,
                'index' => 0
            ];

        $levels = $this->getMonthlyCareerLevels();


        $found = 0;

        for ($i = count($levels) - 1; $i >= 0; $i--) {
            $reps = $levels[$i]['required_eps'];
            $half = $reps * $levels[$i]['rule'];
            $tot = 0;
            $legContribution = [];
            foreach ($legs as $leg) {
                $partial = ($leg->allLineEps > $half) ? $half : $leg->allLineEps;
                $tot += $partial;
                $legContribution[$leg->id] = $partial;
            }
            $tot += $epsToBeAddedFully;
            if ($tot > $reps) {
                $found = 1;
                return ['level' => $levels[$i], 'tot' => $tot, 'legContribution' => $legContribution];
            }
        }

        if ($found == 0) {
            $returnData = ['level' => $defaultLevel, 'tot' => $tot, 'legContribution' => $legContribution];
            #dd($returnData);
            return $returnData;
        }
    }

    public function getClosestMerchantMarketer(){
        $ancestors = $this->ancestors()->get();
        if (!is_null($ancestors)) {
            $ancestors = $ancestors->reverse();
            foreach ($ancestors as $anc) {
                if ($anc->isMerchantMarketer()) {
                    return $anc;
                }
            }
        }
        return null;
    }

    public function getFiscalCodeAttribute($value){

        return Str::upper($value);
    }

    public function getDateOfBirthHumanReadableAttribute($value){
        return date('d/m/Y',strtotime($this->date_of_birth));
    }

    public function getAddressRoadAttribute($value){
        return $this->getAddressComponent('name');
    }

    public function getAddressPostcodeAttribute($value)
    {
        return $this->getAddressComponent('postcode');
    }

    public function getAddressCountyAttribute($value)
    {
        return $this->getAddressComponent('county');
    }

    public function scopeActive($query){
        return $query->where('active',1);
    }

    public function getPurchasedPacks(){
        $packs = $this->packpurchases;
        $return = [];
        if(!is_null($packs)){
            $packs->each(function($pack,$key) use(&$return){
                $return[] = $pack->pack->name;
            });
        }
        if(count($return) > 0){
            return implode(', ',$return);
        }
        return null;
    }

    public function calculateLevel($from = null, $to = null,$monthId = null){
        $monthlyPerformance = $this->getPerformances($from,$to);
        $longRungPerformance = $this->getPerformancesLongRun($monthId);
        $this->monthly_career_level = $monthlyPerformance['level']['index'];
        $this->long_run_career_level = $longRungPerformance['index'];
        $this->monthly_career_level_calculated_at = date('Y-m-d H:i:s');
        $this->long_run_career_level_calculated_at = date('Y-m-d H:i:s');
        $this->monthly_career_eps = $monthlyPerformance['allEps'];
        $this->long_run_career_eps = $longRungPerformance['allEps'];

        $this->monthly_career_personal_eps = $monthlyPerformance['personal_eps'];
        $this->monthly_career_bonus_eps = $monthlyPerformance['bonus_eps'];
        $this->long_run_career_personal_eps = $longRungPerformance['personal_eps'];
        $this->long_run_career_bonus_eps = $longRungPerformance['bonus_eps'];

        $this->monthly_career_all_potential_eps = $monthlyPerformance['monthly_career_all_potential_eps'];

        $this->save();

        return ['monthlyCareer' => $monthlyPerformance,'longRunCareer'=> $longRungPerformance];

    }

    public function calculateCompensations($monthId = null,$debug = 0){

        #$user = Auth::user();
        $user = $this;
        $today = date('Y-m-d');

        $career = $this->careers()->whereMonthId($monthId)->get()->keyBy('career_type');

        $months = $user->getProductionMonthsUntilGivenDate($today);

        $selectedMonth = (!is_null($monthId)) ? $monthId :  count($months) - 1;

        $month = $months[$selectedMonth];

        $from = date('Y-m-d ', strtotime($month[0])).' 00:00:00';
        $to = date('Y-m-d ', strtotime($month[1])).' 00:00:00';

        $child = $this;

        $revenueBonusOperations = $this->commissionoperations()->where('commission_type','revenue_commission')->whereBetween('created_at',[$from,$to])->get();

        $revenueBonus = $revenueBonusOperations->sum('value');

        $compensation = 0;
        $directCommission = 0;

        $descendants =  $this->getAffiliatedCompanies();

        ####################################################
        #                                                  #
        # calculate merchant and merchant booster opations #
        #                                                  #
        ####################################################

        $merchantBonusPacks = Pack::where('merchant_bonus', '>', 0)->orWhere('merchant_booster_bonus', '>', 0)->get()->unique();

        $epOperations =
        Epoperation::whereIn('user_id',$descendants->pluck('id'))
        ->whereBetween('created_at',[$from,$to])
        #->whereIn('value',[49,99])
        ->where('epoperationable_type', 'App\\Models\\Pack')
        ->whereIn('epoperationable_id', $merchantBonusPacks->pluck('id'))
        ->get();

        $merchantBonus = 0;



        $merchantBonusEp = $epOperations->where('value',149)->count() * 149; #must use a proper selector that doesn't count on the fact these operations grant 49 and 99 ep
        $merchantBoosterBonusEp = $epOperations->where('value',199)->count() * 199;


        $monthlyCareerCommissionDetailed = [];

        $this->monthly_career_level = $career['monthly_career']->career_data['level']['index'];
        $children = $this
            ->children()
            ->with(['careers' => function ($query) use ($selectedMonth) {
                $query->whereMonthId($selectedMonth)->whereCareerType('monthly_career');
            }])
            ->get()
            ->transform(function ($item, $key) {
                if(is_object($item->careers->first())){
                    $item->monthly_career_level = $item->careers->first()->career_data['level']['index'];
                }else{
                    $item->monthly_career_level = 0;
                }

                return $item;
            });
            /*
            ->filter(function ($item, $key) {
                return ($item->active);
            });
            */


            ############################ v ###########################
            ############################ v ###########################
            ############################ v ###########################
            ############################ v ###########################
            ############################ v ###########################


            $this->ghi($career['monthly_career']->career_data['level']['index'], $this, $children, $compensation, $monthlyCareerCommissionDetailed, $from, $to, $career['monthly_career']->career_data['personal_eps']);


            ############################ ^ ###########################
            ############################ ^ ###########################
            ############################ ^ ###########################
            ############################ ^ ###########################
            ############################ ^ ###########################
            ############################ ^ ###########################

        $levels = collect($this->getMonthlyCareerLevels())->keyBy('index');

        if($career['monthly_career']->career_data['level']['index'] > 0){
            $userLevelEpValue = $levels[$career['monthly_career']->career_data['level']['index']]['ep_value'];
            $compensation += $userLevelEpValue * ($career['monthly_career']->career_data['personal_eps'] /*+ $career['monthly_career']->career_data['bonus_eps']*/);
        }



        #public function ghi($mainUserLevel, $user, $children, &$currentCompensation = 0 ){}

        $monthlyCareerLevels = collect($this->getMonthlyCareerLevels())->keyBy('index');
        $longRungCareerLevels = collect($this->getLongRunCareerLevels())->keyBy('index');


        $directCommissionOperations = $this->commissionoperations()->whereBetween('created_at', [$from, $to])->where(function($query){
            $query->whereIn('commission_type',['voucher_purchase'])->orWhereNull('commission_type');
        })->get();
        $directCommission = $directCommissionOperations->sum('value');


        $merchantBonusOperations = $this->commissionoperations()->whereBetween('created_at', [$from, $to])->whereIn('commission_type',['company_affiliation', 'merchant_bonus'])->get()->unique();
        $merchantBonus = $merchantBonusOperations->sum('value');


        $merchantBoosterBonusOperations = $this->commissionoperations()->whereBetween('created_at', [$from, $to])->where('commission_type', 'company_booster_affiliation')->get();
        $merchantBoosterBonus = $merchantBoosterBonusOperations->sum('value');

        $monthlySalary = 0;

        if($child->monthly_career_level > 0){
            $monthlySalary = $monthlyCareerLevels[$career['monthly_career']->career_data['level']['index']]['monthly_career_bonus'];
        }

        #$this->long_run_career_level

        #dd($longRungCareerLevels[$this->long_run_career_level]);


        $longRunCareerCommission = 0;
        $longRunCareerBonus = 0;
        $selectedMonthCareerLevel = $career['long_run_career']->career_data['index'];

        $longRunCareerMonthsWithSameLevel = $user->countLongRunCareerSameLevelMonths($selectedMonthCareerLevel) ;




        if($monthId == 0){

            $longRunCareerBonusIndex = 'monthly_career_bonus';
            $longRunCareerCommission = (isset($longRungCareerLevels[$selectedMonthCareerLevel])) ? $longRungCareerLevels[$selectedMonthCareerLevel][$longRunCareerBonusIndex] : 0;

        }else{

            if($longRunCareerMonthsWithSameLevel < 8 && $longRunCareerMonthsWithSameLevel > 0){
                $longRunCareerBonusIndex = 'monthly_career_bonus_next_months';
            }
            if($longRunCareerMonthsWithSameLevel == 0 ){
                $longRunCareerBonusIndex = 'monthly_career_bonus';
            }

            if($longRunCareerMonthsWithSameLevel < 8){
                $longRunCareerBonus = (isset($longRungCareerLevels[$selectedMonthCareerLevel])) ? $longRungCareerLevels[$selectedMonthCareerLevel][$longRunCareerBonusIndex] : 0;
            }else{
                $longRunCareerCommission = 0;
            }

        }



        #in case the user has preserved the same long run career level of last month and

        if($monthId > 0){
            $lastMonthCareer = $this->careers()->whereMonthId(($monthId - 1))->get()->keyBy('career_type');

        }


        $userCompensation = Usercompensation::firstOrCreate([
            'user_id' => $this->id,
            'month_id' => $selectedMonth
        ]);

        $compensationLines = [
            'monthly_career_commission' => $compensation,
            'monthly_career_bonus' => $monthlySalary,
            'long_run_career_commission' => $longRunCareerCommission,
            'long_run_career_bonus' => $longRunCareerBonus,
            'direct_commission' => $directCommission,
            'revenue_bonus' => $revenueBonus,
            'marketing_bonus' => 0,
            'merchant_bonus' => $merchantBonus,
            'merchant_booster_bonus' => $merchantBoosterBonus,
            'merchant_bonus_ep' => $merchantBonusEp,
            'merchant_booster_bonus_ep' => $merchantBoosterBonusEp,
            'marketing_bonus_ep' => 0
        ];

        $compensationLines['compensation_total'] = collect($compensationLines)->sum();
        $compensationLines2 = $compensationLines;

        $compensationLines2['compensation_details'] = [
            'merchantBoosterBonusOperations' => $merchantBoosterBonusOperations,
            'merchantBonusOperations' => $merchantBonusOperations,
            'directCommissionOperations' => $directCommissionOperations,
            'revenueBonusOperations' => $revenueBonusOperations,
            'monthlyCareerCommissionDetailed' => $monthlyCareerCommissionDetailed
        ];

        $userCompensation->fill($compensationLines2)->save();

        $return = [

            'monthly_career_commission_detailed' => $monthlyCareerCommissionDetailed,
            'monthly_career_commission' => $compensation,
            'monthly_career_bonus' => $monthlySalary,
            'long_run_career_commission' => $longRunCareerCommission,
            'long_run_career_bonus' => $longRunCareerBonus,
            'direct_commission' => $directCommission,
            'revenue_bonus' => $revenueBonus,
            'marketing_bonus' => 0,
            'merchant_bonus' => $merchantBonus,
            'merchant_bonus_operations' => $merchantBonusOperations,
            'merchant_bonus_ep' => $merchantBonusEp,
            'merchant_booster_bonus' => $merchantBoosterBonus,
            'merchant_booster_bonus_operations' => $merchantBoosterBonusOperations,
            'merchant_booster_bonus_ep' => $merchantBoosterBonusEp,
            'production_month' => $month,
            'marketing_bonus_ep' => 0,
            'compensation_total' => collect($compensationLines)->sum()

        ];


        return $return;

    }

    public function longruncareerlevels(){
        return $this->hasMany(Longruncareerlevel::class,'user_id');
    }

    public function countLongRunCareerSameLevelMonths($level = 0){

        return $this->longruncareerlevels()->where('level',$level)->count();

    }

    public function ghi($mainUserLevel, $parent, $children, &$currentCompensation = 0, &$monthlyCareerCommissionDetailed, $from, $to, $levelToLiquidate ){



        $levels = collect($this->getMonthlyCareerLevels())->keyBy('index');

        $monthId = $this->getProductionMonthIndexByDate($from);

        $children->each(function($child,$key) use ($monthId, $mainUserLevel, $parent, $levels,  &$currentCompensation,&$monthlyCareerCommissionDetailed, $from, $to, $levelToLiquidate){

            $childDetail = [];
            $childDetail[] = '('.$child->code.') '.$child->full_name;
            $nodeValue = 0;

            $childDescendantsAll =
            $child
            ->descendants()
            ->with(['careers' => function ($query) use ($monthId) {
                $query->whereMonthId($monthId)->whereCareerType('monthly_career');
            }])
            #->whereBetween('created_at', [$from, $to])
            ->get()
            ->transform(function ($item, $key) {
                if(is_object($item->careers->first())){
                    $item->monthly_career_level = $item->careers->first()->career_data['level']['index'];
                }else{
                    $item->monthly_career_level = 0;
                }

                return $item;
            });

            $childDescendants = $childDescendantsAll;/*->filter(function ($item, $key) {
                return ($item->active);
            });*/

            $childChildren = $child
            ->children()
            ->with(['careers' => function ($query) use ($monthId) {
                $query->whereMonthId($monthId)->whereCareerType('monthly_career');
            }])
            ->get()
            ->transform(function ($item, $key) {
                $item->monthly_career_level = (is_object($item->careers->first())) ? $item->careers->first()->career_data['level']['index'] : 0;
                return $item;
            });
            /*
            ->filter(function ($item, $key) {
                return ($item->active);
            });
            */

            $levelToLiquidate = ($child->monthly_career_level > $levelToLiquidate) ? $child->monthly_career_level : $levelToLiquidate ;

            try{
                $nodeLevelValue = $levels[$levelToLiquidate]['ep_value'];
            }catch(Exception $e){
                $nodeLevelValue = 0;
            }


            $childEps =  Epoperation::whereIn('user_id', $childDescendantsAll->pluck('id'))->whereBetween('created_at',[$from, $to])->get()->sum('value');

            if ($child->monthly_career_level < $mainUserLevel) {
                if($childChildren->count() > 0){

                        #get the highest level in the full downline
                        $highestChildDownlineLevel = $childDescendants->sortByDesc('monthly_career_level')->first()->monthly_career_level;


                        if($highestChildDownlineLevel > $child->monthly_career_level){

                            $childDetail[] = 'uno dei suoi discendenti ha un livello superiore al suo, quindi intanto liquido tutti i suoi punti';
                            $childEps = $child->epoperations()->whereBetween('created_at', [$from, $to])->get()->sum('value');


                            ##########################################################################################################################################


                            $nodeValue = ($levels[$mainUserLevel]['ep_value'] - $nodeLevelValue) * $childEps; #this could be a negative value

                            if($nodeValue > 0 ){

                            ##########################################################################################################################################

                                $currentCompensation+=$nodeValue;
                                $childDetail[] = 'nodo liquidato a '.$nodeValue.'  per ep';
                                $childDetail[] = 'livello liquidato L'. $child->monthly_career_level;
                                $childDetail[] = 'ep liquidati: ' . number_format( $childEps, '2',',', '.');
                                $childDetail[] = 'differenza imprenditoriale ('. number_format($levels[$mainUserLevel]['ep_value'], 2,',', '.').' - '. number_format( $nodeLevelValue, 2,',', '.').') = '. number_format(($levels[$mainUserLevel]['ep_value'] - $nodeLevelValue),2,',', '.');
                                $childDetail[] = 'parziale €'. $nodeValue;

                                $this->ghi($mainUserLevel, $child, $childChildren, $currentCompensation, $monthlyCareerCommissionDetailed, $from, $to, $levelToLiquidate);
                            }

                        } else {

                            $childEps = Epoperation::whereIn('user_id', $childDescendantsAll->pluck('id'))->whereBetween('created_at', [$from, $to])->get()->sum('value') + $child->epoperations()->whereBetween('created_at', [$from, $to])->get()->sum('value');
                            $nodeValue = ($levels[$mainUserLevel]['ep_value'] - $nodeLevelValue) * $childEps;


                            if ($nodeValue > 0) {

                                $currentCompensation += $nodeValue;

                                $childDetail[] = 'nodo liquidato a ' . $nodeValue . '  per ep';
                                $childDetail[] = 'nessuno dei discendenti ha un livello superiore al suo';
                                $childDetail[] = 'ep value per livello di mainuser '. $levels[$mainUserLevel]['ep_value'];

                                $childDetail[] = 'livello liquidato L' . $child->monthly_career_level;
                                $childDetail[] = 'ep liquidati: ' . number_format($childEps,'2',',', '.');
                                $childDetail[] = 'differenza imprenditoriale ('. number_format($levels[$mainUserLevel]['ep_value'],2,',', '.').' - '. number_format($nodeLevelValue,2,',', '.').') = '. number_format(($levels[$mainUserLevel]['ep_value'] - $nodeLevelValue), 2,',', '.');
                                $childDetail[] = 'parziale €'. number_format($nodeValue,2,',', '.');
                            }

                        }





                }else{ #if direct child has no children

                    $childDetail[] = 'differenza imprenditoriale (' . $levels[$mainUserLevel]['ep_value'] . ' - ' . $nodeLevelValue . ') = ' . ($levels[$mainUserLevel]['ep_value'] - $nodeLevelValue);
                    $childEps = $child->epoperations()->whereBetween('created_at', [$from, $to])->get()->sum('value');
                    $nodeValue = ($levels[$mainUserLevel]['ep_value'] - $nodeLevelValue) * $childEps;

                    if($nodeValue > 0){
                        $childDetail[] = 'livello liquidato ' . number_format($child->monthly_career_level, 2, ',', '.');
                        $childDetail[] = 'ep liquidati: ' . number_format($childEps, 2, ',', '.');
                        $childDetail[] = 'parziale €' . number_format($nodeValue, 2, ',', '.');
                        $currentCompensation += $nodeValue;
                    }

                }
            }else{
                $childDetail[] = 'ep non liquidati: ' . $childEps;

            }
            #if($nodeValue > 0){
                $monthlyCareerCommissionDetailed[] = implode(', ', $childDetail);
            #}
        });
    }

    public function def($userLevel = 0, $children = null, &$currentCompensation = 0, $fixedLevel, $nestedLevel, $debug, &$monthlyCareerCommissionDetailed)
    {
        if (!is_null($children) && $children->count() > 0) {

            $nestedLevel++;

            foreach ($children as $child) {

                $line = '';

                $childChildren = $child->children;
                $childDescendantsAll = $child->descendants()->get();

                $childChildren = $childChildren->filter(function($child,$key){
                    return ($child->pack_id == 2 && $child->active);
                });

                $childDescendants = $childDescendantsAll->filter(function ($child, $key) {
                    return ($child->pack_id == 2 && $child->active);
                });

                $childrenText = ($childChildren->count() > 0) ? 'has '. $children->count(). ' children ' : 'has not children ';

                #$childEps = $child->monthly_career_eps + $child->monthly_career_personal_eps + $child->monthly_career_bonus_eps;
                #$childEps = $child->monthly_career_eps + $child->monthly_career_personal_eps + $child->monthly_career_all_potential_eps;

                $childEps = Epoperation::whereIn('user_id', $childDescendantsAll->pluck('id'))->sum('value');

                $line .=  $child->full_name . ' level ' . $child->monthly_career_level . ' ep ' . $childEps . ' is son of ' . $child->parent->full_name . ' and ' . $childrenText;

                if (
                    $child->monthly_career_level < $child->parent->monthly_career_level &&
                    (
                        $childChildren->count() == 0 ||
                        $childDescendants->sortByDesc('monthly_career_level')->first()->monthly_career_level <= $child->monthly_career_level)
                    )
                    { #&& $childChildren->count() == 0){

                    $line.=$child->full_name . ' does not have descendant whose level is higher than its level and its level is lower than its parent level ';

                    $userLevel = ($child->parent->monthly_career_level > $child->monthly_career_level && $child->parent->monthly_career_level < $fixedLevel) ? $child->parent->monthly_career_level : $child->monthly_career_level;

                    #$userLevel = $child->monthly_career_level;

                    $levels = collect($this->getMonthlyCareerLevels())->keyBy('index');

                    if ($userLevel > 0) {
                        $increment = ($levels[$fixedLevel]['ep_value'] - $levels[$userLevel]['ep_value']) * $childEps;
                        $currentCompensation += $increment;
                        $line .= '(' . $levels[$fixedLevel]['ep_value'] . '-' . $levels[$userLevel]['ep_value'] . ') x ' . $childEps . ' = ' . ($levels[$fixedLevel]['ep_value'] - $levels[$userLevel]['ep_value']) * $childEps;


                    } else {
                        $increment = $levels[$fixedLevel]['ep_value'] * $childEps;
                        $currentCompensation += $increment;
                        $line .= $levels[$fixedLevel]['ep_value'] . ' x ' . $childEps . ' = ' . $levels[$fixedLevel]['ep_value'] * $childEps;

                    }


                    $monthlyCareerCommissionDetailed[] = [
                        'user' => $child,
                        'increment' => $increment,
                        'line' => $line,
                        'nested_level' => $nestedLevel
                    ];
                } else {
                    $line .= ' we don\' pay this user';

                    if($child->monthly_career_level < $fixedLevel){
                        $this->def($userLevel, $childChildren, $currentCompensation, $fixedLevel, $nestedLevel,$debug, $monthlyCareerCommissionDetailed);
                    }
                }
            }
        }
    }

    public function abc($userLevel = 0, $children = null, &$currentCompensation = 0,$fixedLevel,$nestedLevel){


        if(!is_null($children) && $children->count() > 0){
            echo '<br><br><br><br>';
            echo 'Children '.$children->count();
            $nestedLevel++;
            foreach($children as $child){

                $childChildren = $child->children;
                echo '<br>';
                echo $nestedLevel;
                for($i = 0; $i<=$nestedLevel; $i++){
                    echo '___';
                }

                $childrenText = '<b>';
                $childrenText .= ($childChildren->count() > 0) ? 'HAS CHILDREN' : 'HAS NOT CHILDREN' ;
                $childrenText .= '</b>';

                echo $child->full_name . ' level ' . $child->monthly_career_level . ' ep ' . $child->monthly_career_eps.' is son of '.$child->parent->full_name.' and '.$childrenText;

                if($child->monthly_career_level < $fixedLevel && $childChildren->count() == 0){ #&& $childChildren->count() == 0){
                    echo '<br>';
                    $child->full_name.' does not have children and its level is lower than main user';
                    $userLevel = $child->monthly_career_level;
                    $levels = collect($this->getMonthlyCareerLevels())->keyBy('index');
                    if($userLevel > 0){
                        $currentCompensation += $levels[$fixedLevel]['ep_value'] - $levels[$userLevel]['ep_value'] * $child->monthly_career_eps;
                    }else{
                            $currentCompensation += $levels[$fixedLevel]['ep_value'] * $child->monthly_career_eps;
                    }

                    echo '<br>';
                    echo 'current compensation €'.$currentCompensation;
                    echo '<br>';

                }else{
                    $this->abc($userLevel, $childChildren, $currentCompensation, $fixedLevel, $nestedLevel);
                }

            }
        }
    }

    public function getHasSignedAgreementHumanReadableAttribute(){

        if(!is_null($this->has_signed_agreement)){
            return $this->has_signed_agreement->format('Ymd');
        }
        return $this->has_signed_agreement;

    }

    public function getCompanyActivationDate(){
        if($this->active && !is_null($this->has_signed_agreement) && $this->has_purchased_pack){
            return ($this->has_signed_agreement > $this->has_purchased_pack) ? $this->has_signed_agreement->format('d/m/Y') : $this->has_purchased_pack->format('d/m/Y');
        }else{
            return 'NA';
        }
    }

    public function w2w($userTo, $amount,$receipts = [],$forceTransaction = false){

        $userFrom = $this;

        $receipts['from'] = (isset($receipts['from'])) ? $receipts['from'] : 'Acquisto da ' . $userTo->code;
        $receipts['to'] = (isset($receipts['to'])) ? $receipts['to'] : 'Pagamento da ' . $userFrom->code;

        if ($forceTransaction || $userFrom->wallet_balance > $amount) {

            $walletOperationOut = $userFrom->walletoperations()->create([
                'value' => $amount * -1,
                'receipt' => $receipts['from']
            ]);

            $walletOperationIn = $userTo->walletoperations()->create([
                'value' => $amount,
                'receipt' => $receipts['to'],
                'parent_id' => $walletOperationOut->id
            ]);

            return 1;

        } else {

            return 0;

        }
    }

    public function getSimpleCompanyInfo(){
        return [
            'id' => $this->id,
            'name' => $this->company_name,
            'coordinates' => $this->address['latlng'],
            'address' => $this->address['value'],
            'telephone' => (!is_null($this->phone)) ? $this->phone : ((is_object($this->parent)) ? $this->parent->phone : ''),
            'email' => $this->email,
            'companycategories' => ($this->companycategories->count() > 0) ? $this->companycategories : [],
            'description' => $this->description,
            'short_description' => $this->short_description
        ];
    }

    public function grantEpsFromPurchase($commission_total,$company, $epoperationable_type, $epoperationable_id, $ep_type = 'offline_purchase'){

        $user = $this;

        $eps = 0;

        if ($user->hasRole('Networker')) {

            if ($user->split_eps) {
                $eps = $commission_total / 2;
            } else {
                $eps = $commission_total;
            }

        }

        if ($user->hasRole('Customer') || $user->hasRole('Company')) {
            $eps = $commission_total / 2;
        }

        $smsText = "Complimenti per l'acquisto presso " . $company->company_name . "! Riceverai " . number_format($eps, 2, ',', '') . " EP per la tua carriera in Atlantinex";

        if ($eps > 0) {
            $user->epoperations()->create([
                'value' => $eps,
                'epoperationable_type' => $epoperationable_type,
                'epoperationable_id' => $epoperationable_id,
                'ep_type' => $ep_type,
                'description' => 'User ' . $user->id . ' purchased '. $epoperationable_type.' with id '. $epoperationable_id,
                'is_careerable' => 1,
                'sign' => 1
            ]);

            $skebby = new Skebby;
            $skebby->set_username(env('SKEBBY_USERNAME'));
            $skebby->set_password(env('SKEBBY_PASSWORD'));
            $skebby->set_method('send_sms_classic');
            $skebby->set_text($smsText);
            $skebby->set_sender(env('SKEBBY_SENDER'));
            $recipients = array('+39' . $user->phone);
            $skebby->set_recipients($recipients);
            $sending_status = $skebby->send_sms();
        }


    }

    public function grantEpsPlusFromPurchase($commission_total, $company, $epoperationable_type, $epoperationable_id,  $epplus_type = 'offline_purchase')
    {
        $user = $this;

        $eps_plus = 0;

        if ($user->hasRole('Customer') || $user->hasRole('Company')) {
            $eps_plus = $commission_total / 2;
            $walletRecharge = $commission_total / 4;
        }

        if ($user->hasRole('Networker')) {
            if ($user->split_eps) { #<----------- this is very important because otherwise marketers don't get epplus
                $eps_plus = $commission_total / 2;
                $walletRecharge = $commission_total / 4;
            }
        }

        if ($eps_plus > 0) {

            $smsText = "Complimenti per l'acquisto presso " . $company->company_name . "! Riceverai una ricarica di €" . number_format($walletRecharge, 2, ',', '');

            $user->epplusoperations()->create([
                'value' => $eps_plus,
                'epplusoperationable_type' => $epoperationable_type,
                'epplusoperationable_id' => $epoperationable_id,
                'epplus_type' => $epplus_type,
                'description' => 'User ' . $user->id . ' purchased ' . $epoperationable_type . ' with id ' . $epoperationable_id,
                'is_careerable' => 1,
                'sign' => 1
            ]);

            $skebby = new Skebby;
            $skebby->set_username(env('SKEBBY_USERNAME'));
            $skebby->set_password(env('SKEBBY_PASSWORD'));
            $skebby->set_method('send_sms_classic');
            $skebby->set_text($smsText);
            $skebby->set_sender(env('SKEBBY_SENDER'));
            $recipients = array('+39' . $user->phone);
            $skebby->set_recipients($recipients);
            $sending_status = $skebby->send_sms();

        }
    }

    public function getVoucherRecapAttribute(){
        $vouchers = $this->vouchers;
        $return = [];
        $commission = 0;
        $total = 0;
        if($vouchers->count() > 0){
            foreach($vouchers as $voucher){
                if($voucher->amount > 0){
                    $return[] = $voucher->eps . 'EP -> ' . number_format($voucher->amount, 2).'EUR';
                    $total += $voucher->amount;
                    $commission += $voucher->amount/100 * $voucher->eps ;
                }
            }
            $return[] = 'comm:'.number_format($commission,2).'EUR';
            $return[] = 'tot:'.number_format($total,2) . 'EUR';
            $return[] = 'save:'.number_format($total-$commission,2) . 'EUR';
        }

        return implode(' | ',$return);
    }


    public function getPurchasedProducts(){

        $leads = Lead::where('client_id',$this->id)->get();

        if($leads->count() > 0 ){
            $products = collect([]);
            foreach($leads as $lead){
                $rows = $lead->leadrows;
                if($rows->count() > 0){
                    $products->push($rows->pluck('product'));
                }
            }
            return $products->flatten()->filter()->unique();
        }

        return null;

    }

    public function releasedreviews(){
        return $this->hasMany(Review::class,'user_id');
    }





}

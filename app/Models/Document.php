<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use App\Models\Documenttype;

class Document extends Model
{
    use CrudTrait;
    use HasTranslations;
    protected $translatable = ['name', 'description', 'content', 'acceptable_policy'];
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'documents';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $casts = ['links' => 'array'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */


    public function render($user,$extraFields = [])
    {
        $userArray = $user->toArray();
        #dd($userArray);
        $content = $this->content;
        $tags = [
            'name',
            'surname',
            'email',
            'phone',
            'fiscal_code',
            'date_of_birth',
            'address',
            'iban',
            'sponsor',
            'vat_number',
            'place_of_birth',
            'is_public_employee',
            'place',
            'today',
            'company_name',
            'website',
            'company_registration_number',
            'pec',
            'sdi',
            'contact_person',
            'vat_number',
            'share_capital',
            'corporate_sector'
        ];

        foreach ($tags as $field) {
            #$value = $userArray[$field];
            $content = str_replace('[' . $field . ']', $this->renderField($user, $field), $content);
        }

        $extraFields = collect($extraFields);

        if($extraFields->count() > 0){
            foreach ($extraFields as $field => $value) {
                $content = str_replace('[' . $field . ']', $value, $content);
            }
        }

        return $content;
    }

    public function renderField($user, $field = 'null')
    {

        $specialFields = [
            'address' => function ($val) {
                $address = $val;
                return $address['value'];
            },
            'date_of_birth' => function ($val) {
                $date = date('d/m/Y',strtotime($val));
                return $date;
            },
            'iban' => function($val) use ($user){
                return (isset($user->bank_account['bank_account_iban'])) ? $user->bank_account['bank_account_iban'] : '';
            },
            'sponsor' => function ($val) use ($user){
                return (!is_null($user->sponsor)) ? $user->sponsor->name.' '. $user->sponsor->surname.' ('. $user->sponsor->code.' )' : 'Nessuno sponsor';
            },
            'is_public_employee' => function($val) use ($user){
                return ($user->is_public_employee) ? 'x' : ' ';
            },
            'place' =>  function($val) use ($user){
                $address = $user->address;
                if(isset($address['city'])){
                    return $address['city'];
                }
                return '';

            },
            'today' => function($val) use ($user){
                return date('d/m/Y');
            }

        ];

        $userArray = $user->toArray();
        if(isset($specialFields[$field])){
            return $specialFields[$field](isset($userArray[$field]) ? $userArray[$field] : null);
        }else{

            return $userArray[$field];
        }

    }


    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function users()
    {
        return $this->belongsToMany('App\User', 'document_user')->withPivot(['document_url']);
    }

    public function type(){
        return $this->belongsTo(Documenttype::class,'documenttype_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}

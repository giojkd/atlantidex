<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Insertion;
use App\Traits\Invoicerowable;

class Lead extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'leads';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];


    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function identifierForInvoice()
    {
        return '';
    }

    public function syncMerchants(){
        $this->merchants()->sync(collect($this->leadrows->pluck('merchant_id'))->unique());
    }

    public function printPaymentMethod(){
        switch($this->paid_with){
            case 'use_vouchers':
                return 'Vouchers';
                break;
            case 'bill_user_wallet':
                return 'Wallet';
                break;
            case 'paid_by_card':
                return 'Carta c/o merchant';
                break;
            case 'paid_in_cache':
                return 'Contanti c/o merchant';
                break;
            default:
                return 'Carta di credito (Stripe)';
                break;
        }
    }

    public function printPaymentMethodRaw()
    {
        switch ($this->paid_with) {
            case 'use_vouchers':
                return 'epvoucher';
                break;
            case 'bill_user_wallet':
                return 'wallet';
                break;
            case 'paid_by_card':
                return 'credit_card_at_merchant';
                break;
            case 'paid_in_cache':
                return 'cash_at_merchant';
                break;
            default:
                return 'stored_credit_card';
                break;
        }
    }

    public function consolidate(){

        $lead = $this;

        if ($lead->eps > 0 && $lead->paid_with != 'use_vouchers') { #now leads paid with vouchers don't grand eps anymore
            $lead->client->epoperations()->create([
                'value' => $lead->eps,
                'epoperationable_type' => 'App\Models\Lead',
                'epoperationable_id' => $lead->id,
                'ep_type' => 'offline_purchase',
                'description' => 'User ' . $lead->client->id . ' offline purchase',
                'is_careerable' => 1,
                'sign' => 1
            ]);
        }

        if ($lead->eps_plus > 0) {
            $lead->client->epplusoperations()->create([
                'value' => $lead->eps_plus,
                'epplusoperationable_type' => 'App\Models\Lead',
                'epplusoperationable_id' => $lead->id,
                'epplus_type' => 'offline_purchase',
                'description' => 'User ' . $lead->client->id . ' offline purchase',
                'is_careerable' => 1,
                'sign' => 1
            ]);
        }

        #reduces spendings_to_unlock_merchant_marketer_pack

        $client = $lead->client;
        $clientParent = $client->parent;

        if (!is_null($clientParent)) {

            $clientParent->spendings_to_unlock_merchant_marketer_pack += $lead->grand_total;
            $clientParent->saveWithoutEvents();

        }

        $client->spendings_to_unlock_merchant_marketer_pack += $lead->grand_total;
        $client->saveWithoutEvents();

        ####################################################

        $rows = $lead->leadrows;

        $rows->each(function ($row, $key) {
            $insertion = Insertion::where('product_id', $row->product_id)->where('merchant_id', $row->merchant_id)->first();
            if(!is_null($insertion)){
                $insertion->quantity -= $row->quantity;
                $insertion->save();
            }
        });

        $lead->consolidated_at = date('Y-m-d H:i:s');
        $lead->save();

    }

    public function cancel()    {
        #
        $lead = $this;

        $lead->load([
            'epoperation',
            'epplusoperation',
            'commissionoperation',
            'leadrows',
            'leadrows.epoperation',
            'leadrows.epplusoperation',
            'leadrows.commissionoperation',
        ]);

        if($lead->leadrows->count() > 0){

            $lead->leadrows->each(function($row,$rowKey){
                (!is_null($row->epoperation) ?? $row->epoperation()->delete());
                (!is_null($row->epplusoperation) ?? $row->epplusoperation()->delete());
                (!is_null($row->commissionoperation) ?? $row->commissionoperation()->delete());
                (!is_null($row->invoicerow) ?? $row->invoicerow()->delete());
            });

            $lead->leadrows()->delete();

        }

        (!is_null($this->epoperation) ?? $this->epoperation()->delete());
        (!is_null($this->epplusoperation) ?? $this->epplusoperation()->delete());
        (!is_null($this->commissionoperation) ?? $this->commissionoperation()->delete());
        (!is_null($this->invoicerow) ?? $this->invoicerow()->delete());

        $this->delete();

    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function leadrows(){
        return $this->hasMany(Leadrow::class);
    }

    public function client(){
        return $this->belongsTo(BackpackUser::class,'client_id');
    }

    public function merchants(){
        return $this->belongsToMany(Merchant::class)->withoutGlobalScopes();
    }

    public function epoperation(){
        return $this->morphOne(Epoperation::class,'epoperationable');
    }

    public function epplusoperation(){
        return $this->morphOne(Epplusoperation::class, 'epplusoperationable');
    }

    public function commissionoperation(){
        return $this->morphOne(Commissionoperation::class, 'commissionoperationable');
    }

    public function invoicerow(){
        return $this->morphOne(Invoicerow::class, 'invoicerowable');
    }



    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}

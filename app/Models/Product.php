<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Scout\Searchable;
use Recombee\RecommApi\Client;
use Recombee\RecommApi\Requests as Reqs;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Models\Attribute;
use App\Models\Attributevalue;
use App\Traits\Helpers;
use Illuminate\Support\Facades\Auth;

class Product extends Model
{
  use CrudTrait;
  use HasTranslations;
  use SoftDeletes;
  use Helpers;
  use Searchable;
  use \Venturecraft\Revisionable\RevisionableTrait;
  /*
  |--------------------------------------------------------------------------
  | GLOBAL VARIABLES
  |--------------------------------------------------------------------------
  */

  protected $table = 'products';
  // protected $primaryKey = 'id';
  // public $timestamps = false;
  protected $guarded = ['id'];
  // protected $fillable = [];
  // protected $hidden = [];
  // protected $dates = [];
  protected $translatable = ['name','description','brief','price_per'];
  protected $product_merchants_names = '';

  protected $casts = [
    'pictures' => 'array',
    'service_delivery_address' => 'array'
  ];

  /*
  |--------------------------------------------------------------------------
  | FUNCTIONS
  |--------------------------------------------------------------------------
  */

  public function getProductMerchantsNamesAttribute(){
    return collect($this->merchants()->withoutGlobalScopes()->get()->pluck('name'))->implode(',');
  }

  public function getMerchantValues($id) {
    $merchant = $this->merchants()->where('merchant_id',$id)->first();
    $valuefields['active'] = false;
    $valuefields['price'] = null;
    $valuefields['price_with_vat'] = 0;
    $valuefields['product_status_id'] = 1;
    $valuefields['quantity'] = 0;
    $valuefields['is_used'] = false;
    $valuefields['reassortment_days'] = 0;
    $valuefields['shipping_days'] = null;
    $valuefields['commission_percentage'] = 10;
    $valuefields['commissionb2b_percentage'] = 10;
    $valuefields['delivery_option'] = 1;
    $valuefields['bid_min'] = 0;
    $valuefields['service_delivery_center_lng'] = 0;
    $valuefields['service_delivery_center_lat'] = 0;
    $valuefields['service_delivery_center_radius'] = 0;
    $valuefields['service_delivery_address_input'] = '';
    $valuefields['service_delivery_address'] = json_encode([]);

    if ($merchant) {
        $valuefields['active'] = $merchant->pivot->active;
        $valuefields['price'] = $merchant->pivot->price;
        if ($this->vat) {
            $valuefields['price_with_vat'] = round($merchant->pivot->price * (1+($this->vat->value/100)),2);
        }
        $valuefields['product_status_id'] = $merchant->pivot->product_status_id;
        $valuefields['quantity'] = $merchant->pivot->quantity;
        $valuefields['is_used'] = $merchant->pivot->is_used;
        $valuefields['reassortment_days'] = $merchant->pivot->reassortment_days;
        $valuefields['shipping_days'] = $merchant->pivot->shipping_days;
        $valuefields['commission_percentage'] = $merchant->pivot->commission_percentage;
        $valuefields['commissionb2b_percentage'] = $merchant->pivot->commissionb2b_percentage;
        $valuefields['delivery_option'] = $merchant->pivot->delivery_option;
        $valuefields['eps'] = $merchant->pivot->eps;
        $valuefields['b2b_eps'] = $merchant->pivot->b2b_eps;
        $valuefields['bid_min'] = $merchant->pivot->bid_min;


        if(!is_null($merchant->pivot->service_delivery_address)){
            if(is_object(json_decode($merchant->pivot->service_delivery_address))){
                    $valuefields['service_delivery_address_input'] = json_decode($merchant->pivot->service_delivery_address)->formatted_address;
                    $valuefields['service_delivery_address'] = $merchant->pivot->service_delivery_address;
            }

        }







        $valuefields['service_delivery_center_radius'] = $merchant->pivot->service_delivery_center_radius;
        $valuefields['service_delivery_center_lat'] = $merchant->pivot->service_delivery_center_lat;
        $valuefields['service_delivery_center_lng'] = $merchant->pivot->service_delivery_center_lng;

    }
    return $valuefields;
  }

  public function coverlist($size = 50){
    if(!is_null($this->pictures)){
        if(isset($this->pictures[0])){
            if($this->pictures[0] != ''){
                return '<img src=\''.Route('ir',['size' => $size, 'filename' => $this->pictures[0]]).'\'>';
            }
        }
    }
    return '';
}

  public function cover($size = 50){

    if(!is_null($this->pictures)){
      if(isset($this->pictures[0])){
        if($this->pictures[0] != ''){
          return url(Route('ir',['size' => $size, 'filename' => $this->pictures[0]]));
        }
      }
    }
    return '';
  }

  public function sendToRecombee(){
      return;
    $client = new Client(env('RECOMBEE_DB'), env('RECOMBEE_PRIVATE_KEY'));;
    #$this->updatePrice();
    $client->send(new Reqs\SetItemValues(
      $this->id,[
        'categories' => [$this->category->name],
        'name' => $this->name,
        'short_description' => $this->short_description,
        'description' => $this->description,
        'manufacturer' => $this->manufacturer->name,
        'photos' => $this->photos,
        'price' => $this->price_sell_out,
        'enabled' => ($this->enabled) ? true : false,
      ],[
        "cascadeCreate" => true
      ]));
    }


    public function getRecombeeRecommendations($qty = 5){
      if(Auth::check()){
        $client = new Client(env('RECOMBEE_DB'), env('RECOMBEE_PRIVATE_KEY'));;
        $recomms = $client->send(new Reqs\RecommendItemsToUser(Auth::id(), $qty));
        return $this->find(collect($recomms['recomms'])->pluck('id'));
      }
      else{
        return [];
      }
    }

    public function sendRecombeeInteraction($interaction = 'view',$cfg = []){
      if(!Auth::check()){
        return false;
      }
      $product = $this;
      $user = Auth::user();

      $client = new Client(env('RECOMBEE_DB'), env('RECOMBEE_PRIVATE_KEY'));


      $optionalCfg = [
        'timestamp' => time(),
        #'duration' => <integer>,
        'cascadeCreate' => true,
        #'recommId' => <string>
      ];
      switch($interaction){

        case 'view':
        $client->send(new Reqs\AddDetailView($user->id, $product->id, $optionalCfg));
        break;

        case 'purchase':
        if(isset($cfg['price'])){
          $optionalCfg['price'] = $cfg['price'];
          if(is_null($this->price_sell_in)){
            $optionalCfg['profit'] = $cfg['price']-$this->price_sell_in;
          }
        }
        if(isset($cfg['quantity'])){
          $optionalCfg['amount'] = $cfg['quantity'];
        }
        $client->send(new Reqs\AddPurchase($user->id, $product->id, $optionalCfg));
        break;

        case 'rating':

        $client->send(new Reqs\AddRating($user->id, $product->id, $optionalCfg['rating']));

        break;
        case 'addtocart':
        if(isset($cfg['price'])){
          $optionalCfg['price'] = $cfg['price'];
          if(is_null($this->price_sell_in)){
            $optionalCfg['profit'] = $cfg['price']-$this->price_sell_in;
          }
        }
        if(isset($cfg['quantity'])){
          $optionalCfg['amount'] = $cfg['quantity'];
        }
        $client->send(new Reqs\AddCartAddition($user->id, $product->id, $optionalCfg));
        break;
        case 'bookmark':
        $client->send(new Reqs\AddBookmark($user->id, $product->id, $optionalCfg));
        break;

      }

    }

    public function addAttribute($attribute, $value, $cfg = []) {
        //dd($value);
        $attribute = trim(Str::lower($attribute));
        $attribute = str_replace('_', ' ', $attribute);
        $value = trim(Str::lower($value));
        $cfg['locale'] = (isset($cfg['locale'])) ? $cfg['locale'] : 'it';
        //$attribute = Attribute::firstOrCreate(['name->' . $cfg['locale'] => $attribute]);
        //$attribute = Attribute::firstOrCreate(['name' => $attribute]);
        $eattribute = Attribute::where(['name->' . $cfg['locale'] => $attribute])->first();
        if (!$eattribute) {
            $eattribute = new Attribute();
            $eattribute->setTranslation('name', $cfg['locale'], $attribute);
            $eattribute->save();

        }
        //$attributeValue = AttributeValue::firstOrCreate(['name->' . $cfg['locale'] => $value, 'attribute_id' => $eattribute->id]);
        //$attributeValue = AttributeValue::firstOrCreate(['name' => $value, 'attribute_id' => $attribute->id]);
        $eattributeValue = AttributeValue::where(['name->' . $cfg['locale'] => $value, 'attribute_id' => $eattribute->id])->first();

        if (!$eattributeValue) {
            $eattributeValue = new Attributevalue();
            $eattributeValue->setTranslation('name', $cfg['locale'], $value);
            $eattributeValue->attribute_id = $eattribute->id;
            $eattributeValue->save();

        }
        $this->attributevalues()->syncWithoutDetaching($eattributeValue->id);
        $this->saveWithoutEvents();
    }

    public static function getAllVariant(int $id) {
        return self::where('parent_id',$id)->withoutGlobalScopes()->get();
    }

    public static function rawProduct($id) {
        $ids =  collect(DB::select('SELECT product_id FROM merchant_product mp inner join merchants m on m.id = mp.merchant_id where user_id = ?', [$id]));
        return $ids;
     }

    public function isDefault() {
        return $this->id == $this->parent_id;
    }

    public function isDraft() {
        $rt = "";
        if ($this->active ==  false) {
            $rt = "Bozza";
        }
        if ($this->pictures ==  '') {
            $rt = "Bozza";
        }
        if(is_object($this->product_type)){
            if ($this->product_type->name ==  'Prodotti') {
                if ($this->packaging_weight < 1) {
                    $rt = "Bozza";
                }
                if ($this->packaging_height < 1) {
                    $rt = "Bozza";
                }
                if ($this->packaging_lenght < 1) {
                    $rt = "Bozza";
                }
                if ($this->packaging_width < 1) {
                    $rt = "Bozza";
                }
            }
        }
        return $rt;
    }

    public function getAttributesSerialized(){
        $str = [];
        foreach($this->attributevalues as $item) {
            $value = $item->name;
            $name = $item->relations['attribute']->name;
            $str[] = $name . ":" . $value;
        }
        return implode(',',$str);
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function manufacturer()
    {
        return $this->belongsTo('App\Models\Manufacturer');
    }

    public function vat()
    {
        return $this->belongsTo('App\Models\Vat');
    }

    public function product_type()
    {
        return $this->belongsTo('App\Models\Product_type');
    }

    public function merchants()
    {
        //return $this->belongsToMany('App\Models\Merchant','merchant_product','product_id')->using('App\Models\Merchantproduct');
        return $this->belongsToMany(Merchant::class)
        ->using('App\Models\Merchantproduct')
        ->withPivot('b2b_eps','service_delivery_address','service_delivery_center_radius','service_delivery_center_lat', 'service_delivery_center_lng', 'bid_min','price','price_compare','active','product_status_id','quantity','is_used','reassortment_days','shipping_days','commission_percentage','commissionb2b_percentage','delivery_option','eps')
        ->withTimestamps();
    }

    public function activeMerchants()
    {
        return $this->belongsToMany(Merchant::class)
        ->using('App\Models\Merchantproduct')
        ->wherePivot('active',1)
        ->withPivot('b2b_eps','service_delivery_address','service_delivery_center_radius','service_delivery_center_lat', 'service_delivery_center_lng','bid_min','price','price_compare','active','product_status_id','quantity','is_used','reassortment_days','shipping_days','commission_percentage','commissionb2b_percentage','delivery_option','eps')
        ->withTimestamps();
    }

    public function attributevalues(){
        return $this->belongsToMany(Attributevalue::class);
    }

    public function variants(){
        return $this->hasMany('App\Models\Product','parent_id','id')->withoutGlobalScopes();
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    public function scopeActive($query)
    {
      return $query->where('active', '=', 1);
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    public function getIdentifierAttribute(){
        return $this->name;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

  }

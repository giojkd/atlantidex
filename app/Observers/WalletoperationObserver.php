<?php

namespace App\Observers;

use App\Models\Walletoperation;

class WalletoperationObserver
{
    /**
     * Handle the walletoperation "created" event.
     *
     * @param  \App\Walletoperation  $walletoperation
     * @return void
     */
    public function created(Walletoperation $walletoperation)
    {
        //
        $user = $walletoperation->user;
        $user->wallet_balance += $walletoperation->value;
        $user->save();

    }

    /**
     * Handle the walletoperation "updated" event.
     *
     * @param  \App\Walletoperation  $walletoperation
     * @return void
     */
    public function updated(Walletoperation $walletoperation)
    {
        //
    }

    /**
     * Handle the walletoperation "deleted" event.
     *
     * @param  \App\Walletoperation  $walletoperation
     * @return void
     */
    public function deleted(Walletoperation $walletoperation)
    {
        //
    }

    /**
     * Handle the walletoperation "restored" event.
     *
     * @param  \App\Walletoperation  $walletoperation
     * @return void
     */
    public function restored(Walletoperation $walletoperation)
    {
        //
    }

    /**
     * Handle the walletoperation "force deleted" event.
     *
     * @param  \App\Walletoperation  $walletoperation
     * @return void
     */
    public function forceDeleted(Walletoperation $walletoperation)
    {
        //
    }
}

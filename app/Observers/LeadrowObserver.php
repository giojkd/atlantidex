<?php

namespace App\Observers;

use App\Models\Leadrow;

class LeadrowObserver
{
    /**
     * Handle the leadrow "created" event.
     *
     * @param  \App\Leadrow  $leadrow
     * @return void
     */
    public function created(Leadrow $leadrow)
    {
        //
        $leadrow->lead->syncMerchants();
    }

    /**
     * Handle the leadrow "updated" event.
     *
     * @param  \App\Leadrow  $leadrow
     * @return void
     */
    public function updated(Leadrow $leadrow)
    {
        //
        $leadrow->lead->syncMerchants();
    }

    /**
     * Handle the leadrow "deleted" event.
     *
     * @param  \App\Leadrow  $leadrow
     * @return void
     */
    public function deleted(Leadrow $leadrow)
    {
        //
        $leadrow->lead->syncMerchants();
    }

    /**
     * Handle the leadrow "restored" event.
     *
     * @param  \App\Leadrow  $leadrow
     * @return void
     */
    public function restored(Leadrow $leadrow)
    {
        //
        $leadrow->lead->syncMerchants();
    }

    /**
     * Handle the leadrow "force deleted" event.
     *
     * @param  \App\Leadrow  $leadrow
     * @return void
     */
    public function forceDeleted(Leadrow $leadrow)
    {
        //
        $leadrow->lead->syncMerchants();
    }
}

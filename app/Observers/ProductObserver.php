<?php

namespace App\Observers;
use App\Models\Product;

class ProductObserver
{

  public function saved(Product $product){
    $product->sendToRecombee();
  }

  public function created(Product $product)
  {
        if(is_null($product->parent_id)){
            $product->parent_id = $product->id;
            $product->save();
        }
  }

  /**
  * Handle the review "updated" event.
  *
  * @param  \App\Product  $product
  * @return void
  */
  public function updated(Product $product)
  {

  }

  /**
  * Handle the review "deleted" event.
  *
  * @param  \App\Product  $product
  * @return void
  */
  public function deleted(Product $product)
  {

  }

  /**
  * Handle the review "restored" event.
  *
  * @param  \App\Product  $product
  * @return void
  */
  public function restored(Product $product)
  {

  }

  /**
  * Handle the review "force deleted" event.
  *
  * @param  \App\Product  $product
  * @return void
  */
  public function forceDeleted(Product $product)
  {

  }
}

<?php

namespace App\Observers;

use App\Models\Changesponsorrequest;

class ChangesponsorrequestObserver
{
    /**
     * Handle the changesponsorrequest "created" event.
     *
     * @param  \App\Changesponsorrequest  $changesponsorrequest
     * @return void
     */
    public function created(Changesponsorrequest $changesponsorrequest)
    {
        //
    }

    /**
     * Handle the changesponsorrequest "updated" event.
     *
     * @param  \App\Changesponsorrequest  $changesponsorrequest
     * @return void
     */
    public function updated(Changesponsorrequest $changesponsorrequest)
    {
        //
    }

    /**
     * Handle the changesponsorrequest "deleted" event.
     *
     * @param  \App\Changesponsorrequest  $changesponsorrequest
     * @return void
     */
    public function deleted(Changesponsorrequest $changesponsorrequest)
    {
        //
    }

    /**
     * Handle the changesponsorrequest "restored" event.
     *
     * @param  \App\Changesponsorrequest  $changesponsorrequest
     * @return void
     */
    public function restored(Changesponsorrequest $changesponsorrequest)
    {
        //
    }

    /**
     * Handle the changesponsorrequest "force deleted" event.
     *
     * @param  \App\Changesponsorrequest  $changesponsorrequest
     * @return void
     */
    public function forceDeleted(Changesponsorrequest $changesponsorrequest)
    {
        //
    }
}

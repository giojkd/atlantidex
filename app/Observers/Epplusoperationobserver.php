<?php

namespace App\Observers;

use App\Models\Epplusoperation;

class Epplusoperationobserver
{
    /**
     * Handle the epplusoperation "created" event.
     *
     * @param  \App\Epplusoperation  $epplusoperation
     * @return void
     */
    public function created(Epplusoperation $epplusoperation)
    {
        //
        $user = $epplusoperation->user;
        $user->epplus_balance+=$epplusoperation->value;
        $user->save();
    }

    /**
     * Handle the epplusoperation "updated" event.
     *
     * @param  \App\Epplusoperation  $epplusoperation
     * @return void
     */
    public function updated(Epplusoperation $epplusoperation)
    {
        //
    }

    /**
     * Handle the epplusoperation "deleted" event.
     *
     * @param  \App\Epplusoperation  $epplusoperation
     * @return void
     */
    public function deleted(Epplusoperation $epplusoperation)
    {
        //
    }

    /**
     * Handle the epplusoperation "restored" event.
     *
     * @param  \App\Epplusoperation  $epplusoperation
     * @return void
     */
    public function restored(Epplusoperation $epplusoperation)
    {
        //
    }

    /**
     * Handle the epplusoperation "force deleted" event.
     *
     * @param  \App\Epplusoperation  $epplusoperation
     * @return void
     */
    public function forceDeleted(Epplusoperation $epplusoperation)
    {
        //
    }
}

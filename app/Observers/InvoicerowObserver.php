<?php

namespace App\Observers;

use App\Models\Invoicerow;

class InvoicerowObserver
{
    /**
     * Handle the invoicerow "created" event.
     *
     * @param  \App\Invoicerow  $invoicerow
     * @return void
     */
    public function created(Invoicerow $invoicerow)
    {
        //

        if($invoicerow->value > 0){

            $user = $invoicerow->user;
            $parent = $user->parent;
            if ($invoicerow->invoicerowable_type != 'App\Models\Pack') {
                if(!is_null($parent)){

                    $parent->spendings_to_unlock_merchant_marketer_pack += $invoicerow->value;
                    $parent->save();

                }
                if($user->hasRole('Customer') || ($user->hasRole('Networker') && !$user->isActive()) ){

                    $user->spendings_to_unlock_merchant_marketer_pack += $invoicerow->value;
                    $user->save();

                }
            }
        }



    }

    /**
     * Handle the invoicerow "updated" event.
     *
     * @param  \App\Invoicerow  $invoicerow
     * @return void
     */
    public function updated(Invoicerow $invoicerow)
    {
        //
    }

    /**
     * Handle the invoicerow "deleted" event.
     *
     * @param  \App\Invoicerow  $invoicerow
     * @return void
     */
    public function deleted(Invoicerow $invoicerow)
    {
        //
    }

    /**
     * Handle the invoicerow "restored" event.
     *
     * @param  \App\Invoicerow  $invoicerow
     * @return void
     */
    public function restored(Invoicerow $invoicerow)
    {
        //
    }

    /**
     * Handle the invoicerow "force deleted" event.
     *
     * @param  \App\Invoicerow  $invoicerow
     * @return void
     */
    public function forceDeleted(Invoicerow $invoicerow)
    {
        //
    }
}

<?php

namespace App\Observers;

use App\Models\Epoperation;
use App\Models\Epplusoperation;

class EpoperationObserver
{
    /**
     * Handle the epoperation "created" event.
     *
     * @param  \App\Epoperation  $epoperation
     * @return void
     */

    public function created(Epoperation &$epoperation)
    {
        //
        /*
        $user = $epoperation->user;
        if($user->hasRole('Networker') && $user->split_eps){

            $epoperation->value/=2;

            $user->epplusoperations()->create([
                'value' => $epoperation->value,
                'description' => $epoperation->description,
                'epplus_type' => $epoperation->ep_type,
                'is_careerable' => $epoperation->is_careerable,
                'user_role' => $epoperation->user_role,
                'epplusoperationable_id' => $epoperation->epoperationable_id,
                'epplusoperationable_type' => $epoperation->epoperationable_type
            ]);
        }
        */
    }

    public function creating(Epoperation $epoperation)
    {
        //
    }

    /**
     * Handle the epoperation "updated" event.
     *
     * @param  \App\Epoperation  $epoperation
     * @return void
     */
    public function updated(Epoperation $epoperation)
    {
        //
    }

    /**
     * Handle the epoperation "deleted" event.
     *
     * @param  \App\Epoperation  $epoperation
     * @return void
     */
    public function deleted(Epoperation $epoperation)
    {
        //
    }

    /**
     * Handle the epoperation "restored" event.
     *
     * @param  \App\Epoperation  $epoperation
     * @return void
     */
    public function restored(Epoperation $epoperation)
    {
        //
    }

    /**
     * Handle the epoperation "force deleted" event.
     *
     * @param  \App\Epoperation  $epoperation
     * @return void
     */
    public function forceDeleted(Epoperation $epoperation)
    {
        //
    }
}

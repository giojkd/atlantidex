<?php

namespace App\Observers;

use App\Models\Voucheroperation;

class VoucheroperationObserver
{
    /**
     * Handle the voucheroperation "created" event.
     *
     * @param  \App\Voucheroperation  $voucheroperation
     * @return void
     */
    public function created(Voucheroperation $voucheroperation)
    {
        //
        $voucheroperation->calculateVoucherSpent();
    }

    /**
     * Handle the voucheroperation "updated" event.
     *
     * @param  \App\Voucheroperation  $voucheroperation
     * @return void
     */
    public function updated(Voucheroperation $voucheroperation)
    {
        //
    }

    /**
     * Handle the voucheroperation "deleted" event.
     *
     * @param  \App\Voucheroperation  $voucheroperation
     * @return void
     */
    public function deleted(Voucheroperation $voucheroperation)
    {
        //
    }

    /**
     * Handle the voucheroperation "restored" event.
     *
     * @param  \App\Voucheroperation  $voucheroperation
     * @return void
     */
    public function restored(Voucheroperation $voucheroperation)
    {
        //
    }

    /**
     * Handle the voucheroperation "force deleted" event.
     *
     * @param  \App\Voucheroperation  $voucheroperation
     * @return void
     */
    public function forceDeleted(Voucheroperation $voucheroperation)
    {
        //
    }
}

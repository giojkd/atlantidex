<?php

namespace App\Observers;

use App\Models\Merchantproduct;

class MerchantproductObserver
{
    /**
     * Handle the merchantproduct "created" event.
     *
     * @param  \App\Merchantproduct  $merchantproduct
     * @return void
     */
    public function saving(Merchantproduct $merchantproduct)
    {

    }

    /**
     * Handle the merchantproduct "updated" event.
     *
     * @param  \App\Merchantproduct  $merchantproduct
     * @return void
     */
    public function updated(Merchantproduct $merchantproduct)
    {

    }

    /**
     * Handle the merchantproduct "deleted" event.
     *
     * @param  \App\Merchantproduct  $merchantproduct
     * @return void
     */
    public function deleted(Merchantproduct $merchantproduct)
    {
        //
    }

    /**
     * Handle the merchantproduct "restored" event.
     *
     * @param  \App\Merchantproduct  $merchantproduct
     * @return void
     */
    public function restored(Merchantproduct $merchantproduct)
    {
        //
    }

    /**
     * Handle the merchantproduct "force deleted" event.
     *
     * @param  \App\Merchantproduct  $merchantproduct
     * @return void
     */
    public function forceDeleted(Merchantproduct $merchantproduct)
    {
        //
    }


}

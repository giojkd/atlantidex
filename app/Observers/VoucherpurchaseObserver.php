<?php

namespace App\Observers;

use App\Models\Voucherpurchase;

class VoucherpurchaseObserver
{
    /**
     * Handle the voucherpurchase "created" event.
     *
     * @param  \App\Voucherpurchase  $voucherpurchase
     * @return void
     */
    public function created(Voucherpurchase $voucherpurchase)
    {
        //
    }

    /**
     * Handle the voucherpurchase "updated" event.
     *
     * @param  \App\Voucherpurchase  $voucherpurchase
     * @return void
     */
    public function updated(Voucherpurchase $voucherpurchase)
    {
        //
        $voucherpurchase->calculateVoucherPurchased();
    }

    /**
     * Handle the voucherpurchase "deleted" event.
     *
     * @param  \App\Voucherpurchase  $voucherpurchase
     * @return void
     */
    public function deleted(Voucherpurchase $voucherpurchase)
    {
        //
    }

    /**
     * Handle the voucherpurchase "restored" event.
     *
     * @param  \App\Voucherpurchase  $voucherpurchase
     * @return void
     */
    public function restored(Voucherpurchase $voucherpurchase)
    {
        //
    }

    /**
     * Handle the voucherpurchase "force deleted" event.
     *
     * @param  \App\Voucherpurchase  $voucherpurchase
     * @return void
     */
    public function forceDeleted(Voucherpurchase $voucherpurchase)
    {
        //
    }
}

<?php

namespace App\Observers;

class BidObserver
{
  /**
  * Handle the review "created" event.
  *
  * @param  \App\Bid  $bid
  * @return void
  */
  public function created(Bid $bid)
  {
     $bid->sendToMerchants();
  }

  /**
  * Handle the review "updated" event.
  *
  * @param  \App\Bid  $bid
  * @return void
  */
  public function updated(Bid $bid)
  {

  }

  /**
  * Handle the review "deleted" event.
  *
  * @param  \App\Bid  $bid
  * @return void
  */
  public function deleted(Bid $bid)
  {

  }

  /**
  * Handle the review "restored" event.
  *
  * @param  \App\Bid  $bid
  * @return void
  */
  public function restored(Bid $bid)
  {

  }

  /**
  * Handle the review "force deleted" event.
  *
  * @param  \App\Bid  $bid
  * @return void
  */
  public function forceDeleted(Bid $bid)
  {

  }
}

<?php

namespace App\Observers;

use App\Models\Productimportfile;
use App\Jobs\ProcessProductimportfile;


class Productimportfileobserver
{
    /**
     * Handle the productimportfile "created" event.
     *
     * @param  \App\Productimportfile  $productimportfile
     * @return void
     */

    public function saved(Productimportfile $productimportfile){
        //

    }


    public function created(Productimportfile $productimportfile)
    {
        //
        //ProcessProductimportfile::dispatch($productimportfile);

        $productimportfile->import();

    }

    /**
     * Handle the productimportfile "updated" event.
     *
     * @param  \App\Productimportfile  $productimportfile
     * @return void
     */
    public function updated(Productimportfile $productimportfile)
    {
        //


    }

    /**
     * Handle the productimportfile "deleted" event.
     *
     * @param  \App\Productimportfile  $productimportfile
     * @return void
     */
    public function deleted(Productimportfile $productimportfile)
    {
        //
    }

    /**
     * Handle the productimportfile "restored" event.
     *
     * @param  \App\Productimportfile  $productimportfile
     * @return void
     */
    public function restored(Productimportfile $productimportfile)
    {
        //
    }

    /**
     * Handle the productimportfile "force deleted" event.
     *
     * @param  \App\Productimportfile  $productimportfile
     * @return void
     */
    public function forceDeleted(Productimportfile $productimportfile)
    {
        //
    }
}

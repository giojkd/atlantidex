<?php

return [
    'active' => 'Visibile',
    'name' => 'Nome',
    'category' => 'Categoria',
    'disactive' => 'Disattivato',
    'code' => 'Codice',
    'max_files_exceeded' => 'Hai raggiunto il numero massimo di file consentiti.',
    'cancel_upload' => 'Cancella il caricamento',
    'cancel_upload_confirmation' => 'Caricamento cancellato',
    'remove_file' => 'Rimuovi File',
    'manufacturer' => 'Marchio',
    'product_type' => 'Tipo prodotto',
    'product' => 'Prodotto',
    'date' => 'Data',
    'manage' => 'Vendi anche tu questo prodotto',
    'country' => 'Nazione',
    'description' => 'Descrizione',
    'brief' => 'Descrizione breve',
    'address' => 'Indirizzo',
    'tipology' => 'Tipologia',
    'vat_number' => 'Partita IVA',

];

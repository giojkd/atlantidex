<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Permission Manager Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for Laravel Backpack - Permission Manager
    | Author: Roberto Butti ( https://github.com/roberto-butti )
    | Language: Italian
    |
    */
    'name'                  => 'Nome',
    'surname'               => 'Cognome',
    'phone'               => 'Telefono',
    'date_of_birth'               => 'Data di nascita',
    'address'               => 'Indirizzo di residenza',
    'fiscal_code'               => 'Codice fiscale',
    'role'                  => 'Ruolo',
    'roles'                 => 'Ruoli',
    'roles_have_permission' => 'Ruoli con questo permesso',
    'permission_singular'   => 'Permesso',
    'permission_plural'     => 'Permessi',
    'user_singular'         => 'Utente',
    'user_plural'           => 'Utenti',
    'email'                 => 'Email',
    'extra_permissions'     => 'Permessi Extra',
    'password'              => 'Password',
    'password_confirmation' => 'Conferma Password',
    'user_role_permission'  => 'Utenti Ruoli Permessi',
    'user'                  => 'Utente',
    'users'                 => 'Utenti',
    'country'               => 'Paese',
    'vat_number'            => 'Partita IVA',
    'company_name'          => 'Nome dell\'azienda (Ragione Sociale)',

];

<?php

return [
    'active' => 'active',
    'name' => 'name',
    'category' => 'category',
    'disactive' => 'disabled',
    'code' => 'code',
    'max_files_exceeded' => 'You have reached the maximum number of files supported.',
    'cancel_upload' => 'Cancel upload',
    'cancel_upload_confirmation' => 'Upload cancelled',
    'remove_file' => 'Remove file',
    'manufacturer' => 'Manufacturer',
];

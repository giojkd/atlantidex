<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'La tua password è stata resettata!',
    'sent' => 'Abbiamo inviato un link di reset della password al tuo indirizzo email!',
    'throttled' => 'Per favore aspetta prima di riprovare.',
    'token' => 'Il token di reset non è valido.',
    'user' => "Non troviamo nessun utente corrispondente all'indirizzo email specificato.",

];

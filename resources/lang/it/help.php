<?php


return [

/*
|--------------------------------------------------------------------------
| Authentication Language Lines
|--------------------------------------------------------------------------
|
| The following language lines are used during authentication for various
| messages that we need to display to the user. You are free to modify
| these language lines according to your application's requirements.
|
*/

'price_per-text' => 'Non è la quantità disponibile del prodotto. Inserire l\'unità di misura nella quale viene venduto il prodotto o il servizio Es: m, Pz, m2.',
'price_per-title' => 'Unità di misura',

'sku-text' => 'Inserire il codice che identifica univocamente il prodotto o il servizio e l\'eventuale variante. Il campo è obbligatorio',
'sku-title' => 'SKU',

'pictures-text' => 'Inserire una o più immagini e ordinarle secondo necessità. I prodotti e i servizi che non hanno almeno un\'immagine non saranno visibile nel portale',
'pictures-title' => 'Immagini',

'reassortment_days-text' => 'Giorni necessari per riassortire il prodotto. Sono aggiunti ai giorni di spedizione qualora il prodotto non fosse disponibile',
'reassortment_days-title' => 'Giorni per il riassortimento',

'manufacturer-text' => 'Selezionare il marchio dalla lista, se non fosse presente scrivere il marchio desiderato nel campo di testo e premere invio',
'manufacturer-title' => 'Selezionare o inserire il marchio',

'vat-text' => 'Selezionare il regime IVA al quale è venduto il prodotto o servizio',
'vat-title' => 'Selezionare un regime IVA',

'active' => 'Imposta il prodotto come attivo se vuoi che compaia tra i risultati di ricerca sia sul portale pubblico di Atlantidex che nella barra di ricerca del catalogo delle aziende',
'active-title' => 'Prodotto attivo',
'non-active' => 'Imposta il prodotto come non attivo se vuoi che venga salvato come bozza nella sezione i miei prodotti. NB se un altro merchant avrà impostato un\'offerta sullo stesso prodotto non sarà possibile renderlo non attivo',
    'non-active-title' => 'Prodotto non attivo',

'visible' => 'Quando il prodotto è attivo cliccando questo tasto potrai vendere il tuo prodotto online',
'visible-title' => 'Prodotto visibile',
'non-visible' => 'Cliccando su questo tasto il tuo prodotto rimarrà nella sezione i miei prodotti e non sarà venduto online',
'non-visible-title' => 'Prodotto non visibile',
'minimum-incremental-title' => 'Minimo incremento',
'minimum-incremental-text' => 'Se il tuo prodotto è vendibile singolarmente scrivi 1, se è vendibile a coppie scrivi 2, se è vendibile a numeri maggiori inserisci il numero relativo al quale è vendibile.',
'minimum-quantity-title' => 'Quantità minima acquistabile',
'minimum-quantity-text' => 'Inserisci il numero di pezzi minimo acquistabile',
'warranty-title' => 'Anni di garanzia',
'warranty-text' => 'Compila questo campo se il prodotto ha una garanzia particolare',
    'geo-info-text' => 'Compila questo campo solo nel caso in cui volessi limitare la consegna del tuo prodotto o l\'erogazione del tuo servizio ad un determinato raggio kilometrico rispetto alla posizione specificata nel relativo campo',
    'geo-info-title' => 'Limiti geografici di consegna del prodotto o di erogazione del servizio',
    'attributes-title' => 'Caratteristiche',
    'attributes-text' => 'Inserire le caratteristiche che determinano la variante: Es: colore marrone',
    'attribute-title' => 'Alcuni esempi:',
    'attribute-text' => 'Taglia, colore, misura, materiale...',
    'attribute-value-text' => 'XL, nero, 10cm, plastica...',
    'attribute-value-title' => 'Alcuni esempi:',
 ];

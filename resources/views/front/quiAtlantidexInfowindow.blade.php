<div class="merchantInfoWindow">
    <p>
        <b>{{ $company['name'] }}</b>
    </p>

    <p>
        <small><i class="fas fa-map-marker-alt"></i> {{ $company['address'] }}</small>
    </p>
    <p>
        <small><i class="fas fa-phone"></i> {{ $company['telephone'] }}</small>
    </p>

</div>

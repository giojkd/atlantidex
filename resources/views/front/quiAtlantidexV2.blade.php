<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/90b54092fb.js" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <title>Qui Atlantidex</title>
  </head>
  <body>
    <div class="page-wrapper">
        <div class="container-fluid pr-0 h-100">
                <div class="row">
                    <div class="col-md-2">
                        <div class="@if(!$agent->isMobile()) vhos @endif">
                            <div class="my-2">
                                <img src="/front/img/atalantidex-logo-squared.png" alt="" height="40">
                            </div>
                            <div class="mb-1">
                                <label for="">Dove stai cercando?</label>
                                <input type="text" class="form-control" id="autocomplete">
                                <a class="btn btn-sm btn-secondary mt-2" href="javascript:getCurrentLocation()"><i class="fas fa-map-marker-alt"></i> Cerca intorno a me</a>
                            </div>

                            <div class="w-100 mb-4"></div>
                            <div id="sort-by"></div>
                            <div class="mb-4">
                                <label for="">Chi stai cercando?</label>
                                <div id="searchbox"></div>
                            </div>
                            <label for="">Filtra per categoria</label>
                            <div id="companycategories"></div>
                            <div id="clear-refinements"></div>

                        </div>
                    </div>
                    @if (!$agent->isMobile())
                        <div class="col-md-3 col-6">
                            <div class="vhos">
                                <div class="mt-4" id="hits"></div>
                            </div>
                        </div>
                        <div class="col-md-7 col-6">
                            <div class="position-relative">
                                <div class="vh-100" id="map"></div>
                                <div id="map-tools">

                                        <label class="google-button">
                                            <div style="height: 7px;"></div>
                                            <input type="checkbox" value="1" checked id="map-refining-checkbox">
                                            Mostra solo risultati nella mappa
                                        </label>

                                    <button class="google-button" id="clear-map-refinement-button">Cancella i filtri della mappa</button>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if ($agent->isMobile())
                        <ul class="nav nav-tabs mt-3" id="myTab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#list" type="button" role="tab" aria-controls="list" aria-selected="true"><i class="fa fa-list"></i> Lista</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#map" type="button" role="tab" aria-controls="map" aria-selected="false"><i class="fa fa-map"></i> Mappa</button>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="list" role="tabpanel" aria-labelledby="home-tab">

                                    <div class="mt-4" id="hits"></div>

                            </div>
                            <div class="tab-pane fade" id="map" role="tabpanel" aria-labelledby="profile-tab">
                                <div class="position-relative">
                                    <div class="vh-100" id="map"></div>
                                    <div id="map-tools">

                                            <label class="google-button">
                                                <div style="height: 7px;"></div>
                                                <input type="checkbox" value="1" checked id="map-refining-checkbox">
                                                Mostra solo risultati nella mappa
                                            </label>

                                        <button class="google-button" id="clear-map-refinement-button">Cancella i filtri della mappa</button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    @endif

                </div>
            </div>
        </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-p34f1UUtsS3wqzfto5wAAmdvj+osOnFyQFpp4Ua3gs/ZVWx6oOypYoCJhGGScy+8" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.min.js" integrity="sha384-lpyLfhYuitXl2zRZ5Bn2fqnhNAKOAaM/0Kr9laMspuaMiZfGmfwRNFh8HlMy49eQ" crossorigin="anonymous"></script>
    -->






    <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API') }}&libraries=places&v=weekly"></script>
    <script src="https://cdn.jsdelivr.net/npm/algoliasearch@4.0.0/dist/algoliasearch-lite.umd.js" integrity="sha256-MfeKq2Aw9VAkaE9Caes2NOxQf6vUa8Av0JqcUXUGkd0=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/instantsearch.js@4.0.0/dist/instantsearch.production.min.js" integrity="sha256-6S7q0JJs/Kx4kb/fv0oMjS855QTz5Rc2hh9AkIUjUsk=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/instantsearch.css@7.3.1/themes/algolia-min.css" integrity="sha256-HB49n/BZjuqiCtQQf49OdZn63XuKFaxcIHWf0HNKte8=" crossorigin="anonymous">
    <script src="https://unpkg.com/@google/markerclustererplus@4.0.1/dist/markerclustererplus.min.js"></script>

    <script>
        var isMobile = {{ ($agent->isMobile()) ? 1 : 0 }}
        var mouseEvent = (isMobile) ? 'mousedown' : 'mouseover';
        var indexName = 'merchants';
        var checkbox;
        var mapBounds;
        var markerCluster;
        var autocomplete;
        var setAddress;
        var labels = {
            clean_refinements : 'Rimuovi i filtri',
            on_deal : '@lang('all.on deal')'
        }
        var companyInfowindow;
        var companyInfowindowRoute = '{{ Route('quiAtlantidexCompanyInfowindow') }}';

        var cloudimage = '';

        var presetFacets = @json($facets);
        const searchClient = algoliasearch('{{ env('ALGOLIA_APP_ID') }}', '{{ env('ALGOLIA_SECRET') }}');

        var fzProducts = [];


        var keyword = '{{ $keyword }}';

            // Create the render function
        let map = null;
        let markers = [];
        let isUserInteraction = true;
        let initialPosition = {lat: 41.8719, lng: 12.5674};

        @verbatim

         var showMoreText = `
                        {{#isShowingMore}}
                            Mostra meno
                        {{/isShowingMore}}
                        {{^isShowingMore}}
                            Mostra tutti
                        {{/isShowingMore}}
                    `;



        const search = instantsearch({
            indexName: indexName,
            searchClient,
            initialUiState: {
                products: {
                    query: keyword,
                    refinementList:presetFacets,
                    toggle:{
                        //is_on_sale: onlyOnSale
                    }
                },
            },
        });

        search.addWidgets([
            instantsearch.widgets.searchBox({
                container: '#searchbox',
                cssClasses: {
                    root: '',
                    form: [
                    '',
                    ],
                },
            }),

            instantsearch.widgets.infiniteHits({
                container: '#hits',
                templates:{

                    showMoreText: 'Mostra ancora',

                    empty: '<div class="alert alert-secondary">Ouch! <i class="fas fa-sad-tear"></i> Non abbiamo nessun risultato per la tua ricerca, prova a modificare i filtri o cercare da un\'altra parte.</div>',
                    item(hit){
                        var cats = [];

                        if(hit.companycategories.length > 0){
                            cats.push('<ul>');
                            hit.companycategories.forEach(cat => {
                                if(typeof(cat) != 'undefined' && cat != null){
                                    cats.push('<li>'+cat.name + ' <b>'+(parseFloat(cat.min_commission)/2)+'EP+</b> e <b>'+(parseFloat(cat.min_commission)/2)+'EP</b></li>')
                                }
                            });
                            cats.push('</ul>');
                        }

                        return `
                            <div class="product-item" onclick="highlightMarker(${hit.id})">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4 class="title">${hit.name}</h4>
                                        <p class="item-distance orange">${hit.distance}</p>
                                        <p class="item-categories">${cats.join('')}</p>
                                        <p class="item-short-description">${hit.short_description}</p>
                                        <p><small><i class="fas fa-map-marker-alt"></i> <a href="https://www.google.com/maps/place/${hit.address}" target="_blank">${hit.address}</a></small></p>
                                        ${ (hit.telephone) ? '<p><small><i class="fas fa-phone"></i> ' + hit.telephone + '</small></p>' : '' }
                                    </div>
                                </div>
                            </div>
                        `;
                    }

                },
                 transformItems(items) {

                    if(markers.length > 0){
                        markers = [];
                    }

                    if(!checkbox.checked){
                        mapBounds = new google.maps.LatLngBounds();
                    }

                    //foreach items as item
                    var itemsReturn = items.map(function(item){
                        var distance = '';
                        var distanceNumeric = 0;
                        if(typeof(setAddress) != 'undefined'){
                            var setAddressCoords = setAddress.geometry.location
                            distanceNumeric = calcCrow(setAddressCoords.lat(),setAddressCoords.lng(),item._geoloc.lat,item._geoloc.lng).toFixed(1)
                            distance = 'a '+distanceNumeric + 'km'
                        }
                        var returnItem = {
                            name: item.name,
                            id: item.id,
                            companycategories: item.companycategories,
                            short_description: item.short_description,
                            address: item.address,
                            telephone: item.telephone,
                            distance: distance,
                            distanceNumeric: distanceNumeric
                        };

                        var marker = new google.maps.Marker({
                            position: item._geoloc,
                            map,
                            title: item.name,
                            icon: '/front/img/atalantidex-logo-squared-map-marker.png',
                            optimized: false
                        });
                        marker.id = parseInt(item.id);

                        marker.addListener(mouseEvent, clickMarker);
                        markers.push(marker);
                        if(!checkbox.checked){
                            mapBounds.extend(marker.getPosition());
                        }
                        return returnItem;
                    })

                    if(typeof(setAddress) != 'undefined'){
                        itemsReturn.sort(function(a,b){return a.distanceNumeric-b.distanceNumeric});
                    }

                    //endforeach

                    if(typeof(markerCluster) != 'undefined'){
                        markerCluster.clearMarkers();
                    }
                    markerCluster = new MarkerClusterer(map, markers, {
                        imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
                    });

                    if(!checkbox.checked){
                        map.fitBounds(mapBounds);
                    }

                    return itemsReturn;
                },
            }),
            /*
            instantsearch.widgets.sortBy({
                container: '#sort-by',
                items: [
                    { label: 'Più rilevanti', value: 'products' },
                ],
            }),
            */
            instantsearch.widgets.clearRefinements({
                container: '#clear-refinements',
                cssClasses:{
                    button: ['orange-btn']
                },
                templates:{
                    resetLabel() {
                        return labels.clean_refinements;
                    },
                }
            }),
            instantsearch.widgets.refinementList({
                container: '#companycategories',
                attribute: 'companycategories.name',
                limit: 10,
                showMore: true,
                templates:{
                    showMoreText: showMoreText
                },
                searchable: true,
                sortBy(a, b) {
                    return a.name < b.name ? 1 : -1;
                },
            })

        ]);

        function bounceMarker(id){
            for(var i in markers){
                if(markers[i].id == id){
                    markers[i].setAnimation(google.maps.Animation.BOUNCE);
                    openMarkerInfoWindow(id);
                    var bounceInterval = setInterval(function() {
                        if (markers[i].getAnimation() !== null) {
                            markers[i].setAnimation(null);
                        }
                    },1000);
                    return;
                }
            }
        }

        function highlightMarker(id){
            for(var i in markers){
                if(markers[i].id == id){
                    map.panTo(markers[i].getPosition());
                    bounceMarker(id);
                    return;
                }
            }
        }

        function calcCrow(lat1, lon1, lat2, lon2) {
            var R = 6371; // km
            var dLat = toRad(lat2-lat1);
            var dLon = toRad(lon2-lon1);
            var lat1 = toRad(lat1);
            var lat2 = toRad(lat2);

            var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
            var d = R * c;
            return d;
        }

        // Converts numeric degrees to radians
        function toRad(Value)
        {
            return Value * Math.PI / 180;
        }

        function openMarkerInfoWindow(id){
            $.get(companyInfowindowRoute+'?id='+id,function(r){
                closeInfoWindows();
                companyInfowindow = new google.maps.InfoWindow({
                    content: r
                });
                for(var i in markers){
                    if(markers[i].id == id){
                        companyInfowindow.open(map, markers[i]);
                        return;
                    }
                }


            });
        }

        function clickMarker(){
            var marker = this;
            openMarkerInfoWindow(this.id);
            //scrollToCompany(this.id);
        }


        function closeInfoWindows(){
            if (companyInfowindow) {
                companyInfowindow.close();
            }
        }

        autocomplete = new google.maps.places.Autocomplete(
            document.getElementById("autocomplete"),
            {
                types: ["geocode"],
                country: ['it']
            },
        );

        autocomplete.addListener("place_changed", () => {
            checkbox.checked = true;
            closeInfoWindows();
            setAddress = autocomplete.getPlace();
            map.panTo(setAddress.geometry.location);
            map.setZoom(12);
        });

        const renderGeoSearch = (renderOptions, isFirstRendering) => {

            const {
                items,
                currentRefinement,
                refine,
                clearMapRefinement,
                widgetParams,
            } = renderOptions;

            const {
                initialZoom,
                initialPosition,
                container,
            } = widgetParams;

            if (isFirstRendering) {

                const button = document.getElementById('clear-map-refinement-button');

                checkbox = document.getElementById('map-refining-checkbox');
                checkbox.onchange = function(){
                    if(checkbox.checked){
                        filterByMapBounds();
                    }else{
                        clearMapRefinement();
                    }
                }

                var myStyles =[
                    {
                        featureType: "poi",
                        elementType: "labels",
                        stylers: [
                            { visibility: "off" }
                        ]
                    }
                ];

                map = new google.maps.Map(document.getElementById("map"), {
                    zoom: initialZoom,
                    center: initialPosition,
                    minZoom: 6,
                    maxZoom: 19,
                    fullscreenControl: false,
                    styles: myStyles
                });

                map.addListener("dragend", () => {

                    if(isUserInteraction) {
                        filterByMapBounds();
                    }
                });

                 map.addListener("zoom_changed", () => {
                    if(isUserInteraction) {
                        filterByMapBounds();
                    }
                });

                function filterByMapBounds(){
                    if(checkbox.checked){
                        var bounds = map.getBounds();
                        var ne = bounds.getNorthEast();
                        var sw = bounds.getSouthWest();
                        refine({
                            northEast: { lat: ne.lat(), lng: ne.lng() },
                            southWest: { lat: sw.lat(), lng: sw.lng() },
                        });
                    }
                }

                button.addEventListener('click', () => {
                    clearMapRefinement();
                });

            }

            container.querySelector('button').hidden = !currentRefinement;

            //markers.forEach(marker => marker.setMap(null));

            isUserInteraction = false;

            if (!currentRefinement && markers.length) {
                //to be translated for google
                //map.fitBounds(L.featureGroup(markers).getBounds());
            } else if (!currentRefinement) {
                map.setCenter(initialPosition);
            }

            isUserInteraction = true;

        };


        // Create the custom widget
        const customGeoSearch = instantsearch.connectors.connectGeoSearch(
            renderGeoSearch
        );

        // Instantiate the custom widget
        search.addWidgets([
            customGeoSearch({
                container: document.querySelector('#map-tools'),
                initialZoom: 6,
                initialPosition: initialPosition,
            })
        ]);

        search.start();


        $(function(){
            getCurrentLocation();
            /*
            setInterval(function(){
                $('.shop-sidebar-color').each(function(){
                    var refinements = $(this).find('.ais-RefinementList');
                    var title = $(this).find('h4');
                    var search = $(this).find('.ais-SearchBox-input');
                    if(search.length > 0 && search.is(":focus")){
                        return;
                    }
                    if(refinements.hasClass('ais-RefinementList--noRefinement')){
                        title.hide();
                        refinements.hide();
                    }else{
                        title.show();
                        refinements.show();
                    }
                })
            },200)
            */
        })


        function getCurrentLocation(){

            console.log('geolocating...');

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(
                    (position) => {

                        geocodeLatLng(position.coords.latitude,position.coords.longitude);

                        const pos = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude,
                        };
                        map.panTo(pos);
                        map.setZoom(14);

                    },
                    () => {
                        //alert('Impossibile determinare la posizione');
                        //handleLocationError(true, infoWindow, map.getCenter());
                    }
                );
            } else {
                // Browser doesn't support Geolocation
                //alert('Impossibile determinare la posizione 2')
                //handleLocationError(false, infoWindow, map.getCenter());
            }
        }

        function geocodeLatLng(lat,lng) {

            const geocoder = new google.maps.Geocoder();
            const latlng = {
                lat: parseFloat(lat),
                lng: parseFloat(lng),
            };

            geocoder.geocode( { location: latlng }, function(results,status) {
                     if (status !== google.maps.GeocoderStatus.OK) {
                            alert(status);
                        }
                        // This is checking to see if the Geoeode Status is OK before proceeding
                        if (status == google.maps.GeocoderStatus.OK) {
                            console.log(results[0]);
                            setAddress = results[0];
                            var address = (results[0].formatted_address);
                            $('#autocomplete').val(address);
                        }
            });

        }


        @endverbatim
    </script>

    <style>

           .ais-InfiniteHits-item::before{
            display: none!important;
        }

        .merchantInfoWindow p{
            margin-bottom: 0px;
        }

        .ais-RefinementList-showMore:hover, .ais-InfiniteHits-loadMore:hover, .ais-ClearRefinements-button:hover
        {
            color: #fff;
            background-color: #5c636a;
            border-color: #565e64;
        }

        .ais-ClearRefinements-button{
            margin-top: 15px;
        }

        .ais-RefinementList-showMore, .ais-InfiniteHits-loadMore, .ais-ClearRefinements-button{
            width: 100%;
            margin-bottom: 25px;
            color: #fff;
            background-color: #5c636a;
            border-color: #565e64;

            display: inline-block;
            font-weight: 400;
            line-height: 1.5;
            text-align: center;
            text-decoration: none;
            vertical-align: middle;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;


            padding: .25rem .5rem;
            font-size: .875rem;
            border-radius: .2rem;
            transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
        }

        body{
            background: rgb(249,249,249);
            -webkit-font-smoothing: antialised;
        }

        .google-button{
            background: none padding-box rgb(255, 255, 255);

            border: 0px;
            margin: 0px;
            padding: 0px 17px;
            text-transform: none;
            appearance: none;
            cursor: pointer;
            user-select: none;
            direction: ltr;

            text-align: center;
            height: 40px;
            vertical-align: middle;
            color: rgb(86, 86, 86);
            font-family: Roboto, Arial, sans-serif;
            font-size: 18px;
            border-bottom-right-radius: 2px;
            border-top-right-radius: 2px;
            box-shadow: rgb(0 0 0 / 30%) 0px 1px 4px -1px;

        }

        #map-tools{

            position: absolute;
            top: 10px; right: 10px;
        }

        .page-wrapper{
            height: 100vh;
        }

        .ais-Hits-item, .ais-InfiniteHits-item, .ais-InfiniteResults-item, .ais-Results-item{
            width: 100%;
        }

        b{
            color: #E95C05;
        }

        .ais-InfiniteHits-item{
            padding: .625rem 1rem;
            font-size: .9375rem;
            font-weight: 400;
            line-height: 1.5;
            color: #4b566b;
            background-color: #fff;
            background-clip: padding-box;
            border: none;
            border-radius: .3125rem;
            transition: border-color 0.15s ease-in-out,box-shadow 0.15s ease-in-out;
            cursor: pointer;
            transition: 0.3s all;
            border-bottom: 2px solid #fff;
        }

        .ais-InfiniteHits-item a{
            text-decoration: none;
            color: #E95C05;
        }

        .ais-InfiniteHits-item:hover{

            border-bottom: 2px solid #E95C05;
        }


        .ais-InfiniteHits-item:before{
            content: '';
            display: block;
            position: absolute;
            width: 94%;
            left: 3%;
            bottom: 0;
            height: 10px;
            -webkit-box-shadow: 0 5px 5px rgb(0 0 0 / 7%);
            box-shadow: 0 5px 5px rgb(0 0 0 / 7%);
        }

        .ais-InfiniteHits-item p{
            margin-bottom: 5px;
            padding-bottom: 0px;
        }

        .ais-InfiniteHits-item h4{
            font-size: 16px;
            margin: 5px 0px 5px 0px;
        }

        .ais-InfiniteHits-item .item-distance{
            font-size: 13px;
            font-style: italic;
        }

        .ais-InfiniteHits-item .item-categories{
            font-size: 12px;
        }

        #map{
            height: 800px;
            width: 100%;
        }
        .customCompanyMarker{
            height: 20px;
            width: 20px;
            border-radius: 50%;
        }

        .ais-ClearRefinements-button--disabled, .ais-RefinementList-showMore--disabled, .ais-InfiniteHits-loadMore--disabled{
            display: none;
        }

        .ais-ClearRefinements-button{

        }

        .ais-SearchBox-input{
            padding-left: 25px!important;
            display: block;
    width: 100%;
    padding: .625rem 1rem;
    font-size: .9375rem;
    font-weight: 400;
    line-height: 1.5;
    color: #4b566b;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #dae1e7;
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    border-radius: .3125rem;
    box-shadow: inset 0 1px 2px transparent;
    transition: border-color 0.15s ease-in-out,box-shadow 0.15s ease-in-out;
        }

        .ais-RefinementList-labelText{
            font-size: 13px
        }



        .vhos{
            height: 100vh;
            overflow-y: scroll;
        }

        .orange{
            color:#E95C05;
        }

    </style>


</body>
</html>

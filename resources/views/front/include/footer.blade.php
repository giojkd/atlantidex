    <!-- Footer-->
    {{-- <footer class="bg-dark pt-5">  --}}
    <footer class="bg-dark">
        {{-- @include('front.include.footer-first-row')  --}}
        @include('front.include.footer-second-row')

    </footer>
    <!-- Toolbar for handheld devices (Marketplace)-->
    {{--
    <div class="handheld-toolbar">
        <div class="d-table table-layout-fixed w-100">
          <a class="d-table-cell handheld-toolbar-item" href="dashboard-favorites.html"><span class="handheld-toolbar-icon"><i class="ci-heart"></i></span><span class="handheld-toolbar-label">Favorites</span></a>
          <a class="d-table-cell handheld-toolbar-item" href="javascript:void(0)" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" onclick="window.scrollTo(0, 0)"><span class="handheld-toolbar-icon"><i class="ci-menu"></i></span><span class="handheld-toolbar-label">Menu</span></a>
          <a class="d-table-cell handheld-toolbar-item" href="marketplace-cart.html"><span class="handheld-toolbar-icon"><i class="ci-cart"></i><span class="badge bg-primary rounded-pill ms-1">3</span></span><span class="handheld-toolbar-label">$56.00</span></a>
        </div>
    </div>
     --}}
    <!-- Back To Top Button--><a class="btn-scroll-top" href="#top" data-scroll><span class="btn-scroll-top-tooltip text-muted fs-sm me-2">Top</span><i class="btn-scroll-top-icon ci-arrow-up">   </i></a>
    <!-- Vendor scrits: js libraries and plugins-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="/front/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="/front/vendor/simplebar/dist/simplebar.min.js"></script>
    <script src="/front/vendor/tiny-slider/dist/min/tiny-slider.js"></script>
    <script src="/front/vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js"></script>
    <!-- Main theme script-->
    {{-- <script src="/front/js/theme.min.js"></script>  --}}
    @stack('before-body-closing-scripts')
  </body>
</html>

 <!-- Seller of the month-->
    <section class="border-top py-5">
      <div class="container py-lg-2">
        <h2 class="h3 mb-3 pb-3 pb-lg-4 text-center text-lg-start">Seller of the month</h2>
        <div class="row">
          <div class="col-lg-4 text-center text-lg-start pb-3 pt-lg-2">
            <div class="d-inline-block text-start">
              <div class="d-flex align-items-center pb-3">
                <div class="img-thumbnail rounded-circle flex-shrink-0" style="width: 6.375rem;"><img class="rounded-circle" src="img/marketplace/account/avatar.png" alt="Createx Studio"></div>
                <div class="ps-3">
                  <h3 class="fs-lg mb-0">Createx Studio</h3><span class="d-block text-muted fs-ms pt-1 pb-2">Member since November 2019</span><a class="btn btn-primary btn-sm" href="marketplace-vendor.html">View products</a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-8">
            <div class="tns-carousel">
              <div class="tns-carousel-inner" data-carousel-options="{&quot;items&quot;: 2, &quot;gutter&quot;: 16, &quot;controls&quot;: false, &quot;nav&quot;: true, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:1},&quot;500&quot;:{&quot;items&quot;:2},&quot;768&quot;:{&quot;items&quot;:3}}}">
                <div>
                  <div class="card product-card-alt">
                    <div class="product-thumb">
                      <button class="btn-wishlist btn-sm" type="button"><i class="ci-heart"></i></button>
                      <div class="product-card-actions"><a class="btn btn-light btn-icon btn-shadow fs-base mx-2" href="marketplace-single.html"><i class="ci-eye"></i></a>
                        <button class="btn btn-light btn-icon btn-shadow fs-base mx-2" type="button"><i class="ci-cart"></i></button>
                      </div><a class="product-thumb-overlay" href="marketplace-single.html"></a><img src="img/marketplace/products/13.jpg" alt="Product">
                    </div>
                    <div class="card-body">
                      <h3 class="product-title fs-sm mb-2"><a href="marketplace-single.html">Hardcover Book Catalog Mockup</a></h3>
                      <div class="d-flex flex-wrap justify-content-between align-items-center">
                        <div class="fs-sm me-2"><i class="ci-download text-muted me-1"></i>39<span class="fs-xs ms-1">Sales</span></div>
                        <div class="bg-faded-accent text-accent rounded-1 py-1 px-2">$12.<small>00</small></div>
                      </div>
                    </div>
                  </div>
                </div>
                <div>
                  <div class="card product-card-alt">
                    <div class="product-thumb">
                      <button class="btn-wishlist btn-sm" type="button"><i class="ci-heart"></i></button>
                      <div class="product-card-actions"><a class="btn btn-light btn-icon btn-shadow fs-base mx-2" href="marketplace-single.html"><i class="ci-eye"></i></a>
                        <button class="btn btn-light btn-icon btn-shadow fs-base mx-2" type="button"><i class="ci-cart"></i></button>
                      </div><a class="product-thumb-overlay" href="marketplace-single.html"></a><img src="img/marketplace/products/14.jpg" alt="Product">
                    </div>
                    <div class="card-body">
                      <h3 class="product-title fs-sm mb-2"><a href="marketplace-single.html">Top View Smartwatch 3D Render</a></h3>
                      <div class="d-flex flex-wrap justify-content-between align-items-center">
                        <div class="fs-sm me-2"><i class="ci-download text-muted me-1"></i>28<span class="fs-xs ms-1">Sales</span></div>
                        <div class="bg-faded-accent text-accent rounded-1 py-1 px-2">$14.<small>00</small></div>
                      </div>
                    </div>
                  </div>
                </div>
                <div>
                  <div class="card product-card-alt">
                    <div class="product-thumb">
                      <button class="btn-wishlist btn-sm" type="button"><i class="ci-heart"></i></button>
                      <div class="product-card-actions"><a class="btn btn-light btn-icon btn-shadow fs-base mx-2" href="marketplace-single.html"><i class="ci-eye"></i></a>
                        <button class="btn btn-light btn-icon btn-shadow fs-base mx-2" type="button"><i class="ci-cart"></i></button>
                      </div><a class="product-thumb-overlay" href="marketplace-single.html"></a><img src="img/marketplace/products/07.jpg" alt="Product">
                    </div>
                    <div class="card-body">
                      <h3 class="product-title fs-sm mb-2"><a href="marketplace-single.html">Gravity Device Mockups (PSD)</a></h3>
                      <div class="d-flex flex-wrap justify-content-between align-items-center">
                        <div class="fs-sm me-2"><i class="ci-download text-muted me-1"></i>234<span class="fs-xs ms-1">Sales</span></div>
                        <div class="bg-faded-accent text-accent rounded-1 py-1 px-2">$16.<small>00</small></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

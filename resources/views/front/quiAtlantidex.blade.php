@extends('front.main')
@section('content')
    {{-- @include('front.include.header') --}}
    <!-- Hero section-->
    <section class="bg-accent bg-position-top-center bg-repeat-0 py-5" style="background-image: url(img/home/marketplace-hero.jpg);">
        <div class="pb-lg-5 mb-lg-3">
            <div class="container-fluid pb-lg-5 my-lg-5">
                <div class="row mb-4 mb-sm-5">
                    <div class="col-lg-12 text-center">
                        <img src="/front/img/atlantidex-logo-light-payoff.png" alt="" style="height: 140px; margin-bottom: 80px;">
                        <h1 class="text-white lh-base text-center">
                            <span class='fw-light'>Cerca tra </span>
                            Ristoranti, Hotel, Negozi e Studi Professionali
                            <span class='fw-light'>intorno a te e trova quello che desideri al prezzo che vuoi tu!</span>

                        {{-- <h2 class="h5 text-white fw-light">Un nuovo mondo al prezzo che vuoi tu!</h2>  --}}
                    </div>
                </div>
                <div class="row mb-sm-5 justify-content-center">
                    <div class="col-lg-6 col-md-8 justify-content-center">

                        <div class="input-group input-group-lg">
                            <i class="fa fa-search position-absolute top-50 translate-middle-y ms-3"></i>
                            <input class="form-control rounded-start" type="text" placeholder="Dove stai cercando?" id="autocomplete">
                            {{-- <button class="btn btn-primary btn-lg dropdown-toggle fs-base" type="button" data-bs-toggle="dropdown">All categories</button> --}}
                            {{-- <div class="dropdown-menu dropdown-menu-end my-1"><a class="dropdown-item" href="#">Photos</a><a class="dropdown-item" href="#">Graphics</a><a class="dropdown-item" href="#">UI Design</a><a class="dropdown-item" href="#">Web Themes</a><a class="dropdown-item" href="#">Add-Ons</a></div> --}}
                        </div>
                        <small><a class="text-white" href="javascript:getCurrentLocation()"><i class="fas fa-map-marker-alt"></i> Cerca intorno a me</a></small>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="container-fluid position-relative pt-3 pt-lg-0 pb-5 mt-lg-n10" style="z-index: 10;">
        <div class="card px-lg-2 border-0 shadow-lg overflow-hidden">
            <div class="card-body p-0">
                <div class="row">
                    <div class="col-md-8 p-0">
                        <div id="map"></div>
                    </div>
                    <div class="col-md-4 p-0">
                        <div class="companies-box">
                            @foreach ($companies as $company)
                                <div id="company-{{ $company['id'] }}" class="company" onclick="panToMarker({{ $company['id'] }})">
                                    <div class="px-4">
                                        <h5 class="company-name mb-2">{{ $company['name'] }}</h5>
                                        <p class="mb-2"><small>{{ $company['short_description'] }}</small></p>
                                        <div class="row">

                                                <div class="col-md-12">
                                                    <ul class="list list-unstyled">
                                            @auth

                                                @if (Auth::user()->hasRole('Networker') || Auth::user()->hasRole('Company'))
                                                    @foreach ($company['companycategories'] as $cat)
                                                        <li class="mb-0"><small>{{ $cat->name }} - <b>{{ number_format($cat->pivot->commission/2,2) }}EP</b> e <b>{{ number_format($cat->pivot->commission/2,2) }}EP+</b></small></li>
                                                    @endforeach
                                                @endif

                                                @if (Auth::user()->hasRole('Customer'))
                                                    @foreach ($company['companycategories'] as $cat)
                                                        <li class="mb-0"><small>{{ $cat->name }} - {{ number_format($cat->pivot->commission/2,2) }}EP+ </small></li>
                                                    @endforeach
                                                @endif
                                                <li><hr></li>

                                            @endauth

                                                    <a href="https://www.google.com/maps/place/{{ urlencode($company['address']) }}"><li class="mb-0"><small><i class="fa fa-map"></i> {{ $company['address'] }}</a></small></li>
                                                    <li class="mb-0"><a href="tel:{{ $company['telephone'] }}"><small><i class="fa fa-phone"></i> {{ $company['telephone'] }}</a></small></li>
                                                    <li class="mb-0"><small><a href="mailto:{{ $company['email'] }}"><i class="fa fa-envelope"></i> {{ $company['email'] }}</a></small></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



    {{-- @include('front.include.home.featured-products') --}}

    {{-- @include('front.include.home.new-arrivals') --}}

    {{-- @include('front.include.home.best-sellers') --}}

    {{-- @include('front.include.home.features-stripe') --}}

    {{-- @include('front.include.home.new-from-the-blog') --}}

    <style>
        a{
            color: #4e54c8;
        }
    </style>

@endsection

@push('before-body-closing-scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API') }}&callback=initMap&libraries=places&v=weekly" async></script>
    <script src="https://unpkg.com/@google/markerclustererplus@4.0.1/dist/markerclustererplus.min.js"></script>


    <script>

        var companies = @json($companies);
        var companiesMarkers = [];
        var map;
        var markerCluster;
        var bound ;
        var companyInfowindow;
        var companyInfowindowRoute = '{{ Route('quiAtlantidexCompanyInfowindow') }}';
        var autocomplete;

        // Initialize and add the map
        function initMap() {

            var bound = new google.maps.LatLngBounds();
            for (i = 0; i < companies.length; i++) {
                bound.extend( new google.maps.LatLng(companies[i].coordinates.lat, companies[i].coordinates.lng) );
            }

            autocomplete = new google.maps.places.Autocomplete(
                document.getElementById("autocomplete"),
                {
                    types: ["geocode"],
                    bounds: bound,
                    strictBounds: true,
                    country: ['it']
                },

            );

            autocomplete.addListener("place_changed", () => {
                closeInfoWindows();
                const place = autocomplete.getPlace();
                map.panTo(place.geometry.location);
                map.setZoom(12);
            });

            // The map, centered at Uluru
            map = new google.maps.Map(document.getElementById("map"), {
                zoom: 7,
                center: bound.getCenter(),
            });

            addCompaniesMarkers();

        }

        function addCompaniesMarkers(){

            for (var i in companies){
                var company = companies[i];
                var companyMarker = new google.maps.Marker({
                        position: company.coordinates,
                        map,
                        title: company.name,
                    });
                companyMarker.id = parseInt(company.id);
                companyMarker.name = company.name;
                companyMarker.addListener("click", clickMarker);
                companiesMarkers.push(companyMarker);
            }

            var markerCluster = new MarkerClusterer(map, companiesMarkers, {
                imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
            });

        }

        function clickMarker(){
            var marker = this;
            openMarkerInfoWindow(this.id);
            scrollToCompany(this.id);
        }

        function panToMarker(id){
            id = parseInt(id);
            panAndLoadInfoWindowForMarker(id);
        }

        function panAndLoadInfoWindowForMarker(id){
            openMarkerInfoWindow(id);
        }

        function openMarkerInfoWindow(id){
            var i = getIndexById(id);
            $.get(companyInfowindowRoute+'?id='+id,function(r){
                closeInfoWindows();
                companyInfowindow = new google.maps.InfoWindow({
                    content: r
                });
                companyInfowindow.open(map, companiesMarkers[i]);
                map.panTo(companiesMarkers[i].getPosition());
                map.setZoom(14);
            });
        }

        function getIndexById(id){
            for(var i in companiesMarkers){
                if(companiesMarkers[i].id === id){
                    return i;
                }
            }
        }

        function closeInfoWindows(){
            if (companyInfowindow) {
                companyInfowindow.close();
            }
        }

        function getCurrentLocation(){

            console.log('locating...');

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(
                    (position) => {
                        console.log(position);
                        const pos = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude,
                        };
                        map.panTo(pos);
                        map.setZoom(14);
                    },
                    () => {
                        alert('Impossibile determinare la posizione');
                        //handleLocationError(true, infoWindow, map.getCenter());
                    }
                );
            } else {
                // Browser doesn't support Geolocation
                alert('Impossibile determinare la posizione 2')
                //handleLocationError(false, infoWindow, map.getCenter());
            }
        }

        function scrollToCompany(id){

        }

        $(function(){
            getCurrentLocation();
        })



    </script>

    <style>

        #map{
            width: 100%;
            height: 640px;
        }

        .companies-box{
            height: 640px;
            overflow-y: scroll;

        }

        .companies-box .company{
            padding: 10px 15px;
            border-bottom: 1px solid #ececec;
            transition: 0.5s all;
            cursor: pointer;
        }

        .companies-box .company:hover{
            background-color: #eee;
        }


    </style>
@endpush

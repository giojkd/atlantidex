@auth

@extends('admin_custom.top_left')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col text-center">
                <div class="jumbotron">
                    <div class="text-center mb-4">
                        <img src="/uploads/logo-dark.png" alt="">
                    </div>
                    <h1 class="display-4">{{ $title }}</h1>
                    <p class="lead">Your Career Update is in progress.</p>
                    <hr class="my-4">
                    <p>Nel frattempo consulta la sezione dedicata ai voucher della GDO</p>
                    <p class="lead">
                        <a class="btn btn-primary btn-lg" href="/my-atlantinex/gdovouchersshop/create" role="button">Vai ai voucher della GDO</a>
                    </p>
                </div>
            </div>
        </div>
    </div>

@endsection

@endauth

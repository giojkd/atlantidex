<h5>Righe dell'ordine</h5>
<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Prodotto</th>
            <th>Quantità</th>
            <th>Commissione (%)</th>
            <th>Commissione (€)</th>
            <th>Subtotale</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($rows as $row)
            <tr>
                <td>{{ $row->description }}</td>
                <td>{{ $row->quantity }}</td>
                <td>{{ $row->commission_percentage }}%</td>
                <td>@fp($row->commission_amount)</td>
                <td>@fp($row->subtotal )</td>
            </tr>
        @endforeach
    </tbody>
</table>

@auth

@extends('admin_custom.top_left')

@section('content')

<h2>Inquadra il qr code del biglietto da convalidare</h2>

<video src="" id="preview" style="max-width: 100%; height: 320px"></video>



<table class="table table-hover table-striped" id="ticketInfoTable" style="display: none">
    <thead>
        <tr>
            <th>ID</th>
            <th>Cliente</th>
            <th>Evento</th>
            <th>Data evento</th>
            <th>Biglietto</th>
            <th>Descrizione</th>
            <th>Azioni</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                <span id="ticket_id"></span>
            </td>
            <td>
                <span id="client_name"></span>
            </td>
            <td>
                <span id="event"></span>
            </td>
            <td>
                <span id="event_date"></span>
            </td>
            <td>
                <span id="ticket_name"></span>
            </td>
            <td>
                <span id="ticket_description"></span>
            </td>
            <td>
                <a href="javascript:" id="invalidate_action" onclick="invalidateTicket()" class="btn btn-primary">Invalida</a>
            </td>
        </tr>
    </tbody>
</table>

@endsection

@push('after_scripts')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/webrtc-adapter/3.3.3/adapter.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.1.10/vue.min.js"></script>
    <script type="text/javascript" src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>


    <script>

        var ticketId = 0;

        function invalidateTicket(){
            if(ticketId> 0){
                if(confirm('Sei sicuro di voler convalidare il ticket?')){
                    $.post('{{ backpack_url('invalidate-ticket') }}',{id:ticketId},function(r){
                        alert('Ticket invalidato correttamente');
                        $('#ticketInfoTable').slideUp();
                    },'json')
                }
            }
        }

        $(function(){
            let scanner = new Instascan.Scanner({ video: document.getElementById('preview') });
            scanner.addListener('scan', function (content) {
                $.post('{{ backpack_url('test-ticket') }}',{id:content},function(ticket){
                    if(ticket.is_used){
                        alert('Il ticket è già stato usato');
                        $('#ticketInfoTable').slideUp();
                    }else{
                        $('#ticket_id').html(ticket.id);
                        $('#client_name').html(ticket.user.name+' '+ticket.user.surname);
                        $('#event').html(ticket.event.title.it);
                        $('#event_date').html(ticket.event.happens_at);
                        $('#ticket_name').html(ticket.name);
                        $('#ticket_description').html(ticket.description);
                        ticketId = ticket.id;
                        $('#ticketInfoTable').slideDown();
                    }
                },'json')
            });
            Instascan.Camera.getCameras().then(function (cameras) {
                if (cameras.length > 0) {
                scanner.start(cameras[0]);
                } else {
                console.error('No cameras found.');
                }
            }).catch(function (e) {
                console.error(e);
            });
        })

    </script>


@endpush

@endauth

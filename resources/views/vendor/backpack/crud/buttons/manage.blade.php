@if ($crud->hasAccess('manage'))
    @if (auth()->user()->id == $entry->owner_id)
        <a href="{{ '/admin/product/'.$entry->getKey().'/edit' }}" class="btn btn-sm btn-atlantinex"><i class="la la-edit"></i> {{ trans('backpack::crudcontroll.manage') }}</a>
    @else
        <a href="{{ '/admin/product/'.$entry->getKey().'/show' }}" class="btn btn-sm btn-atlantinex"><i class="la la-edit"></i> {{ trans('backpack::crudcontroll.manage') }}</a>
    @endif
@endif

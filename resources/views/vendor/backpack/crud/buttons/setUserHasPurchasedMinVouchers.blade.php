@if(is_null($entry->has_purchased_min_vouchers))
    <a href="javascript:void(0)"
    onclick="setUsersColumnTimestamp(this)"
    data-id="{{ $entry->getKey() }}"
    data-column="has_purchased_min_vouchers"
    >
        <i class="fa fa-check"></i> Vouchers
    </a>
@else
    <small>Vouchers {{ $entry->has_purchased_min_vouchers->format('d/m/Y') }}</small>
@endif


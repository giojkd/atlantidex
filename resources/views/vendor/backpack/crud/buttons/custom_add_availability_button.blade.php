
<a href="{{ url($crud->getRoute()) }}/create?{{ http_build_query($crud->getRequest()->all()) }}" class="btn btn-primary" data-style="zoom-in">
    <span class="ladda-label">
        <i class="la la-plus"></i> Aggiungi disponibilità
    </span>
</a>

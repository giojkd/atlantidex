@if(is_null($entry->email_verified_at))
    <a href="javascript:void(0)"
    onclick="setUsersColumnTimestamp(this)"
    data-id="{{ $entry->getKey() }}"
    data-column="email_verified_at"
    >
        <i class="fa fa-check"></i> Email {{ $entry->email_verified_at }}
    </a>
@else
    <small>Email {{ $entry->email_verified_at->format('d/m/Y H:i') }}</small>
@endif


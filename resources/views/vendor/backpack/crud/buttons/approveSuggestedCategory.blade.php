@if(is_null($entry->category_id))
    <a href="javascript:void(0)"
    onclick="approveSuggestedCategory(this)"
    data-id="{{ $entry->getKey() }}"
    >
        <i class="fa fa-check"></i> Approva categoria
    </a>
@else
    <small>Approvata</small>
@endif


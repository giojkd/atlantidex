@extends(backpack_view('layouts.plain'))

@section('content')
    <div class="row justify-content-center">
        <div class="col-12 col-md-8 col-lg-6">
            <h3 class="text-center mb-4">{{ trans('backpack::base.register') }}</h3>
            <div class="card">
                <div class="card-body">
                    <form class="col-md-12 p-t-10" role="form" method="POST" action="{{ route('backpack.auth.register') }}">
                        <input type="hidden" name="country_id" value="2">
                        @if(isset($code))
                            <input type="hidden" name="code" value="{{ $code  }}">
                        @endif
                        <input type="hidden" name="address" >

                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                            <label class="control-label" for="name">{{ trans('backpack::base.name') }}</label>

                            <div>
                                <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="name" value="{{ old('name') }}">

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                            </div>
                            <div class="col">
                                    <div class="form-group">
                            <label class="control-label" for="surname">{{ trans('backpack::permissionmanager.surname') }}</label>

                            <div>
                                <input type="text" class="form-control{{ $errors->has('surname') ? ' is-invalid' : '' }}" name="surname" id="surname" value="{{ old('surname') }}">

                                @if ($errors->has('surname'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('surname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                            </div>
                        </div>


                         <div class="row">
                            <div class="col">
                                <label ><input type="checkbox" name="" id=""> Sono un dipendente pubblico</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="phone">Cellulare</label>

                            <div>

                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">+39</span>
                                    </div>
                                    <input type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" id="phone" value="{{ old('phone') }}">
                                </div>

                                @if ($errors->has('phone'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                   <div class="form-group">
                            <label class="control-label" for="date_of_birth">{{ trans('backpack::permissionmanager.date_of_birth') }}</label>

                            <div>
                                <input id="datepicker" type="text" class="form-control{{ $errors->has('date_of_birth') ? ' is-invalid' : '' }}" name="date_of_birth" id="date_of_birth" value="{{ old('date_of_birth') }}">

                                @if ($errors->has('date_of_birth'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('date_of_birth') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                            </div>

                             <div class="col">
                                <div class="form-group">
                                    <label class="control-label" for="place_of_birth">Luogo di nascita</label>

                                    <div>
                                        <input type="text" class="form-control{{ $errors->has('place_of_birth') ? ' is-invalid' : '' }}" name="place_of_birth" id="place_of_birth" value="{{ old('place_of_birth') }}">

                                        @if ($errors->has('place_of_birth'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('place_of_birth') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                             <div class="col">
                                <div class="form-group">
                                    <label class="control-label" for="fiscal_code">{{ trans('backpack::permissionmanager.fiscal_code') }}</label>

                                    <div>
                                        <input type="text" class="form-control{{ $errors->has('fiscal_code') ? ' is-invalid' : '' }}" name="fiscal_code" id="fiscal_code" value="{{ old('fiscal_code') }}">

                                        @if ($errors->has('fiscal_code'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('fiscal_code') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>


                         <div class="form-group">
                            <label class="control-label" for="address">{{ trans('backpack::permissionmanager.address') }}</label>

                            <div>
                                <input id="userAddress" type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" id="address" >

                                @if ($errors->has('address'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <p class="text-muted"><small>Nel caso in cui non trovassi l'indirizzo esatto inserisci l'indirizzo più vicino suggerito dal sistema e comunicaci l'indirizzo corretto via email a amministrazione@atlantidex.com</small></p>
                        </div>


<!--

                        <div class="form-group">
                            <label class="control-label" for="address">{{ trans('backpack::permissionmanager.address') }}</label>

                            <div>
                                <input type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" id="address" value="{{ old('address') }}">

                                @if ($errors->has('address'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                    -->

                        <div class="form-group">
                            <label class="control-label" for="{{ backpack_authentication_column() }}">{{ config('backpack.base.authentication_column_name') }}</label>

                            <div>
                                <input type="{{ backpack_authentication_column()=='email'?'email':'text'}}" class="form-control{{ $errors->has(backpack_authentication_column()) ? ' is-invalid' : '' }}" name="{{ backpack_authentication_column() }}" id="{{ backpack_authentication_column() }}" value="{{ old(backpack_authentication_column()) }}">

                                @if ($errors->has(backpack_authentication_column()))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first(backpack_authentication_column()) }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="row">
                            <div class="col">
                                  <div class="form-group">
                            <label class="control-label" for="password">{{ trans('backpack::base.password') }}</label>

                            <div>
                                <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" id="password">

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                            </div>
                            <div class="col">


                        <div class="form-group">
                            <label class="control-label" for="password_confirmation">{{ trans('backpack::base.confirm_password') }}</label>

                            <div>
                                <input type="password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation" id="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <p class="text-muted"><small>La password deve essere lunga almeno otto caratteri e contenere almeno un carattere maiuscolo, uno minuscolo ed un numero </small></p>
                            </div>
                        </div>

                         <div class="privacy-policy-wrapper">
                            <small>
                                @include('include.privacy_policy')
                            </small>
                        </div>
                        <div class="form-group">
                            <label><input type="checkbox" name="accept_privacy"> Accetto l'informativa sulla privacy</label>
                        </div>
                        <div class="form-group">
                            <div>
                                <button type="submit" class="btn btn-block btn-primary">
                                    {{ trans('backpack::base.register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @if (backpack_users_have_email())
                <div class="text-center">
                    <a href="{{ route('backpack.auth.password.reset') }}">{{ trans('backpack::base.forgot_your_password') }}</a>
                </div>
            @endif
            <div class="text-center"><a href="{{ route('backpack.auth.login') }}">{{ trans('backpack::base.login') }}</a></div>
        </div>
    </div>
@endsection


@push('after_scripts')

    <script src="https://cdn.jsdelivr.net/npm/places.js@1.18.2"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.it.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.standalone.min.css">
    <script>
        $(function(){

            $('footer').hide();

             var placesAutocomplete = places({
                appId: '{{env('ALGOLIA_PLACES_APP_ID')}}',
                apiKey: '{{env('ALGOLIA_PLACES_API_KEY')}}',
                container: document.querySelector('#userAddress')
            });
            placesAutocomplete.on('change', e => {
                $('input[name="address"]').val(JSON.stringify(e.suggestion));

            });
            $('#datepicker').datepicker({
                format: '{{ config('app.date_format_javascript') }}',
                endDate: '-6570d'
            });
        })
    </script>
@endpush

@extends(backpack_view('layouts.plain'))

@section('content')

    <div class="row justify-content-center">
        <div class="col-12 col-md-8 col-lg-6">
            <h3 class="text-center mb-4">{{ trans('backpack::base.register') }}</h3>
            <div class="card">
                <div class="card-body">

                    @if ($errors->has('fiscal_code'))
                        <span class="alert alert-warning d-block">
                            <strong>{!! $errors->first('fiscal_code') !!}</strong>
                        </span>
                        <hr>
                        @hss('10')
                    @endif

                    <form class="col-md-12 p-t-10" role="form" method="POST" action="{{ route('backpack.auth.registercompany') }}">
                        <input type="hidden" name="country_id" value="2">
                        @if(isset($code))
                            <input type="hidden" name="code" value="{{ $code }}">
                        @endif
                        <input type="hidden" name="address" >

                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                            <label class="control-label" for="name">{{ trans('backpack::base.name') }}</label>

                            <div>
                                <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="name" value="{{ old('name') }}">

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                            </div>
                            <div class="col">
                                    <div class="form-group">
                            <label class="control-label" for="surname">{{ trans('backpack::permissionmanager.surname') }}</label>

                            <div>
                                <input type="text" class="form-control{{ $errors->has('surname') ? ' is-invalid' : '' }}" name="surname" id="surname" value="{{ old('surname') }}">

                                @if ($errors->has('surname'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('surname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                            </div>
                        </div>

                        <div class="row">



                            <div class="col">
                                 <div class="form-group">
                                    <label class="control-label" for="date_of_birth">{{ trans('backpack::permissionmanager.date_of_birth') }}</label>
                                    <div>
                                        <input id="datepicker" autocomplete="something-new" type="text" class="datepicker form-control{{ $errors->has('date_of_birth') ? ' is-invalid' : '' }}" name="date_of_birth" id="date_of_birth" value="{{ old('date_of_birth') }}">
                                        @if ($errors->has('date_of_birth'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('date_of_birth') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                                 <div class="col">
                                <div class="form-group">
                                    <label class="control-label" for="place_of_birth">Luogo di nascita</label>

                                    <div>
                                        <input type="text" class="form-control{{ $errors->has('place_of_birth') ? ' is-invalid' : '' }}" name="place_of_birth" id="place_of_birth" value="{{ old('place_of_birth') }}">

                                        @if ($errors->has('place_of_birth'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('place_of_birth') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                             <div class="col">
                                 <div class="form-group">
                                    <label class="control-label" for="fiscal_code">Codice fiscale</label>
                                    <div>
                                        <input id="fiscal_code" type="text" class="form-control{{ $errors->has('fiscal_code') ? ' is-invalid' : '' }}" name="fiscal_code" id="fiscal_code" value="{{ old('fiscal_code') }}">
                                        @if ($errors->has('fiscal_code'))
                                            <span class="invalid-feedback">
                                                <strong>{!! $errors->first('fiscal_code') !!}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                         <div class="form-group">
                            <label class="control-label" for="phone">Cellulare</label>

                            <div>
                                <input type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" id="phone" value="{{ old('phone') }}">

                                @if ($errors->has('phone'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="{{ backpack_authentication_column() }}">{{ config('backpack.base.authentication_column_name') }}</label>

                            <div>
                                <input type="{{ backpack_authentication_column()=='email'?'email':'text'}}" class="form-control{{ $errors->has(backpack_authentication_column()) ? ' is-invalid' : '' }}" name="{{ backpack_authentication_column() }}" id="{{ backpack_authentication_column() }}" value="{{ old(backpack_authentication_column()) }}">

                                @if ($errors->has(backpack_authentication_column()))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first(backpack_authentication_column()) }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <hr>

                        <div class="form-group">
                            <label class="control-label" for="company_name">{{ trans('backpack::permissionmanager.company_name') }}</label>

                            <div>
                                <input type="text" class="form-control{{ $errors->has('company_name') ? ' is-invalid' : '' }}" name="company_name" id="company_name" value="{{ old('company_name') }}">

                                @if ($errors->has('company_name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('company_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="row">

                            <div class="col">
                                <div class="form-group">
                            <label class="control-label" for="vat_number">{{ trans('backpack::permissionmanager.vat_number') }}</label>

                            <div>
                                <input type="text" class="form-control{{ $errors->has('vat_number') ? ' is-invalid' : '' }}" name="vat_number" id="vat_number" value="{{ old('vat_number') }}">

                                @if ($errors->has('vat_number'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('vat_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                            </div>

                                <div class="col">
                                 <div class="form-group">
                                    <label class="control-label" for="company_fiscal_code">Codice fiscale della società</label>
                                    <div>
                                        <input id="fiscal_code" type="text" class="form-control{{ $errors->has('company_fiscal_code') ? ' is-invalid' : '' }}" name="company_fiscal_code" id="company_fiscal_code" value="{{ old('company_fiscal_code') }}">
                                        @if ($errors->has('company_fiscal_code'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('company_fiscal_code') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <p class="text-muted"><small>se uguale alla partita IVA reinserire</small></p>
                                </div>
                            </div>
                        </div>


                         <div class="form-group">
                            <label class="control-label" for="address">Sede della società</label>

                            <div>
                                <input id="userAddress" type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" id="address" >

                                @if ($errors->has('address'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <p class="text-muted"><small>Nel caso in cui non trovassi l'indirizzo esatto inserisci l'indirizzo più vicino suggerito dal sistema e comunicaci l'indirizzo corretto via email a amministrazione@atlantidex.com</small></p>
                        </div>


<!--

                        <div class="form-group">
                            <label class="control-label" for="address">{{ trans('backpack::permissionmanager.address') }}</label>

                            <div>
                                <input type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" id="address" value="{{ old('address') }}">

                                @if ($errors->has('address'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                    -->

                        <div class="form-group row">
                            <div class="col-md-6">
                                <label class="control-label" for="sdi">Tipo di società</label>
                                <div>
                                    <select name="companytype_id" id="companytype_id" class="form-control {{ $errors->has('companytype_id') ? ' is-invalid' : '' }}">
                                        @foreach ($companyTypes as $companyType)
                                            <option value="{{ $companyType->id }}" @if( old('companytype_id') == $companyType->id) selected @endif>
                                                {{ $companyType->name }}
                                            </option>
                                        @endforeach
                                    </select>


                                    @if ($errors->has('companytype_id'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('companytype_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="control-label" for="pec">Capitale sociale</label>
                                <div>

                                        <input type="text" class="form-control {{ $errors->has('share_capital') ? ' is-invalid' : '' }}" name="share_capital" id="share_capital" value="{{ old('share_capital') }}">




                                    @if ($errors->has('share_capital'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('share_capital') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-md-6">
                                <label class="control-label" for="email_for_invoices">Email per invio fatture</label>
                                <div>
                                    <input type="email" class="form-control{{ $errors->has('email_for_invoices') ? ' is-invalid' : '' }}" name="email_for_invoices" id="email_for_invoices" value="{{ old('email_for_invoices') }}">

                                    @if ($errors->has('email_for_invoices'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('email_for_invoices') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="control-label" for="pec">Persona di contatto</label>
                                <div>
                                    <input type="text" class="form-control{{ $errors->has('contact_person') ? ' is-invalid' : '' }}" name="contact_person" id="contact_person" value="{{ old('contact_person') }}">

                                    @if ($errors->has('contact_person'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('contact_person') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6">
                                <label class="control-label" for="email_for_invoices">Fax</label>
                                <div>
                                    <input type="text" class="form-control{{ $errors->has('fax') ? ' is-invalid' : '' }}" name="fax" id="fax" value="{{ old('fax') }}">

                                    @if ($errors->has('fax'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('fax') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="control-label" for="pec">Settore aziendale</label>
                                <div>
                                    <input type="text" class="form-control{{ $errors->has('corporate_sector') ? ' is-invalid' : '' }}" name="corporate_sector" id="corporate_sector" value="{{ old('corporate_sector') }}">

                                    @if ($errors->has('corporate_sector'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('corporate_sector') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                        </div>


                        <div class="form-group row">
                            <div class="col-md-6">
                                <label class="control-label" for="sdi">SDI</label>
                                <div>
                                    <input type="text" class="form-control{{ $errors->has('sdi') ? ' is-invalid' : '' }}" name="sdi" id="sdi" value="{{ old('sdi') }}">

                                    @if ($errors->has('sdi'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('sdi') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="control-label" for="pec">PEC</label>
                                <div>
                                    <input type="email" class="form-control{{ $errors->has('pec') ? ' is-invalid' : '' }}" name="pec" id="pec" value="{{ old('pec') }}">

                                    @if ($errors->has('pec'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('pec') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                        </div>

                        <div class="form-group row">
                             <div class="col-md-6">
                                <label class="control-label" for="website">Sito internet</label>
                                <div>
                                    <input type="text" class="form-control{{ $errors->has('website') ? ' is-invalid' : '' }}" name="website" id="website" value="{{ old('website') }}">

                                    @if ($errors->has('website'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('website') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="control-label" for="pec">Codice REA</label>
                                <div>
                                    <input type="text" class="form-control{{ $errors->has('company_registration_number') ? ' is-invalid' : '' }}" name="company_registration_number" id="company_registration_number" value="{{ old('company_registration_number') }}">

                                    @if ($errors->has('company_registration_number'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('company_registration_number') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>



                         <div class="form-group">
                            <label class="control-label" for="company_email">Email della società</label>

                            <div>
                                <input type="email" class="form-control{{ $errors->has('company_email') ? ' is-invalid' : '' }}" name="company_email" id="company_email" value="{{ old('company_email') }}">

                                @if ($errors->has('company_email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('company_email') }}</strong>
                                    </span>
                                @endif
                                <div class="text-muted">
                                    <small>Utilizza questo indirizzo email per accedere all'account della società</small>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col">
                                  <div class="form-group">
                            <label class="control-label" for="password">{{ trans('backpack::base.password') }}</label>

                            <div>
                                <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" id="password">

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                            </div>
                            <div class="col">


                        <div class="form-group">
                            <label class="control-label" for="password_confirmation">{{ trans('backpack::base.confirm_password') }}</label>

                            <div>
                                <input type="password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation" id="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <p class="text-muted">
                                    <small>
                                        La password deve essere lunga almeno otto caratteri e contenere almeno un carattere maiuscolo, uno minuscolo ed un numero.<br>
                                        La registrazione creerà due account uno con la tua email ed uno con l'email della società, entrambi avranno la stessa password.<br>

                                    </small>
                                </p>
                            </div>
                        </div>
                        <div class="privacy-policy-wrapper">
                            <small>
                                @include('include.privacy_policy')
                            </small>
                        </div>
                         <div class="form-group">
                            <label><input type="checkbox" name="accept_privacy"> Accetto l'informativa sulla privacy</label>
                        </div>
                        <div class="form-group">
                            <div>
                                <button type="submit" class="btn btn-block btn-primary">
                                    {{ trans('backpack::base.register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @if (backpack_users_have_email())
                <div class="text-center"><a href="{{ route('backpack.auth.password.reset') }}">{{ trans('backpack::base.forgot_your_password') }}</a></div>
            @endif
            <div class="text-center"><a href="{{ route('backpack.auth.login') }}">{{ trans('backpack::base.login') }}</a></div>
        </div>
    </div>


@endsection

@push('after_scripts')

    <style>
        .privacy-policy-wrapper{
            height: 240px;
            overflow: scroll;
            margin: 0px 0px 20px 0px;
            padding: .375rem .75rem;
            border: 1px solid #ececec;
            border-radius: 3px;
        }
    </style>

    <script src="https://cdn.jsdelivr.net/npm/places.js@1.18.2"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.it.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.standalone.min.css">
    <script>
        $(function(){
            $('footer').hide();

             var placesAutocomplete = places({
                appId: '{{env('ALGOLIA_PLACES_APP_ID')}}',
                apiKey: '{{env('ALGOLIA_PLACES_API_KEY')}}',
                container: document.querySelector('#userAddress')
            });
            placesAutocomplete.on('change', e => {
                $('input[name="address"]').val(JSON.stringify(e.suggestion));

            });
            $('.datepicker').datepicker({
                format: '{{ config('app.date_format_javascript') }}',
                endDate: '-6570d'
            });



        })
    </script>
@endpush

@if (Auth::user()->hasVerifiedEmail())




<li class='nav-item'><a class='nav-link a2hs-button' href='javascript:'><i class="nav-icon fab fa-app-store-ios"></i> Installa App</a></li>

    @if (Auth::user()->isImpersonating())
            <li class="nav-item"><a class="nav-link" href="{{ url('stop-impersonating') }}"><i class="nav-icon fa fa-user"></i> <span>Stop impersonating</span></a></li>
    @endif


<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
@role('Superuser')
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('language') }}'><i class='nav-icon fa fa-language'></i> Languages</a></li>
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('elfinder') }}\"><i class="nav-icon fa fa-files-o"></i> <span>{{ trans('backpack::crud.file_manager') }}</span></a></li>
@endrole
<!-- Users, Roles, Permissions -->
@role('Superuser')
<li class="nav-item nav-dropdown">
    @if (Auth::user()->isImpersonating())
        <li class="nav-item"><a href="{{ url('stop-impersonating') }}">Stop Impersonating</a></li>
    @endif
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon fa fa-group"></i> Authentication</a>
    <ul class="nav-dropdown-items">
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('user') }}"><i class="nav-icon fa fa-user"></i> <span>Users</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('role') }}"><i class="nav-icon fa fa-group"></i> <span>Roles</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('permission') }}"><i class="nav-icon fa fa-key"></i> <span>Permissions</span></a></li>
    </ul>

</li>
@endrole

@role('Company')
    <li class='nav-item'><a class='nav-link' href='\company\company-data'><i class='nav-icon fa fa-user'></i> I tuoi dati</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('MyAtlantinex.payment-data.index') }}"><i class="nav-icon fa fa-user"></i> <span>Dati di pagamento</span></a></li>

    <li class='nav-item'><a class='nav-link' href='{{ route('catalogueSearch') }}'><i class='nav-icon fa fa-book'></i> Aggiungi prodotti o servizi</a></li>
    <!--<li class='nav-item'><a class='nav-link' href='{{ backpack_url('catalogue') }}'><i class='nav-icon fa fa-book'></i> Catalogo Atlantidex</a></li>-->
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('product') }}'><i class='nav-icon fa fa-tags'></i> I miei prodotti e servizi</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ route('Company.categorySuggest') }}'><i class='nav-icon fa fa-lightbulb-o'></i> Suggerisci categoria</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('merchant') }}'><i class='nav-icon fa fa-building-o'></i> Punti vendita</a></li>
    <!--<li class="nav-item"><a class="nav-link" href="{{ route('IncreaseMyBusiness.my-companies.index') }}"><i class="nav-icon fa fa-building-o"></i> <span>Le mie aziende</span></a></li>-->
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('productimportfile') }}'><i class='nav-icon fa fa-file-excel-o'></i> Importa Catalogo</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('MyAtlantinex.vouchers.index') }}"><i class="nav-icon fa fa-user"></i> <span>I miei voucher</span></a></li>
    <li class='nav-item'><a class='nav-link' href='{{ route('MyAtlantinex.gdovouchersshop.create') }}'><i class='nav-icon fa fa-money'></i> Acquista i voucher della GDO</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ route('MyAtlantinex.gdovouchersshop.index') }}'><i class='nav-icon fa fa-ticket'></i> I miei voucher della GDO</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('MyAtlantinex.epPlusOperationsList') }}"><i class='nav-icon fas fa-file-invoice-dollar'></i> EP+</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('MyAtlantinex.documents.index') }}"><i class="nav-icon fa fa-file"></i> <span>Documenti</span></a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('download') }}'><i class='nav-icon fa fa-download'></i> Downloads</a></li>
    {{--  <li class="nav-item"><a class="nav-link" tabindex="0" data-toggle="tooltip" title="La sezione sarà disponibile a seguito del rilascio del portale al pubblico." href="javascript:"><i class="nav-icon fa fa-dollar"></i> <span>I miei pagamenti</span></a></li> --}}
    <li class='nav-item'><a class='nav-link' href='{{ route('MyAtlantinex.packs.index') }}'><i class='nav-icon fa fa-money'></i> Acquista un pack</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('offlisting') }}'><i class='nav-icon fas fa-laptop-house'></i> Categorie Offline</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ route('Company.pos.index') }}'><i class='nav-icon fa fa-home'></i> Punto Vendita</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('lead') }}'><i class='nav-icon fa fa-dollar'></i> Vendite Offline</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('client') }}'><i class='nav-icon fa fa-user'></i> Clienti</a></li>
    <li class='nav-item'><a tabindex="0" data-toggle="tooltip" title="La sezione si attiverà a partire dal 30/04/2021" class='nav-link' href='javascript:'><i class='nav-icon fa fa-edit'></i> Fatture</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ Route('Company.qrFrame') }}'><i class='nav-icon fa fa-edit'></i> QR-Customer</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('IncreaseMyBusiness.invitation-links.index') }}"><i class="nav-icon fa fa-user"></i> <span>Invita</span></a></li>
    <li class='nav-item'><a class='nav-link' href='{{ route('IncreaseMyBusiness.my-networkers.index') }}'><i class='nav-icon fa fa-user'></i> I miei consumatori</a></li>
    {{--<li class='nav-item'><a class='nav-link' href='{{ Route('Company.adv-packs.index') }}'><i class="nav-icon fas fa-chart-line"></i> Adv Packs</a></li>  --}}
@endrole


@role('Customer')

    <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon fa fa-group"></i> MyAtlantidex</a>
        <ul class="nav-dropdown-items">
            <li class="nav-item"><a class="nav-link" href="{{ route('MyAtlantinex.personal-data.index') }}"><i class="nav-icon fa fa-user"></i> <span>Dati personali</span></a></li>
            <li class="nav-item"><a class="nav-link" href="{{ route('MyAtlantinex.payment-data.index') }}"><i class="nav-icon fa fa-user"></i> <span>Dati di pagamento</span></a></li>

            <li class="nav-item"><a class="nav-link" href="{{ route('MyAtlantinex.vouchers.index') }}"><i class="nav-icon fa fa-user"></i> <span>I miei voucher</span></a></li>
            <li class='nav-item'><a class='nav-link' href='{{ route('MyAtlantinex.gdovouchersshop.create') }}'><i class='nav-icon fa fa-money'></i> Acquista i voucher della GDO</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ route('MyAtlantinex.gdovouchersshop.index') }}'><i class='nav-icon fa fa-ticket'></i> I miei voucher della GDO</a></li>
        </ul>
    </li>
    <li class="nav-item"><a class="nav-link" href="{{ route('MyAtlantinex.documents.index') }}"><i class="nav-icon fa fa-file"></i> <span>Documenti</span></a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('download') }}'><i class='nav-icon fa fa-download'></i> Downloads</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('IncreaseMyBusiness.invitation-links.index') }}"><i class="nav-icon fa fa-user"></i> <span>Invita</span></a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('MyAtlantinex.becomeMarketerConfirmation') }}"><i class="nav-icon fas fa-search-dollar"></i> <span>Diventa Marketer</span></a></li>

    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('lead') }}'><i class='nav-icon fa fa-dollar'></i> Acquisti Offline</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('MyAtlantinex.epPlusOperationsList') }}"><i class='nav-icon fas fa-file-invoice-dollar'></i> EP+</a></li>

@endrole

@role('Networker')
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon fa fa-group"></i> MyAtlantinex</a>
    <ul class="nav-dropdown-items">
        <li class="nav-item"><a class="nav-link" href="{{ route('MyAtlantinex.personal-data.index') }}"><i class="nav-icon fa fa-user"></i> <span>Dati personali</span></a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('download') }}'><i class='nav-icon fa fa-download'></i> Downloads</a></li>
        <li class="nav-item"><a class="nav-link" href="{{ route('MyAtlantinex.payment-data.index') }}"><i class="nav-icon fa fa-user"></i> <span>Dati di pagamento</span></a></li>

        <li class="nav-item"><a class="nav-link" href="{{ route('MyAtlantinex.documents.index') }}"><i class="nav-icon fa fa-file"></i> <span>Documenti</span></a></li>

        {{-- <li class="nav-item"><a class="nav-link" href="{{ route('MyAtlantinex.changeSponsor') }}"><i class="nav-icon fa fa-user"></i> <span>Cambio sponsor</span></a></li>  --}}

        <li class="nav-item"><a class="nav-link" href="{{ route('MyAtlantinex.vouchers.index') }}"><i class="nav-icon fa fa-user"></i> <span>I miei voucher</span></a></li>
        <li class="nav-item nav-dropdown">
            <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon fa fa-group"></i> Eventi</a>
            <ul class="nav-dropdown-items">
                <li class='nav-item'><a class='nav-link' href='{{ route('MyAtlantinex.eventsshop.create') }}'><i class='nav-icon fa fa-money'></i> Shop degli eventi</a></li>
                <li class='nav-item'><a class='nav-link' href='{{ backpack_url('event') }}'><i class='nav-icon fa fa-ticket'></i> I miei eventi</a></li>
                <li class='nav-item'><a class='nav-link' href='{{ route('MyAtlantinex.eventsshop.index') }}'><i class='nav-icon fa fa-ticket'></i> I miei biglietti</a></li>
            </ul>
        </li>

        <li class="nav-item nav-dropdown">
            <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon fa fa-group"></i> GDO</a>
            <ul class="nav-dropdown-items">
                <li class='nav-item'><a class='nav-link' href='{{ route('MyAtlantinex.gdovouchersshop.create') }}'><i class='nav-icon fa fa-money'></i> Acquista i voucher della GDO</a></li>
                <li class='nav-item'><a class='nav-link' href='{{ route('MyAtlantinex.gdovouchersshop.index') }}'><i class='nav-icon fa fa-ticket'></i> I miei voucher della GDO</a></li>
            </ul>
        </li>
    </ul>
</li>


@if(backpack_user()->isActive())
    <li class='nav-item'><a class='nav-link' href='{{ route('MyAtlantinex.packs.index') }}'><i class='nav-icon fa fa-money'></i> Acquista un pack</a></li>
@endif
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon fa fa-group"></i> MyBusiness</a>
    <ul class="nav-dropdown-items">
        <li class="nav-item"><a class="nav-link" href="{{ route('MyBusiness.directCommission') }}"><i class="nav-icon fa fa-money"></i> <span>Direct Commission</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ route('MyBusiness.monthlyCareer') }}"><i class="nav-icon fa fa-user"></i> <span>Monthly Career</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ route('MyBusiness.longRunCareer') }}"><i class="nav-icon fa fa-user"></i> <span>Long Run Career</span></a></li>
         <li class="nav-item"><a class="nav-link" href="{{ route('MyBusiness.myBenefits') }}"><i class="nav-icon fa fa-dollar-sign"></i> <span>MyBenefits</span></a></li>
        <!--<li class="nav-item"><a class="nav-link" href="{{ route('MyBusiness.networkPurchases') }}"><i class="nav-icon fa fa-user"></i> <span>Acquisti del mio Network</span></a></li>-->
    </ul>
</li>

{{-- <li class='nav-item'><a class='nav-link' href='javascript:'><i class='nav-icon fa fa-flag'></i> MerchantBusiness</a></li>  --}}


@if(Auth::user()->isActive())
    @if(Auth::user()->isMerchantMarketer())
        <li class="nav-item nav-dropdown">
            <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon fa fa-group"></i> MerchantBusiness</a>
            <ul class="nav-dropdown-items">

                <li class="nav-item"><a class="nav-link" href="{{ route('MerchantBusiness.affiliatedCompanies') }}"><i class="nav-icon fa fa-user"></i> <span>Aziende affiliate</span></a></li>

                <li class="nav-item"><a class="nav-link" href="{{ route('MerchantBusiness.revenueBonus') }}"><i class="nav-icon fa fa-user"></i> <span>Revenue bonus</span></a></li>
                <li class="nav-item"><a class="nav-link" href="{{ route('MerchantBusiness.merchantBonus') }}"><i class="nav-icon fa fa-user"></i> <span>Merchant bonus</span></a></li>
                <li class="nav-item"><a class="nav-link" href="{{ route('MerchantBusiness.merchantBoosterBonus') }}"><i class="nav-icon fa fa-user"></i> <span>Merchant booster bonus</span></a></li>
                <li class="nav-item"><a class="nav-link" href="{{ route('MerchantBusiness.merchantMarketingBonus') }}"><i class="nav-icon fa fa-user"></i> <span>Merchant marketing bonus</span></a></li>

            </ul>
        </li>
    @else
        <li class='nav-item'><a class='nav-link' href='{{ Route('MyAtlantinex.packs.index') }}'><i class='nav-icon fa fa-flag'></i> MerchantBusiness</a></li>
    @endif
@endif


<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon fa fa-group"></i> Increase MyBusiness</a>
    <ul class="nav-dropdown-items">
        <li class="nav-item"><a class="nav-link" href="{{ route('IncreaseMyBusiness.invitation-links.index') }}"><i class="nav-icon fa fa-user"></i> <span>Invita</span></a></li>
        {{--
        <li class="nav-item"><a class="nav-link" href="{{ route('IncreaseMyBusiness.my-companies.index') }}"><i class="nav-icon fa fa-building-o"></i> <span>Le mie aziende</span></a></li>
        --}}

        <li class="nav-item"><a class="nav-link" href="{{ route('IncreaseMyBusiness.my-networkers.index') }}"><i class="nav-icon fa fa-user"></i> <span>I miei consumatori</span></a></li>
        <li class="nav-item"><a class="nav-link" href="/increase-my-business/{{ backpack_user()->id }}/my-team"><i class="nav-icon fa fa-group"></i> <span>Il mio Network</span></a></li>
    </ul>
</li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('lead') }}'><i class='nav-icon fa fa-dollar'></i> Acquisti Offline</a></li>
<li class="nav-item"><a class="nav-link" href="{{ route('MyAtlantinex.epPlusOperationsList') }}"><i class='nav-icon fas fa-file-invoice-dollar'></i> EP+</a></li>
@endrole

@role('Superuser')
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('advpack') }}'><i class="fas fa-chart-line"></i> Adv Packs</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('country') }}'><i class='nav-icon fa fa-flag'></i> Countries</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('currency') }}'><i class='nav-icon fa fa-money'></i> Currencies</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('address') }}'><i class='nav-icon fa fa-question'></i> Addresses</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('attribute') }}'><i class='nav-icon fa fa-question'></i> Attributes</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('attributevalue') }}'><i class='nav-icon fa fa-question'></i> Attributevalues</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('changesponsorrequest') }}'><i class='nav-icon fa fa-question'></i> Changesponsorrequests</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('setting') }}'><i class='nav-icon la la-cog'></i> <span>Settings</span></a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('invitationlink') }}'><i class='nav-icon fa fa-question'></i> Invitationlinks</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('document') }}'><i class='nav-icon fa fa-question'></i> Documents</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('walletoperation') }}'><i class='nav-icon fa fa-question'></i> Walletoperations</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('manufacturer') }}'><i class='nav-icon fa fa-question'></i> Manufacturers</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('review') }}'><i class='nav-icon fa fa-star'></i> Reviews</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('bid') }}'><i class='nav-icon fa fa-question'></i> Bids</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('documentcode') }}'><i class='nav-icon fa fa-question'></i> Documentcodes</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('catalogue') }}'><i class='nav-icon fa fa-book'></i> Catalogo Atlantidex</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('product_status') }}'><i class='nav-icon fa fa-bookmark'></i> Product statuses</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('product_type') }}'><i class='nav-icon fa fa-info'></i> Product types</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('category') }}'><i class='nav-icon fa fa-folder-open'></i> Categories</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('event') }}'><i class='nav-icon fa fa-question'></i> Events</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('ticket') }}'><i class='nav-icon fa fa-question'></i> Tickets</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('documenttype') }}'><i class='nav-icon fa fa-question'></i> Documenttypes</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('pack') }}'><i class='nav-icon fa fa-question'></i> Packs</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('otp') }}'><i class='nav-icon fa fa-question'></i> Otps</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('gdovoucher') }}'><i class='nav-icon fa fa-question'></i> Voucher della GDO</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('gdovouchercode') }}'><i class='nav-icon fa fa-question'></i> Codici dei voucher della GDO</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('epoperation') }}'><i class='nav-icon fa fa-question'></i> Epoperations</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('voucherpurchase') }}'><i class='nav-icon fa fa-question'></i> Voucherpurchases</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('careeroperation') }}'><i class='nav-icon fa fa-question'></i> Careeroperations</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('commissionoperation') }}'><i class='nav-icon fa fa-question'></i> Commissionoperations</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('scan-ticket') }}'><i class='nav-icon fa fa-question'></i> Scan Ticket</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('vat') }}'><i class='nav-icon fa fa-question'></i> Vats</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ route('Company.epCategory') }}'><i class='nav-icon fa fa-question'></i> EP Group</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('download') }}'><i class='nav-icon fa fa-download'></i> Downloads</a></li>

    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('companytype') }}'><i class='nav-icon fa fa-question'></i> Companytypes</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('usercompensation') }}'><i class='nav-icon fa fa-question'></i> User Compensations</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('voucheroperation') }}'><i class='nav-icon fa fa-question'></i> Voucheroperations</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('career') }}'><i class='nav-icon fa fa-question'></i> Careers</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('bonusepspayment') }}'><i class='nav-icon fa fa-question'></i> Bonusepspayments</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ Route('temporaryBenefitsRecapIndex') }}' target="_blank"><i class='nav-icon fa fa-question'></i> Benefits recap</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('invoiceable') }}'><i class='nav-icon fa fa-question'></i> Invoiceables</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('invoicerow') }}'><i class='nav-icon fa fa-question'></i> Invoicerows</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('epvoucher') }}'><i class='nav-icon fa fa-question'></i> Epvouchers</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('longruncareerlevel') }}'><i class='nav-icon fa fa-question'></i> Longruncareerlevels</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('invoice') }}'><i class='nav-icon fa fa-question'></i> Invoices</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('suggestedcategory') }}'><i class='nav-icon fa fa-question'></i> Suggested categories</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('advpackpurchase') }}'><i class='nav-icon fa fa-question'></i> Advpackpurchases</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('packpurchase') }}'><i class='nav-icon fa fa-question'></i> Packpurchases</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('advinsertion') }}'><i class='nav-icon fa fa-question'></i> Advinsertions</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('epplusoperation') }}'><i class='nav-icon fa fa-question'></i> Epplusoperations</a></li>

    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('lead') }}'><i class='nav-icon fa fa-question'></i> Leads</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('leadrow') }}'><i class='nav-icon fa fa-question'></i> Leadrows</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('client') }}'><i class='nav-icon fa fa-question'></i> Clients</a></li>

    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('companycategory') }}'><i class='nav-icon fa fa-question'></i> Companycategories</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('payout') }}'><i class='nav-icon fa fa-question'></i> Payouts</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('lead') }}'><i class='nav-icon fa fa-dollar'></i> Vendite Offline</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('insertion') }}'><i class='nav-icon fa fa-question'></i> Insertions</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('eppluspayout') }}'><i class='nav-icon fa fa-question'></i> Eppluspayouts</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('MyAtlantinex.epPlusOperationsList') }}"><i class='nav-icon fas fa-file-invoice-dollar'></i> EP+</a></li>
@endrole




@endif


<li class='nav-item'><a class='nav-link' href='{{ Route('myCode') }}'><i class='nav-icon fas fa-qrcode'></i> MyCode</a></li>
<li class='nav-item'><a class='nav-link' href='{{ Route('quiAtlantidexV2') }}'><i class='nav-icon fas fa-home'></i> Merchants</a></li>






@role('Networker')
@if (!Auth::user()->isActive())
    <li class='nav-item'><a class='nav-link' href='{{ route('MyAtlantinex.networkerActivationProcessInterrupt') }}'><i class='nav-icon fa fa-undo'></i> Ritorna a consumatore</a></li>
@endif
@endrole

<li class="nav-item"><a class="nav-link" href="{{ route('MyAtlantinex.myWallet') }}"><img width="70%" src="/front/img/WEB-Logo-Atlantipay.png" alt=""></a></li>





<li class="d-block" style="height: 320px"></li>

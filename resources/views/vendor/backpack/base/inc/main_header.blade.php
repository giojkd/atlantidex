<header class="{{ config('backpack.base.header_class') }}">
  <!-- Logo -->

  {{--<small>
      <a style="font-size: 1rem; border:1px solid #333; border-radius:4px; padding: 2px 6px; display:inline-block" class="navbar-toggler sidebar-toggler d-lg-none mr-auto ml-3 text-dark" href="javascript:" data-toggle="sidebar-show">Menù</a>
  </small>--}}

  <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto ml-3" type="button" data-toggle="sidebar-show" aria-label="{{ trans('backpack::base.toggle_navigation')}}">
    {{-- <span class="navbar-toggler-icon"></span>  --}}
    <span><i class="fas fa-bars"></i></span>
  </button>

  <a class="navbar-brand " href="{{ url(config('backpack.base.home_link')) }}" title="{{ config('backpack.base.project_name') }}" style="opacity:1.0 !important">
    @hasanyrole('Company|Customer')
        {!! config('backpack.base.project_logo_company') !!}
    @endhasanyrole
    @role('Networker')
        {!! config('backpack.base.project_logo') !!}
    @endrole
  </a>

  <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show" aria-label="{{ trans('backpack::base.toggle_navigation')}}">
    {{-- <span class="navbar-toggler-icon"></span>  --}}
    <span><i class="fas fa-bars"></i></span>
  </button>

 {{--
    <small>
      <a style="font-size: 1rem; border:1px solid #333; border-radius:4px; padding: 2px 6px; display:inline-block; " class="navbar-toggler sidebar-toggler d-md-down-none text-dark" href="javascript:" data-toggle="sidebar-show">Menù</a>
  </small>
 --}}
  @include(backpack_view('inc.menu'))
</header>

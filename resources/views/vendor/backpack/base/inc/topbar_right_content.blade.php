<!-- This file is used to store topbar (right) items -->
<div class="d-none d-md-block">
@role('Company')
  <div class="d-flex justify-content-end"><h2>{{ Auth::user()->company_name }}</h2></div>
@endrole
</div>

{{-- <li class="nav-item d-md-down-none"><a class="nav-link" href="#"><i class="la la-bell"></i><span class="badge badge-pill badge-danger">5</span></a></li>
<li class="nav-item d-md-down-none"><a class="nav-link" href="#"><i class="la la-list"></i></a></li>
<li class="nav-item d-md-down-none"><a class="nav-link" href="#"><i class="la la-map"></i></a></li> --}}

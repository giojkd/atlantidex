@auth

@extends('admin_custom.top_left')
@section('content')
    <div class="row">
        <div class="col-md-12 text-center ">
            <div class="d-none d-md-block">
                <img class="w-100" src="{{ asset('front/img/customer-dashboard-background-desktop.png') }}" alt="">
            </div>
            <div class="d-block d-md-none">
                <img class="w-100" src="{{ asset('front/img/customer-dashboard-background-mobile.png') }}" alt="">
            </div>
        </div>
    </div>

@endsection

@endauth

.
@auth

    @extends('admin_custom.top_left')
    @section('content')
        <div class="row">
            <div class="col-md-12">
                <h2>
                    <span class="text-capitalize">La tua card Atlantidex</span>
                    <small id="datatable_info_stack" class="animated fadeIn" style="display: inline-flex;">
                        <div class="dataTables_info" id="crudTable_info" role="status" aria-live="polite">
                            Stampa la tua card Atlantidex e portala con te negli <a target="_blank" href="{{ Route('quiAtlantidex') }}">esercizi commerciali affiliati</a> ad Atlantinex per accumulare vantaggi.
                        </div>
                    </small>
                </h2>

            </div>
        </div>
        @hss('20')
        <div class="row">
            <div class="col-md-5">


                <div class="card">
                    <div class="card-body" style="border: 8px solid  #E94E14">
                        <div class="row">
                            <div class="col-md-5">
                                <img src="{{ $qrCodeSrc }}" alt="">
                            </div>
                            <div class="col-md-7">
                                <img src="/uploads/logo-dark_OLD.png" alt="">
                                @hss('10')
                                <b>Codice:</b> {{ $user->code }} <br>
                                <b>Nome:</b> {{ $user->full_name }} <br>
                                <b>Membro dal:</b> {{ $user->created_at->format('m/Y') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @hss('20')

        <div class="alert alert-warning">
            Prima di pagare verifica che i vantaggi Atlantidex riconosciuti dal Merchant siano quelli visualizzati sul portale.
        </div>

    @endsection

@endauth

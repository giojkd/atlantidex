@auth

@extends('admin_custom.top_left')
@section('content')

<div class="row">


    <div class="col-md-4">
        @if($user->packpurchases()->where('pack_id',2)->get()->count() == 0)
            @if ($user->spendings_to_unlock_merchant_marketer_pack < env('MIN_CHILDREN_PURCHASE_TO_UNLOCK_PACK'))
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Spese mancanti per sbloccare il Merchant Marketer Pack</h5>
                        <h1 class="atlantidex-blue">@fp(env('MIN_CHILDREN_PURCHASE_TO_UNLOCK_PACK')-$user->spendings_to_unlock_merchant_marketer_pack)</h1>
                    </div>
                </div>
            @endif
        @endif
        <div class="card">

            <div class="card-body">
                <h5 class="card-title">Fine del mese di produzione</h5>
                <div class="row" id="production-month-countdown"></div>

            </div>
        </div>
         <div class="card">
            <div class="card-body">
                <h5 class="card-title">Il tuo network (lifeline: {{ $downline['consumers'] + $downline['marketers'] }})</h5>
                <p class="card-text">
                    <h1>Consumer: <span class="atlantidex-blue">{{ $downline['consumers'] }}</span></h1>
                    <h1>Marketer: <span class="atlantidex-blue">{{ $downline['marketers'] }}</span></h1>
                    {{-- <h1>Merchant: <span class="atlantidex-blue">{{ $downline['merchants'] }}</span></h1>  --}}

                    {{-- <h1>Totale: <span class="atlantidex-blue">{{ $downline->sum() }}</span></h1>  --}}
                </p>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Merchant</h5>
                <p class="card-text">
                    {{-- <h1>Consumer: <span class="atlantidex-blue">{{ $downline['consumers'] }}</span></h1>
                    <h1>Marketer: <span class="atlantidex-blue">{{ $downline['marketers'] }}</span></h1> --}}
                    <h1>Merchant: <span class="atlantidex-blue">{{ $downline['merchants'] }}</span></h1>

                    {{-- <h1>Totale: <span class="atlantidex-blue">{{ $downline->sum() }}</span></h1>  --}}
                </p>
            </div>
        </div>

    </div>
    <div class="col-md-4">
         <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <img class="img-fluid" src="/front/img/level-icon.jpeg" alt="">
                    </div>
                    <div class="col-md-8">
                        <h5 class="card-title">Monthly Career Level</h5>
                        <p class="card-text">
                            <h1 class="atlantidex-blue">{{ $performances['level']['index'] }}</h1>
                        </p>
                    </div>
                </div>

            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <img class="img-fluid" src="/front/img/ep-icon.png" alt="">
                    </div>
                    <div class="col-md-8">
                        <h5 class="card-title">Monthly Career</h5>
                        <p class="card-text">
                            @php
                                $monthlyCareerEps = $performances['bonus_eps'] + $performances['personal_eps'] + collect($performances['legContribution'])->sum();
                            @endphp
                            <h1 class="atlantidex-blue">{{ number_format((float)$monthlyCareerEps,2,',','') }}</h1>
                        </p>
                    </div>
                </div>
            </div>
        </div>


            <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                         <img class="img-fluid" src="/front/img/level-icon.jpeg" alt="">
                    </div>
                    <div class="col-md-8">
                        <h5 class="card-title">Long Run Career Level</h5>
                        <p class="card-text">
                            <h1 class="atlantidex-blue">{{ $logruncareerlevel  }}</h1>
                        </p>
                    </div>
                </div>

            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <img class="img-fluid" src="/front/img/ep-icon.png" alt="">
                    </div>
                    <div class="col-md-8">
                        <h5 class="card-title">Long Run Career</h5>
                        <p class="card-text">
                             @php
                                     $longRunCareerEps = $performancesLongRun['legsContributionTotal'] + $performancesLongRun['personal_eps'] + $performancesLongRun['bonus_eps'] + $longRunCareerLastMonthEps;
                             @endphp
                            <h1 class="atlantidex-blue">{{ number_format((float)$longRunCareerEps,2,',','') }}</h1>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
         <div class="card">
             <div class="card-body">
                <h5 class="card-title">I tuoi dati</h5>
                    <div class="row py-2">
                        <div class="col-md-4"><b>Tessera numero</b></div>
                        <div class="col-md-8">{{ $user->code }}</div>
                    </div>
                    <div class="row py-2 bg-light">
                        <div class="col-md-4 text-nowrap"><b>Nome </b></div>
                        <div class="col-md-8">{{ $user->full_name }}</div>
                    </div>
                    <div class="row py-2">
                        <div class="col-md-4 text-nowrap"><b>Email</b></div>
                        <div class="col-md-8">{{ $user->email }}</div>
                    </div>
                    <div class="row  py-2 bg-light">
                        <div class="col-md-4 text-nowrap"><b>Telefono</b></div>
                        <div class="col-md-8">+39{{ $user->phone }}</td></div>
                    </div>
            </div>

         </div>

        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Modalità di remunerazione</h5>
                <form action="{{ Url('admin/user/set-column-boolean') }}" class="ajax-form" method="POST">
                    @csrf
                    @method('POST')
                    <input type="hidden" name="column" value="split_eps">
                    <label>
                        <input class="submit-on-change" @if (!$user->split_eps) checked @endif type="radio" name="value" value="0"> Tutti EP
                    </label>
                    <br>
                    <label>
                        <input class="submit-on-change" @if ($user->split_eps) checked @endif type="radio" name="value" value="1"> 50% EP + 50% EP+
                    </label>
                </form>
            </div>
        </div>
    </div>

</div>

@endsection

@push('after_scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.countdown/2.2.0/jquery.countdown.min.js"></script>
    <script>

        $(function(){
            $('#production-month-countdown').countdown('{{ date($productionMonthEnd) }}', function(event) {
            var $this = $(this).html(event.strftime(''
                + '<div class="col atlantidex-blue-bg alert alert-success text-center d-inline-block mr-2"><h2 class="mb-0">%D</h2><small>giorni</small></div>'
                + '<div class="col atlantidex-blue-bg alert alert-success text-center d-inline-block mr-2"><h2 class="mb-0">%H</h2><small>ore</small></div>'
                + '<div class="col atlantidex-blue-bg alert alert-success text-center d-inline-block mr-2"><h2 class="mb-0">%M</h2><small>min</small></div>'
                + '<div class="col atlantidex-blue-bg alert alert-success text-center d-inline-block mr-2"><h2 class="mb-0">%S</h2><small>sec</small></div>'
                ));
            });
        })
    </script>
    <style>
        .atlantidex-blue{
            color: #009BD8!important;
        }
        .atlantidex-blue-bg{
            background-color: #009BD8!important;
        }
    </style>
@endpush

@endauth


@auth

@extends('admin_custom.top_left')
@section('content')
    <h2>Riepilogo compensi</h2>
    <table class="table table-hover table-striped">
        @foreach ($months as $monthId => $month)
                <tr>
                    <td>{{ $month[3] }}</td>
                    <td>
                        <a target="_blank" href="{{ Route('temporaryBenefitsRecap') }}?month_id={{ $monthId }}">Guarda i compensi di questo mese</a>
                    </td>
                </tr>
        @endforeach
    </table>
@endsection

@endauth

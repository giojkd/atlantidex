<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <title>Temporary recap model</title>
  </head>
  <body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h1>Temporary recap model</h1>
                @foreach ($compensations as $compensation)
                <div class="card mb-2">
                    <div class="card-header">
                       ({{ $compensation->user->id }}) {{ $compensation->user->super_detailed_name}} MCL {{ $compensation->user->monthly_career_level }} LRCL {{ $compensation->user->long_run_career_level }}
                       <br>
                       @if(!is_null($compensation->user->address))
                        Address: {{ $compensation->user->address['value'] }} <br>
                        Fiscal code: {{ $compensation->user->fiscal_code }} <br>
                        Vat number: {{ $compensation->user->vat_number }} <br>
                        Bank account: {{ json_encode($compensation->user->bank_account) }}
                       @endif

                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table-hover table-striped">
                                <thead>
                                    <tr>

                                            @foreach (collect($compensation)->except(['id','created_at','updated_at','deleted_at','user_id','month_id','user','compensation_details'])->toArray() as $key => $value)
                                                @if (!in_array($key,['compensation_total']))
                                                    <th>
                                                        {{ Str::ucfirst(str_replace('_',' ',$key)) }}
                                                    </th>
                                                @endif
                                            @endforeach

                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        @foreach (collect($compensation)->except(['id','created_at','updated_at','deleted_at','user_id','month_id','user','compensation_details'])->toArray() as $key => $value)
                                            @if (!in_array($key,['compensation_total']))
                                                <td>
                                                    {{ number_format($value,2) }}
                                                </td>
                                            @endif
                                        @endforeach
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <h3 class="mt-2">Monthly Career Commission</h3>
                        <table class="table table-hover table-striped">
                            @foreach ($compensation->compensation_details->monthlyCareerCommissionDetailed as $row)
                                <tr>
                                    {{-- <td>{{ $row->line }}</td>
                                    <td>@fp($row->increment)</td>  --}}
                                    <td>{{ $row }}</td>

                                </tr>
                            @endforeach
                        </table>
                        <h3 class="mt-2">Direct Commission</h3>
                        <table class="table table-hover table-striped">
                            @foreach ($compensation->compensation_details->directCommissionOperations as $row)
                                <tr>
                                    <td>{{ $row->description }}</td>
                                    <td>@fp($row->value)</td>
                                </tr>
                            @endforeach
                        </table>
                        <h3 class="mt-2">Merchant Bonus</h3>
                        <table class="table table-hover table-striped">
                            @foreach ($compensation->compensation_details->merchantBonusOperations as $row)
                                <tr>
                                    <td>{{ $row->description }}</td>
                                    <td>@fp($row->value)</td>
                                </tr>
                            @endforeach
                        </table>
                         <h3 class="mt-2">Merchant Booster Bonus</h3>
                        <table class="table table-hover table-striped">
                            @foreach ($compensation->compensation_details->merchantBoosterBonusOperations as $row)
                                <tr>
                                    <td>{{ $row->description }}</td>
                                    <td>@fp($row->value)</td>
                                </tr>
                            @endforeach
                        </table>
                        <h3 class="mt-2">Revenue Bonus</h3>
                         <table class="table table-hover table-striped">
                            @foreach ($compensation->compensation_details->revenueBonusOperations as $row)
                                <tr>
                                    <td>{{ $row->description }}</td>
                                    <td>@fp($row->value)</td>
                                </tr>
                            @endforeach
                        </table>
                        <h3 class="mt-2">Marketing Bonus</h3>
                    </div>
                </div>
                <div style="page-break-after: always;"></div>
                @endforeach

            </div>
        </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    -->
  </body>
</html>

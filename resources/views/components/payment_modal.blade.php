
<div class="modal fade" id="voucherPurchaseModal" tabindex="-1" role="dialog" aria-labelledby="voucherPurchaseModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">{!! $payment_modal_title !!}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">


            <input type="hidden" class="eps" value="">
            @if(!isset($show_vouchers_amount) || $show_vouchers_amount)
            <div class="form-group">
                <label for="exampleInputEmail1">Quanto vuoi vincolare su questa categoria di voucher?</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">€</span>
                    </div>
                    <input id="amount" type="email" class="form-control" placeholder="0.00" required>
                </div>
                <small id="emailHelp" class="form-text text-muted">Imposta l'ammontare da vincolare.</small>
            </div>
                @endif
                @if(!isset($card_is_disabled) || !$card_is_disabled)
                    @if($payment_methods->count() > 0)
                        @foreach($payment_methods as $payment_method)
                        <div class="card mb-3">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        Carta di credito: <b>{{ Str::ucfirst($payment_method->card->brand) }} ({{ $payment_method->card->last4 }})</b>
                                        Scad: <b>{{ $payment_method->card->exp_month }}/{{ $payment_method->card->exp_year }}</b>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <button data-id="{{ $payment_method->id }}" class="btn btn-primary btn-sm stored-credit-card">Usa questa carta</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    @else
                        <div class="alert alert-primary">
                            Non hai ancora nessun metodo di pagamento impostato.
                        </div>
                    @endif

                 <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="card-holder-name">Usa una nuova carta</label>
                                <input type="text" id="card-holder-name" placeholder="Nome del titolare della carta" class="form-control">
                            </div>
                            <div id="new-card-form-wrapper" class="stripe-new-card"></div>
                            <button data-secret="{{ $client_secret_intent->client_secret }}" id="card-button" class="btn btn-sm btn-primary btn-block mt-4">Usa questa carta</button>
                            </div>
                        </div>
                         @endif
                         @if(!isset($card_is_disabled) || !$card_is_disabled)
                            <div class="text-center mt-1 mb-3">
                                <small><span class="text-muted"><i>oppure</i></span></small>
                            </div>
                        @endif

                        <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8">
                                Il saldo del tuo wallet è : @fp($user->wallet_balance)  <br>
                                <small><a href="{{ Route('MyAtlantinex.myWallet') }}">Ricarica il tuo wallet </a></small>
                                <br>
                                <small><span class="text-muted">Puoi vincolare un massimo di @fp($user->wallet_balance)</span></small>
                            </div>
                            <div class="col-md-4 text-right">
                                <button class="btn btn-primary btn-sm" id="useWallet">Usa il wallet</button>
                            </div>
                        </div>
                    </div>
                </div>
      </div>
      <!--
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
        <button type="button" class="btn btn-success btn-block">Conferma</button>
      </div>-->
    </div>
  </div>
</div>

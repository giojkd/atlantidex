@extends('main_front')
@section('content')
<div class="container">
    <div class="row">
        <div class="col">
                 <h1>Cookie Policy</h1>
                 @hss('50')
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a href="#cookie-policy-atlantidex" class="nav-link active" data-toggle="tab"><h1>Atlantidex</h1></a>
                </li>
                <li class="nav-item">
                    <a href="#cookie-policy-atlantinex" class="nav-link" data-toggle="tab"><h1>Atlantinex</h1></a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade show active" id="cookie-policy-atlantidex">

@hss('50')




<p>(Informativa conforme al Provvedimento Garante per la Protezione dei dati personali 8.5.2014 n. 229)</p>

<p>&nbsp;</p>

<p>Si, informa che questo sito internet fa uso di cookie al fine di rendere i propri servizi il pi&ugrave; possibile efficienti e semplici da utilizzare. Questo sito utilizza cookie tecnici, che possono essere impiegati senza chiedere il consenso dell&rsquo;utente poich&eacute; strettamente necessari alla fornitura del servizio, e cookie analitici di terze parti, forniti dal servizio Google Analytics.</p>

<p>Utilizzando questo sito, l&rsquo;utente dichiara di accettare e acconsentire all&rsquo;utilizzo dei cookie in conformit&agrave; con i termini di uso dei cookie espressi in questo documento.</p>

<p>&nbsp;</p>

<p>Cosa sono i cookie?</p>

<p>I cookie sono stringhe di testo di piccole dimensioni che il sito internet visitato dall&rsquo;utente invia al suo browser o dispositivo mobile. I cookie sono memorizzati da questi ultimi, e vengono ritrasmessi al sito alla successiva visita dell&rsquo;utente. Navigando nel sito, l&rsquo;utente pu&ograve; ricevere anche cookie inviati da siti o da web server diversi (c.d. &ldquo;terze parti&rdquo;), sui quali possono risiedere alcuni elementi (quali, ad esempio, immagini, mappe, suoni, specifici link a pagine di altri domini) presenti sul sito che lo stesso sta visitando.</p>

<p>&nbsp;</p>

<p>Tipologie di cookie</p>

<p>I cookie si possono dividere nelle seguenti categorie:</p>

<p>&nbsp;</p>

<p>Cookie tecnici</p>

<p>I cookie tecnici sono quelli utilizzati al solo scopo di consentire la corretta navigazione e utilizzo del sito. In assenza di tali cookie il sito o alcune porzioni di esso potrebbero non funzionare correttamente. Pertanto sono sempre utilizzati, indipendentemente dalle preferenze dall&rsquo;utente. I cookie di questa categoria comprendono sia i cookie persistenti sia i cookie di sessione.</p>

<p>I cookie tecnici si possono suddividere in:</p>

<p>&ndash; Cookie di navigazione e sessione</p>

<p>&ndash; Cookie di funzionalit&agrave;</p>

<p>&ndash; Cookie analitici</p>

<p>I cookie di navigazione e sessione garantiscono la corretta navigazione e fruizione del sito. Ad esempio, consentono l&rsquo;autenticazione e l&rsquo;accesso alle aree riservate.</p>

<p>I cookie di funzionalit&agrave; consentono all&rsquo;utente di navigare in funzione dei criteri che ha selezionato, ad esempio la lingua, al fine di migliorarne il servizio.</p>

<p>I cookie analitici (o &ldquo;cookie analytics&rdquo;) sono utilizzati per raccogliere informazioni in forma aggregata e anonima sull&rsquo;utilizzo del sito e l&rsquo;attivit&agrave; degli utenti, ad esempio il numero degli utenti collegati e le pagine visitate.</p>

<p>Il Titolare usa tali informazioni per analisi statistiche, per migliorare il sito e semplificarne l&rsquo;utilizzo, e per monitorarne il corretto funzionamento.</p>

<p>&nbsp;</p>

<p>Cookie di profilazione</p>

<p>I cookie di profilazione consentono di raccogliere informazioni sull&rsquo;utilizzo del sito da parte del singolo utente, consentendo di creare dei profili specifici per utente. Sono utilizzati frequentemente per scopi promozionali e per messaggi pubblicitari.</p>

<p>Data la particolare invasivit&agrave; che questi possono avere nell&rsquo;ambito della sfera privata degli utenti, &egrave; richiesto che l&rsquo;utente sia adeguatamente informato sull&rsquo;uso degli stessi e possa esprimere il proprio consenso o rifiuto all&rsquo;utilizzo. Il presente sito non fa uso di cookie di profilazione.</p>

<p>&nbsp;</p>

<p>Google Analytics&nbsp;</p>

<p>Il sito utilizza il servizio Google Analytics per analizzare l&rsquo;utilizzo del sito da parte degli utenti. Google Analytics &egrave; un servizio di analisi web fornito da Google, Inc. (&ldquo;Google&rdquo;). Google Analytics utilizza dei cookie proprietari per consentire al sito web di analizzare come gli utenti utilizzano il sito. Le informazioni generate dal cookie sull&rsquo;utilizzo del sito web da parte Vostra (compreso il Vostro indirizzo IP) verranno trasmesse e depositate presso i server di Google negli Stati Uniti. Google (autonomo titolare del trattamento) utilizzer&agrave; queste informazioni allo scopo di tracciare ed esaminare il Vostro utilizzo del sito web, compilare report sulle attivit&agrave; del sito web per gli operatori del sito web e fornire altri servizi relativi alle attivit&agrave; del sito web e all&rsquo;utilizzo di Internet. Google pu&ograve; anche trasferire queste informazioni a terzi ove ci&ograve; sia imposto dalla legge o laddove tali terzi trattino le suddette informazioni per conto di Google. Google non assocer&agrave; il vostro indirizzo IP a nessun altro dato posseduto da Google. Potete rifiutarvi di usare i cookie selezionando l&rsquo;impostazione appropriata sul vostro browser, ma ci&ograve; potrebbe impedirvi di utilizzare tutte le funzionalit&agrave; di questo sito web. Per ulteriori informazioni consultare l&rsquo;informativa sulla privacy di Google disponibile all&rsquo;indirizzo: http://www.google.com/privacypolicy.html.</p>

<p>Controllare l&rsquo;installazione di Cookie</p>

<p>L&rsquo;Utente pu&ograve; gestire le preferenze relative ai Cookie direttamente all&rsquo;interno del proprio browser ed impedire &ndash; ad esempio &ndash; che terze parti possano installarne. Tramite le preferenze del browser &egrave; inoltre possibile eliminare i Cookie installati in passato, incluso il Cookie in cui venga eventualmente salvato il consenso all&rsquo;installazione di Cookie da parte di questo sito. &Egrave; importante notare che disabilitando tutti i Cookie, il funzionamento di questo sito potrebbe essere compromesso.</p>

<p>L&rsquo;Utente pu&ograve; trovare informazioni su come gestire i Cookie nel suo browser ai seguenti indirizzi:&nbsp;</p>

<ul>
	<li>Google Chrome (<a href="https://support.google.com/chrome/answer/95647?hl=it&amp;p=cpn_cookies">https://support.google.com/chrome/answer/95647?hl=it&amp;p=cpn_cookies</a>)</li>
	<li>Mozilla Firefox (<a href="https://support.mozilla.org/it/kb/Attivare%2520e%2520disattivare%2520i%2520cookie">https://support.mozilla.org/it/kb/Attivare%20e%20disattivare%20i%20cookie</a>)</li>
	<li>Apple Safari (<a href="https://support.apple.com/kb/PH19214?viewlocale=it_IT&amp;locale=en_US">https://support.apple.com/kb/PH19214?viewlocale=it_IT&amp;locale=en_US</a>)</li>
	<li>Microsoft Windows Explorer (<a href="https://support.microsoft.com/it-it/help/17442/windows-internet-explorer-delete-manage-cookies">https://support.microsoft.com/it-it/help/17442/windows-internet-explorer-delete-manage-cookies</a>).</li>
</ul>

<p>In caso di servizi erogati da terze parti, l&rsquo;Utente pu&ograve; inoltre esercitare il proprio diritto ad opporsi al tracciamento informandosi tramite la privacy policy della terza parte contattando direttamente la stessa.</p>

<p>&nbsp;</p>

<p><strong>Titolare del trattamento dei dati</strong></p>

<p>Il Titolare del trattamento dei dati &egrave; <strong>Atlantidex&nbsp; Srl</strong> (C.F., P.IVA e n. di iscrizione nel Registro delle Imprese di Massa-Carrara: 01414870459; REA: MS-139174), con Sede legale in &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; (54033) Marina di Carrara (MS) - Viale C. Colombo n.11 (PEC: atlantidex@pec.it),</p>


                </div>
                <div class="tab-pane fade" id="cookie-policy-atlantinex">
@hss('50')

<p>(Informativa conforme al Provvedimento Garante per la Protezione dei dati personali 8.5.2014 n. 229)</p>

<p>&nbsp;</p>

<p>Si, informa che questo sito internet fa uso di cookie al fine di rendere i propri servizi il pi&ugrave; possibile efficienti e semplici da utilizzare. Questo sito utilizza cookie tecnici, che possono essere impiegati senza chiedere il consenso dell&rsquo;utente poich&eacute; strettamente necessari alla fornitura del servizio, e cookie analitici di terze parti, forniti dal servizio Google Analytics.</p>

<p>Utilizzando questo sito, l&rsquo;utente dichiara di accettare e acconsentire all&rsquo;utilizzo dei cookie in conformit&agrave; con i termini di uso dei cookie espressi in questo documento.</p>

<p>&nbsp;</p>

<p>Cosa sono i cookie?</p>

<p>I cookie sono stringhe di testo di piccole dimensioni che il sito internet visitato dall&rsquo;utente invia al suo browser o dispositivo mobile. I cookie sono memorizzati da questi ultimi, e vengono ritrasmessi al sito alla successiva visita dell&rsquo;utente. Navigando nel sito, l&rsquo;utente pu&ograve; ricevere anche cookie inviati da siti o da web server diversi (c.d. &ldquo;terze parti&rdquo;), sui quali possono risiedere alcuni elementi (quali, ad esempio, immagini, mappe, suoni, specifici link a pagine di altri domini) presenti sul sito che lo stesso sta visitando.</p>

<p>&nbsp;</p>

<p>Tipologie di cookie</p>

<p>I cookie si possono dividere nelle seguenti categorie:</p>

<p>&nbsp;</p>

<p>Cookie tecnici</p>

<p>I cookie tecnici sono quelli utilizzati al solo scopo di consentire la corretta navigazione e utilizzo del sito. In assenza di tali cookie il sito o alcune porzioni di esso potrebbero non funzionare correttamente. Pertanto sono sempre utilizzati, indipendentemente dalle preferenze dall&rsquo;utente. I cookie di questa categoria comprendono sia i cookie persistenti sia i cookie di sessione.</p>

<p>I cookie tecnici si possono suddividere in:</p>

<p>&ndash; Cookie di navigazione e sessione</p>

<p>&ndash; Cookie di funzionalit&agrave;</p>

<p>&ndash; Cookie analitici</p>

<p>I cookie di navigazione e sessione garantiscono la corretta navigazione e fruizione del sito. Ad esempio, consentono l&rsquo;autenticazione e l&rsquo;accesso alle aree riservate.</p>

<p>I cookie di funzionalit&agrave; consentono all&rsquo;utente di navigare in funzione dei criteri che ha selezionato, ad esempio la lingua, al fine di migliorarne il servizio.</p>

<p>I cookie analitici (o &ldquo;cookie analytics&rdquo;) sono utilizzati per raccogliere informazioni in forma aggregata e anonima sull&rsquo;utilizzo del sito e l&rsquo;attivit&agrave; degli utenti, ad esempio il numero degli utenti collegati e le pagine visitate.</p>

<p>Il Titolare usa tali informazioni per analisi statistiche, per migliorare il sito e semplificarne l&rsquo;utilizzo, e per monitorarne il corretto funzionamento.</p>

<p>&nbsp;</p>

<p>Cookie di profilazione</p>

<p>I cookie di profilazione consentono di raccogliere informazioni sull&rsquo;utilizzo del sito da parte del singolo utente, consentendo di creare dei profili specifici per utente. Sono utilizzati frequentemente per scopi promozionali e per messaggi pubblicitari.</p>

<p>Data la particolare invasivit&agrave; che questi possono avere nell&rsquo;ambito della sfera privata degli utenti, &egrave; richiesto che l&rsquo;utente sia adeguatamente informato sull&rsquo;uso degli stessi e possa esprimere il proprio consenso o rifiuto all&rsquo;utilizzo. Il presente sito non fa uso di cookie di profilazione.</p>

<p>&nbsp;</p>

<p>Google Analytics&nbsp;</p>

<p>Il sito utilizza il servizio Google Analytics per analizzare l&rsquo;utilizzo del sito da parte degli utenti. Google Analytics &egrave; un servizio di analisi web fornito da Google, Inc. (&ldquo;Google&rdquo;). Google Analytics utilizza dei cookie proprietari per consentire al sito web di analizzare come gli utenti utilizzano il sito. Le informazioni generate dal cookie sull&rsquo;utilizzo del sito web da parte Vostra (compreso il Vostro indirizzo IP) verranno trasmesse e depositate presso i server di Google negli Stati Uniti. Google (autonomo titolare del trattamento) utilizzer&agrave; queste informazioni allo scopo di tracciare ed esaminare il Vostro utilizzo del sito web, compilare report sulle attivit&agrave; del sito web per gli operatori del sito web e fornire altri servizi relativi alle attivit&agrave; del sito web e all&rsquo;utilizzo di Internet. Google pu&ograve; anche trasferire queste informazioni a terzi ove ci&ograve; sia imposto dalla legge o laddove tali terzi trattino le suddette informazioni per conto di Google. Google non assocer&agrave; il vostro indirizzo IP a nessun altro dato posseduto da Google. Potete rifiutarvi di usare i cookie selezionando l&rsquo;impostazione appropriata sul vostro browser, ma ci&ograve; potrebbe impedirvi di utilizzare tutte le funzionalit&agrave; di questo sito web. Per ulteriori informazioni consultare l&rsquo;informativa sulla privacy di Google disponibile all&rsquo;indirizzo: http://www.google.com/privacypolicy.html.</p>

<p>Controllare l&rsquo;installazione di Cookie</p>

<p>L&rsquo;Utente pu&ograve; gestire le preferenze relative ai Cookie direttamente all&rsquo;interno del proprio browser ed impedire &ndash; ad esempio &ndash; che terze parti possano installarne. Tramite le preferenze del browser &egrave; inoltre possibile eliminare i Cookie installati in passato, incluso il Cookie in cui venga eventualmente salvato il consenso all&rsquo;installazione di Cookie da parte di questo sito. &Egrave; importante notare che disabilitando tutti i Cookie, il funzionamento di questo sito potrebbe essere compromesso.</p>

<p>L&rsquo;Utente pu&ograve; trovare informazioni su come gestire i Cookie nel suo browser ai seguenti indirizzi:&nbsp;</p>

<ul>
	<li>Google Chrome (<a href="https://support.google.com/chrome/answer/95647?hl=it&amp;p=cpn_cookies">https://support.google.com/chrome/answer/95647?hl=it&amp;p=cpn_cookies</a>)</li>
	<li>Mozilla Firefox (<a href="https://support.mozilla.org/it/kb/Attivare%2520e%2520disattivare%2520i%2520cookie">https://support.mozilla.org/it/kb/Attivare%20e%20disattivare%20i%20cookie</a>)</li>
	<li>Apple Safari (<a href="https://support.apple.com/kb/PH19214?viewlocale=it_IT&amp;locale=en_US">https://support.apple.com/kb/PH19214?viewlocale=it_IT&amp;locale=en_US</a>)</li>
	<li>Microsoft Windows Explorer (<a href="https://support.microsoft.com/it-it/help/17442/windows-internet-explorer-delete-manage-cookies">https://support.microsoft.com/it-it/help/17442/windows-internet-explorer-delete-manage-cookies</a>).</li>
</ul>

<p>In caso di servizi erogati da terze parti, l&rsquo;Utente pu&ograve; inoltre esercitare il proprio diritto ad opporsi al tracciamento informandosi tramite la privacy policy della terza parte contattando direttamente la stessa.</p>

<p>&nbsp;</p>

<p><strong>Titolare del trattamento dei dati</strong></p>

<p>Il Titolare del trattamento dei dati &egrave; <strong>Atlantinex Srl</strong> (C.F., P.IVA e n. di iscrizione nel Registro delle Imprese di Massa-Carrara: 01415870458), con Sede legale a Marina di Carrara (MS), V.le Colombo n.11 (Capitale sociale: 10.000,00 Euro;PEC: atlantinex@pec.it; sito Web: www.atlantinex.com),</p>

                </div>
            </div>

        </div>
    </div>
</div>


@endsection

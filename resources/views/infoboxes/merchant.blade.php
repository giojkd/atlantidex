<small class="text-muted"><i class="fas fa-info-circle"></i></small>

    Se hai più di un punto vendita puoi aggiungerlo cliccando il tasto
    "aggiungi punto vendita". Al momento del caricamento di un prodotto
    o servizio potrai selezionare il punto vendita in cui gli stessi verranno venduti o erogati.


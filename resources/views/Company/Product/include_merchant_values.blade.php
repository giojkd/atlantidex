<ul class="nav nav-tabs" id="myTab" role="tablist">
    @foreach ($merchants as $key => $item)
        <li class="nav-item">
        <a class="nav-link" id="home-tab" data-toggle="tab" href="#tab_{{$key}}" role="tab" aria-controls="home" aria-selected="true"><h5>{{$item}}</h5></a>
        </li>
    @endforeach
</ul>
<div class="tab-content card" id="myTabContent">
    @foreach ($merchants as $key => $item)
    <div class="tab-pane fade show" id="tab_{{$key}}" role="tabpanel" aria-labelledby="home-tab">
        <div class="card">
            <div class="card-body">

                <div class="row">
                    <div class="col-md-12"><h4>Stato <span class="text-warning font-bigger">*</span></h4></div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="hidden" name="merchants[{{$key}}][merchant_id]" value={{$key}}>

                            @isset($product)

                                <div class="from-check form-check-inline">
                                    <label for="active_t_{{$key}}">Visibile
                                    {{ Form::radio('merchants['.$key.'][active]', '1' , $product->getMerchantValues($key)['active'], ['id' => 'active_t_'.$key, 'class' =>  'form-check-input']) }}</label>
                                    <i data-toggle="popover" data-content="{{trans('help.visible')}}" data-original-title="{{trans('help.visible-title')}}" class="fa fa-info info-icon"></i></label>
                                </div>
                                <div class="from-check form-check-inline">
                                    <label for="active_f_{{$key}}">Non visibile
                                    {{ Form::radio('merchants['.$key.'][active]', '0' , !$product->getMerchantValues($key)['active'], ['id' => 'active_f_'.$key, 'class' =>  'form-check-input']) }}</label>
                                    <i data-toggle="popover" data-content="{{trans('help.non-visible')}}" data-original-title="{{trans('help.non-visible-title')}}" class="fa fa-info info-icon"></i></label>
                                </div>

                            @else

                                <div class="from-check form-check-inline">
                                    <label for="active_t_{{$key}}">Visibile
                                    {{ Form::radio('merchants['.$key.'][active]', '1' , false, ['id' => 'active_t_'.$key, 'class' =>  'form-check-input']) }}</label>
                                    <i data-toggle="popover" data-content="{{trans('help.visible')}}" data-original-title="{{trans('help.visible-title')}}" class="fa fa-info info-icon"></i></label>
                                </div>
                                <div class="from-check form-check-inline">
                                    <label for="active_f_{{$key}}">Non visibile
                                    {{ Form::radio('merchants['.$key.'][active]', '0' , true, ['id' => 'active_f_'.$key, 'class' =>  'form-check-input']) }}</label>
                                    <i data-toggle="popover" data-content="{{trans('help.non-visible')}}" data-original-title="{{trans('help.non-visible-title')}}" class="fa fa-info info-icon"></i></label>
                                </div>

                            @endisset

                        </div>
                    </div>
                    <div class="col-md-6 only-product">
                        <div class="form-group">
                            @isset($product)
                                <div class="from-check form-check-inline">
                                    <label for="is_used_f_{{$key}}">Il prodotto è nuovo
                                    {{ Form::radio('merchants['.$key.'][is_used]', '0' , !$product->getMerchantValues($key)['is_used'], ['id' => 'is_used_f_'.$key, 'class' =>  'form-check-input']) }}</label>
                                </div>
                                <div class="from-check form-check-inline">
                                    <label for="is_used_t_{{$key}}">Il prodotto è usato
                                    {{ Form::radio('merchants['.$key.'][is_used]', '1' , $product->getMerchantValues($key)['is_used'], ['id' => 'is_used_t_'.$key, 'class' =>  'form-check-input']) }}</label>
                                </div>
                            @else
                                <div class="from-check form-check-inline">
                                    <label for="is_used_f_{{$key}}">Il prodotto è nuovo
                                    {{ Form::radio('merchants['.$key.'][is_used]', '0' , true, ['id' => 'is_used_f_'.$key, 'class' =>  'form-check-input']) }}</label>
                                </div>
                                <div class="from-check form-check-inline">
                                    <label for="is_used_t_{{$key}}">Il prodotto è usato
                                    {{ Form::radio('merchants['.$key.'][is_used]', '1' , false, ['id' => 'is_used_t_'.$key, 'class' =>  'form-check-input']) }}</label>
                                </div>
                            @endisset
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row only-product">
                    <div class="col-md-12"><h4>Inventario</h4></div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="quantity_{{$key}}">Quantità disponibile <span class="text-error font-bigger">*</span></label>
                            @isset($product)
                                <input type="number" min="0" step="any" name="merchants[{{$key}}][quantity]" id="quantity_{{$key}}" class="form-control" value="{{$product->getMerchantValues($key)['quantity']}}">
                            @else
                        <input type="number" min="0" step="any" name="merchants[{{$key}}][quantity]" id="quantity_{{$key}}" class="form-control" value="{{old('merchants.'.$key.'.quantity')}}">
                            @endisset

                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="active_{{$key}}">Stato inventario <span class="text-danger font-bigger">*</span></label>
                            @isset($product)
                                {!! Form::select('merchants['.$key.'][product_status_id]', $product_statuses, $product->getMerchantValues($key)['product_status_id'], ['class' => 'form-control']) !!}
                            @else
                                {!! Form::select('merchants['.$key.'][product_status_id]', $product_statuses, null, ['class' => 'form-control']) !!}
                            @endisset
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="reassortment_days_{{$key}}">GG. per il riassortimento <i data-toggle="popover" data-content="{{trans('help.reassortment_days-text')}}" data-original-title="{{trans('help.reassortment_days-title')}}" class="fa fa-info info-icon"></i></label>
                            @isset($product)
                                <input type="number" min="0" step="any" name="merchants[{{$key}}][reassortment_days]" id="reassortment_days_{{$key}}" class="form-control" value="{{$product->getMerchantValues($key)['reassortment_days']}}">
                            @else
                                <input type="number" min="0" step="any" name="merchants[{{$key}}][reassortment_days]" id="reassortment_days_{{$key}}" class="form-control" value="{{old('merchants.'.$key.'.reassortment_days')}}">
                            @endisset

                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="shipping_days_{{$key}}">GG. per la spedizione <span class="text-error font-bigger">*</span></label>
                            @isset($product)
                                <input type="number" min="0" step="any" name="merchants[{{$key}}][shipping_days]" id="shipping_days_{{$key}}" class="form-control" value="{{$product->getMerchantValues($key)['shipping_days']}}">
                            @else
                                <input type="number" min="0" step="any" name="merchants[{{$key}}][shipping_days]" id="shipping_days_{{$key}}" class="form-control" value="{{old('merchants.'.$key.'.shipping_days')}}">
                            @endisset

                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="delivery_option_{{$key}}">Opzioni di consegna</label>
                            @isset($product)
                                {!! Form::select('merchants['.$key.'][delivery_option]', $delivery_options, $product->getMerchantValues($key)['delivery_option'], ['class' => 'form-control']) !!}
                            @else
                                {!! Form::select('merchants['.$key.'][delivery_option]', $delivery_options, null, ['class' => 'form-control']) !!}
                            @endisset

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12"><h4>Prezzi</h4></div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="price_{{$key}}">Prezzo senza IVA<span class="text-error font-bigger">*</span></label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">€</span>
                                </div>
                                @isset($product)
                                    <input type="number" min="0" step="any" name="merchants[{{$key}}][price]" id="price_{{$key}}" class="form-control eps-calc price-noiva" value="{{$product->getMerchantValues($key)['price']}}">
                                @else
                                    <input type="number" min="0" step="any" name="merchants[{{$key}}][price]" id="price_{{$key}}" class="form-control eps-calc price-noiva" value="{{old('merchants.'.$key.'.price')}}">
                                @endisset

                            </div>
                            <p class="text-muted"><small>Inserire il prezzo iva esclusa</small></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="price_compare_{{$key}}">Prezzo con IVA</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">€</span>
                                </div>
                                @isset($product)
                                    <input type="number" min="0" step="any" id="priceiva_{{$key}}" class="form-control price-iva" value="{{$product->getMerchantValues($key)['price_with_vat']}}">
                                @else
                                    <input type="number" min="0" step="any" id="priceiva_{{$key}}" class="form-control price-iva" value="">
                                @endisset
                            </div>
                            <p class="text-muted"><small>Inserire il prezzo iva inclusa</small></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="price_{{$key}}">Soglia ricezione offerte</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">€</span>
                                </div>
                                @isset($product)
                                    <input type="number" min="0" step="any" name="merchants[{{$key}}][bid_min]" id="bid_min_{{$key}}" class="form-control" value="{{$product->getMerchantValues($key)['bid_min']}}">
                                @else
                                    <input type="number" min="0" step="any" name="merchants[{{$key}}][bid_min]" id="bid_min_{{$key}}" class="form-control" value="{{old('merchants.'.$key.'.bid_min')}}">
                                @endisset

                            </div>
                            <p class="text-muted"><small>Qual è l'offerta minima che vuoi ricevere per questo prodotto o servizio</small></p>
                        </div>
                    </div>

                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <h4>Limiti geografici <i data-toggle="popover" data-content="{{trans('help.geo-info-text')}}" data-original-title="{{trans('help.geo-info-title')}}" class="fa fa-info info-icon"></i></h4>

                    </div>
                    @if (isset($product))
                            <input type="hidden" name="merchants[{{$key}}][service_delivery_center_lat]" value="{{$product->getMerchantValues($key)['service_delivery_center_lat']}}">
                            <input type="hidden" name="merchants[{{$key}}][service_delivery_center_lng]" value="{{$product->getMerchantValues($key)['service_delivery_center_lng']}}">
                            <input type="hidden" name="merchants[{{$key}}][service_delivery_center_radius]" value="{{$product->getMerchantValues($key)['service_delivery_center_radius']}}">
                            <input type="hidden" name="merchants[{{$key}}][service_delivery_address]" value="{{$product->getMerchantValues($key)['service_delivery_address']}}">
                             <div class="col-md-12">
                                <label for="service_delivery_address_input_{{$key}}">Raggio di erogazione del servizio</label>
                                <input type="text" id="service_delivery_address_input_{{$key}}" class="form-control" value="{{$product->getMerchantValues($key)['service_delivery_address_input']}}">
                                @hss('20')
                                <label for="service_delivery_address_{{$key}}">Raggio di erogazione del servizio</label>
                                <br>
                                <div class="h3 text-center" id="service_delivery_center_radius_{{ $key }}">{{$product->getMerchantValues($key)['service_delivery_center_radius']}}km</div>
                                <div data-value="{{$product->getMerchantValues($key)['service_delivery_center_radius']}}" data-merchant_id="{{ $key }}" class="slider-range-min"></div>
                                <p class="text-muted"><small>Trascina il quadratino a destra e sinistra per impostare la distanza desiderata</small></p>
                            </div>
                    @else
                            <input type="hidden" name="merchants[{{$key}}][service_delivery_center_lat]">
                            <input type="hidden" name="merchants[{{$key}}][service_delivery_center_lng]">
                            <input type="hidden" name="merchants[{{$key}}][service_delivery_center_radius]">
                            <input type="hidden" name="merchants[{{$key}}][service_delivery_address]">
                             <div class="col-md-12">
                                <label for="service_delivery_address_input_{{$key}}">Raggio di erogazione del servizio</label>
                                <input type="text" id="service_delivery_address_input_{{$key}}" class="form-control">
                                @hss('20')
                                <label for="service_delivery_address_{{$key}}">Raggio di erogazione del servizio</label>
                                <br>
                                <div class="h3 text-center" id="service_delivery_center_radius_{{ $key }}">0 km</div>
                                <div data-value="0" data-merchant_id="{{ $key }}" class="slider-range-min"></div>
                                <p class="text-muted"><small>Trascina il quadratino a destra e sinistra per impostare la distanza desiderata</small></p>
                            </div>
                    @endif


                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12"><h4>Commissioni</h4></div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="commission_percentage_{{$key}}">Mediazione per Atlantidex <span class="text-error font-bigger">*</span></label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">%</span>
                                </div>
                                @isset($product)
                                    <input type="number" min="5" max="100" step="any" name="merchants[{{$key}}][commission_percentage]" id="commission_percentage_{{$key}}" class="form-control eps-calc" value="{{$product->getMerchantValues($key)['commission_percentage']}}">
                                @else
                                    <input type="number" min="5" max="100" step="any" name="merchants[{{$key}}][commission_percentage]" id="commission_percentage_{{$key}}" class="form-control eps-calc" value="{{old('merchants.'.$key.'.commission_percentage') ?? 10}}">
                                @endisset
                            </div>
                            <p class="text-muted"><small>La percentuale deve essere compresa tra il 5% ed il 100%. La commissione dal 5% al 50% è da calcolarsi più iva, dal 51% al 100% è da calcolarsi iva inclusa.</small></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="commissionb2b_percentage_{{$key}}">Mediazione per Atlantidex B2B</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">%</span>
                                </div>
                                @isset($product)
                                    <input type="number" min="2" max="100" step="any" name="merchants[{{$key}}][commissionb2b_percentage]" id="commissionb2b_percentage_{{$key}}" class="form-control" value="{{$product->getMerchantValues($key)['commissionb2b_percentage']}}">
                                @else
                                    <input type="number" min="2" max="100" step="any" name="merchants[{{$key}}][commissionb2b_percentage]" id="commissionb2b_percentage_{{$key}}" class="form-control" value="{{old('merchants.'.$key.'.commissionb2b_percentage') ?? 10}}">
                                @endisset
                            </div>
                            <p class="text-muted"><small>La percentuale deve essere compresa tra il 2% ed il 100%. La commissione dal 2% al 50% è da calcolarsi più iva, dal 51% al 100% è da calcolarsi iva inclusa.</small></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="eps_{{$key}}">Earning Points</label> <span class="text-error font-bigger">*</span></label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" style="padding:0 .3em !important"><button id="calc_{{$key}}" class="btn btn-sm btn-default calculator"><i class="fa fa-calculator"></i></button></span>
                                </div>
                                @isset($product)
                                    <input readonly type="number" step="any" name="merchants[{{$key}}][eps]" id="eps_{{$key}}" class="form-control" value="{{(isset($product->getMerchantValues($key)['eps'])) ? $product->getMerchantValues($key)['eps'] : 0}}">
                                @else
                                    <input readonly type="number" step="any" name="merchants[{{$key}}][eps]" id="eps_{{$key}}" class="form-control" value="{{old('merchants.'.$key.'.eps')}}">
                                @endisset
                            </div>
                            <p class="text-muted"><small>Clicca sull'icona della calcolatrice per calcolare gli EP</small></p>
                        </div>
                        <div class="form-group">
                            <label for="b2b_eps_{{$key}}">Earning Points B2B</label> <span class="text-error font-bigger">*</span></label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" style="padding:0 .3em !important">
                                        <button id="b2b_calc_{{$key}}" class="btn btn-sm btn-default b2b_calculator"><i class="fa fa-calculator"></i></button>
                                    </span>
                                </div>
                                @isset($product)
                                    <input readonly type="number" step="any" name="merchants[{{$key}}][b2b_eps]" id="b2b_eps_{{$key}}" class="form-control" value="{{isset($product->getMerchantValues($key)['b2b_eps']) ? $product->getMerchantValues($key)['b2b_eps'] : 0}}">
                                @else
                                    <input readonly type="number" step="any" name="merchants[{{$key}}][b2b_eps]" id="b2b_eps_{{$key}}" class="form-control" value="{{old('merchants.'.$key.'.b2b_eps')}}">
                                @endisset
                            </div>
                            <p class="text-muted"><small>Clicca sull'icona della calcolatrice per calcolare gli EP B2B</small></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>

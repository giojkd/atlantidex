@auth

@extends('admin_custom.top_left')

@section('content')
<div class="row">
    <div class="col-md-12">

        <h5>Per iniziare ad aggiungere prodotti</h5>
        <h2>Trova i tuoi prodotti nel catalogo Atlantidex</h2>
        <div class="card">
            <div class="card-body p-5">
                <form method="get" action="{{ backpack_url('catalogue') }}">
                    <div class="input-group">
                        <input name="search" type='text' class="form-control form-control-lg" placeholder="Cerca per nome o per SKU">

                        <div class="input-group-append">
                            <button style="font-size: 17px" type="submit" class="btn btn-lg btn-success">Cerca</button>
                        </div>

                    </div>
                    <p class="text-muted"><small>Il codice SKU, il cui acronimo sta per <b>Stock Keeping Unit</b>, è una sequenza di caratteri alafanumerici univoca che le aziende utilizzano per identificare e tracciare i propri prodotti all'interno del magazzino, giacché semplifica la localizzazione e impedisce la confusione tra articoli diversi.</small></p>
                </form>
                @include('Company.Product.components.add_product_upload_file')
            </div>
        </div>

    </div>
</div>

@endsection
@push('after_scripts')

@endpush
@endauth

@auth

@extends('admin_custom.top_left')

@section('content')
<link href="/packages/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css">
<link href="/packages/select2-bootstrap-theme/dist/select2-bootstrap.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.css" integrity="sha256-e47xOkXs1JXFbjjpoRr1/LhVcqSzRmGmPqsrUQeVs+g=" crossorigin="anonymous" />
<style>


.show-image {
  border-radius: 20px;
  display: inline-grid;
  height: 120px;
  overflow: hidden;
  position: relative;
  width: 120px;
  z-index: 10;
}

.value-field {
    font-weight: bold;
}

.value-text{
    border: 1px solid rgba(0,40,100,.12) !important;
    border-radius: 3px !important;
    padding:5px;
}

fieldset.scheduler-border {
    border: 1px solid rgba(0,40,100,.12) !important;
    border-radius: 3px !important;
    padding: 0 1.4em 1.4em 1.4em !important;
    margin: 0 0 1.5em 0 !important;
    -webkit-box-shadow:  0px 0px 0px 0px #000;
            box-shadow:  0px 0px 0px 0px #000;
}

legend.scheduler-border {
    font-size: 1.2em !important;
    text-align: left !important;
    width:inherit; /* Or auto */
    padding:0 10px; /* To give a bit of padding on the left and right */
    border-bottom:none;
}
</style>

<form id="product-form" action="/admin/product/update-merchant" method="POST" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="id" value= {{$product['id'] ?? ''}}>
    <input type="hidden" name="locale" value= {{$locale ?? ''}}>
    <input type="hidden" id="vat_value" value="{{$product->vat->value ?? 22}}">
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <h2>{{$product['sku']?? ''}} - {{$product['name']?? ''}}
            @isset($product)
                <small>{{$product->getAttributesSerialized()}}</small>
                <select class="form-control select2" id="select-variant">
                    <option>Varianti</option>
                    @foreach ($prodcut_variants as $item)
                        <option value="/admin/product/{{$item->id}}/show"> {{$item->sku}} - {{$item->name}}: {{$item->getAttributesSerialized()}}</option>
                    @endforeach
                </select>
            @else
                <small>Aggiungi un prodotto</small>
            @endisset
            <small><a href="/admin/product"><i class="la la-angle-double-left"></i> Torna a tutti i  <span>prodotti</span></a></small>
            </h2>
            <div class="row">
                <div class="col-md-6">
                    @if ($product['active'])
                    <span>
                        <i class="la la-check-circle"></i>
                    </span>
                    @else
                        <i class="la la-circle"></i>
                    @endif
                    <label for="active">Attivo</label>
                </div>
                <div class="col-md-6">
                    <div class="mb-2 text-right">
                        <!-- Single button -->
                        <div class="btn-group">
                          <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Lingua:  {{Request::get('locale')?? 'it'}} &nbsp; <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu">
                          <a class="dropdown-item" href="{{Request::url()}}?locale=en">English</a>
                                  <a class="dropdown-item" href="{{Request::url()}}?locale=fr">French</a>
                                  <a class="dropdown-item" href="{{Request::url()}}?locale=it">Italian</a>
                          </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <!--taxonomy-->
            <div class="card">
                <div class="card-body">
                    <h4>Caratteristiche</h4>
                    <div class="form-group">
                        <!--<label for="manufacturer_id">Venditore</label>
                        {*!! Form::select('merchant_id', $merchants, $productmerchant->pluck('id'), ['class' => 'form-control select2','multiple' => 'multiple']) !!*}-->
                        <label for="product_type_id">Tipologia</label>
                        <div class="value-field">{{$product->product_type->name ?? null}}</div>
                        <label for="category_id">Categoria</label>
                        <div class="value-field">{{$product->category->name ?? null}}</div>
                        <label for="manufacturer_id">Marchio</label>
                        <div class="value-field">{{$product->manufacturer->name ?? null}}</div>
                    </div>
                </div>
            </div>
            <!--dimesions and packaging-->
            <div class="card">
                <div class="card-body">
                    <h4>Dimensioni e Packaging</h4>
                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border">Packaging</legend>
                        <div class="form-group">
                            <label for="packaging_weight">Peso</label>
                            g: <div class="value-field">{{$product->packaging_weight ?? ''}}</div>
                            <label for="packaging_height">Altezza</label>
                            mm: <div class="value-field">{{$product->packaging_height ?? ''}}</div>
                            <label for="packaging_lenght">Larghezza</label>
                            mm: <div class="value-field">{{$product->packaging_lenght ?? ''}}</div>
                            <label for="packaging_width">Profondità</label>
                            mm:  <div class="value-field">{{$product->packaging_width ?? ''}}</div>
                        </div>
                    </fieldset>
                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border">Prodotto</legend>
                        <div class="form-group">
                            <label for="product_weight">Peso</label>
                            g: <div class="value-field">{{$product->product_weight ?? ''}}</div>
                            <label for="product_height">Altezza</label>
                            mm: <div class="value-field">{{$product->product_height ?? ''}}</div>
                            <label for="product_lenght">Larghezza</label>
                            mm: <div class="value-field">{{$product->product_lenght ?? ''}}</div>
                            <label for="product_width">Profondità</label>
                            mm: <div class="value-field">{{$product->product_width ?? ''}}</div>
                        </div>
                    </fieldset>

                </div>
            </div>
            <!--commerce-->
            <div class="card">
                <div class="card-body">
                    <h4>Caratteristiche commerciali</h4>
                    <div class="form-group">
                        <label for="minimum_increase">Minimo incremento quantitativo</label>
                        <div class="value-field">{{$product->minimum_increase ?? ''}}</div>
                        <label for="min_quantity">Quantità minima acquistabile</label>
                        <div class="value-field">{{$product->min_quantity ?? ''}}</div>
                        <label for="warranty_years">Anni di garanzia</label>
                        <div class="value-field">{{$product->warranty_years ?? ''}}</div>
                    </div>
                </div>
            </div>
            <!--descriptions-->
            <div class="card">
                <div class="card-body">
                    <h4>Descrizioni</h4>
                    <div class="form-group">
                        <label for="name">Nome prodotto</label>
                        <div class="value-field">{{$product['name'] ?? ''}}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="sku">SKU <i>(codice aziendale)</i></label>
                                <div class="value-field">{{$product['sku'] ?? ''}}</div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="price_per">Unità di misura</label>
                                <div class="value-field">{{$product['price_per'] ?? ''}}</div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="vat_id">IVA applicata</label>
                                <div class="value-field">{{$product->vat->name ?? ''}}</div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description">Descrizione</label>
                        <div class="value-text">{!!$product['description'] ?? ''!!}</div>
                    </div>
                    <div class="form-group">
                        <label for="brief">Descrizione breve</label>
                        <div class="value-text">{!!$product['brief'] ?? ''!!}</div>
                    </div>
                </div>
            </div>
            <!--media-->
            <div class="card">
                <div class="card-body">
                    <h4>Media</h4>
                    <div class="form-group">
                        <div id="pictures-existing">
                            @foreach ($product['pictures'] as $picture)
                                <div class="show-image"><img data-dz-thumbnail="" alt="" src="/ir/h120/{{$picture}}"></div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <!-- merchants -->
            @include('Company.Product.include_merchant_values')
            <!-- attributes -->
            <div class="card">
                <div class="card-body">
                    <div id="row-new">
                        <div class="row" >
                            <div class="col-md-12"><h4>Caratteristiche</h4></div>
                        </div>
                    </div>
                    @foreach ($attributevalues as $key => $item)
                        <div class="row" id="row_{{$item->id}}">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="value-field">{{$item->attribute->name}}</div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="value-field">{{$item->name}}</div>
                                </div>
                            </div>

                        </div>
                    @endforeach
                </div>
            </div>
        </div>

    </div>
    <div class="row">
    <div class="col-md-6 offset-md-3"><button type="submit" class="btn btn-primary btn-lg  mt-2 btn-block">salva</button></div>
    </div>
</form>
@endsection
@push('after_scripts')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API') }}&libraries=places"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.js" integrity="sha256-p2l8VeL3iL1J0NxcXbEVtoyYSC+VbEbre5KHbzq1fq8=" crossorigin="anonymous"></script>
    <script src="https://raw.githack.com/SortableJS/Sortable/master/Sortable.js"></script>
    <script src="/packages/select2/dist/js/select2.full.min.js"></script>
    <script>
        var merchants = @json($merchants);
        console.log(merchants);
        $( function() {
            $('.slider-range-min').each(function(){
                $(this).slider({
                    range: "min",
                    value: $(this).data('value'),
                    min: 0,
                    max: {{ env('SERVICE_DELIVERY_CENTER_RADIUS') }},
                    slide: function( event, ui ) {
                        var mid = $(this).data('merchant_id');
                        $( "#service_delivery_center_radius_"+mid ).html(  ui.value+" km"  );
                        $('input[name="merchants['+mid+'][service_delivery_center_radius]"]').val(ui.value );

                    }
                })
            })

            var autocompleters = [];
            for(var i in merchants){

                autocompleters[i] = new google.maps.places.Autocomplete(document.getElementById("service_delivery_address_input_"+i));
                autocompleters[i].merchant_id = i;
                autocompleters[i].addListener("place_changed", getAutocompleteData );

            }

            function getAutocompleteData(){
                var place = this.getPlace();
                var merchantId = this.merchant_id;
                var addressData = new Object();
                $.each(place.address_components, function(index,item){
                    var data = {
                        long_name : item.long_name,
                        short_name : item.short_name
                    }
                    addressData[item.types[0]] = data
                });
                addressData['lat'] = place.geometry.location.lat();
                addressData['lng'] = place.geometry.location.lng();
                addressData['formatted_address'] = place.formatted_address;
                $('input[name="merchants['+merchantId+'][service_delivery_address]"]').val(JSON.stringify(addressData));
                $('input[name="merchants['+merchantId+'][service_delivery_center_lng]"]').val(addressData['lng']);
                $('input[name="merchants['+merchantId+'][service_delivery_center_lat]"]').val(addressData['lat']);


            }



        });

        $('.calculator').click(function (e){
            e.preventDefault();
            var elid = $(this).attr('id');
            var keyid = elid.split('_')[1];
            console.log(elid);
            var price = $('#priceiva_'+keyid).val();
            var commission = $('#commission_percentage_'+keyid).val();
            var eps = parseFloat(price * (commission / 100)).toFixed(2);
            $('#eps_'+keyid).val(eps);
        });

        $('.b2b_calculator').click(function (e){
            e.preventDefault();
            var elid = $(this).attr('id');
            var keyid = elid.split('_')[2];
            console.log(keyid);
            var price = $('#priceiva_'+keyid).val();
            var commission = $('#commissionb2b_percentage_'+keyid).val();
            var eps = parseFloat(price * (commission / 100)).toFixed(2);
            $('#b2b_eps_'+keyid).val(eps);
        });




        $('.price-iva').focusout(function (e){
            e.preventDefault();
            var elid = $(this).attr('id');
            var keyid = elid.split('_')[1];
            var vat = $('#vat_value').val();
            var priceiva = $('#priceiva_'+keyid).val();
            console.log(priceiva,vat)
            var price = parseFloat(priceiva /  (1 + (vat/100))).toFixed(2);
            $('#price_'+keyid).val(price);
            $('.calculator').click();
            $('.b2b_calculator').click();
        });
        $('.price-noiva').focusout(function (e){
            e.preventDefault();
            var elid = $(this).attr('id');
            var keyid = elid.split('_')[1];
            var vat = $('#vat_value').val();
            var price = $('#price_'+keyid).val();
            console.log(price,vat)
            var priceiva = parseFloat(price * (1 + (vat/100))).toFixed(2);
            $('#priceiva_'+keyid).val(priceiva);
            $('.calculator').click();
            $('.b2b_calculator').click();
        });

        $('#myTab li:first-child a').tab('show')
        $(".select2").select2({
            theme: "bootstrap"
        });
        $( "#select-variant" ).change(function() {
            var url = $(this).val();
            if (url != '')
                location.href = $(this).val();
        });
        @if(session()->has('message'))
            new Noty({
                text: '{{ session()->get('message') }}',
                class:'success',
                animation: {
                    open: 'animated bounceInRight', // Animate.css class names
                    close: 'animated bounceOutRight' // Animate.css class names
                }
            }).show();
        @endif
      </script>
@endpush
@endauth

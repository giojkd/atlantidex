@auth

@extends('admin_custom.top_left')

@section('content')
<link href="/packages/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css">
<link href="/packages/select2-bootstrap-theme/dist/select2-bootstrap.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.css" integrity="sha256-e47xOkXs1JXFbjjpoRr1/LhVcqSzRmGmPqsrUQeVs+g=" crossorigin="anonymous" />
<style>

.invalid-feedback-visibile {
    display: block;
    width: 100%;
    margin-top: .25rem;
    font-size: 80%;
    color: #cd201f;
}


.info-icon {
    border-radius: 50%;
    cursor:pointer;
}

.font-bigger {
    /*font-size:1.5em;*/
}

fieldset.scheduler-border {
    border: 1px solid rgba(0,40,100,.12) !important;
    border-radius: 3px !important;
    padding: 0 1.4em 1.4em 1.4em !important;
    margin: 0 0 1.5em 0 !important;
    -webkit-box-shadow:  0px 0px 0px 0px #000;
            box-shadow:  0px 0px 0px 0px #000;
}

legend.scheduler-border {
    font-size: 1.2em !important;
    text-align: left !important;
    width:inherit; /* Or auto */
    padding:0 10px; /* To give a bit of padding on the left and right */
    border-bottom:none;
}
</style>
@if($errors->any())
    <div class="alert alert-danger" role="alert">
        <h6>Attenzione!! La pagina contiene i seguenti errori</h6>
        <ul>
        {!! implode('', $errors->all('<li>:message</li>')) !!}
        </ul>
    </div>
@endif


<form id="product-form" action="/admin/product/update" method="POST" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="id" value= {{$product['id'] ?? ''}}>
    <input type="hidden" name="product_type_id" value= {{$product['product_type_id'] ?? ''}}>
    <input type="hidden" name="locale" value= {{$locale ?? ''}}>
    <input type="hidden" id="vat_value" value="{{$product->vat->value ?? 22}}">
    <input type="hidden" name="category_id" value="@if (isset($product)) {{ $product->category_id }} @endif">
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-info">
                <ul>
                    <li>i campi contrassegnati da <span class="text-danger font-bigger">*</span> sono obbligatori per l'inserimento del prodotto. Per salvare una bozza è necessario compilarli tutti, selezionare il pulsante <b>"NON Attivo"</b> e cliccare su salva in fondo alla pagina.</li>
                    <li>i campi contrassegnati da <span class="text-warning font-bigger">*</span> sono necessari per la visibilità del prodotto online</li>
                </ul>
            </div>
            <h2>
            @isset($product)
                {{$product['sku']?? ''}} - {{$product['name']?? ''}}
                <small>{{$product->getAttributesSerialized()}}</small>
                <select class="form-control select2" id="select-variant">
                    <option>Varianti</option>
                    @foreach ($prodcut_variants as $item)
                        <option value="/admin/product/{{$item->id}}/edit"> {{$item->sku}} - {{$item->name}}: {{$item->getAttributesSerialized()}}</option>
                    @endforeach
                </select>
                <small>
                   <a href="/admin/product/{{$product['id']}}/clone"><i class="la la-plus"></i> Crea  <span> variante</a>
                </small>
            @else
                <small>Stai aggiungendo un prodotto o servizio nuovo</small>
            @endisset
            <small><a href="/admin/product"><i class="la la-angle-double-left"></i> Torna a tutti i  <span>prodotti e servizi</span></a></small>
            </h2>
            <div class="row">
                <div class="col-md-6">
                    @if(Auth::id() != env('TEST_COMPANY_ID'))
                        @isset($product)

                        <label for=""><input type="radio" name="active" id="" value="1" {{$product['active'] ? 'checked' : ''}}> Attivo</label> <i data-toggle="popover" data-content="{{trans('help.active')}}" data-original-title="{{trans('help.active-title')}}" class="fa fa-info info-icon"></i></label> <br>
                        <label for=""><input type="radio" name="active" id="" value="0" {{!$product['active'] ? 'checked' : ''}}> NON Attivo</label> <i data-toggle="popover" data-content="{{trans('help.non-active')}}" data-original-title="{{trans('help.non-active-title')}}" class="fa fa-info info-icon"></i></label>
                        @else
                            <label for=""><input type="radio" name="active" id="" value="1" checked> Attivo</label> <i data-toggle="popover" data-content="{{trans('help.active')}}" data-original-title="{{trans('help.active-title')}}" class="fa fa-info info-icon"></i></label> <br>
                        <label for=""><input type="radio" name="active" id="" value="0"> NON Attivo</label> <i data-toggle="popover" data-content="{{trans('help.non-active')}}" data-original-title="{{trans('help.non-active-title')}}" class="fa fa-info info-icon"></i></label>
                        @endisset
                    @else
                        <label for=""><input type="radio" name="active" id="" value="0" checked> NON Attivo</label> <i data-toggle="popover" data-content="{{trans('help.non-active')}}" data-original-title="{{trans('help.non-active-title')}}" class="fa fa-info info-icon"></i></label>
                    @endif

                </div>
                <div class="col-md-6">
                    <div class="mb-2 text-right">
                        <!-- Single button -->
                        <div class="btn-group">
                          <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Lingua:  {{Request::get('locale')?? 'it'}} &nbsp; <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu">
                          <a class="dropdown-item" href="{{Request::url()}}?locale=en">English</a>
                                  <a class="dropdown-item" href="{{Request::url()}}?locale=fr">French</a>
                                  <a class="dropdown-item" href="{{Request::url()}}?locale=it">Italian</a>
                          </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
             <!--taxonomy-->
            <div class="card">
                <div class="card-body">
                    <h4>Caratteristiche</h4>
                    <div class="form-group">



                        <ul class="nav nav-tabs">
                            @foreach ($product_types as $_item)
                            @php
                                $key = $_item->id;
                                $item = $_item->name;
                            @endphp
                                <li class="nav-item">
                                    @if(isset($product))
                                        <a class="categories-tab-nav-link nav-link @if($product->category->isDescendantOf($_item->category)) active @endif" data-toggle="tab" data-type="{{ $key }}" data-target="#product_type_{{ $key }}">{{ $item }}</a>
                                    @else
                                        <a class="categories-tab-nav-link nav-link @if($key == 1) active @endif" data-toggle="tab" data-type="{{ $key }}" data-target="#product_type_{{ $key }}">{{ $item }}</a>
                                    @endif
                                </li>
                             @endforeach
                        </ul>

                        <div class="tab-content">
                            @foreach ($product_types as $_item)
                            @php
                                $key = $_item->id;
                                $item = $_item->name;
                            @endphp
                            @if(isset($product))
                                <div class="categories-tab tab-pane container @if($product->category->isDescendantOf($_item->category)) active @else fade @endif"  id="product_type_{{ $key }}">
                            @else
                                <div class="categories-tab tab-pane container @if($key == 1) active @else fade @endif"  id="product_type_{{ $key }}">
                            @endif
                                        <select name="" id="categories_type_{{ $key }}" class="form-control select2 category_id_select">
                                            <option value="">Seleziona tra i {{ $item }}</option>
                                            @foreach ($categories[$key] as $category)
                                                <option @if (isset($product))
                                                    @if($product->category_id == $category->id)
                                                        selected
                                                    @endif
                                                @else

                                                @endif value="{{ $category->id }}">
                                                    {{ $category->stored_ancestors_breadcrumb }} > <b>{{ $category->name }}</b>
                                                </option>
                                            @endforeach

                                        </select>

                                </div>
                            @endforeach
                        </div>
                        <div class="help-block">
                            <p class="muted">
                                <small>
                                    Si consiglia di ricercare le categorie utilizzando il plurale, ad esempio: maglietta -> magliette.<br>
                                    Non trovi la categoria che stai cercando ? <a target="_blank" href="{{ url('company/category-suggest') }}">Clicca qui</a> per suggerirne una nuova. La categoria suggerita verrà approvata o meno entro 24/48h. E' possibile comunque continuare con l'inserimento di tutti i dati del prodotto o servizio e salvare una bozza.
                                </small>
                            </p>
                        </div>
                        <div class="only-product">
                        <label for="manufacturer_id">Marchio <span class="text-danger font-bigger">*</span>
                            <i data-toggle="popover" data-content="{{trans('help.manufacturer-text')}}" data-original-title="{{trans('help.manufacturer-title')}}" class="fa fa-info info-icon"></i></label>
                        {!! Form::select('manufacturer_id', $manufacturers, $product['manufacturer_id'] ?? null, ['placeholder' => 'Seleziona o inserisci uno nuovo','id' => 'select-manufacturer','class' => 'form-control']) !!}
                        @error('manufacturer_id')
                        <span class="invalid-feedback-visibile">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                        </div>
                    </div>
                </div>
            </div>
            <!--dimesions and packaging-->
            <div class="card only-product">
                <div class="card-body">
                    <h4>Dimensioni e Packaging</h4>
                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border">Packaging </legend>
                        <img class="img-fluid" src="/resources/box-sizes.png" alt="">
                        <div class="form-group">
                            <label for="packaging_weight">Peso <span class="text-error font-bigger">*</span></label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">g</span>
                                </div>
                                @isset($product)
                                    <input type="number" min="0" step="any" name="packaging_weight" id="packaging_weight" class="form-control" value="{{$product->packaging_weight ?? old('packaging_weight')}}">
                                @else
                                    <input type="number" min="0" step="any" name="packaging_weight" id="packaging_weight" class="form-control" value="{{old('packaging_weight')}}">
                                @endisset
                            </div>
                            <label for="packaging_height">Altezza <span class="text-error font-bigger">*</span></label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">mm</span>
                                </div>
                                @isset($product)
                                    <input type="number" min="0" step="any" name="packaging_height" id="packaging_height" class="form-control" value="{{$product->packaging_height ?? old('packaging_height')}}">
                                @else
                                    <input type="number" min="0" step="any" name="packaging_height" id="packaging_height" class="form-control" value="{{old('packaging_height')}}">
                                @endisset
                            </div>
                            <label for="packaging_lenght">Larghezza <span class="text-error font-bigger">*</span></label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">mm</span>
                                </div>
                                @isset($product)
                                    <input type="number" min="0" step="any" name="packaging_lenght" id="packaging_lenght" class="form-control" value="{{$product->packaging_lenght ?? old('packaging_lenght')}}">
                                @else
                                    <input type="number" min="0" step="any" name="packaging_lenght" id="packaging_lenght" class="form-control" value="{{old('packaging_lenght')}}">
                                @endisset
                            </div>
                            <label for="packaging_width">Profondità <span class="text-error font-bigger">*</span></label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">mm</span>
                                </div>
                                @isset($product)
                                    <input type="number" min="0" step="any" name="packaging_width" id="packaging_width" class="form-control" value="{{$product->packaging_width ?? old('packaging_width')}}">
                                @else
                                    <input type="number" min="0" step="any" name="packaging_width" id="packaging_width" class="form-control" value="{{old('packaging_width')}}">
                                @endisset
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border">Prodotto</legend>
                        <div class="form-group">
                            <label for="product_weight">Peso</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">g</span>
                                </div>
                                @isset($product)
                                    <input type="number" min="0" step="any" name="product_weight" id="product_weight" class="form-control" value="{{$product->product_weight ?? old('product_weight')}}">
                                @else
                                    <input type="number" min="0" step="any" name="product_weight" id="product_weight" class="form-control" value="{{old('product_weight')}}">
                                @endisset
                            </div>
                            <label for="product_height">Altezza</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">mm</span>
                                </div>
                                @isset($product)
                                    <input type="number" min="0" step="any" name="product_height" id="product_height" class="form-control" value="{{$product->product_height ?? old('product_height')}}">
                                @else
                                    <input type="number" min="0" step="any" name="product_height" id="product_height" class="form-control" value="{{old('product_height')}}">
                                @endisset
                            </div>
                            <label for="product_lenght">Larghezza</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">mm</span>
                                </div>
                                @isset($product)
                                    <input type="number" min="0" step="any" name="product_lenght" id="product_lenght" class="form-control" value="{{$product->product_lenght ?? old('product_lenght')}}">
                                @else
                                    <input type="number" min="0" step="any" name="product_lenght" id="product_lenght" class="form-control" value="{{old('product_lenght')}}">
                                @endisset
                            </div>
                            <label for="product_width">Profondità</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">mm</span>
                                </div>
                                @isset($product)
                                    <input type="number" min="0" step="any" name="product_width" id="product_width" class="form-control" value="{{$product->product_width ?? old('product_width')}}">
                                @else
                                    <input type="number" min="0" step="any" name="product_width" id="product_width" class="form-control" value="{{old('product_width')}}">
                                @endisset
                            </div>
                        </div>
                    </fieldset>
                    <h4>Prodotto digitale</h4>
                    <div class="form-group {{$uploadedvisibility}}" id="uploaded-file">
                        @isset($product)
                            <input type="hidden" name="file" value="{{$product['file']}}" id="hidden-file">
                            <a href="/{{$product['file']}}" target="_blank" class="btn btn-default">file da scaricare</a><input type="submit" id="del-download" class="btn btn-error" value="-">
                        @endisset
                    </div>
                    <div class="form-group {{$uploadvisibility}}" id="upload-file">
                        <div class="dropzone dropzone-target-mono dz-clickable" id="download-dropzone"></div>
                    </div>
                </div>
            </div>
            <!--commerce-->
            <div class="card only-product">
                <div class="card-body">
                    <h4>Caratteristiche commerciali</h4>
                    <div class="form-group">
                        <label for="minimum_increase">Minimo incremento quantitativo <i data-toggle="popover" data-content="{{trans('help.minimum-incremental-text')}}" data-original-title="{{trans('help.minimum-incremental-title')}}" class="fa fa-info info-icon"></i></label>
                        @isset($product)
                            <input type="number" min="0" step="any" name="minimum_increase" id="minimum_increase" class="form-control" value="{{$product->minimum_increase ?? old('minimum_increase')}}">
                        @else
                            <input type="number" min="0" step="any" name="minimum_increase" id="minimum_increase" class="form-control" value="{{old('minimum_increase')}}">
                        @endisset

                        <label for="min_quantity">Quantità minima acquistabile <i data-toggle="popover" data-content="{{trans('help.minimum-quantity-text')}}" data-original-title="{{trans('help.minimum-quantity-title')}}" class="fa fa-info info-icon"></i></label>
                        @isset($product)
                            <input type="number" min="0" step="any" name="min_quantity" id="min_quantity" class="form-control" value="{{$product->min_quantity ?? old('min_quantity')}}">
                        @else
                            <input type="number" min="0" step="any" name="min_quantity" id="min_quantity" class="form-control" value="{{old('min_quantity')}}">
                        @endisset
                        <label for="warranty_years">Anni di garanzia <i data-toggle="popover" data-content="{{trans('help.warranty-text')}}" data-original-title="{{trans('help.warranty-title')}}" class="fa fa-info info-icon"></i></label>
                        @isset($product)
                            <input type="number" min="0" step="1" name="warranty_years" id="warranty_years" class="form-control" value="{{$product->warranty_years ?? old('warranty_years')}}">
                        @else
                            <input type="number" min="0" step="1" name="warranty_years" id="warranty_years" class="form-control" value="{{old('warranty_years')}}">
                        @endisset
                    </div>
                </div>
            </div>
            <!--descriptions-->
            <div class="card">
                <div class="card-body">
                    <h4>Descrizione</h4>
                    <div class="form-group">
                        <label for="name">Nome prodotto o servizio<span class="text-danger font-bigger">*</span></label>
                        <input type="text"  name="name" id="name" placeholder="Nome prodotto" class="form-control " value="{{$product['name'] ?? old('name')}}">
                        @error('name')
                            <span class="invalid-feedback-visibile">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="sku">SKU <i>(o codice aziendale)</i><span class="text-danger font-bigger">*</span>
                                <i data-toggle="popover" data-content="{{trans('help.sku-text')}}" data-original-title="{{trans('help.sku-title')}}" class="fa fa-info info-icon"></i></label>
                                <input type="text"   name="sku" id="sku" placeholder="sku" class="form-control" value="{{$product['sku'] ?? old('sku')}}">
                                @error('sku')
                                    <span class="invalid-feedback-visibile">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                         <div class="col-md-4">
                            <div class="form-group">
                                <label for="vat_id">IVA applicata<span class="text-error font-bigger">*</span>
                                <i data-toggle="popover" data-content="{{trans('help.vat-text')}}" data-original-title="{{trans('help.vat-title')}}" class="fa fa-info info-icon"></i></label>
                                {!! Form::select('vat_id', $vat_options, $product['vat_id'] ?? 1, ['id' => 'vat_id','class' => 'form-control']) !!}
                                @error('vat_id')
                                    <span class="invalid-feedback-visibile">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4 only-product">
                            <div class="form-group">
                                <label for="price_per">Unità di misura
                                <i data-toggle="popover" data-content="{{trans('help.price_per-text')}}" data-original-title="{{trans('help.price_per-title')}}" class="fa fa-info info-icon"></i></label>
                                <input  type="text" name="price_per" id="price_per" placeholder="Unità di misura" class="form-control" value="{{$product['price_per'] ?? old('price_per')}}">
                                @error('price_per')
                                    <span class="invalid-feedback-visibile">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>
                        </div>

                    </div>
                    <div class="form-group">
                        <label for="description">Descrizione <span class="text-danger font-bigger">*</span></label>
                        <textarea name="description" id="description" placeholder="descrizione" class="form-control">{{$product['description'] ?? old('description')}}</textarea>
                        @error('description')
                            <span class="invalid-feedback-visibile">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="brief">Descrizione breve</label>
                        <textarea name="brief" id="brief" placeholder="abstract" class="form-control">{{$product['brief'] ?? old('brief')}}</textarea>
                    </div>
                </div>
            </div>
            <!--media-->
            <div class="card">
                <div class="card-body">
                    <h4>Media <span class="text-warning font-bigger">*</span></h4>
                    <div class="form-group">
                        <label for="pictures">
                            Carica uno o più immagini per il prodotto o servizio (con orientamento orizzontale) la prima immagine sarà utilizzata come immagine di copertina, le immagini caricate possono essere riordinate trascinandole. Carica foto jpg ad alta risoluzione in qualsiasi formato o dimensione.
                        <i data-toggle="popover" data-content="{{trans('help.pictures-text')}}" data-original-title="{{trans('help.pictures-title')}}" class="fa fa-info info-icon"></i>
                            <a href="{{ asset('resources/LineeGuidaCaricamentoImmaginiProdotti.pdf') }}">Scarica la guida per il caricamento delle immagini del prodotto</a>
                    </label>
                        <div id="pictures-existing" class="dropzone-previews dropzone"></div>
                        <div class="dropzone dropzone-target dz-clickable" id="document-dropzone"></div>
                    </div>
                </div>
            </div>
            <!-- merchants -->
            @include('Company.Product.include_merchant_values')
            <!-- attributes -->
            <div class="card only-product">
                <div class="card-body">
                    <div id="row-new">
                        <div class="row" >

                            <div class="col-md-12"><h4>Caratteristiche <i data-toggle="popover" data-content="{{trans('help.attributes-text')}}" data-original-title="{{trans('help.attributes-title')}}" class="fa fa-info info-icon"></i></h4>


                            </div>

                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="">Nome dell'attributo <i data-toggle="popover" data-content="{{trans('help.attribute-text')}}" data-original-title="{{trans('help.attribute-title')}}" class="fa fa-info info-icon"></i></label>
                                    <input type="text" name="attributes[0][attribute]" id="attribute_name" placeholder="Nome attributo, ES: Taglia" class="form-control" value="">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="">Il suo valore <i data-toggle="popover" data-content="{{trans('help.attribute-value-text')}}" data-original-title="{{trans('help.attribute-value-title')}}" class="fa fa-info info-icon"></i></label>
                                    <input type="text" name="attributes[0][value]" id="attribute_value" placeholder="Valore attributo, ES: 46" class="form-control" value="">
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <input type="submit" class="btn btn-success btn-block add-row" value="+">
                                </div>
                            </div>
                        </div>
                    </div>
                    @foreach ($attributevalues as $key => $item)
                        <div class="row" id="row_{{$item->id}}">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <input type="text"  name="attributes[{{$item->id}}][attribute]" placeholder="Nome attributo" class="form-control" value="{{$item->attribute->name}}">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <input type="text"  name="attributes[{{$item->id}}][value]" placeholder="Valore attributo" class="form-control" value="{{$item->name}}">
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <button class='btn btn-error btn-block delete-row' value="{{$item->id}}"> - </button>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

        </div>

    </div>
    <div class="row">
        <div class="col-md-8"><button name="save_type" value="save" type="submit" class="btn btn-primary btn-lg  mt-2 btn-block">Salva</button></div>
        <div class="col-md-4"><button name="save_type" value="save_and_new" type="submit" class="btn btn-secondary btn-lg  mt-2 btn-block">Salva e crea nuovo</button></div>
    </div>
</form>
@endsection
@push('after_scripts')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API') }}&libraries=places"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.js" integrity="sha256-p2l8VeL3iL1J0NxcXbEVtoyYSC+VbEbre5KHbzq1fq8=" crossorigin="anonymous"></script>
    <script src="https://raw.githack.com/SortableJS/Sortable/master/Sortable.js"></script>
    <script src="/packages/select2/dist/js/select2.full.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
    <script>

        var merchants = @json($merchants);

        console.log(merchants);
        $( function() {

            $('a[data-toggle="tab"].categories-tab-nav-link').on('shown.bs.tab', function (e) {
                var key = $(e.target).data("type");
                hideInsertElements(key);
                $('input[type="hidden"][name="category_id"]').val($('#categories_type_'+key).val())
            });

            $('.category_id_select').change(function(){
                $('input[type="hidden"][name="category_id"]').val($(this).val());
            })

            $('.slider-range-min').each(function(){
                $(this).slider({
                    range: "min",
                    value: $(this).data('value'),
                    min: 0,
                    max: {{ env('SERVICE_DELIVERY_CENTER_RADIUS') }},
                    slide: function( event, ui ) {
                        var mid = $(this).data('merchant_id');
                        $( "#service_delivery_center_radius_"+mid ).html(  ui.value+" km"  );
                        $('input[name="merchants['+mid+'][service_delivery_center_radius]"]').val(ui.value );

                    }
                })
            })

            var autocompleters = [];
            for(var i in merchants){

                autocompleters[i] = new google.maps.places.Autocomplete(document.getElementById("service_delivery_address_input_"+i));
                autocompleters[i].merchant_id = i;
                autocompleters[i].addListener("place_changed", getAutocompleteData );

            }

            function getAutocompleteData(){
                var place = this.getPlace();
                var merchantId = this.merchant_id;
                var addressData = new Object();
                $.each(place.address_components, function(index,item){
                    var data = {
                        long_name : item.long_name,
                        short_name : item.short_name
                    }
                    addressData[item.types[0]] = data
                });
                addressData['lat'] = place.geometry.location.lat();
                addressData['lng'] = place.geometry.location.lng();
                addressData['formatted_address'] = place.formatted_address;
                $('input[name="merchants['+merchantId+'][service_delivery_address]"]').val(JSON.stringify(addressData));
                $('input[name="merchants['+merchantId+'][service_delivery_center_lng]"]').val(addressData['lng']);
                $('input[name="merchants['+merchantId+'][service_delivery_center_lat]"]').val(addressData['lat']);


            }



        } );


        $('.calculator').click(function (e){
            e.preventDefault();
            var elid = $(this).attr('id');
            var keyid = elid.split('_')[1];
            console.log(elid);
            var price = $('#priceiva_'+keyid).val();
            var commission = $('#commission_percentage_'+keyid).val();
            var eps = parseFloat(price * (commission / 100)).toFixed(2);
            $('#eps_'+keyid).val(eps);
        });

        $('.b2b_calculator').click(function (e){
            e.preventDefault();
            var elid = $(this).attr('id');
            var keyid = elid.split('_')[2];
            console.log(keyid);
            var price = $('#priceiva_'+keyid).val();
            var commission = $('#commissionb2b_percentage_'+keyid).val();
            var eps = parseFloat(price * (commission / 100)).toFixed(2);
            $('#b2b_eps_'+keyid).val(eps);
        });




        $('.price-iva').focusout(function (e){
            e.preventDefault();
            var elid = $(this).attr('id');
            var keyid = elid.split('_')[1];
            var vat = $('#vat_value').val();
            var priceiva = $('#priceiva_'+keyid).val();
            console.log(priceiva,vat)
            var price = parseFloat(priceiva /  (1 + (vat/100))).toFixed(2);
            $('#price_'+keyid).val(price);
            $('.calculator').click();
            $('.b2b_calculator').click();
        });
        $('.price-noiva').focusout(function (e){
            e.preventDefault();
            var elid = $(this).attr('id');
            var keyid = elid.split('_')[1];
            var vat = $('#vat_value').val();
            var price = $('#price_'+keyid).val();
            console.log(price,vat)
            var priceiva = parseFloat(price * (1 + (vat/100))).toFixed(2);
            $('#priceiva_'+keyid).val(priceiva);
            $('.calculator').click();
            $('.b2b_calculator').click();
        });
    </script>



    <script>
        CKEDITOR.replace( 'description' );
        CKEDITOR.replace( 'brief' );
    </script>
    <script>
        var uploadedDocumentMap = {}
        var tipology_id_sel = '{{(isset($product)) ? $product['product_type_id'] : 1}}';
        var category_id_sel = '{{$product['category_id'] ?? 0}}';
        var sector_id_sel = '{{$sector_selected ?? 0}}'
        var subcategory = $('#category_id');
        var sector = $('#sector');
        var types = {!! json_encode($product_types_arr) !!}
        var vats = {!! json_encode($vat_options_arr) !!}
        var el = document.getElementById('pictures-existing');
        hideInsertElements(tipology_id_sel);


        $('#myTab li:first-child a').tab('show')
        Sortable.create(el, {});
        Dropzone.autoDiscover = false;
        $('[data-toggle="popover"]').popover({
            trigger: 'hover'
        });
        $('#download-dropzone').dropzone({
            maxFiles:1,
            acceptedFiles: ".zip",
            dictDefaultMessage: "Trascina o fai qui per caricare il file <i>(solo un file)</i> in formato .zip",
            addRemoveLinks: true,
            maxFilesize: 500, // MB
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            url: '/admin/media-dropzone/download',
            success: function (file, response) {
                console.log(response);
                $('form').append('<input type="hidden" name="file" value="' + response.filename + '">')
                uploadedDocumentMap[file.name] = response.filename
            },
            removedfile: function (file) {
                file.previewElement.remove()
                var name = ''
                if (typeof file.file_name !== 'undefined') {
                    name = file.file_name
                } else {
                    name = uploadedDocumentMap[file.name]
                }
                $('form').find('input[name="document[]"][value="' + name + '"]').remove()
            },
            init: function () {
                this.on("maxfilesexceeded", function(file) {
                    this.removeAllFiles();
                    this.addFile(file);
                });
                @if(isset($product) && $product['file']&& false)
                    var file = {'name':'{!! $product['file'] !!}'};
                    this.options.addedfile.call(this, file)
                    file.previewElement.classList.add('dz-complete')
                    $('form').append('<input type="hidden" name="file" value="' + file.name + '">')
                @endif
            }
        });
        $('#document-dropzone').dropzone({
            previewsContainer: "div#pictures-existing",
            dictDefaultMessage: "Trascina o fai click qui per caricare le immagini <i>(puoi caricare più immagini contemporaneamente)</i> è accettato solo il formato .jpg",
            previewTemplate: '<div class="dz-preview dz-file-preview"><input type="hidden" name="pictures[]" class="dropzone-filename-field" /><div class="dz-image-no-hover"><img data-dz-thumbnail /></div><div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div>',
            url: '/admin/media-dropzone/product',
            maxFilesize: 2, // MB
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            success: function (file, response) {
                console.log(response);
                $(file.previewElement).find('.dropzone-filename-field').val(response.filename);
            },
            removedfile: function (file) {
                file.previewElement.remove()
            },
            init: function () {
                @if(isset($product) && $product['pictures'])
                var files = {!! json_encode($product['pictures']) !!};
                for (var i in files) {
                    var item = {};
                    var file = files[i]
                    item.name = file;
                    this.options.addedfile.call(this, item)
                    this.options.thumbnail.call(this, item, '/ir/h120/'+item.name);
                    item.previewElement.classList.add('dz-complete')
                    $(item.previewElement).find('.dropzone-filename-field').val(item.name);
                }
                @endif
            }
        });
        $(".select2").select2({
            theme: "bootstrap",
        });
        $("#vat_id").change(function(e) {
            e.preventDefault(); // avoid to execute the actual submit of the form.
            var vatval = $(this).val();
            $('#vat_value').val(vats[vatval]);
        });
        $("#tipology").change(function(e) {
            e.preventDefault(); // avoid to execute the actual submit of the form.
            var typeselected = $(this).val();
            hideInsertElements(typeselected);

        });

        $("#select-manufacturer").select2({
            theme: "bootstrap",
            tags: true
        });
        $( "#select-variant" ).change(function() {
            var url = $(this).val();
            if (url != '')
                location.href = $(this).val();
        });
        $("#del-download").click(function(e) {
            e.preventDefault(); // avoid to execute the actual submit of the form.
            $('#hidden-file').val('');
            $('#uploaded-file').addClass('d-none');
            $('#upload-file').removeClass('d-none');
        });
        // this is the id of the form
        $(".add-row").click(function(e) {
            e.preventDefault(); // avoid to execute the actual submit of the form.
            var attribute = $('#attribute_name').val();
            var value = $('#attribute_value').val();
            if ((attribute == '') || (value == '')) {return;}
            $('#attribute_name').val('');
            $('#attribute_value').val('');
            var eltoappend = $('#row-new');
            var randomindex = randomInteger(1000, 9000);
            var str =   '<div class="row" id="row_'+randomindex+'">'+
                        '<div class="col-md-5">'+
                        '<div class="form-group">'+
                        '<input type="text"  name="attributes['+randomindex+'][attribute]" id="attribute_name" placeholder="Nome attributo" class="form-control" value="'+attribute+'">'+
                        '</div></div>'+
                        '<div class="col-md-5">'+
                        '<div class="form-group">'+
                        '<input type="text"  name="attributes['+randomindex+'][value]" id="attribute_value" placeholder="Valore attributo" class="form-control" value="'+value+'">'+
                        '</div></div>'+
                        '<div class="col-md-1">'+
                        '<div class="form-group">'+
                        '<button class="btn btn-error btn-block delete-row" value="'+randomindex+'"> - </button>'+
                        '</div></div></div>';
            eltoappend.append(str);
        });
        $(document).on('click','.delete-row',function(event){
            event.preventDefault();
            removeelement($(this));
        });
        @if(session()->has('message'))
            new Noty({
                text: '{{ session()->get('message') }}',
                type:'success',
                animation: {
                    open: 'animated bounceInRight', // Animate.css class names
                    close: 'animated bounceOutRight' // Animate.css class names
                }
            }).show();
        @endif

        function removeelement(element) {
            var el = "#row_"+element.val();
            $(el).remove();
        }

        function hideInsertElements(tipology_id) {

            $('input[type="hidden"][name="product_type_id"]').val(tipology_id);

            if (tipology_id == 1) {
                $('.only-product').each(function(){
                    $(this).removeClass('d-none');

                });
            } else {
                $('.only-product').each(function(){
                    $(this).addClass('d-none');

                });
            }
        }

        function randomInteger(min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }

      </script>
@endpush
@endauth

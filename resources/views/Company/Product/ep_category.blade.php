@auth

@extends('admin_custom.top_left')

@section('content')
<div class="row">
    <div class="col-md-12">
        <!--descriptions-->
        <a href="{{ route('MyAtlantinex.vouchers.index') }}"><i class="fa fa-arrow-left"></i> Torna ai miei voucher</a>
        <div class="card">
            <div class="card-body">
                <h2>Seleziona la categoria di EP</h2>
                <form method="GET" id="form">
                    <select name="epcategory" id="epcategory" class="form-control">
                        <option>seleziona</option>
                        @foreach ($epcategory as $item)
                            <option value="{{$item}}" @if (Request::get('epcategory') == $item) selected @endif>Categoria EP: {{$item}}</option>
                        @endforeach
                    </select>
                </form>
                <hr>
                <h2>Prodotti e servizi che garantiscono la categoria di EP selezionata</h2>
                @foreach ($products->chunk(4) as $chunks)
                    <div class="row">
                        @foreach ($chunks as $item)
                           <div class="col-md-3">
                                <div class="card w-100">
                                    <div class="card-body">
                                        <div class="text-center" style="overflow: hidden;">
                                            <img class="mb-2" style="height: 180px!important; max-width: 100%" src="{{ $item->cover(240) }}" alt="Card image cap">
                                        </div>

                                        <h5>{{ Str::limit(Str::ucfirst(Str::lower($item->name)),20,'...') }}</h5>
                                        <p class="card-text">
                                            {{ Str::limit(Str::ucfirst(Str::lower(strip_tags($item->brief))),50,'...') }}
                                        </p>
                                    </div>
                                    <div class="card-footer">
                                        <span class="badge badge-primary">{{ $item->merchants->sortByDesc('commission_percentage')->first()->pivot->commission_percentage}}EP/100€</span>
                                    </div>
                                </div>
                           </div>
                        @endforeach
                    </div>
                @endforeach

                <hr>
                <h2>Voucher della GDO che garantiscono la categoria di EP selezionata</h2>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Nome</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($vouchers as $item)
                            <tr>
                                <td><img src="/{!! $item->cover !!}" width="50"></td>
                                <td>
                                    {{$item->name}}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@push('after_scripts')
<script>
    $('#epcategory').change(function (e){
        $("#form").submit();
    });
</script>


@endpush
@endauth

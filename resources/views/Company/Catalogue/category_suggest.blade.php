@auth

@extends('admin_custom.top_left')
@section('content')



@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
{{--
<div class="row">
    <div class="col-md-8 bold-labels">
        <h2>Categorie in approvazione</h2>
        <div class="card">
            <div class="card-body">
                <ul>
                    @forelse ($categoryToApprove as $item)
                        <li>{{$item->name}}</li>
                    @empty
                         <div>non ci sono categorie in attesa d'importazione</div>
                    @endforelse
                </ul>
            </div>
        </div>
    </div>
</div>
 --}}
<form id="product-form" action="{{ route('Company.categorySuggestUp') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="active" value='0'>
    <input type="hidden" name="parent_id">
    <div class="row">
        <div class="col-md-8 bold-labels">
            <h2>Suggerisci una categoria</h2>
            <div class="card">
                <div class="card-body">
                    <div class="help-block">
                            <p class="muted">
                                <small>
                                    La categoria suggerita verrà approvata o meno entro 24/48h.
                                </small>
                            </p>
                        </div>
                    <div class="form-group">
                        <h4>Seleziona la categoria padre</h4>
                         <ul class="nav nav-tabs">
                            @foreach ($product_types as $_item)
                            @php
                                $key = $_item->id;
                                $item = $_item->name;
                            @endphp
                                <li class="nav-item">
                                    @if(isset($product))
                                        <a class="categories-tab-nav-link nav-link @if($product->category->isDescendantOf($_item->category)) active @endif" data-toggle="tab" data-type="{{ $key }}" data-target="#product_type_{{ $key }}">{{ $item }}</a>
                                    @else
                                        <a class="categories-tab-nav-link nav-link @if($key == 1) active @endif" data-toggle="tab" data-type="{{ $key }}" data-target="#product_type_{{ $key }}">{{ $item }}</a>
                                    @endif
                                </li>
                             @endforeach
                        </ul>
                           <div class="tab-content mb-2">
                          @foreach ($product_types as $_item)
                            @php
                                $key = $_item->id;
                                $item = $_item->name;
                            @endphp
                            @if(isset($product))
                                <div class="categories-tab tab-pane container @if($product->category->isDescendantOf($_item->category)) active @else fade @endif"  id="product_type_{{ $key }}">
                            @else
                                <div class="categories-tab tab-pane container @if($key == 1) active @else fade @endif"  id="product_type_{{ $key }}">
                            @endif
                                        <select name="" id="categories_type_{{ $key }}" class="form-control select2 category_id_select">
                                            <option value="">Seleziona tra i {{ $item }}</option>
                                            @foreach ($categories[$key] as $category)
                                                <option @if (isset($product))
                                                    @if($product->category_id == $category->id)
                                                        selected
                                                    @endif
                                                @else

                                                @endif value="{{ $category->id }}">
                                                    {{ $category->stored_ancestors_breadcrumb }} > <b>{{ $category->name }}</b>
                                                </option>
                                            @endforeach

                                        </select>

                                </div>
                            @endforeach
                            </div>
                        <label for="name">Nome sottocategoria suggerita <span class="text-danger">*</span></label>
                        <input type="text" required name="name" id="name" placeholder="Nome categoria" class="form-control " value="">
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12"><button type="submit" class="btn btn-primary btn-lg  mt-2">salva</button></div>
    </div>
<form>


@endsection

@push('after_scripts')
    <link href="/packages/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css">
    <link href="/packages/select2-bootstrap-theme/dist/select2-bootstrap.min.css" rel="stylesheet" type="text/css">
    <script src="/packages/select2/dist/js/select2.full.min.js"></script>
    <script>
        var tipology_id_sel = '0';
        var category_id_sel = '0';
        var subcategory = $('#parent_id');
        var types = {!! json_encode($product_types_arr) !!}
          $('a[data-toggle="tab"].categories-tab-nav-link').on('shown.bs.tab', function (e) {
                var key = $(e.target).data("type");
                $('input[type="hidden"][name="parent_id"]').val($('#categories_type_'+key).val())
            });

            $('.category_id_select').change(function(){
                $('input[type="hidden"][name="parent_id"]').val($(this).val());
            })
             $(".select2").select2({
            theme: "bootstrap",
        });
    </script>
@endpush


@endauth


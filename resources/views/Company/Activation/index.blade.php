@auth

@extends('admin_custom.top_left')
@section('content')

<h2>Attiva il tuo account</h2>


    <div class="row">
        <div class="col-md-4">
            <div class="card">
                    <div class="card-body">
                        <img class="img-fluid" src="{{ url('/front/img/merchant_pack.jpg') }}" alt="">
                        Acquista il Merchant Pack al prezzo di
                        <h1>@fp($price) <small>più iva</small></h1>
                        <p class="text-muted">
                            {{-- <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed faucibus nisi quis justo vestibulum molestie.</small>  --}}
                        </p>
                    </div>
                </div>
            </div>
        <div class="col-md-8">
             <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="card-holder-name">Paga con carta di credito</label>
                                <input type="text" id="card-holder-name" placeholder="Nome del titolare della carta" class="form-control">
                            </div>
                            <div id="new-card-form-wrapper"></div>
                            <button data-secret="{{ $client_secret_intent->client_secret }}" id="card-button" class="btn btn-sm btn-primary btn-block mt-4">Acquista lo starter pack</button>
                            </div>
                        </div>
        </div>
    </div>



@endsection

@push('after_scripts')

    <script src="https://js.stripe.com/v3/"></script>
    <script>
        //var _token = '{{ csrf_token() }}';

         $(() => {
            const stripe = Stripe('{{ env('STRIPE_KEY') }}');
            const elements = stripe.elements();
            const cardElement = elements.create('card');
            cardElement.mount('#new-card-form-wrapper');

            const cardHolderName = document.getElementById('card-holder-name');
            const cardButton = document.getElementById('card-button');
            const clientSecret = cardButton.dataset.secret;

            cardButton.addEventListener('click', async (e) => {

                const { setupIntent, error } = await stripe.confirmCardSetup(
                    clientSecret, {
                        payment_method: {
                            card: cardElement,
                            billing_details: { name: cardHolderName.value }
                        }
                    }
                );

                if (error) {
                    alert('unsuccess')
                } else {
                        $.post('{{ route('Company.activate.store') }}',setupIntent,function(r){
                            if(r.status == 1){
                                alert('Congratulazioni, hai attivato il tuo account!');
                                location.replace('/admin');
                            }else{
                                alert('Si è verificato un problema');
                                location.reload();
                            }

                        },'json')
                    }
            });
        })

    </script>

@endpush


@endauth


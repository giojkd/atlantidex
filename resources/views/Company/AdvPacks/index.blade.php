.
@auth

    @extends('admin_custom.top_left')
    @section('content')


        @if (!is_null($advpacks) && $advpacks->count() > 0 && $activeadvpackpurchases->count() == 0)
            @foreach ($advpacks->chunk(4) as $advpackChunk)
                <div class="row">
                    @foreach ($advpackChunk as $pack)
                        <div class="col-md-3">
                            <div class="card">
                                <div class="card-header">
                                    @if (!is_null($pack->photos[0]))
                                        <img src="{{ url($pack->photos[0]) }}" alt="" class="img-fluid mb-4 mt-2">
                                    @endif
                                    <h3>{{ $pack->name }}</h3>
                                </div>
                                <div class="card-body">
                                    <div>{!! $pack->description !!}</div>
                                    <ul class="list">
                                        {{-- @if (!is_null($pack->revenue_bonus) && $pack->revenue_bonus > 0)<li> {{ $pack->revenue_bonus }}% di revenue bonus </li>@endif
                                    @if (!is_null($pack->merchant_bonus) && $pack->merchant_bonus > 0)<li> @fp($pack->merchant_bonus) di merchant bonus e {{ $pack->merchant_bonus_eps }} EPs ad ogni nuova affiliazione di aziende </li>@endif
                                    @if (!is_null($pack->merchant_booster_bonus) && $pack->merchant_booster_bonus > 0)<li> @fp($pack->merchant_booster_bonus) di merchant booster bonus per le prime {{ $pack->merchant_booster_bonus_affiliations }} affiliazioni di aziende e {{ $pack->merchant_booster_bonus_eps }} </li>@endif
                                    @if (!is_null($pack->merchant_marketing_bonus) && $pack->merchant_marketing_bonus > 0)<li> {{ $pack->merchant_marketing_bonus }} EPs ogni @fp(100) di spazi pubblicitari acquistati dalle aziende affiliate grazie al merchant marketing bonus  </li>@endif
                                    @if (!is_null($pack->granted_eps) && $pack->granted_eps > 0)<li> {{ $pack->granted_eps }} EPs all'acquisto di questo pack </li>@endif
                                    @if (!is_null($pack->direct_commission) && $pack->direct_commission > 0)<li> {{ $pack->direct_commission }}% lordo di commissione sugli acquisti di voucher effettuati dagli affiliati diretti </li>@endif
                                    @if (!is_null($pack->merchant_bonus_affiliations) && $pack->merchant_bonus_affiliations > 0)<li>Infinite affiliazioni per le aziende</li>@endif
                                    @if (!is_null($pack->network_affiliations) && $pack->network_affiliations > 0)<li>Infinite affiliazioni per i networker</li>@endif --}}
                                    </ul>
                                </div>

                                <div class="card-footer">
                                    @if ($user->canPurchaseAdvPack($pack))
                                        <button data-toggle="modal" class="btn btn-primary btn-block"
                                            data-amount="{{ $pack->price }}" data-pack_id="{{ $pack->id }}"
                                            data-target="#voucherPurchaseModal">Acquista questo pack @if (!is_null($pack->price) && $pack->price > 0) al prezzo di
                                                <b>@fp($pack->price)</b> IVA inclusa @endif</button>
                                    @else
                                        <a href="{{ Route('MyAtlantinex.documents.index') }}"
                                            class="btn btn-primary btn-block">Firma prima i documenti richiesti</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endforeach
        @else
            {{-- <div class="alert alert-info">
                Non ci sono pack disponibili
            </div>  --}}
        @endif

        @if (!is_null($activeadvpackpurchases) && $activeadvpackpurchases->count() > 0)


            <div class="card">
                <div class="card-header">
                    <h3>Grazie al pack {{ $activeadvpackpurchases->first()->advpack->name }} che hai acquistato puoi usufruire di</h3>
                </div>
                <div class="card-body">
                    {!! $activeadvpackpurchases->first()->advpack->description !!}
                </div>
                <div class="card-footer">
                    <h3>Hai acquistato il pack in data {{ $activeadvpackpurchases->first()->created_at->format('d/m/Y') }} e scadrà il {{ $activeadvpackpurchases->first()->expires_at->format('d/m/Y') }}</h3>
                </div>
            </div>

            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th>Prodotto</th>
                        @foreach ($placements as $index => $item)
                            @if ($activeadvpackpurchases->first()->advpack->$index > 0)
                                <th>
                                    {{ $item['name'] }} ({{ $activeadvpackpurchases->first()->advpack->$index }})
                                </th>
                            @endif
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    @foreach ($products as $product)
                        <tr>
                            <td>
                              ({{ $product->id }})  {{ $product->sku }} | {{ $product->name }}
                            </td>
                            @foreach ($placements as $index => $item)
                                @if ($activeadvpackpurchases->first()->advpack->$index > 0)
                                    <td>
                                        <input
                                            @if (isset($activeInsertions[$activeadvpackpurchases->first()->id][$product->id][$index]))
                                                @if ($activeInsertions[$activeadvpackpurchases->first()->id][$product->id][$index]['active'])
                                                    checked
                                                @endif
                                            @endif

                                            class="{{ $index }}_checkbox placement_checkbox"
                                            type="checkbox"
                                            data-_token="{{ csrf_token() }}"
                                            data-placement="{{ $index }}"
                                            data-product_id="{{ $product->id }}"
                                            data-advpackpurchase_id="{{ $activeadvpackpurchases->first()->id }}"
                                        >
                                        @if (isset($activeInsertions[$activeadvpackpurchases->first()->id][$product->id][$index]))
                                           <i class="fa fa-eye"></i> {{ $activeInsertions[$activeadvpackpurchases->first()->id][$product->id][$index]['views'] }} |
                                           <i class="fas fa-mouse"></i> {{ $activeInsertions[$activeadvpackpurchases->first()->id][$product->id][$index]['clicks'] }}
                                        @endif
                                    </td>
                                @endif
                            @endforeach
                        </tr>
                    @endforeach
                </tbody>
            </table>



        @endif

    @endsection

    @push('before_closing_body')
        @include('components.payment_modal',
        [
            'payment_modal_title' => 'Acquista un ADV Pack',
            'show_vouchers_amount' => false,
            'card_is_disabled' => true
        ])
    @endpush


@push('after_scripts')
<script src="https://js.stripe.com/v3/"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css" integrity="sha512-kq3FES+RuuGoBW3a9R2ELYKRywUEQv0wvPTItv3DSGqjpbNtGWVdvT8qwdKkqvPzT93jp8tSF4+oN4IeTEIlQA==" crossorigin="anonymous" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>

        var eps,amount,method, pack_id;
        var payment_method_id = '';
        var _token = '{{ csrf_token() }}';
        var route = '{{ Route('Company.adv-packs.store') }}';

        $('.placement_checkbox').click(function(e){
            var confirmSentence = '';
            var checkbox = $(this);
            if(checkbox.is(':checked')){
                confirmSentence = 'Sei sicuro di voler sponsorizzare il prodotto?';
            }else{
                confirmSentence = 'Sei sicuro di voler rimuovere il prodotto dalle sponsorizzate? Perderai tutti i dati delle sue performance.' ;
            }
            if(confirm(confirmSentence)){
                $.post('{{ Route('Company.toggleAdvPackProduct') }}',checkbox.data(),function(r){

                },'json')
            }else{
               return false;
            }
        })


        function performStandardChecks(){
            /*
            amount = parseInt($('#amount').val());
              if(isNaN(amount) || amount == 0){
                    alert('Il valore inserito non è valido');
                    return false;
                }
            */
            return true;
        }


        function postPurchase(){

            $.post(route,{
                _token,method,amount,payment_method_id,pack_id
            },function(r){
                if(r.status == 1){
                    alert('Congratulazioni, hai acquistato il pack!');
                    location.href= '/company/adv-packs';
                }
                console.log(r);
            },'json')
        }

        $(function(){

            $('.select2').select2({
                'theme' : 'bootstrap'
            })

            $('#voucherPurchaseModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                amount = button.data('amount') // Extract info from data-* attributes
                pack_id = button.data('pack_id');
                // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this)
            })
        })

        var maxAmountForWalletUsage = parseFloat({{ $user->wallet_balance }});

        $(function(){
            $('#useWallet').click(function(){
                if(!performStandardChecks()){
                    return false;
                }
                if(amount > maxAmountForWalletUsage ){
                    alert('Il valore inserito supera la disponibilità del tuo wallet');
                    return false;
                }
                method = 'wallet';
                postPurchase();
            })
            $('.stored-credit-card').click(function(){
                if(!performStandardChecks()){
                    return false;
                }
                payment_method_id = $(this).data('id');
                method = 'stored_credit_card';
                postPurchase();
            });
        })


        $(() => {
            const stripe = Stripe('{{ env('STRIPE_KEY') }}');
            const elements = stripe.elements();
            const cardElement = elements.create('card');
            //cardElement.mount('#new-card-form-wrapper');

            const cardHolderName = document.getElementById('card-holder-name');
            const cardButton = document.getElementById('card-button');
            //const clientSecret = cardButton.dataset.secret;
            /*
            cardButton.addEventListener('click', async (e) => {



                if(!performStandardChecks()){
                    return false;
                }else{

                      const { setupIntent, error } = await stripe.confirmCardSetup(
                    clientSecret, {
                        payment_method: {
                            card: cardElement,
                            billing_details: { name: cardHolderName.value }
                        }
                    }
                );

                if (error) {
                    alert('unsuccess')
                } else {
                        console.log(setupIntent);
                        $.post('{{ route('MyAtlantinex.attachPaymentMethodToUser') }}',setupIntent,function(r){
                            payment_method_id = setupIntent.payment_method;
                            method = 'pay_with_new_card';
                            postPurchase();
                        },'json')
                    }
                }


            });
            */
        })
    </script>
@endpush




@endauth

@auth

@extends('admin_custom.top_left')
@section('content')

<h2>I Dati della tua Azienda</h2>
<div class="row">
    <div class="col-md-8">
        <div class="card">
    <div class="card-body">
        <p>
            @if(!is_null($user->packpurchases))
            <ul>
                @foreach ($user->packpurchases as $item)
                    <li>
                    Hai acquistato il pack <b>{{ $item->pack->name }}</b> il: <b>{{ $item->created_at->format('d/m/Y') }}</b>
                    @if($item->pack->is_renewable)
                        @if (!is_null($item->pack->expires_after))
                            scade il <b>{{ date('d/m/Y',strtotime($item->created_at.'+'.$item->pack->expires_after)) }}</b> potrai rinnovarlo a partire dal
                            <b>{{ date('d/m/Y',strtotime($item->created_at.'+'.$item->pack->expires_after.'-30days')) }}</b>
                        @endif
                        @if (!is_null($item->pack->renew_price))
                            al costo di <b>@fp($item->pack->renew_price)</b>
                        @endif
                    @endif
                </li>
                @endforeach

            </ul>
            @endif
        </p>
    </div>
</div>
    </div>
</div>
 <div class="row">
    <div class="col-md-8">
        <div class="card">
            <div class="card-body">
                <form action="/company/company-data/{{$user->id}}" method="POST">
                    @csrf
                    <input type="hidden" name="address" value="{{json_encode($user->address)}}">
                    <input type="hidden" name="country_id" value="2">
                    <input name="_method" type="hidden" value="PUT">
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label class="control-label" for="name">{{ trans('backpack::base.name') }}</label>
                                <div>
                                    <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="name" value="{{ $user->name }}">
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label class="control-label" for="surname">{{ trans('backpack::permissionmanager.surname') }}</label>
                                <div>
                                    <input type="text" class="form-control{{ $errors->has('surname') ? ' is-invalid' : '' }}" name="surname" id="surname" value="{{ $user->surname }}">
                                    @if ($errors->has('surname'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('surname') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="phone">{{ trans('backpack::permissionmanager.phone') }}</label>
                        <div>
                            <input type="text" disabled class="unenableable form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" id="phone" value="{{ $user->phone }}">
                            @if ($errors->has('phone'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="company_name">{{ trans('backpack::permissionmanager.company_name') }}</label>
                        <div>
                            <input type="text" class="form-control{{ $errors->has('company_name') ? ' is-invalid' : '' }}" name="company_name" id="company_name" value="{{ $user->company_name }}">
                            @if ($errors->has('company_name'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('company_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label class="control-label" for="vat_number">{{ trans('backpack::permissionmanager.vat_number') }}</label>
                                <div>
                                    <input type="text" class="form-control{{ $errors->has('vat_number') ? ' is-invalid' : '' }}" name="vat_number" id="vat_number" value="{{ $user->vat_number }}">
                                    @if ($errors->has('vat_number'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('vat_number') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col">
                             <div class="form-group">
                                <label class="control-label" for="company_fiscal_code">Codice fiscale della società</label>
                                <div>
                                    <input id="fiscal_code" type="text" class="form-control{{ $errors->has('company_fiscal_code') ? ' is-invalid' : '' }}" name="company_fiscal_code" id="company_fiscal_code" value="{{ $user->company_fiscal_code }}">
                                    @if ($errors->has('company_fiscal_code'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('company_fiscal_code') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <p class="text-muted"><small>se uguale alla partita IVA reinserire</small></p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="address">Sede della società</label>
                        <div>
                            <input id="userAddress" type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" id="userAddress" value="{{$user->address['value']}}">
                            @if ($errors->has('address'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('address') }}</strong>
                                </span>
                            @endif
                        </div>
                        <p class="text-muted"><small>Nel caso in cui non trovassi l'indirizzo esatto inserisci l'indirizzo più vicino suggerito dal sistema e comunicaci l'indirizzo corretto via email a amministrazione@atlantidex.com</small></p>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label class="control-label" for="sdi">Tipo di società</label>
                            <div>
                                <select name="companytype_id" id="companytype_id" class="form-control {{ $errors->has('companytype_id') ? ' is-invalid' : '' }}">
                                    @foreach ($companyTypes as $companyType)
                                        <option value="{{ $companyType->id }}" @if($user->companytype_id == $companyType->id) selected @endif>
                                            {{ $companyType->name }}
                                        </option>
                                    @endforeach
                                </select>


                                @if ($errors->has('companytype_id'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('companytype_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label" for="pec">Capitale sociale</label>
                            <div>

                                    <input type="text" class="form-control {{ $errors->has('share_capital') ? ' is-invalid' : '' }}" name="share_capital" id="share_capital" value="{{ $user->share_capital }}">
                                @if ($errors->has('share_capital'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('share_capital') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label class="control-label" for="email_for_invoices">Email per invio fatture</label>
                            <div>
                                <input type="email" class="form-control{{ $errors->has('email_for_invoices') ? ' is-invalid' : '' }}" name="email_for_invoices" id="email_for_invoices" value="{{ $user->email_for_invoices}}">

                                @if ($errors->has('email_for_invoices'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email_for_invoices') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label" for="pec">Persona di contatto</label>
                            <div>
                                <input type="text" class="form-control{{ $errors->has('contact_person') ? ' is-invalid' : '' }}" name="contact_person" id="contact_person" value="{{ $user->contact_person }}">

                                @if ($errors->has('contact_person'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('contact_person') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="control-label" for="email_for_invoices">Fax</label>
                            <div>
                                <input type="text" class="form-control{{ $errors->has('fax') ? ' is-invalid' : '' }}" name="fax" id="fax" value="{{ $user->fax }}">

                                @if ($errors->has('fax'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('fax') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label" for="pec">Settore aziendale</label>
                            <div>
                                <input type="text" class="form-control{{ $errors->has('corporate_sector') ? ' is-invalid' : '' }}" name="corporate_sector" id="corporate_sector" value="{{ $user->corporate_sector }}">

                                @if ($errors->has('corporate_sector'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('corporate_sector') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                         <div class="col-md-4">
                            <label class="control-label" for="website">Sito internet</label>
                            <div>
                                <input type="text" class="form-control{{ $errors->has('website') ? ' is-invalid' : '' }}" name="website" id="website" value="{{ $user->website }}">

                                @if ($errors->has('website'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('website') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-3">
                            <label class="control-label" for="sdi">SDI</label>
                            <div>
                                <input type="text" class="form-control{{ $errors->has('sdi') ? ' is-invalid' : '' }}" name="sdi" id="sdi" value="{{ $user->sdi }}">

                                @if ($errors->has('sdi'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('sdi') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label" for="pec">PEC</label>
                            <div>
                                <input type="email" class="form-control{{ $errors->has('pec') ? ' is-invalid' : '' }}" name="pec" id="pec" value="{{ $user->pec }}">

                                @if ($errors->has('pec'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('pec') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label" for="pec">Codice REA</label>
                            <div>
                                <input type="text" class="form-control{{ $errors->has('company_registration_number') ? ' is-invalid' : '' }}" name="company_registration_number" id="company_registration_number" value="{{ $user->company_registration_number }}">

                                @if ($errors->has('company_registration_number'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('company_registration_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label" for="pec">Telefono aziendale</label>
                            <div>
                                <input type="text" class="form-control{{ $errors->has('company_phone') ? ' is-invalid' : '' }}" name="company_phone" id="company_phone" value="{{ $user->company_phone }}">

                                @if ($errors->has('company_phone'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('company_phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="">Descrizione breve</label>
                        <textarea class="form-control{{ $errors->has('short_description') ? ' is-invalid' : '' }}" name="short_description" id="" rows="2" >{{ $user->short_description }}</textarea>
                         @if ($errors->has('short_description'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('short_description') }}</strong>
                                    </span>
                                @endif
                    </div>
                     <div class="form-group">
                        <label for="">Descrizione</label>
                        <textarea class="form-control" name="description" id="" rows="8" >{{ $user->description }}</textarea>
                    </div>

                    <div class="form-group">
                        <div>
                            <a href="javascript:" onclick="unlockDataEdit(this)" class="btn btn-block btn-primary" >
                                Modifica i tuoi dati
                            </a>
                            <button type="submit" class="btn btn-block btn-primary d-none">
                                Conferma i tuoi dati
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        @if(!is_null($user->sponsor))

            <div class="card">
                <div class="card-body">
                    <h4>Il tuo sponsor</h4>
                    <table class="table table-striped">
                        <tbody>
                            <tr><td><b>Tessera numero</b></td><td>{{ $user->sponsor->code }}</td></tr>
                            <tr><td><b>Nome </b></td><td>{{ $user->sponsor->full_name }}</td></tr>
                            <tr><td><b>Email</b></td><td>{{ $user->sponsor->email }}</td></tr>
                            <tr><td><b>Telefono</b></td><td>{{ $user->sponsor->phone }}</td></tr>
                        </tbody>
                    </table>
                </div>
            </div>

        @endif
         @if(!is_null($user->virtual_sponsor) && $user->virtual_sponsor->id != $user->sponsor->id)

                <div class="card">
                    <div class="card-body">
                        <h4>Il tuo Merchant Marketer</h4>
                        <table class="table table-striped">
                            <tbody>
                                <tr><td><b>Tessera numero</b></td><td>{{ $user->virtual_sponsor->code }}</td></tr>
                                <tr><td><b>Nome </b></td><td>{{ $user->virtual_sponsor->full_name }}</td></tr>
                                <tr><td><b>Email</b></td><td>{{ $user->virtual_sponsor->email }}</td></tr>
                                <tr><td><b>Telefono</b></td><td>{{ $user->virtual_sponsor->phone }}</td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>

        @endif
    </div>
</div>
@endsection

@push('after_scripts')
    <script src="https://cdn.jsdelivr.net/npm/places.js@1.18.2"></script>
    <script>

        function unlockDataEdit(btn){

            $(btn).addClass('d-none');
            $('button[type="submit"]').removeClass('d-none');
            $('input:not(.unenableable),textarea').removeAttr('disabled');
        }

    $(() => {

        $('input:not([type="hidden"]),textarea').attr('disabled','disabled');

        @if(session()->has('message'))
            new Noty({
                text: '{{ session()->get('message') }}',
                type:'success',
                animation: {
                    open: 'animated bounceInRight', // Animate.css class names
                    close: 'animated bounceOutRight' // Animate.css class names
                }
            }).show();
        @endif
            $('.addressType').change(function() {
                if($(this).val() == 'billing'){
                    $(this).closest('form').find('.companyFields').fadeIn();
                }else{
                    $(this).closest('form').find('.companyFields').fadeOut();
                }
            })

           var placesAutocomplete = places({
                appId: '{{env('ALGOLIA_PLACES_APP_ID')}}',
                apiKey: '{{env('ALGOLIA_PLACES_API_KEY')}}',
                container: document.querySelector('#userAddress')
            });
            placesAutocomplete.on('change', e => {
                $('input[name="address"]').val(JSON.stringify(e.suggestion));
            });

              var newAddressPlacesAutocomplete = places({
                appId: '{{env('ALGOLIA_PLACES_APP_ID')}}',
                apiKey: '{{env('ALGOLIA_PLACES_API_KEY')}}',
                container: document.querySelector('#newAddress')
            });
            newAddressPlacesAutocomplete.on('change', e => {
                $('#newAddressForm input[name="address"]').val(JSON.stringify(e.suggestion));
            });
    })
    </script>
@endpush


@endauth

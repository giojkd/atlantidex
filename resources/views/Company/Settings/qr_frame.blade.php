<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>QR-Frame</title>

</head>

<body>
    <div style="text-align: center">
            <img style="width: 90%" src="{{ public_path('uploads/logo-atlantidex-payoff-big.png') }}" alt="">
            <h1 style="font-family: sans-serif; font-size: 40px; color: #555">Scansiona il QRCode<br>ed entra anche tu a far parte del<br>circuito Atlantidex!</h1>
            <div style="height: 100px"></div>
            <img src="{{ $qrCodeSrc }}" alt="" style="border: 8px solid #E94E14; border-radius: 24px; height: 240px" >
            <div style="height: 160px"></div>
            <div style="font-family: sans-serif; color: #555">
                Seguici su
            </div>
            <div style="height: 20px"></div>
            <div>

                <img style="height: 50px; display: inline-block; margin-right: 15px" src="{{ public_path('uploads/social_icons/facebook.png') }}" alt="">
                <img style="height: 50px; display: inline-block; margin-right: 15px" src="{{ public_path('uploads/social_icons/instagram.png') }}" alt="">
                <img style="height: 50px; display: inline-block; margin-right: 15px" src="{{ public_path('uploads/social_icons/linkedin.png') }}" alt="">
            </div>
    </div>
</body>

</html>

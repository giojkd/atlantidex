@auth

@extends('admin_custom.top_left')

@section('content')

    <h5>Seleziona la filiale</h5>

    <ul class="nav nav-tabs">
        @foreach ($merchants as $merchant)
            <li class="nav-item">
                <a href="{{ Route('Company.pos.index') }}?merchant_id={{ $merchant->id }}" class="nav-link @if ($merchant->id == $selectedMerchantId) active @endif" href="">{{ $merchant->name }}</a>
            </li>
        @endforeach
    </ul>





    <canvas hidden="" id="qr-canvas"></canvas>

    <div class="row" id="userCodeManualInputWrapper">
        <div class="col-md-6">
            <div class="input-group my-4">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="">+39</span>
                </div>
                <input id="userCode" placeholder="Oppure digita qui il codice della card Atlantidex del cliente" type="text" class="form-control" placeholder="" aria-label="" aria-describedby="basic-addon1">
                <div class="input-group-append">
                    <button onclick="qrCodeReadingCallback('39'+$('#userCode').val())" class="btn btn-primary" type="button">Conferma</button>
                </div>
            </div>
        </div>
    </div>

    {{-- <button onclick="qrCodeReadingCallback('39000000001')">Find Qr Code</button>  --}}
    <div id="qr-result" hidden="">
        <div class="card">
            <div class="card-body">
                <a id="btn-scan-qr">
                    <img src="https://uploads.sitepoint.com/wp-content/uploads/2017/07/1499401426qr_icon.svg">
                <a/>
                <br>
                <b>Codice:</b> <span id="outputData"></span> <br>
                <b>Nome:</b> <span id="outputDataName"></span> <br>
                <b>Cognome:</b> <span id="outputDataSurname"></span>
            </div>
        </div>

    </div>

    <button id="btn-reset" class="btn btn-primary mb-4">Scansiona un altro codice</button>

    <div class="row">
        <div class="col-md-6">

            <div class="card">
                <h3 class="card-header">Cerca tra i tuoi prodotti</h3>
                <div class="card-body">
                    <div class="ais-InstantSearch">
                        <div class="row">
                            {{--
                            <div class="col-md-3">

                                <h2>Brands</h2>
                                <div id="brand-list"></div>
                            </div>
                            --}}
                            <div class="col-md-12">

                                <div id="searchbox" class="ais-SearchBox"></div>
                                <div id="clear-refinements"></div>
                                <div id="hits"></div>
                                <div id="pagination"></div>
                                <div id="refinement-id" class="d-none"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-6">

            <div class="card">
                <h3 class="card-header">Carrello</h3>
                <div class="card-body">
                    @verbatim
                        <div id="app">
                            <button class="btn btn-sm btn-outline-success mb-4" v-on:click="addNewProduct">Inserisci prodotto non presente a catalogo</button>
                            <div id="cart">
                                <div class="cartProduct" v-for="(product, index) in cartProducts">
                                    <div class="row">
                                        <div class="col">
                                            <div class="shadow p-2 mb-4" v-if="product.id != ''">
                                                <img height="50" class="mb-2 d-inline-block" :src="product.cover.replace('atlantidex.eu.ngrok.io','atlantinex.com')">
                                                <h5 class="d-inline-block mb-2">{{ product.name }}</h5>
                                                <table class="table">
                                                    <tr>
                                                        <td>
                                                            <label for="">Prezzo</label>
                                                            <div>
                                                                <span v-if="cartProducts.length > 1 || product.quantity > 1" class="btn btn-outline-danger btn-sm" v-on:click="removeQuantity(product)"><i class="fa fa-minus"></i></span>
                                                                <span class="btn btn-outline-success btn-sm" v-on:click="addQuantity(product)"><i class="fa fa-plus"></i></span>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="d-inline-block product-price">
                                                                <input v-model="product.price" type="text" class=" mr-1"> <span class="text-nowrap">x {{ product.quantity }} = {{ product.price * product.quantity }}</span>
                                                            </div>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label for="">Commissione Atlantidex</label>
                                                            <p class="text-muted"><small>Si ricorda al merchant che può applicare o la commissione minima o una superiore relativa alla categoria del prodotto inserito a carrello come dichiarato nella sezione “Categorie offline” a sua discrezione</small></p>
                                                        </td>
                                                        <td>
                                                            <input :min="userMinCommission" v-model="product.commission" type="number" step="any" class="product-price mr-2 text-center" style="width: 80px">% (+iva)
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                              <div class="shadow p-2 mb-4" v-if="product.id == ''">
                                                <input v-model="product.name" type="text" class="product-name">
                                                <table class="table">
                                                    <tr>
                                                        <td>
                                                            <label for="">Prezzo</label>
                                                            <div>
                                                                <span v-if="cartProducts.length > 1 || product.quantity > 1" class="btn btn-outline-danger btn-sm" v-on:click="removeQuantity(product)"><i class="fa fa-minus"></i></span>
                                                                <span class="btn btn-outline-success btn-sm" v-on:click="addQuantity(product)"><i class="fa fa-plus"></i></span>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="d-inline-block product-price">
                                                                <input v-model="product.price" type="text" class=" mr-1"> <span class="text-nowrap">x {{ product.quantity }} = {{ product.price * product.quantity }}</span>
                                                            </div>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label for="">Commissione Atlantidex</label>
                                                            <p class="text-muted"><small>Si ricorda al merchant che può applicare, a sua discrezione, o la commissione minima o una superiore relativa alla categoria del prodotto inserito a carrello come dichiarato nella sezione “Categorie offline”</small></p>
                                                        </td>
                                                        <td>
                                                            <input :min="userMinCommission" v-model="product.commission" type="number" step="any" class="product-price mr-2 text-center" style="width: 80px">% (+iva)
                                                        </td>
                                                    </tr>
                                                </table>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div v-if="grand_total > 0">
                                <hr>
                                <div class="d-none">
                                    <label for="basic-url">Sconto</label>
                                    <p class="text-muted"><small>Utilizzare questo campo nel caso in cui si voglia variare il prezzo inserito a catalogo del prodotto.</small></p>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon2">-</span>
                                        </div>
                                        <input v-if type="number" step="0.01" class="form-control only-numbers text-center" v-model="discount" :max="grand_total-1">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">€</span>
                                        </div>
                                    </div>
                                    <hr>
                                </div>

                                <div >
                                    <h2>Totale: €{{ grand_total.toFixed(2) }}</h2>
                                </div>
                            </div>
                            <div v-if="cartProducts.length == 0">
                                <p class="alert alert-secondary">
                                    Il carrello è ancora vuoto
                                </p>
                            </div>

                            <div v-if="user == null">
                                <div class="alert alert-secondary">
                                    Devi scansionare il codice del cliente prima di procedere
                                </div>
                            </div>

                            <div v-if="user != null && grand_total > 0">
                                <h3>Pagamento</h3>
                                <div class="card">
                                    <div class="card-body">


                                            <ul class="list list-unstyled">

                                                <li>
                                                    <label><input v-model="selectedPaymentMethod" type="radio" name="paymentmethod" value="paid_by_card"> Pagato con carta presso il punto vendita </label>
                                                </li>

                                                <li>
                                                    <label><input v-model="selectedPaymentMethod" type="radio" name="paymentmethod" value="paid_in_cache"> Pagato in contanti, bonifico, RiBa o RID </label>
                                                </li>

                                                 <li v-for="method in user.paymentMethos">
                                                    <label><input v-model="selectedPaymentMethod" :value="method.id" type="radio" name="paymentmethod" > Paga ora con {{ method.brand }} {{ method.last4 }} {{ method.exp_month }}/{{ method.exp_year }}</label>
                                                </li>

                                                <li>
                                                    <label><input v-model="selectedPaymentMethod" type="radio" name="paymentmethod" value="bill_user_wallet"> Addebita il wallet </label>
                                                </li>

                                                 <li v-if="discount == 0">
                                                    <label><input v-model="selectedPaymentMethod" type="radio" name="paymentmethod" value="use_vouchers"> Usa i voucher <span class="text-muted"><small>(Verranno utilizzati automaticamente per il pagamento i voucher Atlantidex della categoria maggiore rispetto al prodotto o servizio acquistati)</small></span></label>

                                                </li>

                                            </ul>

                                            <button v-if="!pendingOtpConfirmation" class="btn btn-primary btn-block" v-on:click="confirmOrder">Conferma</button>


                                            <button v-if="pendingOtpConfirmation && !otpRequested" class="btn btn-primary btn-block" v-on:click="requestOtp">Richiedi OTP</button>

                                            <div v-if="otpRequested && !otpConfirmed">
                                                <span style="cursor: pointer" v-on:click="requestOtp" class="text-muted">Richiedi un altro codice</span>
                                                <input v-model="otpCode" type="text" class="form-control mb-1" placeholder="Inserisci qui il codice ricevuto dal cliente">
                                                <button class="btn btn-primary btn-block" v-on:click="confirmOtp">Conferma l'OTP</button>
                                            </div>

                                    </div>
                                </div>

                            </div>

                        </div>
                    @endverbatim
                </div>
            </div>
        </div>
    </div>

@endsection


@push('after_scripts')

<script>
    var defaultMinCommission = 5;
    var products = @json($products->keyBy('id'));
    var presetFacets = @json(['id' => $products->pluck('id')]);
    console.log(products);
    var listToKeep = @json($products->pluck('id'));
    var loadUserForPosRoute = '{{ Route('Company.loadUserForPos') }}';
    var confirmOrderRoute = '{{ Route('Company.confirmOrder') }}';
    var otpRequestRoute = '{{ Route('Company.requestOtp') }}';
    var otpConfirmationRoute = '{{ Route('Company.confirmOtp') }}';
    var postToken = '{{ csrf_token() }}';
    var merchantId = {{ $selectedMerchantId }};
    var userMinCommission = defaultMinCommission;
    var minCommissionForCompanies =  {{ Auth::user()->min_commission }};
</script>

@verbatim

    <script src="/front/js/qr_packed.js"></script>
    <script src="/front/js/qrCodeScanner.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/algoliasearch@4.5.1/dist/algoliasearch-lite.umd.js" integrity="sha256-EXPXz4W6pQgfYY3yTpnDa3OH8/EPn16ciVsPQ/ypsjk=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/instantsearch.js@4.8.3/dist/instantsearch.production.min.js" integrity="sha256-LAGhRRdtVoD6RLo2qDQsU2mp+XVSciKRC8XPOBWmofM=" crossorigin="anonymous"></script>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default%2CArray.prototype.find%2CArray.prototype.includes%2CPromise%2CObject.assign%2CObject.entries"></script>
    <link rel="stylesheet" href="/front/css/pos-products-product-grid.css">
    <script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>


    <script>



        const search = instantsearch({

            searchClient: algoliasearch('9XP4QVMZVQ', 'de42a98197eb72f0a7bd1fb8474aecf1'),
            indexName: 'products',
            initialUiState: {
                products: {
                    refinementList:presetFacets,
                },
            },

        });

        search.addWidgets([
            instantsearch.widgets.searchBox({
                container: '#searchbox',
            }),
            instantsearch.widgets.refinementList({
                container: '#refinement-id',
                attribute: 'id',
                limit: 10,
                showMore: true,
            }),
            instantsearch.widgets.hits({
                container: '#hits',
                templates: {
                    item: `
                        <div class="product-hit" onclick="addProduct({{ id }})">
                            <img height="30" src="{{ image }}">
                            <h5 class="d-inline-block">{{ name.it }}</h5>
                            <small><span class="text-muted">{{ id }}</span></small>
                        </div>
                        `
                    },
                    transformItems(items) {
                        return items.map(function(item){
                            var image = (typeof(item.pictures) != 'undefined' && item.pictures !== null) ? 'https://www.atlantinex.com/ir/100/' + item.pictures[0] : 'https://www.atlantidex.com/img/favicon.jpg';
                            return{
                                name: item.name,
                                image:  image,
                                id: item.id
                            }
                        })
                    },
            }),
            instantsearch.widgets.pagination({
                container: '#pagination',
            }),
        ]);

        search.start();

        var cart = [];




        var app = new Vue({
            el: '#app',
            data: {
                cartProducts: [
                    {
                        'id':'',
                        'name':'Nome del prodotto',
                        'price':'0.00',
                        'quantity':1,
                        'commission':userMinCommission
                    }
                ],
                //grand_total: 0,
                user:null,
                pendingOtpConfirmation: false,
                otpRequested: false,
                otpConfirmed: false,
                selectedPaymentMethod: 'paid_by_card',
                otpCode:'',
                discount:0,
                userMinCommission:userMinCommission
            },
            computed:{
                grand_total: function(){

                    var total = 0;

                    for(var i in this.cartProducts){
                        total += this.cartProducts[i].price * this.cartProducts[i].quantity
                    }

                    total-=this.discount;

                    return total;
                }
            },
            watch: {
                userMinCommission:function(oldVal,newVal){
                    for(var i in this.cartProducts){
                        if(this.cartProducts[i].commission < this.userMinCommission){
                            this.cartProducts[i].commission = this.userMinCommission;
                        }
                    }
                },
                selectedPaymentMethod:function(newMethod,oldMethod){
                    if(!this.otpConfirmed){
                        var nonOtpRequiringPaymentmethods = ['paid_by_card','paid_in_cache'];
                        var n = nonOtpRequiringPaymentmethods.includes(newMethod);
                        if(!n){
                            this.pendingOtpConfirmation = true;
                        }else{
                            this.pendingOtpConfirmation = false;
                        }
                    }

                },
                cartProducts:function(newCartProducts,oldCartProducts){
                    for(var i in newCartProducts){
                        var product = newCartProducts[i];
                        product.index = i;
                    }
                }
            },
            methods: {
                requestOtp:function (){
                    $.post(otpRequestRoute,{code:this.user.code},function(r){
                        if(r.status == 'success'){
                            app.otpRequested = true;
                            new Noty({
                                    text: 'Codice richiesto con successo',
                                    type:'success',
                            }).show();
                        }else{
                            alert('Si è verificato un problema con la richiesta, prova a cliccare su Richiedi OTP di nuovo.');
                        }
                    },'json')
                },
                confirmOtp:function(){
                    if(this.otpCode != ''){
                        $.post(otpConfirmationRoute,{code:this.user.code,otp:this.otpCode},function(r){
                            if(r.status == 1){
                                app.otpConfirmed = true;
                                app.pendingOtpConfirmation = false;
                                new Noty({
                                    text: 'Hai confermato l\'OTP con successo',
                                    type:'success',
                                }).show();
                            }else{
                                new Noty({
                                    text: 'Il codice inserito non è corretto, hai un altro tentativo a disposizione',
                                    type:'danger',
                                }).show();
                            }
                        },'json')
                    }else{
                        new Noty({
                            text: 'Inserisci il codice ricevuto dal cliente prima di riprovare',
                            type:'danger',
                        }).show();
                    }
                },
                addNewProduct:function(){
                    this.cartProducts.push({
                        'id':'',
                        'name':'Nome del prodotto',
                        'price':'0.00',
                        'quantity':1,
                        'commission': userMinCommission
                    });
                },
                addQuantity: function(product){
                    product.quantity ++;

                },
                removeQuantity: function(product){
                    if(product.quantity > 1){
                        product.quantity --;
                    }else{
                        removeProduct(product.index);
                    }

                },

                confirmOrder: function(){



                    $.post(confirmOrderRoute,{
                        '_token': postToken,
                        'products': this.cartProducts,
                        'paymentmethod': this.selectedPaymentMethod,
                        'discount': this.discount,
                        'code': this.user.code,
                        'merchant_id': merchantId
                    },function(r){
                        if(r.status == 0){
                            new Noty({
                                    text: r.message,
                                    type:'danger',
                                }).show();
                        }
                        if(r.status == 1){
                            location.href="/admin/lead"
                        }
                    },'json')

                }
            }

        })

        function removeProduct(index){
            var cartProducts = app.cartProducts;
             if(cartProducts.length > 0){
                for(var i in cartProducts){
                    if(cartProducts[i].index == index){
                        cartProducts.splice(i, 1);
                    }
                }
            }

        }

        function addProduct(id){
            var justAddedProduct = products[id];
            var cartProducts = app.cartProducts;
            var alreadyExists = false;
            if(cartProducts.length > 0){
                for(var i in cartProducts){
                    if(cartProducts[i].id == justAddedProduct.id){
                        cartProducts[i].quantity++;
                        alreadyExists = true;
                    }
                }
            }
            if(!alreadyExists){
                justAddedProduct.quantity = 1;
                app.cartProducts.push(justAddedProduct);
            }


        }

        function resetUserQrCodeScan(){
            app.user = null;
        }

        function loadUserForPos(code){
            $.get(loadUserForPosRoute,{code},function(r){
                app.user = r;

                $('#outputDataName').html(r.name);
                $('#outputDataSurname').html(r.surname);

                if(r.roles.includes('Company')){
                    new Noty({
                        text: 'Il cliente è una società',
                        type: 'success',
                    }).show();
                    app.userMinCommission = minCommissionForCompanies;
                }else{
                    app.userMinCommission = defaultMinCommission;
                }

                new Noty({
                    text: 'Il cliente è stato riconosciuto con successo',
                    type: 'success',
                }).show();

            },'json')
            .fail(function() {
                new Noty({
                    text: 'Si è verificato un problema prova a riscansionare il QR code o digita il codice manualmente',
                    type: 'danger',
                }).show();
                resetUserQrCodeScan();
                startScanning();
            })
        }


    </script>



    <style>
        #qr-canvas {
            border: 20px solid white;
        }
        #btn-scan-qr {
            cursor: pointer;
            max-width: 100%;
        }
        #btn-scan-qr img {
            height: 10em;
            padding: 15px;
            margin: 15px;
            background: white;
        }
        #pagination, #hits{
            margin-top: 20px;
        }
        .product-name{
            border: none;
            outline: none;
            display: block;
            margin-bottom: 5px;
            width: 100%;
        }
        .product-price{
            border: 1px solid #c4c8d8;
            box-shadow: 0 2px 5px 0 #e3e5ec;
            border-radius: 5px;
            padding: 10px 15px;
            margin-bottom: 15px;
            width: 180px;
            display: inline-block;
            outline: none;
        }

        .product-price input{
            border: none;
            display: inline!important;
            width: 60px;
            outline: none;
            text-align: center;
        }
    </style>
@endverbatim
@endpush

@endauth

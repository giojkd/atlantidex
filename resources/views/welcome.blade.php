@extends('main_front')
@section('content')

    <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <!--<div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>-->
            @endif

            <div class="content">
                <div class="d-block d-md-none">
                        <a href="/admin"><img src="/uploads/Welcome_Atlantinex_Mobile.png" width="100%"></a>
                </div>
                <div class="d-none d-md-block">
                    <a href="/admin"><img src="/uploads/Welcome_Atlantinex.jpg" width="100%"></a>
                </div>
            </div>
        </div>

@endsection

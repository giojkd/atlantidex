@auth

@extends('admin_custom.top_left')
@section('content')


<div class="row">
    <div class="col-md-4 offset-md-4">
        <h2>Inserisci il codice che hai ricevuto per sms</h2>
        <div class="card">
    <div class="card-body">
        <form action="{{ Route('MyAtlantinex.OtpCheck') }}" method="POST">
            @csrf
            <input type="hidden" name="id" value="1">
            <input type="hidden" name="model" value="generic">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="form-group ">
                <div class="input-group">
                    <input size="6" maxlength="6" type="text" name="code" class="form-control form-control-lg text-center" placeholder="_ _ _ _ _ _ ">
                </div>
            </div>

            <button class="btn btn-primary btn-lg btn-block text-center" type="submit">Conferma</button>


        </form>
    </div>
</div>
    </div>
</div>



@push('after_scripts')
    <script>

    </script>
@endpush
@endsection
@endauth

<table class="table table-hover table-striped">

    <tbody>
        @if(isset($rows))
            @foreach ($rows as $row)
            @if(is_array($row))
                <tr>
                    <td>{{ $row['line'] }}</td>
                    <td>@fp($row['value'])</td>
                </tr>
                @else
                    <tr>
                        <td>{{ $row }}</td>
                    </tr>
                @endif
            @endforeach
        @else
                <tr>
                    <td colspan="2" class="text-center">
                        Nessun record disponibile
                    </td>
                </tr>
        @endif
    </tbody>
</table>

@auth

@extends('admin_custom.top_left')

@section('content')
    <h2>Monthly career </h2>
    <form class="form form-inline " action="{{ route('MyBusiness.monthlyCareer') }}">

            <div class="form-group">
                <label for="">Mese di produzione</label>
                <select name="month" id="" class="form-control ml-2">
                    @foreach($form['productionMonths'] as $index => $month)
                        <option value="{{ $index }}" @if($index == $filters['month']) selected @endif>{{ $month['3'] }}</option>
                    @endforeach
                </select>
            </div>


            <button class="btn btn-outline-primary ml-2 mt-3 mt-md-0" type="submit">Applica filtro</button>



    </form>
    <hr>
@hss('50')


<div class="text-center w-100" >
       <table style="border-spacing: 0px;" cellspacing="0" cellpadding="0" class="d-inline-block">

        <tr>
            <td></td>
            <td colspan="{{ (count($lines) * 2) - 1}}">
                <div class="box">
                    <div class="box-header">
                        {{ $user->full_name }}
                    </div>
                    {{ $user->code }}
                </div>
                <div class="vs-height">
                    <div class="vs"></div>
                </div>
            </td>
        </tr>
    </table>
</div>

<div class="text-center w-100" style="overflow-x: scroll">
       <table style="border-spacing: 0px;" cellspacing="0" cellpadding="0" class="d-inline-block">


        <tr>
            <td></td>
            <td class="line-height-zero" colspan="{{ (count($lines) * 2) - 1}}">
                <div class="hs"></div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="vs bg-color "></div>
                <div class="text-right">
                    <div class="box-header bg-color">
                    .
                    </div>
                    <div class="box-first-row">
                        Ep totali network
                    </div>
                    <div class="box-second-row">
                        Regola del 50%
                    </div>
                </div>
            </td>
            @foreach($lines as $index => $line)
            <td class="data-td pt-0">
                <div><div class="vs"></div></div>

                <div class="box">
                    <div class="box-header text-nowrap">
                        {{ $line['label'] }}
                    </div>
                    <div class="box-first-row">{{ $line['totalNetworkEp'] }}</div>
                    <div class="box-second-row">{{ $line['fiftyPercentRule'] }}</div>
                </div>
            </td>
            @if($index >= 0 && $index < (count($lines)-1))
                <td class="data-td pt-0">
                    <div class="vs bg-color "></div>
                    <div class="text-right">
                        <div class="box-header bg-color">
                            .
                        </div>
                        <div class="box-first-row-darker box-first-row-color">
                            .
                        </div>
                        <div class="box-second-row-darker box-second-row-color">
                            .
                        </div>
                    </div>
                </td>
            @endif
            @endforeach
            <td>
                <div class="vs bg-color "></div>
                <div class="text-left">
                    <div class="box-header bg-color">
                    .
                    </div>
                    <div class="box-first-row">
                        {{ $totalNetworkEpSum }}
                    </div>
                    <div class="box-second-row">
                        {{ $fiftyPercentRuleSum }}
                    </div>
                </div>
            </td>
        </tr>
    </table>

    @hss('50')


</div>

<div class="container-fluid">
        <div class="row">
            @if(isset($previousMonth))
                <div class="col-md-4 text-left">
                    <div class="row">
                        <div class="col-md-8">
                            <h2>Livello di carriera</h2>
                            <h5>{{ $previousMonth }}</h5>
                        </div>
                        <div class="col-md-4">
                            <span class="career-level">{{ $previousMonthLevel }}</span>
                        </div>
                    </div>
                </div>
            @endif
              <div class="col-md-4 text-left">
                <div class="row">
                    <div class="col-md-8">
                        <h2>Livello di carriera</h2>
                        <h5>{{ $currentMonth }}</h5>
                    </div>
                    <div class="col-md-4">
                        <span class="career-level">{{ $currentMonthLevel }}</span>
                    </div>
                </div>
            </div>
              <div class="col-md-4 text-left">
                <div class="row">
                    <div class="col-md-8">
                        <h5>EP Totali Network</h5>
                    </div>
                    <div class="col-md-4">
                        <h5 class="bg-primary text-center">{{ number_format((float)$totalNetworkEp,2,',','.') }}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <h5>Regola del 50%</h5>
                    </div>
                    <div class="col-md-4">
                        <h5 class="bg-secondary text-center text-white">{{ number_format((float)$fiftyPercentRule,2,',','.') }}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <h5>EP Bonus</h5>
                    </div>
                    <div class="col-md-4">
                        <h5 class="bg-primary text-center">{{ $bonusEps }}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <h5>EP personali</h5>
                    </div>
                    <div class="col-md-4">
                        <h5 class="bg-secondary text-center text-white">{{ $personalEps }}</h5>
                    </div>
                </div>
                  <div class="row">
                    <div class="col-md-8">
                        <h5>Tutti gli EP (Regola del 50%+EP bonus+Ep personali)</h5>
                    </div>
                    <div class="col-md-4">
                        @php
                            $allMonthlyCareerEps = $bonusEps +  $personalEps + $fiftyPercentRule;
                        @endphp
                        <h5 class="bg-primary text-center">{{ number_format((float)$allMonthlyCareerEps,2,',','.') }}</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @push('after_scripts')
        <script>
            $(function(){
                var width = 0;
                $('.data-td').each(function(){
                    width+=$(this).outerWidth();
                })
                width -= ($('.data-td').eq(0).outerWidth());
                width++;
                width++;
                $('.hs').css({
                    width: width+'px'
                })
              //  alert(width);
            })
        </script>
    @endpush

    <style>
        .bg-color{
            background: #f0f3f9!important;
            color: #f0f3f9!important;
            border-color: #f0f3f9!important;
        }
        .box{
            padding: 15px 0px;
            border: 1px solid grey;
            margin-top: -7px;
            display:block;

            display: inline-block;

        }
        .box .box-header{
            height: 30px;
            line-height: 30px;
            font-weight: bold;
            display: block;
            padding: 0px 40px;
        }

          .box-first-row{
            color:#fff;
            background: #ed682c;
            padding: 5px 20px;
            text-align: center;
        }

        .box-first-row-darker{
            background: #fba984;
            padding: 5px 20px;
        }

        .box-first-row-color{
            color: #ed682c;
        }


          .box-second-row{
            color:#fff;
            background: #f29d18;
            padding: 5px 40px;
            text-align: center;
        }

        .box-second-row-darker{
            background: #f5c06d;
            padding: 5px 20px;
        }

        .box-second-row-color{
            color:#f29d18;
        }



        .vs{
            width: 1px;
            border-right: 1px solid grey;
            height: 35px;
            margin-left: -1px;
            display: inline-block;

        }
        .vs-height{
            height: 30px;
        }
        .hs{
            margin-top: -8px;
            border-top: 1px solid grey;
            display: inline-block;
        }

        .wedge{
            margin-top: 6px;
        }

        .wedge-n{
            margin-top: -6px;
        }

        .line-height-zero{
            line-height: 0px!important;
        }

        .career-level{
            font-size: 64px;
            font-weight: bold;
            color: #467fd0!important
        }


</style>



@endsection



@endauth

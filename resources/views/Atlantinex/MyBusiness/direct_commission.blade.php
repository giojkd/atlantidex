@auth

@extends('admin_custom.top_left')

@section('content')
@section('content')
    <h2>Direct Commission</h2>
    <form class="form form-inline" action="{{ route('MyBusiness.directCommission') }}">
        <div class="form-group">
            <label for="">Mese di produzione</label>
            <select name="month" id="" class="form-control ml-2">
                 @foreach($form['productionMonths'] as $index => $month)
                    <option value="{{ $index }}" @if($index == $filters['month']) selected @endif>{{ $month['3'] }}</option>
                @endforeach
            </select>
        </div>
        <button class="btn btn-outline-primary ml-2 mt-3 mt-md-0" type="submit">Applica filtro</button>
    </form>

    <hr>
    @hss('50')

    <h2>Direct Commission</h2>

    @if(!is_null($commissions))
    <table class="table table-hover table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Data</th>
                <th>Descrizione</th>
                <th>Valore</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($commissions as $commission)
                <tr>
                    <td>{{ $commission->id }}</td>
                    <td>{{ $commission->created_at->format('d/m/Y') }}</td>
                    <td>{{  $commission->description }}</td>
                    <td>@fp($commission->value)</td>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <td colspan="3"></td>
                <td><b>Totale:</b> @fp($commissions->sum('value'))</td>
            </tr>
        </tfoot>
    </table>
    @else
        <div class="alert alert-info">Nessuna commissione work your ass off!</div>
    @endif
@endsection

@endauth

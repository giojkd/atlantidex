@auth

@extends('admin_custom.top_left')

@section('content')
@include('Atlantinex.MerchantBusiness.filters_inc')
<hr>


<div class="row">
    <div class="col">
        <small>
            Mese di produzione da <b>{{ Carbon\Carbon::parse($production_month[0])->format('d M Y') }}</b> a <b>{{ Carbon\Carbon::parse($production_month[1])->format('d M Y') }}</b> pagato il <b>{{ Carbon\Carbon::parse($production_month[2])->format('d M Y') }}</b>
            </small>
    </div>
</div>
@hss('20')
<div class="row">
    <div class="col-md-12 text-center">
        <h1 class="marketer-color marketer-border marketer-color">Marketer Commissions</h1>
    </div>
</div>

@if ($compensations_count > 0)



<div class="row">
    <div class="col-md-4">
        <div class="card marketer-border marketer-color">
            <div class="card-body">
                    <h5 class="card-title">Long Run Career Commission</h5>
                <div class="row">
                    <div class="col">
                        <h1><i class="fas fa-funnel-dollar"></i></h1>
                    </div>
                    <div class="col text-right">
   <h1>@fp($long_run_career_commission)</h1>
                    </div>
                </div>



            </div>
        </div>
    </div>
      <div class="col-md-4">
        <div class="card marketer-border marketer-color">
            <div class="card-body">
                <h5 class="card-title">Long Run Career Bonus</h5>
                <div class="row">
                    <div class="col">
                        <h1><i class="fas fa-comment-dollar"></i></h1>
                    </div>
                    <div class="col text-right"> <h1>@fp($long_run_career_bonus)</h1></div>
                </div>


            </div>
        </div>
    </div>

     <div class="col-md-4">
        <div class="card marketer-border marketer-color">
            <div class="card-body">
                <h5 class="card-title">Direct Commission</h5>
                <div class="row">
                    <div class="col"><h1><i class="fas fa-wallet"></i></h1></div>
                    <div class="col text-right"><h1>@fp($direct_commission) <a data-toggle="modal" data-target="#detailsModal" href="javascript:" data-type="direct-commission"><i class="fa fa-search-plus"></i></a></h1></div>
                </div>


            </div>
        </div>
    </div>
</div>


<div class="row ">
    <div class="col-md-6">
        <div class="card marketer-border marketer-color">
            <div class="card-body">
                <h5 class="card-title">Monthly Career Bonus</h5>
                <div class="row">
                    <div class="col"><h1><i class="fas fa-chart-line"></i></h1></div>
                    <div class="col text-right"><h1>@fp($monthly_career_bonus)</h1></div>
                </div>


            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card marketer-border marketer-color">
            <div class="card-body ">
                <h5 class="card-title">Monthly Career Commission</h5>
                <div class="row">
                         <div class="col"><h1><i class="fas fa-chart-bar"></i></h1></div>
                    <div class="col text-right">      <h1>@fp($monthly_career_commission) <a data-toggle="modal" data-target="#detailsModal" href="javascript:" data-type="monthly-career-commission"><i class="fa fa-search-plus"></i></a></h1> <small></small></div>
                </div>


            </div>
        </div>
    </div>
</div>

@if(Auth::user()->pack_id == 2) {{-- TO BE FIXED --}}
<div class="row">
    <div class="col-md-12 text-center">
        <h1 class="marketer-color merchant-marketer-border merchant-marketer-color">Merchant Marketer Commissions</h1>
    </div>
</div>
<div class="row">
         <div class="col-md-6">
        <div class="card merchant-marketer-border merchant-marketer-color">
            <div class="card-body">
                <h5 class="card-title">Merchant Bonus</h5>
                <div class="row">
                    <div class="col">
                        <h1><i class="fas fa-briefcase"></i></h1>
                    </div>
                    <div class="col text-right">    <h1>@fp($merchant_bonus) + {{ $merchant_bonus_ep }}EP <a data-toggle="modal" data-target="#detailsModal" href="javascript:" data-type="merchant-bonus"><i class="fa fa-search-plus"></i></a></h1> <small></small></div>
                </div>


            </div>
        </div>
    </div>
          <div class="col-md-6">
        <div class="card merchant-marketer-border merchant-marketer-color">
            <div class="card-body">
                <h5 class="card-title">Merchant Booster Bonus</h5>
                <div class="row">
                    <div class="col-md-6"><h1><i class="fas fa-money-bill-wave-alt"></i></h1></div>
                    <div class="col-md-6 text-right"><h1>@fp($merchant_booster_bonus) + {{ $merchant_booster_bonus_ep }}EP <a data-toggle="modal" data-target="#detailsModal" href="javascript:" data-type="merchant-booster-bonus"><i class="fa fa-search-plus"></i></a></h1> <small></small></div>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="row">
      <div class="col-md-6">
        <div class="card merchant-marketer-border merchant-marketer-color">
            <div class="card-body">
                <h5 class="card-title">Revenue Bonus</h5>
                <div class="row">
                    <div class="col">
                        <h1><i class="fas fa-trophy"></i></h1>
                    </div>
                    <div class="col text-right">
                         <h1>@fp($revenue_bonus) <a data-toggle="modal" data-target="#detailsModal" href="javascript:" data-type="revenue-bonus"><i class="fa fa-search-plus"></i></a></h1> <small></small>
                    </div>

                </div>


            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card merchant-marketer-border merchant-marketer-color">
            <div class="card-body">
                <h5 class="card-title">Merchant Marketing Bonus</h5>
                <div class="row">
                    <div class="col">
                        <h1><i class="fas fa-money-check-alt"></i></h1>
                    </div>
                    <div class="col text-right"> <h1>@fp($marketing_bonus) + {{ $marketing_bonus_ep }}EP</h1></div>
                </div>


            </div>
        </div>
    </div>

</div>

@push('before_scripts')
    <div class="modal fade" id="detailsModal" tabindex="-1" role="dialog" aria-labelledby="detailsModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
                    {{-- <button type="button" class="btn btn-primary">Send message</button>  --}}
                </div>
            </div>
        </div>
    </div>
@endpush

@push('after_scripts')
    <script>
        var monthId = '{{ $filters['month'] }}';
        var baseUrl = '{{ Route('MyBusiness.myBenefitsDetails') }}';
        $(function(){
            $('#detailsModal').on('show.bs.modal', function (event) {

                var button = $(event.relatedTarget);
                var type = button.data('type');
                console.log(type);
                var modal = $(this);

                $.get(baseUrl+'/'+type+'?month_id='+monthId,{},function(data){
                    $('#detailsModal .modal-body').html(data);
                })

            })
        })
    </script>

    <style>
    .marketer-color, .marketer-color a{
        color: #333;
    }
    .marketer-border{
        border: 1px solid #333;
        border-radius: 7px;
        padding: 20px;
        background: transparent;

    }
     .merchant-marketer-color, .merchant-marketer-color a{
        color: #333;
    }
    .merchant-marketer-border{
        border: 1px solid #333;
        border-radius: 7px;
         padding: 20px;
         background: transparent;

    }
    </style>

@endpush

@endif

@else

<div class="alert alert-secondary">Benefits non disponibili</div>

@endif

@endsection

@endauth

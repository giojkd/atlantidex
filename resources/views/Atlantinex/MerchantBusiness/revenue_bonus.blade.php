@auth

@extends('admin_custom.top_left')
@section('content')

<div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <h2>Revenue Bonus</h2>
            @include('Atlantinex.MerchantBusiness.filters_inc')
            <hr>
            <table class="table table-striped data-table" width="100%">
                <thead>
                    <tr>
                        <th>Codice ID azienda</th>
                        <th>Nome Azienda</th>
                        <th>Data di affiliazione</th>
                        <th>Fatturato generato</th>
                        <th>Revenue bonus</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($companies as $item)
                    <tr>
                        <td>{{$item->id}}</td>
                        <td>{{$item->company_name}}</td>
                        <td>{{date('d/m/Y', strtotime($item->created_at))}}</td>
                        <td>@fp(0)</td>
                        <td>@fp(0)</td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
            @if(isset($gdo_companies))
                <h2>Revenue Bonus GDO</h2>
                <hr>
                <table class="table table-striped data-table" width="100%">
                    <thead>
                        <tr>

                            <th>Nome Azienda</th>
                            <th>Data di affiliazione</th>
                            <th>Revenue bonus</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($gdo_companies as $item)
                        <tr>

                            <td>{{$item['company_name']}}</td>
                            <td>{{ $item['affiliated_at'] }}</td>
                            <td>@fp($item['total_commission'])</td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            @endif
            <hr>
            @if ($commissionoperations->count() > 0)
                <h2>Operazioni</h2>
                <table class="table table-striped data-table" width="100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Data</th>
                            <th>Descrizione</th>
                            {{-- <th>Tipo</th>  --}}
                            <th>Valore</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($commissionoperations as $op)
                            <tr>
                                <td>{{ $op->id }}</td>
                                <td>{{ $op->created_at->format('d/m/Y H:i') }}</td>
                                <td>{{ $op->description }}</td>
                                {{-- <td>{{ Str::of($op->commission_type)->replaceFirst('_',' ')->ucFirst() }}</td>  --}}
                                <td>@fp($op->value)</td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="3"></td>
                            <td>
                                <h3>
                                    @fp($commissionoperations->sum('value'))
                                </h3>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            @endif
        </div>
    </div>
</div>
@endsection

@endauth

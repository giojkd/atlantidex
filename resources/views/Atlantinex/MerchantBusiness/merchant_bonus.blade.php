@auth

@extends('admin_custom.top_left')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <h2>Merchant Bonus</h2>
            @include('Atlantinex.MerchantBusiness.filters_inc')
            <hr>
            <table class="table table-striped data-table" width="100%">
                <thead>
                    <tr>
                        <th>Codice ID azienda</th>
                        <th>Nome Azienda</th>
                        <th>Data di affiliazione</th>
                        <th>Commissione pagata</th>
                        <th>EP</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($companies as $item)
                    <tr>
                        <td>{{$item->id}}</td>
                        <td>{{$item->company_name}}</td>
                        <td>{{date('d/m/Y', strtotime($item->created_at))}}</td>
                        <td>@fp(50)</td>
                        <td>49</td>
                    </tr>
                    @endforeach

                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="3" class="text-right">Totale mese di produzione</td>
                        <td>@fp($companies->count() * 50)</td>
                        <td>{{ $companies->count() * 49}}</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

@endsection

@endauth

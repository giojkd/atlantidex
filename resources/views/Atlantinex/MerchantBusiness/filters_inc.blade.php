<form class="form form-inline" action="{{ $formAction }}">
    <div class="form-group">
        <label for="">Mese di produzione</label>
        <select name="month" id="" class="form-control ml-2">
            @foreach($form['productionMonths'] as $index => $month)
                <option value="{{ $index }}" @if($index == $filters['month']) selected @endif>{{ $month['3'] }}</option>
            @endforeach
        </select>
    </div>
    <button class="btn btn-outline-primary ml-2" type="submit">Applica filtro</button>
</form>

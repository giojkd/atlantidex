@auth

@extends('admin_custom.top_left')

@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <h2>Aziende Affiliate</h2>
            {{-- <form class="form form-inline" action="{{ route('MerchantBusiness.affiliatedCompanies') }}">
                <div class="form-group">
                    <label for="">Mese di produzione</label>
                    <select name="month" id="" class="form-control ml-2">
                        @foreach($form['productionMonths'] as $month)
                            <option value="{{ $month['value'] }}" @if($month['value'] === $filters['month']) selected @endif>{{ $month['label'] }}</option>
                        @endforeach
                    </select>
                </div>
                <button class="btn btn-outline-primary ml-2" type="submit">Applica filtro</button>
            </form>  --}}
            <hr>
            <div class="table-responsive">
                <table class="table table-striped data-table" width="100%">
                    <thead>
                        <tr>
                            <th>Codice ID azienda</th>
                            <th>Nome Azienda</th>
                            <th>Data di affiliazione</th>
                            <th>Commissione pagata</th>
                            <th>Data di attivazione</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($companies as $item)
                        <tr>
                            <td>{{$item->code}}</td>
                            <td>{{$item->company_name}}</td>
                            <td>{{date('d/m/Y', strtotime($item->created_at))}}</td>
                            <td> @if($item->virtual_parent_id == Auth::id()) <div class="badge badge-success">Revenue Bonus</div> @endif </td>
                            <td> {{ $item->getCompanyActivationDate() }}</td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@endauth

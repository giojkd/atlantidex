@auth

@extends('admin_custom.top_left')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <h2>Merchant Marketing Bonus</h2>
            @include('Atlantinex.MerchantBusiness.filters_inc')
            <hr>
            <table class="table table-striped data-table" width="100%">
                <thead>
                    <tr>
                        <th>Codice ID azienda</th>
                        <th>Nome Azienda</th>
                        <th>Data di acquisto</th>
                        <th>Pack acquistato</th>
                        <th>EP</th>
                        <th>Commissione pagata</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($companies as $item)
                    <tr>

                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection

@endauth

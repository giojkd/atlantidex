@auth

@extends('admin_custom.top_left')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <h2>Merchant booster Bonus</h2>
            @include('Atlantinex.MerchantBusiness.filters_inc')
            <hr>
            <table class="table table-striped data-table" width="100%">
                <thead>
                    <tr>
                        <th>ID operazione</th>
                        <th>Descrizione</th>
                        <th>Data</th>
                        <th>Commissione pagata</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($companies as $item)
                    <tr>
                        <td>{{$item->id}}</td>
                        <td>{{$item->description}}</td>
                        <td>{{date('d/m/Y', strtotime($item->created_at))}}</td>
                        <td>@fp($item->value) €</td>
                    </tr>
                    @endforeach
                    <tfoot>
                        <tr>
                            <td colspan="3" class="text-right">Totale mese di produzione</td>
                            <td>@fp($companies->sum('value'))</td>
                        </tr>
                    </tfoot>
                </tbody>

            </table>
        </div>
    </div>
</div>

@endsection

@endauth

     <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Inviata il </th>
                                <th>Destinatario </th>

                                <th>Firma richiedente</th>
                                <th>Feedback ricevente</th>
                                <th>Firma ricevente</th>
                                <th>Azioni</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($requests as $item)
                                <tr>
                                    <td>{{ $item->id}}</td>
                                    <td>{{ $item->created_at->format('d/m/Y H:i') }}</td>
                                    <td>{{ $item->receiving_user->name}}</td>

                                    <td>
                                        @if(is_null($item->applicant_signature))
                                            @if(Auth::id() == $item->applicant_user_id)
                                                <a href="{{ $item->signatureUrl() }}" class="btn btn-primary">Firma</a>
                                            @else
                                                <span class="text-danger"><i class="fa fa-ban"></i></span>
                                            @endif
                                        @else
                                            <span class="text-success"><i class="fa fa-check"></i></span>
                                        @endif
                                    </td>
                                    <td>@if(is_null($item->receiving_feedback))
                                           @if(Auth::id() == $item->receiving_user_id)
                                                <form action="{{ Route('MyAtlantinex.changeSponsorFeedback') }}" method="POST">
                                                    @csrf
                                                    <input type="hidden" name="id" value="{{ $item->id }}">
                                                    <span class="text-nowrap">
                                                        <button name="receiving_feedback" value="accepted" type="submit" class="btn btn-success">Accetta</button>
                                                        <button name="receiving_feedback" value="accepted" type="submit" class="btn btn-danger">Rifiuta</button>
                                                    </span>
                                                </form>
                                            @else
                                                <span class="text-danger"><i class="fa fa-ban"></i></span>
                                            @endif
                                        @else
                                                @if($item->receiving_feedback == 'accepted')
                                                    <span class="badge badge-success">
                                                        Accettato
                                                    </span>
                                                @else
                                                    <span class="badge badge-danger">
                                                        Rifiutato
                                                    </span>
                                                @endif
                                        @endif
                                    </td>
                                    <td>
                                        @if(is_null($item->receiving_signature))
                                            @if(Auth::id() == $item->receiving_user_id)
                                                <a href="{{ $item->signatureUrl('receiving') }}" class="btn btn-primary">Firma</a>
                                            @else
                                                <span class="text-danger"><i class="fa fa-ban"></i></span>
                                            @endif
                                        @else
                                            <span class="text-success"><i class="fa fa-check"></i></span>
                                        @endif
                                    </td>
                                    <td>


                                        @if($item->canBeRendered())
                                        <form action="{{ Route('MyAtlantinex.changeSponsorRenderDocument') }}" method="POST">
                                            @csrf
                                            <input type="hidden" name="id" value="{{ $item->id }}">
                                            <button type="submit" class="btn btn-primary">Download</button>
                                        </form>

                                        @else
                                            Nessuna azione disponibile
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

@auth

@extends('admin_custom.top_left')
@section('content')
<h2>I miei voucher della GDO</h2>
 @if(isset($status))
                    <div class="alert @if($status == 1) alert-success @else alert-danger @endif">
                        {{ $message }}
                    </div>
                @endif
@if(!is_null($vouchers) && $vouchers->count() > 0)
    <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Acquistato il</th>
                    <th>Codice</th>

                    <th>Voucher</th>
                    <th>GDO</th>
                    <th>Valore</th>
                    <th>Speso</th>
                    <th>Scade il</th>
                    <th>Scaricato</th>
                    <th>Download</th>
                </tr>
            </thead>
            <tbody>
                @foreach($vouchers as $voucher)

                    <tr>
                        <td>@if(!is_null($voucher->purchased_at)) {{ $voucher->purchased_at->format('d/m/Y H:i') }} @else na @endif</td>
                        <td>@if($voucher->code) {{ $voucher->code }} @else NA @endif</td>

                        <td>{{ $voucher->gdovoucher->name }}</td>
                        <td>{{ $voucher->gdovoucher->manufacturer->name }}</td>
                        <td>@fp($voucher->value)</td>
                        <td>
                            <form action="{{ Route('MyAtlantinex.gdovouchersshop.update',['gdovouchersshop' => $voucher->id]) }}" class="ajax-form" method="POST">
                                @csrf
                                <input type="hidden" name="_method" value="PUT">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">€</span>
                                    </div>
                                    <input name="amount_spent" type="text" class="form-control submit-on-blur" value="{{ number_format($voucher->amount_spent,2) }}">
                                </div>



                            </form>
                        </td>
                        <td>
                            {{ (!is_null($voucher->expires_at)) ? date('d/m/Y',strtotime($voucher->expires_at)) : ((!is_null($voucher->purchased_at)) ? date('d/m/Y',strtotime($voucher->purchased_at.' +2years')) : 'na' )}}
                        </td>
                        <td>
                            @if($voucher->downloaded_times > 0) {{ $voucher->downloaded_times }} volte @else mai @endif
                        </td>
                        <td>
                            <a href="@if($voucher->link) {{ $voucher->link }} @else /my-atlantinex/{{$voucher->id}}/downgdovoucher @endif"><i class="la la-download"></i> Download</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="4"></th>
                    <th>@fp($vouchers->sum('value'))</th>
                    <th colspan="4"></th>
                </tr>
            </tfoot>
        </table>
    </div>
@else

    <div class="card">
        <div class="card-body">
            Vai subito nella sezione <a href="{{ Route('MyAtlantinex.gdovouchersshop.create') }}">Acquista i voucher della GDO</a> e fai shopping acquistando i voucher delle migliori aziende presenti sul mercato!
        </div>
    </div>

@endif
@endsection

@endauth

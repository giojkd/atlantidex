<html>
    <head>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <style>
           @font-face {
                font-family: 'Open Sans';
                font-style: normal;
                font-weight: normal;
                src: url(/packages/source-sans-pro/WOFF2/TTF/SourceSansPro-Regular.ttf.woff2);
                }
            h5 {
                font-family: 'Open Sans';
            }
        </style>

    </head>
<body>
    <div style="text-align:center">
    <table width="100%">
        <tr>
            <td colspan="2" style="text-align:center">
                <img src="https://www.atlantinex.com/uploads/logo-dark.png">
                <hr>
                <span style="font-size: 20px">{{$gdovoucher->gdovoucher->name}}</span>
                <div style="height:50px"></div>
                <span style="font-size: 20px">{{$gdovoucher->code}}</span>
                <div style="height:50px"></div>
                @if(!is_null($gdovoucher->barcode))
                    <img src="data:image/png;base64,' . {{ DNS1D::getBarcodePNG($gdovoucher->barcode,'C128') }} . '" alt="barcode"   />
                    <br>
                @endif
                <span style="font-size: 20px">Credito:</span><br>
                <span style="font-size: 36px">{{$gdovoucher->value - $gdovoucher->amount_spent}},00 €</span>
                <div style="height:10px"></div>
                {{--
                <span style="font-size: 20px">Credito speso:</span><br>
                <span style="font-size: 36px">{{$gdovoucher->amount_spent ?? 0}},00 €</span>
                <div style="height:50px"></div>
                 --}}
                <hr>
            </td>
            </tr>
            <tr>
                <td  style="text-align:center">Acquistato il:<br>{{\Carbon\Carbon::parse($gdovoucher->purchased_at)->format('d/m/Y')}}</td>
                <td  style="text-align:center">Valido fino al:<br>{{ (!is_null($gdovoucher->expires_at)) ? \Carbon\Carbon::parse($gdovoucher->expires_at)->format('d/m/Y') : \Carbon\Carbon::parse($gdovoucher->purchased_at)->addYears(2)->format('d/m/Y')}}</td>
            </tr>
        </table>
    </div>
</body>
</html>

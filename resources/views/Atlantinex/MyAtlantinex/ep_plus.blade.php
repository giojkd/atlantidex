@auth

@extends('admin_custom.top_left')
@section('content')

    <div class="row">
        <div class="col-md-8">
            <h2>Richieste di trasferimento sul wallet</h2>
                <div class="card">
                    <div class="card-body">
                        @if ($payouts->count() > 0)

                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Data</th>
                                        <th>Importo</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($payouts as $item)
                                        <tr>
                                            <td>{{ $item->id }}</td>
                                            <td>{{ $item->created_at->format('d/m/Y H:i') }}</td>
                                            <td>@fp($item->value)</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        @else

                            <div class="alert alert-secondary">Non hai ancora inviato nessuna richiesta di payout</div>

                        @endif

                        @if ($user->epplus_balance > env('MIN_EPS_BALANCE_FOR_PAYOUT'))

                            <h5>Invia una nuova richiesta di payout (somma minima @fp((float)env('MIN_EPS_BALANCE_FOR_PAYOUT')*(float)env('EPPLUS_EUR_RATIO')))</h5>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            €
                                        </span>
                                    </div>
                                    <input id="payoutRequestValue" value="{{ number_format($user->epplus_balance * env('EPPLUS_EUR_RATIO'),2,'.','') }}" placeholder="Quanto vuoi richiedere?" min="{{ env('MIN_PAYOUT_VALUE') }}" name="value" type="number" class="form-control form-control-lg only-numbers" placeholder="" >
                                    <div class="input-group-append">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#payoutConfirmationModal">Richiedi codice di conferma</button>
                                    </div>
                                </div>
                            </div>
                        @else

                            <div class="alert alert-secondary">
                               Hai a disposizione  {{ $user->epplus_balance }}EP+, ti mancano ancora almeno {{ env('MIN_EPS_BALANCE_FOR_PAYOUT') - $user->epplus_balance }}EP+ prima di poterli convertire in una ricarica per il wallet
                            </div>

                        @endif



                    @push('after_scripts')
                        <div class="modal fade" id="payoutConfirmationModal" tabindex="-1" role="dialog" aria-labelledby="payoutConfirmationModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Conferma la richiesta di payout</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form method="POST" action="{{ Route('MyAtlantinex.payoutRequest') }}" id="payoutRequestForm">
                                        <div class="modal-body">
                                            <p>Stai richiedendo un payout di €<span id="payoutRequestValueSpan"></span> inserisci il codice OTP che hai ricevuto sul tuo cellulare per confermare</p>
                                            <small><a href="javscript:" onclick="requestPayoutOtp()">Clicca qui se non hai riceuvto il codice</a></small>

                                                <input type="hidden" id="requestValue" name="value">
                                                <div class="form-group">
                                                    <input name="otp" type="text" class="form-control text-center" placeholder="_ _ _ _ _ " id="recipient-name">
                                                </div>

                                        </div>
                                        <div class="modal-footer">
                                            <a href="javascript:" class="btn btn-secondary" data-dismiss="modal">Annulla</a>
                                            <button type="submit" class="btn btn-primary">Conferma la richiesta</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <script>

                            var payoutConfirmationModal = $('#payoutConfirmationModal');
                            var requestValue = 0;

                            payoutConfirmationModal.on('show.bs.modal', function (event) {
                                requestValue = parseFloat($('#payoutRequestValue').val());
                                $('#requestValue').val(requestValue);
                                $('#payoutRequestValueSpan').html(requestValue.toFixed(2));
                                requestPayoutOtp();
                            });

                            var payoutRequestForm = $('#payoutRequestForm');

                            payoutRequestForm.submit(function(e){

                                e.preventDefault();

                                $.post('{{ Route('MyAtlantinex.epPlusOperationsPayoutConfirm') }}',payoutRequestForm.serialize(),function(r){
                                    if(r.status == 1){
                                        payoutConfirmationModal.modal('hide');
                                        new Noty({
                                                text: 'Abbiamo ricevuto la tua richiesta, aggiorna la pagina.',
                                                type:'success',
                                            }).show();
                                        }else{
                                            new Noty({
                                                text: r.message,
                                                type:'danger',
                                            }).show();
                                        }

                                },'json')
                            });

                            function requestPayoutOtp(){
                                $.post('{{ Route('MyAtlantinex.epPlusOperationsPayoutOtp') }}',{requestValue},function(r){},'json')
                            }


                        </script>
                    @endpush
                    </div>
                </div>
            <div>
                <h2>I miei EP+</h2>
                @if ($operations->count() > 0)
                    <table class="table tale-hover table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Data</th>
                                <th>Importo</th>
                                <th>Descrizione</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($operations as $operation)


                            <tr>
                                <td>
                                    {{ $operation->id }}
                                </td>
                                <td>{{ $operation->created_at->format('d/m/Y H:i') }}</td>
                                <td>{{ $operation->value }}EP+</td>
                                <td>{{ $operation->description }}</td>
                            </tr>

                        @endforeach
                        </tbody>
                    </table>
                @else
                    <div class="alert alert-secondary">
                        Non hai accumulato ancora nessun EP+
                    </div>
                @endif
            </div>
        </div>
    </div>

@endsection

@endauth

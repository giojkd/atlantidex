@auth

@extends('admin_custom.top_left')
@section('content')


    <div class="row">
        <div class="col-md-9">
            <div>
                <h2 class="mb-4"><img height="40" src="/front/img/WEB-Logo-Atlantipay.png" alt=""></h2>
                <div class="card">
                    <div class="card-body">
                        Il saldo del tuo wallet è : @fp($user->wallet_balance)
                    </div>
                </div>
                <h2>Ricarica</h2>
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('MyAtlantinex.myWalletTopUp') }}" method="POST">
                            @csrf

                            @foreach($payment_methods as $index =>  $payment_method)
                            <div class="form-group">
                                <label>
                                    <input @if($index == 0) checked @endif type="radio" name="data[paymentmethod_id]" value="{{ $payment_method->id }}" >
                                    Carta di credito: <b>{{ Str::ucfirst($payment_method->card->brand) }} ({{ $payment_method->card->last4 }})</b>
                                    Scad: <b>{{ $payment_method->card->exp_month }}/{{ $payment_method->card->exp_year }}</b>
                                </label>
                            </div>
                            @endforeach


                            <div>
                                <a class="btn btn-primary btn-sm mb-2" href="{{ route('MyAtlantinex.payment-data.index') }}">Aggiungi un nuovo metodo di pagamento</a>
                            </div>


                            <div class="form-group row">
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                Ricarichi
                                            </span>
                                            </div>
                                                <input name="data[fake_value]" type="text" class="form-control form-control-lg only-numbers" placeholder="" >

                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        Carta di credito
                                                    </span>
                                                </div>
                                                <input onfocus="$('input[name=\'data[fake_value]\']').focus()" name="data[value]" type="text" class="form-control form-control-lg" placeholder="" >
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                             <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        PayPal
                                                    </span>
                                                </div>
                                                <input onfocus="$('input[name=\'data[fake_value]\']').focus()" name="data[value_paypal]" type="text" class="form-control form-control-lg" placeholder="" >
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>

                       <p class="text-muted">
                           <small>
                               Gentile utente ti comunichiamo che per ogni ricarica del wallet effettuata tramite carta di credito Atlantinex tratterrà una commissione pari al 2%,
                               se effettuata tramite PayPal la commissione sarà del 4.5%, che verrà aggiunta all'importo ricaricato.
                               Utilizzando PayPal alcune volte l'importo della ricarica non è immediatamente disponibile, in alcuni casi è, infatti, necessario attendere i tempi tecnici per l'accredito.
                            </small>
                       </p>

                       <div id="confirmPayment">
                            <button type="submit" class="btn btn-primary btn-block mt-4 btn-lg"><i class="fa fa-bank"></i> Ricarica con carta di credito</button>
                            <div class="text-center mt-3">
                                <small><p class="text-muted text-center" ><i>Oppure</i></p></small>
                            </div>

                        <div id="paypal-button-container" style="height: 60px; overflow: hidden; text-align: center"></div>
                        </div>
                             <div class="my-4">
                                <small><p class="text-muted text-center"><i>Oppure per evitare le commissioni della carta di credito</i></p></small>
                            </div>
                            <p class="border border-light rounded p-2 text-center">
                                Invia un bonifico dell'importo che vuoi ricaricare intestato a: <br>
                                <b>Atlantidex SRL IBAN: IT85J0617524510000082580680 </b>
                                <br> specificando nella causale<br><b>"{{ Auth::user()->full_name }} {{ Auth::user()->code }} Num. Cell. {{ Auth::user()->phone }}"</b><br>
                                Inviare la contabile del bonifico al seguente indirizzo mail: <a href="mailto:amministrazione@atlantidex.com?subject=Invio contabile bonifico per ricarica wallet {{ Auth::user()->full_name }} {{ Auth::user()->code }}">amministrazione@atlantidex.com</a><br>La ricarica diventerà disponibile entro 24 ore dalla ricezione del bonifico.

                            </p>

                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul class="list list-unstyled">
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif


                        </form>
                    </div>
                </div>
                <h2>Richieste di trasferimento sul conto corrente</h2>
                <div class="card">
                    <div class="card-body">
                        @if ($payouts->count() > 0)

                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Data</th>
                                        <th>Importo</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($payouts as $item)
                                        <tr>
                                            <td>{{ $item->id }}</td>
                                            <td>{{ $item->created_at->format('d/m/Y H:i') }}</td>
                                            <td>@fp($item->value)</td>
                                            <td>{{ $item->status }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        @else

                            <div class="alert alert-secondary">Non hai ancora inviato nessuna richiesta di payout</div>

                        @endif
                            <h5>Invia una nuova richiesta di payout (somma minima @fp((float)env('MIN_PAYOUT_VALUE')))</h5>

                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                €
                                            </span>
                                        </div>
                                        <input id="payoutRequestValue" value="{{ number_format($user->wallet_balance,2,'.','') }}" placeholder="Quanto vuoi richiedere?" min="{{ env('MIN_PAYOUT_VALUE') }}" name="value" type="number" class="form-control form-control-lg only-numbers" placeholder="" >
                                        <div class="input-group-append">
                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#payoutConfirmationModal">Richiedi codice di conferma</button>
                                        </div>
                                    </div>
                                </div>

                                <p class="text-muted"><small><i>Verrà detratto il costo del bonifico che sarà eseguito entro 4 giorni lavorativi.</i></small></p>


                    @push('after_scripts')
                        <div class="modal fade" id="payoutConfirmationModal" tabindex="-1" role="dialog" aria-labelledby="payoutConfirmationModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Conferma la richiesta di payout</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form method="POST" action="{{ Route('MyAtlantinex.payoutRequest') }}" id="payoutRequestForm">
                                        <div class="modal-body">
                                            <p>Stai richiedendo un payout di €<span id="payoutRequestValueSpan"></span> inserisci il codice OTP che hai ricevuto sul tuo cellulare per confermare</p>
                                            <small><a href="javscript:" onclick="requestPayoutOtp()">Clicca qui se non hai riceuvto il codice</a></small>

                                                <input type="hidden" id="requestValue" name="value">
                                                <div class="form-group">
                                                    <input name="otp" type="text" class="form-control text-center" placeholder="_ _ _ _ _ " id="recipient-name">
                                                </div>

                                        </div>
                                        <div class="modal-footer">
                                            <a href="javascript:" class="btn btn-secondary" data-dismiss="modal">Annulla</a>
                                            <button type="submit" class="btn btn-primary">Conferma la richiesta</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <script>

                            var payoutConfirmationModal = $('#payoutConfirmationModal');
                            var requestValue = 0;

                            payoutConfirmationModal.on('show.bs.modal', function (event) {
                                requestValue = parseFloat($('#payoutRequestValue').val());
                                $('#requestValue').val(requestValue);
                                $('#payoutRequestValueSpan').html(requestValue.toFixed(2));
                                requestPayoutOtp();
                            });

                            var payoutRequestForm = $('#payoutRequestForm');

                            payoutRequestForm.submit(function(e){

                                e.preventDefault();

                                $.post('{{ Route('MyAtlantinex.payoutRequest') }}',payoutRequestForm.serialize(),function(r){
                                    if(r.status == 1){
                                        payoutConfirmationModal.modal('hide');
                                        new Noty({
                                                text: 'Abbiamo ricevuto la tua richiesta, aggiorna la pagina.',
                                                type:'success',
                                            }).show();
                                        }else{
                                            new Noty({
                                                text: r.message,
                                                type:'danger',
                                            }).show();
                                        }

                                },'json')
                            });

                            function requestPayoutOtp(){
                                $.post('{{ Route('MyAtlantinex.payoutRequestOtp') }}',{requestValue},function(r){},'json')
                            }


                        </script>
                    @endpush
                    </div>
                </div>
                <h2>Riepilogo operazioni</h2>
                <div class="card">
                    <div class="card-body">
                        @if($operations->count() > 0)
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Importo</th>
                                        <th>Ricevuta</th>
                                        <th>Data</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($operations as $operation)
                                    <tr>
                                        <td>{{ $operation->id }}</td>
                                        <td>@fp($operation->value)</td>
                                        <td>{!! $operation->receipt !!}</td>
                                        <td>{{ $operation->created_at->format('d/m/Y') }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-secondary">
                                Nessuna operatione eseguita su questo conto
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('after_scripts')

    <script src="https://www.paypal.com/sdk/js?client-id={{ env('PAYPAL_CLIENT_ID') }}&currency=EUR&intent=capture"></script>

    <script>

        var PayPalActions;

        paypal.Buttons({
                createOrder: function(data, actions) {
                    return actions.order.create({
                        purchase_units: [{
                            description: '{{ $PayPalTopUpDescription }}',
                            amount: {
                                value: parseFloat($('input[name="data[value_paypal]"]').val())
                            },


                        }]
                    });
                },
                onApprove: function(data, actions) {

                    actions.order.capture().then(function(details) {


                        //$.post('{{ Route('logDispatcher') }}',{data:JSON.stringify(details)},function(r){});


                        var postData = {
                            value:details.purchase_units[0].amount.value/1.045,
                            actionDetails: JSON.stringify(details)
                        };
                        $.post('{{ Route('MyAtlantinex.myWalletTopUpPayPal') }}',postData,
                        function(r){
                            if(r.status == 1){
                                location.reload();
                            }
                        },'json')


                    });


                },
                onError: function(err) {
                    alert(err)
                }
        }).render('#paypal-button-container');


    </script>

    <script>
        $(function(){

            $('input[name=\'data[fake_value]\']').keyup(function(){

                var input = $(this);
                var value = input.val();
                if(value != '' && !isNaN(value)){
                    $('#confirmPayment').show();
                    var topUpValue = parseFloat(value) * 1.02;
                    var topUpValuePaypal = parseFloat(value) * 1.045;
                    $('input[name=\'data[value]\']').val(topUpValue.toFixed(2));
                    $('input[name=\'data[value_paypal]\']').val(topUpValuePaypal.toFixed(2));
                }else{
                    $('#confirmPayment').hide();
                    $('input[name=\'data[value]\']').val('');
                }

            })

            $('#confirmPayment').hide();

        })



    </script>

@endpush


@endauth

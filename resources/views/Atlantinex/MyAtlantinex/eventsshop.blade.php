@auth

@extends('admin_custom.top_left')
@section('content')
    <h2>
        Acquista un evento
    </h2>
        @if($events->count() > 0)
            <div class="row">
                @foreach($events as $event)
                    <div class="col-md-3">
                        <div class="card">
                            @if(!is_null($event->photos))
                                <div class="card-header">
                                    <img src="{{ url(collect($event->photos)->first()) }}" alt="" class="img-fluid">
                                </div>
                            @endif
                            <div class="card-body">
                                <h5>{{ $event->title }}</h5>
                                <p>
                                    {{ $event->short_description }}
                                </p>
                                <div class="py-2">
                                    <small><i class="fa fa-calendar"></i> {{ date('d/m/Y H:i',strtotime($event->happens_at)) }}</small>
                                </div>
                                <a href="{{ Route('MyAtlantinex.eventsshop.show',$event) }}" class="btn btn-primary btn-block">Guarda</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

        @else
            <div class="alert alert-primary">
                Nessun evento in programma.
            </div>
        @endif
@endsection

@push('after_scripts')

    <script>
        $(function(){
            var event_id;
            $('#selectedEventModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                event_id = button.data('id') // Extract info from data-* attributes

            })
        })
    </script>

@endpush

@endauth

@auth

@extends('admin_custom.top_left')
@section('content')

    <div class="row">
        <div class="col-md-6">
            <div>
                <h2>Acquista un nuovo pack</h2>
            </div>
        </div>
    </div>
    @if (!Auth::user()->hasRole('Company'))
        <div class="row mb-4">
            <div class="col-md-12">
                <a href="{{ $compensationPlanDownloadLink }}" download class="btn btn-sm btn-primary">Scarica il compensation plan</a>
            </div>
        </div>
    @endif


    @if(!is_null($user->pack))
    @php
        $pack = $user->pack;
    @endphp
    <div class="row">
        <div class="col-md-8">
            <div class="card">
         <div class="card-body">
        <p>
              @if(!is_null($user->packpurchases()->orderBy('created_At','DESC')->get()))
                <ul>
                    @foreach ($user->packpurchases()->orderBy('created_At','DESC')->get() as $item)
                        <li>
                        Hai acquistato il pack <b>{{ $item->pack->name }}</b> il: <b>{{ $item->created_at->format('d/m/Y') }}</b>
                        @if($item->pack->is_renewable)
                            @if (!is_null($item->pack->expires_after))
                                scade il <b>{{ date('d/m/Y',strtotime($item->created_at.'+'.$item->pack->expires_after)) }}</b> puoi rinnovarlo a partire dal
                                <b>{{ date('d/m/Y',strtotime($item->created_at.'+'.$item->pack->expires_after.'-30days')) }}</b>
                            @endif
                            @if (!is_null($item->pack->renew_price))
                                al costo di <b>@fp($item->pack->renew_price)</b>
                            @endif
                        @endif
                        @if ($item->hasExpired())
                            <span class="badge badge-secondary">Il pack è scaduto</span>
                        @endif
                    </li>
                    @endforeach
                </ul>
            @endif
        </p>
    </div>
    </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h5>I tuoi pack</h5>
        </div>
    </div>
        <div class="row">
            @foreach ($user->packpurchases as $purchase)
                @php
                    $pack = $purchase->pack;
                @endphp


                    <div class="col-md-4">

                        <div class="card">
                            <div class="card-header">
                                @if(!is_null($pack->cover))
                                    <img src="{{ url($pack->cover) }}" alt="" class="img-fluid">
                                @endif
                                <h3>{{ $pack->name }}</h3>
                            </div>
                            <div class="card-body">
                                <p>{!! $pack->description !!}</p>
                                <ul class="list">
                                    {{--
                                    @if(!is_null($pack->revenue_bonus) && $pack->revenue_bonus > 0)<li> {{ $pack->revenue_bonus }}% di revenue bonus </li>@endif
                                    @if(!is_null($pack->merchant_bonus) && $pack->merchant_bonus > 0)<li> @fp($pack->merchant_bonus) di merchant bonus e {{ $pack->merchant_bonus_eps }} EPs ad ogni nuova affiliazione di aziende </li>@endif
                                    @if(!is_null($pack->merchant_booster_bonus) && $pack->merchant_booster_bonus > 0)<li> @fp($pack->merchant_booster_bonus) di merchant booster bonus per le prime {{ $pack->merchant_booster_bonus_affiliations }} affiliazioni di aziende e {{ $pack->merchant_booster_bonus_eps }} </li>@endif
                                    @if(!is_null($pack->merchant_marketing_bonus) && $pack->merchant_marketing_bonus > 0)<li> {{ $pack->merchant_marketing_bonus }} EPs ogni @fp(100) di spazi pubblicitari acquistati dalle aziende affiliate grazie al merchant marketing bonus  </li>@endif
                                    @if(!is_null($pack->granted_eps) && $pack->granted_eps > 0)<li> {{ $pack->granted_eps }} EPs all'acquisto di questo pack </li>@endif
                                    @if(!is_null($pack->direct_commission) && $pack->direct_commission > 0)<li> {{ $pack->direct_commission }}% lordo di commissione sugli acquisti di voucher effettuati dagli affiliati diretti </li>@endif
                                    @if(!is_null($pack->merchant_bonus_affiliations) && $pack->merchant_bonus_affiliations > 0)<li>Infinite affiliazioni per le aziende</li>@endif
                                    @if(!is_null($pack->network_affiliations) && $pack->network_affiliations > 0)<li>Infinite affiliazioni per i networker</li>@endif
                                     --}}
                                </ul>
                            </div>
                        </div>
                    </div>
 @endforeach
        </div>
    @endif

    @if(!is_null($packs) && $packs->count() > 0)
        @foreach($packs->chunk(3) as $packsChunks)
            <div class="row">
                @foreach($packsChunks as $pack)
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">
                                 @if(!is_null($pack->cover))
                                    <img src="{{ url($pack->cover )}}" alt="" class="img-fluid mb-4 mt-2">
                                @endif
                                <h3>{{ $pack->name }}</h3>
                            </div>
                            <div class="card-body">
                                <div>{!! $pack->description !!}</div>
                                <ul class="list">
                                    {{--
                                    @if(!is_null($pack->revenue_bonus) && $pack->revenue_bonus > 0)<li> {{ $pack->revenue_bonus }}% di revenue bonus </li>@endif
                                    @if(!is_null($pack->merchant_bonus) && $pack->merchant_bonus > 0)<li> @fp($pack->merchant_bonus) di merchant bonus e {{ $pack->merchant_bonus_eps }} EPs ad ogni nuova affiliazione di aziende </li>@endif
                                    @if(!is_null($pack->merchant_booster_bonus) && $pack->merchant_booster_bonus > 0)<li> @fp($pack->merchant_booster_bonus) di merchant booster bonus per le prime {{ $pack->merchant_booster_bonus_affiliations }} affiliazioni di aziende e {{ $pack->merchant_booster_bonus_eps }} </li>@endif
                                    @if(!is_null($pack->merchant_marketing_bonus) && $pack->merchant_marketing_bonus > 0)<li> {{ $pack->merchant_marketing_bonus }} EPs ogni @fp(100) di spazi pubblicitari acquistati dalle aziende affiliate grazie al merchant marketing bonus  </li>@endif
                                    @if(!is_null($pack->granted_eps) && $pack->granted_eps > 0)<li> {{ $pack->granted_eps }} EPs all'acquisto di questo pack </li>@endif
                                    @if(!is_null($pack->direct_commission) && $pack->direct_commission > 0)<li> {{ $pack->direct_commission }}% lordo di commissione sugli acquisti di voucher effettuati dagli affiliati diretti </li>@endif
                                    @if(!is_null($pack->merchant_bonus_affiliations) && $pack->merchant_bonus_affiliations > 0)<li>Infinite affiliazioni per le aziende</li>@endif
                                    @if(!is_null($pack->network_affiliations) && $pack->network_affiliations > 0)<li>Infinite affiliazioni per i networker</li>@endif
                                     --}}
                                </ul>
                            </div>

                            <div class="card-footer">

                                    @if($user->canPurchasePack($pack))
                                        @if ($pack->check_if_purchasable)
                                            @if ($pack->checkPurchasability())
                                                  <button data-toggle="modal" class="btn btn-primary btn-block" data-amount="{{ $pack->price }}" data-pack_id="{{ $pack->id }}" data-target="#voucherPurchaseModal">Acquista questo pack @if(!is_null($pack->price) && $pack->price > 0) al prezzo di <b>@fp($pack->price)</b> IVA inclusa @endif</button>
                                            @else

                                                <button disabled class="btn btn-secondary btn-block" >Ancora non puoi acquistare il pack, La invitiamo a consultare il Compensation Plan.</button>

                                            @endif
                                        @else
                                            <button data-toggle="modal" class="btn btn-primary btn-block" data-amount="{{ $pack->price }}" data-pack_id="{{ $pack->id }}" data-target="#voucherPurchaseModal">Acquista questo pack @if(!is_null($pack->price) && $pack->price > 0) al prezzo di <b>@fp($pack->price)</b> IVA inclusa @endif</button>
                                        @endif

                                    @else
                                        <a href="{{ Route('MyAtlantinex.documents.index') }}" class="btn btn-primary btn-block">Firma prima i documenti richiesti</a>
                                    @endif
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @endforeach
        @else
                <div class="alert alert-info">
                    Non ci sono pack da acquistare
                </div>
        @endif
        @if(!is_null($renewable_packs) && $renewable_packs->count() > 0)
        <div class="row">
            <div class="col-md-6">
                <div>
                    <h2>Rinnova un pack</h2>
                </div>
            </div>
        </div>
        @foreach($renewable_packs->chunk(3) as $packsChunks)
            <div class="row">
                @foreach($packsChunks as $pack)
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">
                                 @if(!is_null($pack->cover))
                                    <img src="{{ url($pack->cover )}}" alt="" class="img-fluid mb-4 mt-2">
                                @endif
                                <h3>{{ $pack->name }}</h3>
                            </div>
                            <div class="card-body">
                                <div>{!! $pack->description !!}</div>
                                <ul class="list">
                                    {{--
                                    @if(!is_null($pack->revenue_bonus) && $pack->revenue_bonus > 0)<li> {{ $pack->revenue_bonus }}% di revenue bonus </li>@endif
                                    @if(!is_null($pack->merchant_bonus) && $pack->merchant_bonus > 0)<li> @fp($pack->merchant_bonus) di merchant bonus e {{ $pack->merchant_bonus_eps }} EPs ad ogni nuova affiliazione di aziende </li>@endif
                                    @if(!is_null($pack->merchant_booster_bonus) && $pack->merchant_booster_bonus > 0)<li> @fp($pack->merchant_booster_bonus) di merchant booster bonus per le prime {{ $pack->merchant_booster_bonus_affiliations }} affiliazioni di aziende e {{ $pack->merchant_booster_bonus_eps }} </li>@endif
                                    @if(!is_null($pack->merchant_marketing_bonus) && $pack->merchant_marketing_bonus > 0)<li> {{ $pack->merchant_marketing_bonus }} EPs ogni @fp(100) di spazi pubblicitari acquistati dalle aziende affiliate grazie al merchant marketing bonus  </li>@endif
                                    @if(!is_null($pack->granted_eps) && $pack->granted_eps > 0)<li> {{ $pack->granted_eps }} EPs all'acquisto di questo pack </li>@endif
                                    @if(!is_null($pack->direct_commission) && $pack->direct_commission > 0)<li> {{ $pack->direct_commission }}% lordo di commissione sugli acquisti di voucher effettuati dagli affiliati diretti </li>@endif
                                    @if(!is_null($pack->merchant_bonus_affiliations) && $pack->merchant_bonus_affiliations > 0)<li>Infinite affiliazioni per le aziende</li>@endif
                                    @if(!is_null($pack->network_affiliations) && $pack->network_affiliations > 0)<li>Infinite affiliazioni per i networker</li>@endif
                                     --}}
                                </ul>
                            </div>

                            <div class="card-footer">

                                    @if($user->canPurchasePack($pack))
                                        @if ($pack->check_if_purchasable)
                                            @if ($pack->checkPurchasability())
                                                  <button data-is_renewal="1" data-toggle="modal" class="btn btn-primary btn-block" data-amount="{{ $pack->price }}" data-pack_id="{{ $pack->id }}" data-target="#voucherPurchaseModal">Rinnova questo pack @if(!is_null($pack->price) && $pack->price > 0) al prezzo di <b>@fp($pack->price)</b> IVA inclusa @endif</button>
                                            @else

                                                <button disabled class="btn btn-secondary btn-block" >Ancora non puoi acquistare il pack, La invitiamo a consultare il Compensation Plan.</button>

                                            @endif
                                        @else
                                            <button data-is_renewal="1" data-toggle="modal" class="btn btn-primary btn-block" data-amount="{{ $pack->price }}" data-pack_id="{{ $pack->id }}" data-target="#voucherPurchaseModal">Rinnova questo pack @if(!is_null($pack->price) && $pack->price > 0) al prezzo di <b>@fp($pack->price)</b> IVA inclusa @endif</button>
                                        @endif

                                    @else
                                        <a href="{{ Route('MyAtlantinex.documents.index') }}" class="btn btn-primary btn-block">Firma prima i documenti richiesti</a>
                                    @endif
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @endforeach
        @else
                <div class="alert alert-info">
                    Non ci sono pack da rinnovare
                </div>
        @endif
@endsection

@push('before_closing_body')
    @include('components.payment_modal',['show_vouchers_amount' => false, 'payment_modal_title' => 'Procedi con l\'acquisto del pack'])
@endpush


@push('after_scripts')
<script src="https://js.stripe.com/v3/"></script>
    <script>

        var eps,amount,method, pack_id, is_renewal;
        var payment_method_id = '';
        var _token = '{{ csrf_token() }}';
        var route = '{{ Route('MyAtlantinex.packs.store') }}';


        function performStandardChecks(){
            /*
            amount = parseInt($('#amount').val());
              if(isNaN(amount) || amount == 0){
                    alert('Il valore inserito non è valido');
                    return false;
                }
            */
            return true;
        }


        function postPurchase(){

            $.post(route,{
                _token,method,amount,payment_method_id,pack_id,is_renewal
            },function(r){
                if(r.status == 1){
                    alert('Congratulazioni, hai acquistato il pack!');
                    location.href= '/admin';
                }
                console.log(r);
            },'json')
        }

        $(function(){
            $('#voucherPurchaseModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                amount = button.data('amount') // Extract info from data-* attributes
                pack_id = button.data('pack_id');
                is_renewal = (typeof(button.data('is_renewal')) != 'undefined') ? button.data('is_renewal') : 0;
//                alert(is_renewal);

                // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this)
            })
        })

        var maxAmountForWalletUsage = parseFloat({{ $user->wallet_balance }});

        $(function(){
            $('#useWallet').click(function(){
                if(!performStandardChecks()){
                    return false;
                }
                if(amount > maxAmountForWalletUsage ){
                    alert('Il valore inserito supera la disponibilità del tuo wallet');
                    return false;
                }
                method = 'wallet';
                postPurchase();
            })
            $('.stored-credit-card').click(function(){
                if(!performStandardChecks()){
                    return false;
                }
                payment_method_id = $(this).data('id');
                method = 'stored_credit_card';
                postPurchase();
            });
        })


        $(() => {
            const stripe = Stripe('{{ env('STRIPE_KEY') }}');
            const elements = stripe.elements();
            const cardElement = elements.create('card');
            cardElement.mount('#new-card-form-wrapper');

            const cardHolderName = document.getElementById('card-holder-name');
            const cardButton = document.getElementById('card-button');
            const clientSecret = cardButton.dataset.secret;

            cardButton.addEventListener('click', async (e) => {



                if(!performStandardChecks()){
                    return false;
                }else{

                      const { setupIntent, error } = await stripe.confirmCardSetup(
                    clientSecret, {
                        payment_method: {
                            card: cardElement,
                            billing_details: { name: cardHolderName.value }
                        }
                    }
                );

                if (error) {
                    alert('unsuccess')
                } else {
                        console.log(setupIntent);
                        $.post('{{ route('MyAtlantinex.attachPaymentMethodToUser') }}',setupIntent,function(r){
                            payment_method_id = setupIntent.payment_method;
                            method = 'pay_with_new_card';
                            postPurchase();
                        },'json')
                    }
                }


            });
        })
    </script>
@endpush


@endauth

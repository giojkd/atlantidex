@auth

@extends('admin_custom.top_left')
@section('content')
    <h2>
        I miei biglietti
    </h2>
    @if(!is_null($tickets))
    <table class="table table-hover table-striped">
        <thead>
            <tr>
                <th>Utente</th>
                <th>Evento</th>
                <th>Data</th>
                <th>Biglietto</th>
                <th>Descrizione</th>
                <!--<th>Quantità</th>-->
                <th>Prezzo</th>
                {{-- <th>Slides</th>  --}}
            </tr>
        </thead>
        <tbody>
            @foreach($tickets as $ticket)
                <tr>
                    <td><a target="_blank" href="/admin/user/{{ $ticket->user->id }}/edit">{{ $ticket->user->name }} {{ $ticket->user->surname }}</a> <a href="mailto:{{ $ticket->user->email }}"> <i class="fa fa-envelope"></i> </a></td>
                    <td>{{ $ticket->event->title }}</td>
                    <td>{{ $ticket->event->happens_at }}</td>
                    <td>{{ $ticket->name }}</td>
                    <td>{{ $ticket->description }}</td>
                    <!--<td>{{ $ticket->quantity }}</td>-->
                    <td>@fp($ticket->price)</td>
                    {{-- <td>
                        @if(!is_null($ticket->event->slides_file))
                            <a href="{{ url($ticket->event->slides_file) }}" class="btn btn-primary" download>Scarica le slides</a>
                        @endif
                    </td>
                     --}}
                </tr>
            @endforeach
        </tbody>
    </table>
    @else
        <div class="alert alert-info">
            Non hai acquistato ancora nessun biglietto.
        </div>
    @endif

@endsection

@endauth

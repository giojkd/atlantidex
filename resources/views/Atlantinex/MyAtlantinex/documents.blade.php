@auth

@extends('admin_custom.top_left')
@section('content')

    <div class="row">
        <div class="col-md-8">
            <div>
                <h2>I miei documenti</h2>
                <table class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th>Documento</th>
                            <th>Firmato</th>
                            <th>Azioni</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($documents as $document)
                            <tr>
                                <td>{{ $document->name }}</td>
                                <td>@if($document->users->count() > 0) <span class="text-success"><i class="fa fa-check"></i></span> @else <span class="text-danger"><i class="fa fa-times-circle"></i></span> @endif</td>
                                <td>
                                    @if($document->users->count() > 0)
                                        <a class="btn btn-primary" href="{{ $document->users->first()->pivot->document_url }}" download>Scarica</a>
                                    @else
                                        <a class="btn btn-primary" href="{{ Route('MyAtlantinex.documents.show',['document' => $document->id]) }}">Guarda</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection


@push('after_scripts')

@endpush

@endauth

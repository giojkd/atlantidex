@auth

@extends('admin_custom.top_left')
@section('content')

<h2>I tuoi dati personali - Tessera numero: {{ $user->code }}</h2>


        <div class="">
            <div class="row">
                <div class="col-md-7">
                    <div class="card">
    <div class="card-body">
        <p>
            @if(!is_null($user->packpurchases))
                <ul>
                    @foreach ($user->packpurchases as $item)
                        <li>
                        Hai acquistato il pack <b>{{ $item->pack->name }}</b> il: <b>{{ $item->created_at->format('d/m/Y') }}</b>
                        @if($item->pack->is_renewable)
                            @if (!is_null($item->pack->expires_after))
                                scade il <b>{{ date('d/m/Y',strtotime($item->created_at.'+'.$item->pack->expires_after)) }}</b> potrai rinnovarlo a partire dal
                                <b>{{ date('d/m/Y',strtotime($item->created_at.'+'.$item->pack->expires_after.'-30days')) }}</b>
                            @endif
                            @if (!is_null($item->pack->renew_price))
                                al costo di <b>@fp($item->pack->renew_price)</b>
                            @endif
                        @endif
                        @if ($item->hasExpired())
                            <span class="badge badge-secondary">Il pack è scaduto</span>
                        @endif
                    </li>
                    @endforeach
                </ul>
            @endif
        </p>
    </div>
</div>
<hr>
                    <div class="card">
    <div class="card-body">
                     {{ Aire::open()->route('MyAtlantinex.personal-data.update',$user)->addClass('modifiable-form') }}
                        {{ Aire::bind($user) }}
                        {{ Aire::hidden('address')->value(json_encode($user->address)) }}
                        {{ Aire::input('email','Email')->disabled(true) }}
                        {{ Aire::input('name','Nome') }}
                        {{ Aire::input('surname','Cognome') }}
                        {{ Aire::input('fiscal_code','Codice Fiscale') }}
                        {{ Aire::input('masked_phone','Telefono')->disabled()->addClass('undisableable') }}
                        {{ Aire::input('place_of_birth','Luogo di nascita') }}

                        @if (Auth::user()->hasRole('Networker'))

                            {{ Aire::input('vat_number','Partita iva') }}

                        @endif



                        {{ Aire::input('','Indirizzo di residenza')->value(isset($user->address['value']) ? $user->address['value'] : '')->id('userAddress') }}

                        {{ Aire::date('date_of_birth','Data di Nascita') }}
                        {{ Aire::summary() }}
                        {{ Aire::submit('Modifica')->variant()->gray() }}
                    {{ Aire::close() }}

                    <hr>

                    <h2>I tuoi indirizzi di spedizione o fatturazione</h2>

                    @if($user->addresses->count() > 0)





                    <ul class="nav nav-tabs" role="tablist">
                        @foreach($user->addresses->groupBy('type') as $group => $addresses)
                            <li class="nav-item ">
                                <a class="nav-link @if($group == 'shipping') active @endif" data-toggle="tab" role="tab" href="#{{ $group }}-addresses-tab">{{ Str::ucfirst($group) }}</a>
                            </li>
                        @endforeach
                    </ul>

                    <div class="tab-content">
                        @foreach($user->addresses->groupBy('type') as $group => $addresses)
                        <div class="table-responsive">
                            <div id="{{ $group }}-addresses-tab" class="tab-pane fade @if($group == 'shipping') show active @endif">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>
                                                Indirizzo
                                            </th>
                                            <th>Telefono</th>
                                            <th>Interno</th>
                                            <th>Note</th>
                                            <th>
                                                Azioni
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($addresses as $address)
                                            <tr>
                                                <td>
                                                    {{ $address->address->value }}
                                                </td>
                                                <td>{{ $address->phone}}</td>
                                                <td>{{ $address->address_int }}</td>
                                                <td>{{ $address->notes }}</td>
                                                <td>
                                                    <form action="{{ Route('MyAtlantinex.address.destroy',$address) }}" method="post">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @endforeach
                    </div>

                    @else
                        <div class="alert alert-secondary" role="alert">
                            Non hai ancora impostato nessun indirizzo alternativo di fatturazione o spedizione
                        </div>
                    @endif

                    <button type="button" class="btn btn-primary mt-2" data-toggle="modal" data-target="#newAddressModal">Aggiungi un nuovo indirizzo</button>
                </div>
            </div>
        </div>
        @if(!is_null($user_sponsor))
        <div class="col-md-5">
                <div class="card">
                    <div class="card-body">
                        <h4>Il tuo sponsor</h4>
                        <table class="table table-striped">
                            <tbody>
                                <tr><td><b>Tessera numero</b></td><td>{{ $user_sponsor->code }}</td></tr>
                                <tr><td><b>Nome </b></td><td>{{ $user_sponsor->full_name }}</td></tr>
                                <tr><td><b>Email</b></td><td>{{ $user_sponsor->email }}</td></tr>
                                <tr><td><b>Telefono</b></td><td>{{ $user_sponsor->phone }}</td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
        @endif

    </div>
</div>


@push('before_closing_body')

    @include('components.payment_modal',['payment_modal_title' => 'Rinnova il pack','show_vouchers_amount' => false])

    <div class="modal fade" id="newAddressModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Nuovo indirizzo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {{ Aire::open()->route('MyAtlantinex.address.store')->id('newAddressForm') }}
            <div class="modal-body">
                    {{ Aire::hidden('address') }}
                    {{ Aire::input('name','Nome e cognome*')->required()}}
                    {{ Aire::input('phone','Numero di telefono*')->required()}}
                    {{ Aire::select(['shipping'=>'Spedizione','billing' => 'Fatturazione'],'type','Tipo di indirizzo')->addClass('addressType')->required() }}
                    {{ Aire::input('','Indirizzo*')->id('newAddress')}}
                    <p class="text-muted"><small>Nel caso in cui non trovassi l'indirizzo esatto inserisci l'indirizzo più vicino suggerito dal sistema e comunicaci l'indirizzo corretto via email a amministrazione@atlantidex.com</small></p>
                    {{ Aire::input('address_int','Interno')}}
                    <div class="companyFields" style="display: none;">
                        {{ Aire::input('vat_number','Partita iva')}}
                        {{ Aire::input('sdi_code','Codice univoco')}}
                        {{ Aire::input('company_name','Nome dell\'azienda (Ragione sociale)')}}
                    </div>
                    {{ Aire::textArea('notes','Note')->placeholder('Es. se è un indirizzo di spedizione, dai indicazioni al corriere per la consegna')}}

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
                {{ Aire::submit('Salva')->variant()->gray() }}
            </div>
            {{ Aire::close() }}
            </div>
        </div>
    </div>
@endpush





@endsection

@push('after_scripts')

<script src="https://cdn.jsdelivr.net/npm/places.js@1.18.2"></script>
    <script src="https://js.stripe.com/v3/"></script>

    <script>
        //var _token = '{{ csrf_token() }}';
        var packpurchase_id = 0;
         $(() => {

            const stripe = Stripe('{{ env('STRIPE_KEY') }}');
            const elements = stripe.elements();
            const cardElement = elements.create('card');
            cardElement.mount('#new-card-form-wrapper');

            const cardHolderName = document.getElementById('card-holder-name');
            const cardButton = document.getElementById('card-button');
            const clientSecret = cardButton.dataset.secret;

            cardButton.addEventListener('click', async (e) => {

                const { setupIntent, error } = await stripe.confirmCardSetup(
                    clientSecret, {
                        payment_method: {
                            card: cardElement,
                            billing_details: { name: cardHolderName.value }
                        }
                    }
                );

                if (error) {
                    alert('unsuccess')
                } else {

                    alert(packpurchase_id);

                    /*
                        $.post('{{ route('Company.activate.store') }}',setupIntent,function(r){
                            if(r.status == 1){
                                alert('Congratulazioni, hai attivato il tuo account!');
                                location.replace('/admin');
                            }else{
                                alert('Si è verificato un problema');
                                location.reload();
                            }

                        },'json')
                    */
                    }
            });
        })


    $(() => {
        $('.addressType').change(function() {
            if($(this).val() == 'billing'){
                $(this).closest('form').find('.companyFields').fadeIn();
            }else{
                $(this).closest('form').find('.companyFields').fadeOut();
            }
        })

        var placesAutocomplete = places({
            appId: '{{env('ALGOLIA_PLACES_APP_ID')}}',
            apiKey: '{{env('ALGOLIA_PLACES_API_KEY')}}',
            container: document.querySelector('#userAddress')
        });
        placesAutocomplete.on('change', e => {
            $('input[name="address"]').val(JSON.stringify(e.suggestion));
        });

            var newAddressPlacesAutocomplete = places({
            appId: '{{env('ALGOLIA_PLACES_APP_ID')}}',
            apiKey: '{{env('ALGOLIA_PLACES_API_KEY')}}',
            container: document.querySelector('#newAddress')
        });
        newAddressPlacesAutocomplete.on('change', e => {
            $('#newAddressForm input[name="address"]').val(JSON.stringify(e.suggestion));
        });
    })

    </script>
@endpush

@endauth


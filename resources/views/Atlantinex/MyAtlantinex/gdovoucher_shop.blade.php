@auth

@extends('admin_custom.top_left')
@section('content')

     <div class="row">
            <div class="col-md-12">
                <h2>{{ $voucher->name }}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                @if(!is_null($voucher->cover))
                <img class="img-fluid" src="{{ url($voucher->cover) }}" alt="">
                @endif
                <p>
                    {!! $voucher->description !!}
                </p>
            </div>
            <div class="col-md-6">
                @if(!is_null($codeGroups))
                    <table class="table table-hover table-striped">
                        <thead>
                            <tr>
                                <th>Valore del voucher</th>
                                <th>Quantità</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($codeGroups as $groupAmount =>$codes)
                                <tr>
                                    <td>@fp($groupAmount)</td>
                                    <td>
                                        <select name="" id="" class="form-control vouchers-select" data-price="{{ $codes->first()->value }}">
                                            @for($i = 0; $i <= count($codes) ; $i++)
                                                <option value="{{ $i }}">{{ $i }}</option>
                                            @endfor
                                        </select>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endif
                <div id="purchaseSummary"  style="display: none;">
                    <div class="text-right">
                        <h1>Totale: €<span id="purchaseTotal"></span></h1>
                    </div>
                    <button data-toggle="modal" class="btn btn-primary btn-block" data-target="#voucherPurchaseModal">Acquista con il tuo wallet</button>
                    <div class="text-center my-4">
                        <small><i>oppure</i></small>
                    </div>
                    <form action="{{ Route('MyAtlantinex.spendVouchers') }}">
                        <input type="hidden" name="voucher_id" value="{{ $voucher->id }}">
                        <input type="hidden" name="selected_vouchers" id="selectedVouchersInput">
                        <button class="btn btn-primary btn-block">Utilizza i tuoi voucher</button>
                        <p class="text-muted mt-2 text-center">
                            <small>Gentile cliente,
la informiamo che, nel caso in cui volesse effettuare un acquisto utilizzando come mezzo di pagamento i suoi voucher Atlantidex, se il valore degli stessi nella categoria da lei selezionata non fosse sufficiente per il saldo, il
sistema procederà comunque in automatico al preliievo dei suoi voucher a partire dalla categoria di EP più vicina a quella da lei già selezionata fino al saldo dell’importo richiesto.</small>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection


@push('before_closing_body')
    @include('components.payment_modal',
    [
        'payment_modal_title' => 'Acquista i voucher della GDO',
        'show_vouchers_amount' => false,
        'card_is_disabled' => true
    ])
@endpush


@push('after_scripts')
        <script src="https://js.stripe.com/v3/"></script>
    <script>
        var selectedVouchers = [];
        var quantitySelected = 0;
        var amount,method;
        var selectedVouchersInput = [];
        var payment_method_id = '';
        var _token = '{{ csrf_token() }}';
        var route = '{{ Route('MyAtlantinex.gdovouchersshop.store') }}';
        var voucher_id = {{ $voucher->id }};

        function selectVouchers(){
            selectedVouchers = [];
            selectedVouchersInput = [];
            quantitySelected = 0;
            amount = 0;
            $('.vouchers-select').each(function(){
                if(parseInt($(this).val()) > 0){
                    selectedVouchers.push({
                            'price':$(this).data('price'),
                            'quantity':parseInt($(this).val())
                        })
                    selectedVouchersInput.push($(this).data('price')+'_'+parseInt($(this).val()));

                    amount += parseFloat($(this).data('price')) * parseInt($(this).val());
                    quantitySelected += parseInt($(this).val());
                }
            })

            $('#selectedVouchersInput').val(selectedVouchersInput.join('-'));

            if(quantitySelected > 0){
                $('#purchaseSummary').slideDown();
                $('#purchaseTotal').html(amount.toFixed(2))
            }else{
                $('#purchaseSummary').slideUp();
            }
        }
        $(function(){
            $('.vouchers-select').change(function(){
                selectVouchers();
            })
        })




        function performStandardChecks(){
            return true;

            amount = parseInt($('#amount').val());
              if(isNaN(amount) || amount == 0){
                    alert('Il valore inserito non è valido');
                    return false;
                }
            return true;
        }


        function postPurchase(){
            $('#useWallet').prop('disabled',true);
            $.post(route,{
                _token,method,amount,payment_method_id,selectedVouchers,voucher_id
            },function(r){
                if(r.status == 1){
                    alert('Congratulazioni, l\'acquisto dei voucher è andato a buon fine!');
                    location.reload();
                }
                console.log(r);
            },'json')
        }

        $(function(){
            $('#voucherPurchaseModal').on('show.bs.modal', function (event) {
                var modal = $(this);
                var button = $(event.relatedTarget);
                selectVouchers();

            })
        })

        var maxAmountForWalletUsage = parseFloat({{ $user->wallet_balance }});

        $(function(){
            $('#useWallet').click(function(){
                if(!performStandardChecks()){
                    return false;
                }
                if(amount > maxAmountForWalletUsage ){
                    alert('Il valore inserito supera la disponibilità del tuo wallet');
                    return false;
                }
                method = 'wallet';
                postPurchase();
            })
            $('.stored-credit-card').click(function(){
                if(!performStandardChecks()){
                    return false;
                }
                payment_method_id = $(this).data('id');
                method = 'stored_credit_card';
                postPurchase();
            });
        })


        $(() => {
            const stripe = Stripe('{{ env('STRIPE_KEY') }}');
            const elements = stripe.elements();
            const cardElement = elements.create('card');
            cardElement.mount('#new-card-form-wrapper');

            const cardHolderName = document.getElementById('card-holder-name');
            const cardButton = document.getElementById('card-button');
            const clientSecret = cardButton.dataset.secret;

            cardButton.addEventListener('click', async (e) => {

                if(!performStandardChecks()){
                    return false;
                }else{

                      const { setupIntent, error } = await stripe.confirmCardSetup(
                    clientSecret, {
                        payment_method: {
                            card: cardElement,
                            billing_details: { name: cardHolderName.value }
                        }
                    }
                );

                if (error) {
                    alert('unsuccess')
                } else {
                        console.log(setupIntent);
                        $.post('{{ route('MyAtlantinex.attachPaymentMethodToUser') }}',setupIntent,function(r){
                            payment_method_id = setupIntent.payment_method;
                            method = 'pay_with_new_card';
                            postPurchase();
                        },'json')
                    }
                }


            });
        })
    </script>
@endpush


@endauth

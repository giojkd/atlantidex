@auth

@extends('admin_custom.top_left')
@section('content')

    <div class="row">
        <div class="col-md-9">
            <div>
                <h2>Acquista i voucher</h2>

                @if(isset($status))
                    <div class="alert @if($status == 1) alert-success @else alert-danger @endif">
                        {{ $message }}
                    </div>
                @endif

                <div class="text-justify">

                    {{--
                    {{ Setting::get('vouchers_intro_'.app()->getLocale())}}
                     --}}
                     <p>
                        Imposta l'importo dei voucher che vuoi acquistare vincolandoli alla relativa categoria di EP.<br>
                        Ti ricordiamo che:<br>
                        <ul>
                            <li>
                                Potrai acquistare prodotti e servizi anche nelle categorie di EP superiori a quella del voucher da te selezionato ma non in quelle inferiori.
                            </li>
                            <li>
                                Una volta acquistato il voucher avrai tempo 2 anni per riscattarlo.
                            </li>
                        </ul>
                     </p>
                </div>
            </div>
        </div>
    </div>

    @if($vouchers->count() > 0)

        <div class="table-responsive">
            <table class="table table-hover table-striped">
                <head>
                    <tr>
                        <th>Categoria voucher acquistati</th>
                        {{-- <th class="text-right">Ammontare</th>  --}}
                        <th>Importo acquistato</th>
                        <th>Importo utilizzato</th>
                        <th>Importo residuo</th>
                    </tr>
                </head>
                <tbody>
                    @foreach($vouchers as $voucher)
                        <tr>
                            <td>{{ $voucher->eps }}EP</td>
                              {{-- <td class="text-right">@fp($voucher->amount)</td>  --}}
                            <td>@fp($voucher->purchased)</td>
                            <td>@fp($voucher->spent)</td>
                            <td>@fp($voucher->amount)</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#vouchersDetailsModal">
            <i class="fa fa-search-plus"></i> Guarda il dettaglio
        </button>

        @push('before_closing_body')

            <div class="modal fade" id="vouchersDetailsModal" tabindex="-1" role="dialog" aria-labelledby="vouchersDetailsModal" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="vouchersDetailsModal">Dettaglio dei tuoi voucher</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="card">
                            <div class="card-header">
                                <h5>Acquisti di vouchers</h5>
                            </div>
                            <div class="card-body">
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <tr>
                                            <th>EP</th>
                                            <th>Ammontare</th>
                                            <th>Scadono il</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($voucherPurchases as $purchase)
                                        <tr>
                                            <td>{{ $purchase->eps }}</td>
                                            <td>@fp($purchase->amount)</td>
                                            <td>{{ date('d/m/Y',strtotime($purchase->created_at.' +2years')) }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @if ($voucherOperations->count() > 0)
                            <div class="card">
                                <div class="card-header">
                                    <h5>Spese dei vouchers</h5>
                                </div>
                                <div class="card-body">
                                    <table class="table table-hover table-striped">
                                        <thead>
                                            <tr>
                                                <th>Data acquisto</th>
                                                <th>EP</th>
                                                <th>Ammontare</th>
                                                <th>Acquisto</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($voucherOperations as $operation)
                                            <tr>
                                                <td>{{ date('d/m/Y',strtotime($operation->created_at)) }}</td>
                                                <td>{{ $operation->eps }}</td>
                                                <td>@fp($operation->amount)</td>
                                                <td>{{ $operation->description }}</td>

                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        {{-- <button type="button" class="btn btn-primary">Save changes</button>  --}}
                    </div>
                    </div>
                </div>
            </div>

        @endpush


    @endif

    <hr>
    <div class="row">
        @for($i = $eps_interval[0] ; $i <= $eps_interval[1]; $i++)
            <div class="col-md-2">
                <div class="voucher-thumbnail p-3 border border-secondary mb-3 rounded-lg">
                    <div class="voucher-thumbnail-header">
                        <h4>Voucher da {{ $i }} EP</h4>
                    </div>
                    <div class="voucher-thumbnail-body">
                        @if(isset($vouchers[$i]))
                            <div class="alert alert-primary p-1">
                                <small>Hai @fp($vouchers[$i]->amount) in questa categoria</small>
                            </div>
                        @endif
                    </div>
                    <div class="voucher-thumbnail-footer">
                        <button data-toggle="modal" class="btn btn-secondary btn-sm btn-block" data-eps="{{ $i }}" data-target="#voucherPurchaseModal">Acquista questi voucher</button>
                        <div class="text-center">
                            <small><a target="_blank" href="{{ Route('Company.epCategory') }}?{{ http_build_query(['epcategory'=>$i,'product_type_id' => 1]) }}">Guarda i prodotti</a></small><br>
                            <small><a target="_blank" href="{{ Route('Company.epCategory') }}?{{ http_build_query(['epcategory'=>$i, 'product_type_id' => 2]) }}">Guarda i servizi</a></small>
                        </div>
                    </div>

                </div>
            </div>
        @endfor
    </div>

@push('before_closing_body')
    @include('components.payment_modal',['payment_modal_title' => 'Stai per acquistare voucher da <span class="eps"></span> EP','card_is_disabled' => true])
@endpush


@push('after_scripts')
<script src="https://js.stripe.com/v3/"></script>
    <script>

        var eps,amount,method;
        var payment_method_id = '';
        var _token = '{{ csrf_token() }}';
        var route = '{{ Route('MyAtlantinex.vouchers.store') }}';


        function performStandardChecks(){
            amount = parseInt($('#amount').val());
              if(isNaN(amount) || amount == 0){
                    alert('Il valore inserito non è valido');
                    return false;
                }
            return true;
        }


        function postPurchase(){

            $.post(route,{
                eps,_token,method,amount,payment_method_id
            },function(r){
                if(r.status == 1){
                    alert('Congratulazioni, hai acquistato il voucher!');
                    location.reload();
                }else{
                    alert(r.message);
                }
                console.log(r);
            },'json')
        }

        $(function(){
            $('#voucherPurchaseModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                eps = button.data('eps') // Extract info from data-* attributes
                // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this)
                modal.find('.eps:not(input)').text(eps)
                modal.find('input.eps').val(eps)
            })
        })

        var maxAmountForWalletUsage = parseFloat({{ $user->wallet_balance }});

        $(function(){
            $('#useWallet').click(function(){
                if(!performStandardChecks()){
                    return false;
                }
                if(amount > maxAmountForWalletUsage ){
                    alert('Il valore inserito supera la disponibilità del tuo wallet');
                    return false;
                }
                method = 'wallet';
                postPurchase();
            })
            $('.stored-credit-card').click(function(){
                if(!performStandardChecks()){
                    return false;
                }
                payment_method_id = $(this).data('id');
                method = 'stored_credit_card';
                postPurchase();
            });
        })


        $(() => {
            const stripe = Stripe('{{ env('STRIPE_KEY') }}');
            const elements = stripe.elements();
            const cardElement = elements.create('card');
            cardElement.mount('#new-card-form-wrapper');

            const cardHolderName = document.getElementById('card-holder-name');
            const cardButton = document.getElementById('card-button');
            const clientSecret = cardButton.dataset.secret;

            cardButton.addEventListener('click', async (e) => {



                if(!performStandardChecks()){
                    return false;
                }else{
                      const { setupIntent, error } = await stripe.confirmCardSetup(
                    clientSecret, {
                        payment_method: {
                            card: cardElement,
                            billing_details: { name: cardHolderName.value }
                        }
                    }
                );

                if (error) {
                    alert('unsuccess')
                } else {
                        console.log(setupIntent);
                        $.post('{{ route('MyAtlantinex.attachPaymentMethodToUser') }}',setupIntent,function(r){
                            payment_method_id = setupIntent.payment_method;
                            method = 'pay_with_new_card';
                            postPurchase();
                        },'json')
                    }
                }


            });
        })
    </script>
@endpush

@endsection

@endauth

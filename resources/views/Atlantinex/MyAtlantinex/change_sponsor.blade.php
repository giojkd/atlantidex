@auth

@extends('admin_custom.top_left')

@section('content')

    <div class="row">
        <div class="col-md-9">
            <div>
                <h2>Richiesta di cambio sponsor</h2>
                <div class="text-justify">
                    {{ Setting::get('change_sponsor_intro_it')}}
                </div>
                <h4 class="mt-4">Richieste ricevute</h4>
                @if($received_requests->count() > 0)

                    @include('Atlantinex.MyAtlantinex.ChangeSponsor.requests_table',['requests' => $received_requests])

                @else
                    <div class="alert alert-secondary">Non hai ricevuto nessuna richiesta.</div>
                @endif

                <h4 class="mt-4">Richieste inviate</h4>
                @if($sent_requests->count() > 0)

                    @include('Atlantinex.MyAtlantinex.ChangeSponsor.requests_table',['requests' => $sent_requests])

                @else
                    <div class="alert alert-secondary">Non hai inviato nessuna richiesta.</div>
                @endif
                <hr>
                <form action="{{ Route('MyAtlantinex.changeSponsorCreate') }}" class="form" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="">Invia una nuova richiesta</label>
                        <input name="user_code" placeholder="Codice del networker di destinazione" type="text" class="form-control">
                    </div>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <button type="submit" class="btn btn-primary btn-block">Invia richiesta</button>
                </form>


            </div>
        </div>
    </div>

@endsection

@endauth

@auth

@extends('admin_custom.top_left')

@section('content')


        <div class="row">
            <div class="col-md-12">
                <h2>{{ $event->title }}</h2>
                <h4 class="py-2">
                                    <i class="fa fa-calendar"></i> {{ date('d/m/Y H:i',strtotime($event->happens_at)) }}
                </h4>
            </div>

        </div>
        <div class="row">
            <div class="col-md-6">
                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        @if(!is_null($event->photos))
                            @foreach($event->photos as $index => $photo)
                                <div class="carousel-item @if($index == 0) active @endif">
                                    <img class="d-block w-100" src="/{{ $photo }}" alt="First slide">
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                @if(!is_null($event->description))
                    <div class="mt-4">
                        {!! $event->description !!}
                    </div>
                @endif
            </div>
            <div class="col-md-6">

                    @foreach($event->available_tickets as $index =>  $ticket)
                        <div class="card">
                            <div class="card-header">
                                <h4>{{ $ticket['name'] }}</h4>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-9">
                                        <p>
                                            @if(isset($ticket['description']))
                                                {{ $ticket['description'] }}
                                            @endif
                                        </p>
                                    </div>
                                    <div class="col-md-3">
                                        @if(isset($ticket['price']))
                                            <h2>@fp($ticket['price'])</h2>
                                        @endif
                                        <div class="form-group form-group-lg">
                                            <select name="" id="" class="form-control tickets-select" data-index="{{ $index }}"   @if(isset($ticket['price'])) data-price="{{ $ticket['price'] }}" @else data-price="0" @endif>
                                                @for($i = 0 ; $i <= $ticket['quantity'] ; $i++)
                                                    <option value="{{ $i }}">{{ $i }}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                <div id="purchaseSummary"  style="display: none;">
                    <div class="text-right">
                        <h1>Totale: €<span class="purchaseTotal"></span></h1>
                    </div>
                    <button data-toggle="modal" class="btn btn-primary btn-block" data-target="#voucherPurchaseModal">Acquista</button>
                </div>
                <div id="purchaseSummaryNetworker"  style="display: none;">
                    <div class="text-right">
                        <h1>Totale: €<span class="purchaseTotal"></span></h1>
                    </div>
                    <a href="javascript:postPurchaseFree();" class="btn btn-primary btn-block" data-target="#voucherPurchaseModal">Acquista</a>
                </div>
            </div>
        </div>



@endsection

@push('before_closing_body')
    @include('components.payment_modal',['payment_modal_title' => 'Acquista i biglietti','show_vouchers_amount' => false])
@endpush

@push('after_scripts')
        <script src="https://js.stripe.com/v3/"></script>
    <script>
        var eventBelongsToAnAdministrator = {{ ($event->user->isAdministrator()) ? 'true' : 'false' }};
        var selectedTickets = [];
        var quantitySelected = 0;
        var amount,method;
        var payment_method_id = '';
        var _token = '{{ csrf_token() }}';
        var route = '{{ Route('MyAtlantinex.eventsshop.store') }}';
        var routeFree = '{{ Route('MyAtlantinex.eventsShopFree') }}';
        var event_id = {{ $event->id }};

        function selectTickets(){
            selectedTickets = [];
            quantitySelected = 0;
            amount = 0;
            $('.tickets-select').each(function(){
                if(parseInt($(this).val()) > 0){
                    selectedTickets.push({
                            'id':$(this).data('index'),
                            'price':$(this).data('price'),
                            'quantity':parseInt($(this).val())
                        })

                    amount += parseFloat($(this).data('price')) * parseInt($(this).val());
                    quantitySelected += parseInt($(this).val());
                }
            })

            if(eventBelongsToAnAdministrator || amount > 0){
                if(quantitySelected > 0){
                    $('#purchaseSummary').slideDown();
                    $('.purchaseTotal').html(amount.toFixed(2))
                }else{
                    $('#purchaseSummary').slideUp();
                }
            }else{

                if(quantitySelected > 0){
                    $('#purchaseSummaryNetworker').slideDown();
                    $('.purchaseTotal').html(amount.toFixed(2))
                }else{
                    $('#purchaseSummaryNetworker').slideUp();
                }
            }

        }
        $(function(){
            $('.tickets-select').change(function(){
                selectTickets();
            })
        })




        function performStandardChecks(){
            return true;

            amount = parseInt($('#amount').val());
              if(isNaN(amount) || amount == 0){
                    alert('Il valore inserito non è valido');
                    return false;
                }
            return true;
        }

        function postPurchaseFree(){
             $.post(routeFree,{
                _token,method,amount,payment_method_id,selectedTickets,event_id
            },function(r){
                if(r.status == 1){
                    alert('Congratulazioni, l\'acquisto dei biglietti è andato a buon fine!');

                }
                console.log(r);
            },'json')
        }

        function postPurchase(){

            $.post(route,{
                _token,method,amount,payment_method_id,selectedTickets,event_id
            },function(r){
                if(r.status == 1){
                    alert('Congratulazioni, l\'acquisto dei biglietti è andato a buon fine!');

                }
                console.log(r);
            },'json')
        }

        $(function(){
            $('#voucherPurchaseModal').on('show.bs.modal', function (event) {
                var modal = $(this);
                var button = $(event.relatedTarget);
                selectTickets();

            })
        })

        var maxAmountForWalletUsage = parseFloat({{ $user->wallet_balance }});

        $(function(){
            $('#useWallet').click(function(){
                if(!performStandardChecks()){
                    return false;
                }
                if(amount > maxAmountForWalletUsage ){
                    alert('Il valore inserito supera la disponibilità del tuo wallet');
                    return false;
                }
                method = 'wallet';
                postPurchase();
            })
            $('.stored-credit-card').click(function(){
                if(!performStandardChecks()){
                    return false;
                }
                payment_method_id = $(this).data('id');
                method = 'stored_credit_card';
                postPurchase();
            });
        })


        $(() => {
            const stripe = Stripe('{{ env('STRIPE_KEY') }}');
            const elements = stripe.elements();
            const cardElement = elements.create('card');
            cardElement.mount('#new-card-form-wrapper');

            const cardHolderName = document.getElementById('card-holder-name');
            const cardButton = document.getElementById('card-button');
            const clientSecret = cardButton.dataset.secret;

            cardButton.addEventListener('click', async (e) => {

                if(!performStandardChecks()){
                    return false;
                }else{

                      const { setupIntent, error } = await stripe.confirmCardSetup(
                    clientSecret, {
                        payment_method: {
                            card: cardElement,
                            billing_details: { name: cardHolderName.value }
                        }
                    }
                );

                if (error) {
                    alert('unsuccess')
                } else {
                        console.log(setupIntent);
                        $.post('{{ route('MyAtlantinex.attachPaymentMethodToUser') }}',setupIntent,function(r){
                            payment_method_id = setupIntent.payment_method;
                            method = 'pay_with_new_card';
                            postPurchase();
                        },'json')
                    }
                }


            });
        })
    </script>
@endpush

@endauth

@auth

@extends('admin_custom.top_left')
@section('content')

    <h2>Acquista i voucher per la GDO</h2>
     <div class="card my-4">
         <div class="card-body">
             In questa sezione potrai fare shopping acquistando i voucher delle migliori aziende della GDO presenti sul mercato!
         </div>
     </div>
    <div class="row">
        @foreach($vouchers as $voucher)
            <div class="col-md-3">
                <div class="card">
                    <div class="card-header">
                        {{ $voucher->name }}
                        @if(!is_null($voucher->cover))
                            <div class="card-cover" style="background: url({{ url($voucher->cover) }})"></div>

                        @endif
                    </div>
                    <div class="card-body">
                        <p>{!! $voucher->short_description !!}</p>
                        <div>
                            <span class="badge badge-primary">{{ $voucher->eps }}EP/100€</span>
                        </div>
                            <a class="btn btn-primary btn-block text-white mt-3" href="{{ Route('MyAtlantinex.gdovouchersshop.show',$voucher) }}" >Acquista</a>

                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection


@push('after_scripts')

@endpush

@endauth

@auth

@extends('admin_custom.top_left')
@section('content')


    <div class="row">
        <div class="col-md-6">
            <div>
                <h2>I tuoi metodi di pagamento</h2>


                @if($payment_methods->count() > 0)
                    @foreach($payment_methods as $payment_method)
                    <div class="card mb-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-10">
                                    Carta di credito: <b>{{ Str::ucfirst($payment_method->card->brand) }} ({{ $payment_method->card->last4 }})</b>
                                    Scad: <b>{{ $payment_method->card->exp_month }}/{{ $payment_method->card->exp_year }}</b>
                                </div>
                                <div class="col-md-2 text-right">
                                    <form method="POST" action="{{ Route('MyAtlantinex.payment-data.destroy',['payment_datum' =>  $payment_method->id]) }}">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                @else
                    <div class="alert alert-primary">
                        Non hai ancora nessun metodo di pagamento impostato.
                    </div>
                @endif
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="card-holder-name">Aggiungi una nuova carta di credito </label>
                                <input type="text" id="card-holder-name" placeholder="Nome del titolare della carta" class="form-control">
                            </div>
                            <div id="new-card-form-wrapper"></div>
                            <div class="alert alert-secondary text-center mt-3" style="background-color: #ececec">
                                <span class="text-success"><i class="fa fa-lock"></i></span> <span class="text-dark">Garantito <b>protetto e sicuro</b> da</span> <img height="40" src="{{ asset('front/img/icons/stripe.png') }}" alt=""><br>
                                <img src="{{ asset('front/img/icons/american_express.png') }}" alt="">
                                <img src="{{ asset('front/img/icons/visa_electron.png') }}" alt="">
                                <img src="{{ asset('front/img/icons/mastercard2.png') }}" alt="">
                                <img src="{{ asset('front/img/icons/diners_club.png') }}" alt="">
                            </div>
                            <button data-secret="{{ $client_secret_intent->client_secret }}" id="card-button" class="btn btn-primary btn-block mt-4">Aggiungi la carta di credito</button>
                            </div>
                        </div>
            </div>


        </div>
        <div class="col-md-6">
            <div>
                <h2>Le tue coordinate bancarie</h2>
                <p>
                    <small>
                        Specifica il C/C sul quale vuoi ricevere i pagamenti.

                    </small>
                </p>
                   <div class="card">
                        <div class="card-body">


                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            {{ Aire::open()->route('MyAtlantinex.setUserBankAccount')->addClass('modifiable-form') }}
                                    {{ Aire::bind($user->bank_account) }}
                                    {{ Aire::input('bank','Istituto bancario') }}
                                    {{ Aire::input('bank_account_holder_name','Titolare del conto') }}
                                    {{ Aire::input('bank_account_iban','Codice IBAN')->placeHolder('ITXXXXXXXXXXXXXXXXXXXXXXXXX') }}
                                    {{ Aire::input('bank_account_bic','Codice BIC') }}

                                    {{ Aire::submit('Modifica')->variant()->gray() }}
                                {{ Aire::close() }}




            </div>
            </div>
            </div>
        </div>
    </div>


@endsection
@push('after_scripts')

    <script src="https://js.stripe.com/v3/"></script>

    <script>
        $(() => {
            const stripe = Stripe('{{ env('STRIPE_KEY') }}');
            const elements = stripe.elements();
            const cardElement = elements.create('card');
            cardElement.mount('#new-card-form-wrapper');

            const cardHolderName = document.getElementById('card-holder-name');
            const cardButton = document.getElementById('card-button');
            const clientSecret = cardButton.dataset.secret;

            cardButton.addEventListener('click', async (e) => {

                const { setupIntent, error } = await stripe.confirmCardSetup(
                    clientSecret, {
                        payment_method: {
                            card: cardElement,
                            billing_details: { name: cardHolderName.value }
                        }
                    }
                );

                if (error) {
                    alert('Abbiamo riscontrato un problema con la carta di credito che hai inserito.');
                    location.reload();
                } else {
                    console.log(setupIntent);
                    $.post('{{ route('MyAtlantinex.attachPaymentMethodToUser') }}',setupIntent,function(r){
                        location.reload();
                    },'json')
                }

            });
        })
    </script>
@endpush

@endauth

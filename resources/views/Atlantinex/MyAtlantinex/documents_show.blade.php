@auth

@extends('admin_custom.top_left')
@section('content')

    <div class="row">
        <div class="col-md-8">
            <h1>{{ $document->name }}</h1>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="my-4">
                {!! $document->description !!}
            </div>
              @if (!is_null($document->links))
                            <ul class="list-group mb-4">
                                @foreach ($document->links as $link)
                                        <li class="list-group-item">
                                            <a download href="{{ url($link['href']) }}"><i class="fa fa-link"></i> {{ $link['label'] }}</a>
                                        </li>
                                @endforeach
                            </ul>
                        @endif


            <p class="text-muted">
                <small>
                    Per poter procedere alla sottoscrizione è necessario leggere tutto il documento sottostante, scorrendo sopra allo stesso con il cursore.
                </small>
            </p>
            <div class="document-holder">
                {!! $documentRendered !!}
            </div>




            @if($document->is_signable_with_checkbox)
                @hss('10')
                <div class="signature-wrapper" style="display: none">
                    <form action="{{ Route('MyAtlantinex.signDocumentWithCheckbox') }}" method="POST">
                        @csrf
                        @hss('10')
                        <div class="form-group">
                            <label required ><input name="checkbox" value="1" type="checkbox"> Accetto il contratto</label>
                        </div>

                        @if(!is_null($document->acceptable_policy) && $document->acceptable_policy != '')

                            <h3>Accetta la policy</h3>
                            <div class="privacy-policy-wrapper">
                                <small>
                                    {!! $document->acceptable_policy !!}
                                </small>
                            </div>
                            <div class="form-group">
                                <label required ><input name="accepts_policy" value="1" type="checkbox"> Accetto la policy</label>
                            </div>
                        @endif
                        <input type="hidden" name="id" value="{{ $document->id }}">

                        <h3>Carica un documento di identità in corso di validità</h3>
                        @include('components.dropzone',['field'=>['name' => 'identity_document','upload-url' => '/admin/media-dropzone/document']])
                        @hss('20')

                        <button type="submit" class="btn btn-primary btn-block">Conferma</button>
                    </form>
                </div>
            @else
            <div class="signature-intro">
                <p class="text-muted">
                    <small>Devi leggere tutto il documento prima di procedere alla firma</small>
                </p>
            </div>
            <div class="signature-wrapper" style="display: none">
                <h2 class="m-0 p-4 bg-white text-center">Firma il contratto</h2>




                <div class="my-4">
                    <div class="otp-verification-form">
                        <form action="" class="form ">
                            @csrf
                            <input type="hidden" name="id" value="{{ $document->id }}">
                            <input type="hidden" name="model" value="App\Models\Document">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">+39</span>
                                    </div>
                                    <input type="text" class="form-control" name="phone" placeholder="Inserisci il tuo numero di telefono">
                                </div>
                                <p class="text-muted">
                                    <small>Per firmare il contratto devi verificare il tuo numero di telefono cellulare</small>
                                </p>
                            </div>
                            <button class="btn btn-primary" type="submit">Invia codice di verifica</button>
                        </form>
                        <form action="" style="display: none">
                            @csrf
                            <input type="hidden" name="id" value="{{ $document->id }}">
                            <input type="hidden" name="model" value="App\Models\Document">
                            <div class="form-group">
                                <input type="text" class="form-control" name="code" placeholder="XXXXXX">
                                <p class="text-muted">
                                    <small>Inserisci il codice che hai ricevuto per SMS</small>
                                </p>
                            </div>
                            <button class="btn btn-primary" type="submit">Invia codice di verifica</button>
                        </form>
                    </div>
                </div>
                <div class="autograph-signature" style="display: none">
                    <div class="signature-pad" id="sig">

                    </div>
                    <p class="text-muted">
                        <small>Inserisci la firma nell'area sopra per contrassegnare il contratto</small>
                    </p>

                    <form action="{{ route('MyAtlantinex.documents.update',['document' => $document->id ]) }}" method="POST">
                        @csrf
                        @method('PUT')

                        <div class="dropzone-wrapper">
                            @include('components.dropzone',['field'=>['name' => 'identity_document','upload-url' => '/admin/media-dropzone/document']])
                        </div>

                        @if(isset($signAs) && isset($changeSponsorRequestId))
                            <input type="hidden" name="sign_as" value="{{ $signAs }}">
                            <input type="hidden" name="change_sponsor_request_id" value="{{ $changeSponsorRequestId }}">
                        @endif
                        <div class="row">
                            <div class="col-md-3">
                                <button  id="clear" type="button" class="btn btn-secondary btn-lg btn-block mt-2">Riprova</button>
                            </div>
                            <div class="col-md-9">
                                <button type="submit" class="btn btn-primary btn-lg btn-block mt-2">Conferma la firma</button>
                            </div>
                        </div>
                        <textarea id="signature64" name="signed" style="display: none"></textarea>
                    </form>
                </div>

            </div>
            @endif
        </div>
    </div>

@endsection

@push('custom_styles')
    <style rel="stylesheet" type="text/css">
        .document-holder{
            background:#fff;
            padding: 40px;
            max-height: 800px;
            overflow: scroll;
        }
        #sig{
            height: 240px;
            width: 100%;
            border: 4px dashed #e9e9e9
        }

        .kbw-signature {
            display: inline-block;
            border: 1px solid #a0a0a0;
            -ms-touch-action: none;
        }
        .kbw-signature-disabled {
            opacity: 0.35;
        }
    </style>

    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
@endpush

@push('before_closing_body')

    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="{{ asset('front/js/jquery_signature/jquery.signature.js') }}"></script>

    <script>

        $(document).ready(function () {
            $('.document-holder').on('scroll', chk_scroll);

        });

        function chk_scroll(e) {

            var elem = $(e.currentTarget);
            var elemHeight = elem.outerHeight();
            elemHeight = elemHeight;

             var elScroll = elem[0].scrollHeight - 640;

            if(elem.scrollTop() + elem.innerHeight() >= elScroll) {
                $('.signature-intro').slideUp();
                $('.signature-wrapper').slideDown();
            }

        }

        var form_1 = $('.otp-verification-form form:first-child');
        var form_2 = $('.otp-verification-form form:last-child');



        form_1.submit(function(e){
            e.preventDefault();
            $.post('{{ Route('MyAtlantinex.otps.store') }}',form_1.serialize(),function(r){
                if(r.status == 1){
                    form_1.slideUp();
                    form_2.slideDown();
                }else{
                    alert('L\'invio del codice non ha avuto successo');
                }
            },'json');
        })

        form_2.submit(function(e){
            e.preventDefault();
            $.post('{{ Route('MyAtlantinex.OtpCheck') }}',form_2.serialize(),function(r){
                console.log(r);
                if(r.status == 1){
                    form_2.slideUp('normal',function(){
                        //var signaturePadWidth = $('.signature-pad').width();
                        //$('.signature-pad canvas').attr('width',signaturePadWidth);
                        var sig = $('#sig').signature({syncField: '#signature64', syncFormat: 'PNG', background: '#fff',color: 'black'});
                        $('#clear').click(function(e) {
                            e.preventDefault();
                            sig.signature('clear');
                            $("#signature64").val('');
                        });
                    });

                    $('.autograph-signature').slideDown();
                }else{
                    alert('Il codice inserito non è valido')
                }
            })
        })


    </script>
@endpush

@endauth

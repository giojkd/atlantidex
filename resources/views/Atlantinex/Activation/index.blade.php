@auth

@extends('admin_custom.top_left')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                    <h2>Attiva il tuo account</h2>
                    <p class="text-muted mt-4">

                        {!! $intro ?? '' !!}

                        </p>
            </div>
            <div class="col-md-8">

                @foreach($steps as $index =>  $step)
                    <div class="card">
                        <div class="card-body">
                            <div class="row">

                                <div class="col-md-12">
                                    <h4>{{ $step['name'] }}</h4>
                                    <div>{!! $step['description'] !!}</div>
                                    <hr>
                                    @if($user->stepIsAllowed($index,$steps))

                                            @if($step['status'])
                                                Completato il {{ date('d/m/Y',strtotime($step['completed_at'])) }}
                                                @if(count($step['buttons']) > 0)
                                                    @foreach ($step['buttons'] as $button)
                                                        <a class="btn btn-sm btn-primary" @if($button['is_download']) download @endif href="{{ $button['href'] }}">{{ $button['label'] }}</a>
                                                        @if(isset($button['helptext']))
                                                            <p>
                                                                <small>{{ $button['helptext'] }}</small>
                                                            </p>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @else
                                                <a href="{{ $step['activation_link'] }}" class="btn btn-primary btn-block">Completa lo step</a>
                                            @endif
                                    @else

                                        <a href="#" class="btn btn-secondary btn-block">Completa lo step</a>

                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection

@endauth

@auth

@extends('admin_custom.top_left')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2>Conferma la tua volontà di diventare marketer</h2>
                <p class="mt-4">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam posuere nulla libero, eget rutrum nisl pulvinar sed. Nullam tristique sapien libero, vitae bibendum lectus tincidunt at. Ut vulputate, quam eget laoreet dignissim, risus orci porttitor diam, at luctus felis massa vel libero. Aliquam et ligula quis mauris volutpat tincidunt eget dictum est. Aliquam varius imperdiet libero, nec mollis tellus vestibulum at. Suspendisse potenti. Sed nunc ipsum, tincidunt non molestie eget, suscipit ac nibh. Praesent vehicula, nunc sed varius porttitor, metus nisi tincidunt nulla, vel vulputate sem lectus vel tellus. Sed felis mi, congue et finibus sit amet, luctus sit amet justo.
                    <hr>
                    Attenzione una volta confermato per tornare indietro dovrai rivolgerti alla nostra assistenza.
                    <form action="{{ Route('MyAtlantinex.becomeMarketerConfirm') }}" method="POST">
                        @csrf
                        <button type="submit" class="btn btn-primary mt-4">Confermo</button>
                    </form>
                </p>

            </div>
        </div>
    </div>

@endsection

@endauth

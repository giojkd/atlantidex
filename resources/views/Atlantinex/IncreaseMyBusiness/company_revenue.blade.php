@auth

@extends('admin_custom.top_left')
@section('content')



    <div class="row">
        <div class="col-md-3">
            <h3>{{$merchant->name}}</h3>
            <div class="card">
                <div class="card-body image-flex text-center">
                    {!! $merchant->cover(120) !!}
                </div>
            </div>
        </div>
        <div class="col-md-9 align-self-end">
            <div class="form-group row justify-content-md-end align-items-center">
                <label for="">Mese di produzione</label>
                <select name="month" id="month" class="form-control col-md-2 mx-md-2">
                    @foreach($form['productionMonths'] as $month)
                        <option value="{{ $month['value'] }}" @if($month['value'] == $filters['month']) selected @endif>{{ $month['label'] }}</option>
                    @endforeach
                </select>
                <button id="set-date" class="btn btn-outline-primary btn col-md-2 mx-md-2">Applica filtro</button>
            </div>
            <input type="hidden" id="user_id" value="{{$merchant->id}}">

        </div>
    </div>
    <hr>
    <div class="row justify-content-md-end">
        <div class="col-md-9">
            <div class="table-responsive">
            <table class="table table-striped">
                <tbody>
                <tr>
                    <td class="dark-gray">Fatturato azienda</td>
                    <td class="dark-gray text-right">0 €</td>
                </tr>
                <tr>
                    <td class="dark-gray">Totale EURO acquisiti</td>
                    <td class="dark-gray text-right">0 €</td>
                </tr>
                <tr>
                    <td class="dark-gray">Totale EP generati</td>
                    <td class="dark-gray text-right">0 EP</td>
                </tr>
                <tr>
                    <td class="dark-gray">Consumatori affiliati</td>
                    <td class="dark-gray text-right">0</td>
                </tr>
                <tr>
                    <td class="dark-gray">Totale acquisti consumatori affiliati</td>
                    <td class="dark-gray text-right">0 €</td>
                </tr>
                <tr>
                    <td class="dark-gray">% mio guadagno EURO su fatturato generato</td>
                    <td class="dark-gray text-right">0 €</td>
                </tr>
                </tbody>
            </table>
        </div>
        </div>
    </div>


@endsection

@push('custom_styles')
<style>
.image-flex img {
    max-width: 100%;
    height: 150px;
}
.card {
    margin-bottom:5px !important;
}

.dark-gray {
    background-color: lightgray;
    font-weight: bold;
    border: 2px solid #f0f3f9 !important;
    padding:3px !important;
}
</style>

@endpush

@push('before_closing_body')

@endpush

@push('after_scripts')
<script>
    function getData() {
        var net_id = $('#user_id').val();
        var month = $('#month').val();
        $.ajax({
            method: "GET",
            data:{
                month
            },
            url: "/increase-my-business/"+net_id+"/company",
        }).done(function (data) {
            console.log(data);
            $('#networker-code').html(data['networker'].code);
            $('#networker-purchase').html(formatter.format(data['revenues'].npurchase));

        });
    }

    $('#set-date').click(function (e) {
        /*var start = dateSum($('#start').val());
        var end = dateSum($('#end').val());
        if (start > end) {
            alert('Controllare le date, non sono coerenti');
            return
        }*/
        getData();
    });

    function dateSum(datestring) {
        var datecomponent = datestring.split('-');
        return parseInt(datecomponent[0]) + parseInt(datecomponent[1]);
    }

    const formatter = new Intl.NumberFormat('it-IT', {
    style: 'currency',
    currency: 'EUR',
    minimumFractionDigits: 2
    })

</script>



@endpush

@endauth


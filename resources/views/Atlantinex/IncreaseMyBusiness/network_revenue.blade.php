@auth

@extends('admin_custom.top_left')
@section('content')

<link rel="stylesheet" href="//cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">


    <div class="row">
        <div class="col-md-12 align-self-end  revenue-report">
            <form class="form" action="/increase-my-business/{{$user->id}}/my-team">
                <div class="form-group row justify-content-md-end align-items-center">
                    <label for="">Mese di produzione</label>
                    <select name="month" id="month" class="form-control col-md-2 mx-md-2">
                        @foreach($form['productionMonths'] as $index => $month)
                            <option value="{{ $index }}" @if($index == $filters['month']) selected @endif>{{ $month['3'] }}</option>
                        @endforeach
                    </select>
                    <button id="set-date" class="btn btn-outline-primary btn col-md-2 mx-md-2" type="submit">Applica filtro</button>
                </div>
            </form>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h3>Il mio network</h3>

                </div>
                <div class="card-body card-center" id="tree-container">

                    <div class="tree" id="tree">

                        @if (!$isroot)
                            <ul>
                                <li>
                                    <a href="/increase-my-business/{{$rootuser->id}}/my-team?month={{ $filters['month'] }}">{{$rootuser->name}} {{$rootuser->surname}} <br>ID{{$rootuser->code}} ({{ $rootuser->roles->first()->name }})
                                </a>
                                <ul class="root">
                                    <li>
                                        <a class="to-root" href="/increase-my-business/{{$parent->id}}/my-team?month={{ $filters['month'] }}">↑</a>
                        @endif
                        <ul>

                            <li>
                                <a href="/increase-my-business/{{$user->id}}/my-team?month={{ $filters['month'] }}">{{$user->name}} {{$user->surname}}<br>ID{{$user->code}} ({{ $user->roles->first()->name }})</a>
                                @if($entries->count())
                                    @include('Atlantinex.IncreaseMyBusiness.tree_entry')
                                @endif
                            </li>
                        </ul>
                        @if (!$isroot)
                            </li></ul>
                        </li></ul>
                        @endif
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h2>{{$user->name}} {{$user->surname}}</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-7 text-center">
                                    <h5>Montly career</h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <h6>EP Totali Network</h6>
                                </div>
                                <div class="col-md-4">
                                    <h6 class="bg-primary text-center">{{$totalNetworkEp}}</h6>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <h6>Regola del 50%</h6>
                                </div>
                                <div class="col-md-4">
                                    <h6 class="bg-secondary text-center text-white">{{$fiftyPercentRule}}</h6>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <h6>EP Bonus</h6>
                                </div>
                                <div class="col-md-4">
                                    <h6 class="bg-primary text-center">{{$bonusEps}} </h6>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <h6>EP personali</h6>
                                </div>
                                <div class="col-md-4">
                                    <h6 class="bg-secondary text-center text-white">{{$personalEps}}</h6>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <h6>Tutti gli EP (Regola del 50%+EP bonus+Ep personali)</h6>
                                </div>
                                <div class="col-md-4">
                                    <h6 class="bg-primary text-center text-white">{{$bonusEps + $fiftyPercentRule + $personalEps}}</h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-7 text-center">
                                    <h5>Long run career</h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <h6>EP Totali Long Run Career</h6>
                                </div>
                                <div class="col-md-4">
                                    <h6 class="bg-primary text-center">{{$totalNetworkEplr}}</h6>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <h6>EP Personali</h6>
                                </div>
                                <div class="col-md-4">
                                    <h6 class="bg-secondary text-center text-white">{{$personalEpslr}}</h6>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <h6>EP Bonus</h6>
                                </div>
                                <div class="col-md-4">
                                    <h6 class="bg-primary text-center">{{$bonusEpslr}} </h6>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <h6>Tutti gli EP</h6>
                                </div>
                                <div class="col-md-4">
                                    <h6 class="bg-secondary text-center text-white">{{$personalEpslr + $totalNetworkEplr + $bonusEpslr}}</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="table-responsive">
                    <table class="table table-striped data-table" width="100%">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>tessera n°</th>
                                <th>Email</th>
                                <th>Data di affiliazione</th>

                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($entries as $entry)
                                <tr>
                                    <td><a href="/increase-my-business/{{$entry->id}}/my-team">{{ $entry->name }} {{ $entry->surname }}</a></td>
                                    <td>{{ $entry->code }}</td>
                                    <td><a href="mailto:{{ $entry->email }}">{{ $entry->email }}</a></td>
                                    <td>{{ $entry->created_at }}</td>

                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
    </div>
    <hr class="d-none revenue-report">
    <div class="row justify-content-md-end d-none revenue-report">
        <div class="col-md-3">
            <h3 id="networker"></h3>
            <h5 id="networker-code"></h5>
        </div>
        <div class="col-md-9">
            <div class="table-responsive">
            <table class="table table-striped">
                <tbody>
                <tr>
                    <td class="dark-gray">Totale EURO acquisiti</td>
                    <td id="networker-purchase" class="dark-gray text-right">0 €</td>
                </tr>
                <tr>
                    <td class="dark-gray">Totale EP generati</td>
                    <td class="dark-gray text-right">0 EP</td>
                </tr>
                <tr>
                    <td class="dark-gray">Consumatori affiliati</td>
                    <td class="dark-gray text-right">0</td>
                </tr>
                <tr>
                    <td class="dark-gray">Totale acquisti consumatori affiliati</td>
                    <td class="dark-gray text-right">0 €</td>
                </tr>
                <tr>
                    <td class="dark-gray">% mio guadagno EURO su fatturato generato</td>
                    <td class="dark-gray text-right">0 €</td>
                </tr>
                </tbody>
            </table>
        </div>
        </div>
    </div>
    <hr class="d-none revenue-report">
    <div class="row justify-content-md-end d-none revenue-report">
        <div class="col-md-3">
            <h3>Downline</h3>
        </div>
        <div class="col-md-9">
            <table class="table table-striped">
                <tbody>
                <tr>
                    <td class="dark-gray">Totale EURO acquisiti</td>
                    <td class="dark-gray text-right">0 €</td>
                </tr>
                <tr>
                    <td class="dark-gray">Totale EP generati</td>
                    <td class="dark-gray text-right">0 EP</td>
                </tr>
                <tr>
                    <td class="dark-gray">Consumatori affiliati</td>
                    <td class="dark-gray text-right">0</td>
                </tr>
                <tr>
                    <td class="dark-gray">Totale acquisti consumatori affiliati</td>
                    <td class="dark-gray text-right">0 €</td>
                </tr>
                <tr>
                    <td class="dark-gray">% mio guadagno EURO su fatturato generato</td>
                    <td class="dark-gray text-right">0 €</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>



@endsection

@push('custom_styles')
<style>
.dark-gray {
    background-color: lightgray;
    font-weight: bold;
    border: 2px solid #f0f3f9 !important;
    padding:3px !important;
}




* {margin: 0; padding: 0;}

.tree
{
	width: auto;
	margin-left: auto;
	margin-right: auto;
}

.tree ul {
	padding-top: 20px; position: relative;

	transition: all 0.5s;
	-webkit-transition: all 0.5s;
	-moz-transition: all 0.5s;
}

.tree li {
	float: left; text-align: center;
	list-style-type: none;
	position: relative;
	padding: 20px 5px 0 5px;

	transition: all 0.5s;
	-webkit-transition: all 0.5s;
	-moz-transition: all 0.5s;
}

/*We will use ::before and ::after to draw the connectors*/

.tree li::before, .tree li::after{
	content: '';
	position: absolute; top: 0; right: 50%;
	border-top: 1px solid #ccc;
	width: 50%; height: 20px;
}

.tree li::after{
	right: auto; left: 50%;
	border-left: 1px solid #ccc;
}


/*We need to remove left-right connectors from elements without
any siblings*/
.tree li:only-child::after, .tree li:only-child::before {
	display: none;
}

/*Remove space from the top of single children*/
.tree li:only-child{ padding-top: 0;}

/*Remove left connector from first child and
right connector from last child*/
.tree li:first-child::before, .tree li:last-child::after{
	border: 0 none;
}
/*Adding back the vertical connector to the last nodes*/
.tree li:last-child::before{
	border-right: 1px solid #ccc;
	border-radius: 0 5px 0 0;
	-webkit-border-radius: 0 5px 0 0;
	-moz-border-radius: 0 5px 0 0;
}
.tree li:first-child::after{
	border-radius: 5px 0 0 0;
	-webkit-border-radius: 5px 0 0 0;
	-moz-border-radius: 5px 0 0 0;
}

/*Time to add downward connectors from parents*/
.tree ul ul::before{
	content: '';
	position: absolute; top: 0; left: 50%;
	border-left: 1px solid #ccc;
	width: 0; height: 20px;
}

.tree ul ul.root::before{
	content: '';
	position: absolute; top: 0; left: 50%;
	border-left: 1px dashed #ccc;
	width: 0; height: 20px;
}

.tree li a{
	border: 1px solid #ccc;
	padding: 5px 10px;
	text-decoration: none;
	color: #666;
	font-family: arial, verdana, tahoma;
	font-size: 11px;
	display: inline-block;
    width:120px;
	border-radius: 5px;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
    background-position: left center;
    background-position-x: 5px;
    background-image: none; /*url('/uploads/logo-dark2.png');*/
    background-repeat: no-repeat;
	transition: all 0.5s;
	-webkit-transition: all 0.5s;
	-moz-transition: all 0.5s;
}

.tree li a.to-root{
	border: 1px solid #ccc;
	padding: 5px 10px;
	text-decoration: none;
	color: #666;
	font-family: arial, verdana, tahoma;
	font-size: 11px;
	display: inline-block;
    width:50px;
	border-radius: 5px;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
    background-position: left center;
    background-image: none;
    background-repeat: no-repeat;
	transition: all 0.5s;
	-webkit-transition: all 0.5s;
	-moz-transition: all 0.5s;
}


/*Time for some hover effects*/
/*We will apply the hover effect the the lineage of the element also*/
.tree li a:hover, .tree li a:hover+ul li a {
	background: #c8e4f8; color: #000; border: 1px solid #94a0b4;
}
/*Connector styles on hover*/
.tree li a:hover+ul li::after,
.tree li a:hover+ul li::before,
.tree li a:hover+ul::before,
.tree li a:hover+ul ul::before{
	border-color:  #94a0b4;
}

/*Thats all. I hope you enjoyed it.
Thanks :)*/

.card-center {
    /*text-align: center;
    margin: auto;
    display: flex;*/
    overflow-y: scroll;
    overflow-x: scroll;
}

</style>

@endpush

@push('before_closing_body')

@endpush

@push('after_scripts')

<script src="//cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>

<script>
$(function() {
    console.log( "ready!" );
    changeWidth();
});

function changeWidth(){
    var e1 = $('#tree');
    console.log(e1);
    var elements = {{$countentries}};
    if (elements == 0) {
        e1.width(150);
    } else {
        e1.width(150*elements);
    }
}

$('.data-table').DataTable({
    language:{
        url: "/front/js/datatable-it.json"
    }
});



const formatter = new Intl.NumberFormat('it-IT', {
  style: 'currency',
  currency: 'EUR',
  minimumFractionDigits: 2
})


</script>

@endpush

@endauth


<ul>
    @foreach ($entries as $entry)
        <li>
            <a href="/increase-my-business/{{$entry->id}}/my-team?month={{ $filters['month'] }}">{{ $entry->name }} {{ $entry->surname }}<br>ID{{$entry->code}}<br>MCL {{ $entry->monthly_career_level }} EP {{ $entry->epoperations()->where('created_at','<','2020-11-18')->sum('value') }}
                ({{ $entry->roles->first()->name }})
                @if ($entry->children->count())
                    <br>↓
                @endif
            </a>
        </li>
    @endforeach
</ul>

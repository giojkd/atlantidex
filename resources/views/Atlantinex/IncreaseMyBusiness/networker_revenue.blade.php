@auth

@extends('admin_custom.top_left')
@section('content')

    <div class="row">
        <div class="col-md-2">
            <div class="card">
                <div class="card-body">
                    <h3>{{$customer->name}} {{$customer->surname}}</h3>
                </div>
            </div>
        </div>
        <div class="col-md-10 align-self-end">
            <div class="form-group row justify-content-md-end align-items-center">
                <form class="form form-inline" method="GET">
                    <label for="">Mese di produzione</label>
                    <select name="month" id="month" class="form-control ml-2">
                        @foreach($form['productionMonths'] as $index => $month)
                            <option value="{{ $index }}" @if($index == $filters['month']) selected @endif>{{ $month['3'] }}</option>
                        @endforeach
                    </select>
                    <button type="submit" class="btn btn-outline-primary ml-2">Applica filtro</button>
                </form>

            </div>
        </div>
    </div>
    <hr>
    <div class="row justify-content-md-end">
        <div class="col-md-9">
            <div class="table-responsive">
            <table class="table table-striped">
                <tbody>
                    <tbody>
                        @foreach ($purchases as $purchase)
                            <tr>
                                <td>
                                    {{date('d/m/Y',strtotime($purchase->created_at))}}
                                </td>
                                <td>
                                    @if ($purchase->description != '')
                                        {{$purchase->description}}
                                    @else
                                        Descrizione mancante
                                    @endif

                                </td><td>@fp($purchase->value)</td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="2" class="dark-gray">Totale acquisiti</th>
                            <th class="dark-gray text-right">@fp($purchases->sum('value'))</th>
                        </tr>
                    </tfoot>
                </tbody>
            </table>
        </div>
        </div>
    </div>


@endsection



@endauth


@auth

@extends('admin_custom.top_left')
@section('content')

<div class="row">
    <div class="col-md-12">
        <h3>Le mie Aziende</h3>
    </div>
        @forelse ($companies as $item)
            <div class="col-md-2 ">
                <a href="\increase-my-business\{{$item->id}}\company-revenue">
                <div class="card">
                    <div class="card-body image-flex text-center">
                        {!! $item->cover(120) !!}
                    </div>
                </div>
                <b>{{$item->name}}</b>
                </a>
            </div>
        @empty
            <div class="col-md-8">Non sono presenti Aziende associate</div>
        @endforelse
</div>
<div class="row">
    <div class="col-md-12">
        <h3>I miei consumatori</h3>
    </div>
        @forelse ($networkers as $item)
            <div class="col-md-2 text-center">
                <div class="card">
                    <div class="card-body">
                        <a href="\increase-my-business\{{$item->id}}\networker-revenue">
                            <b>{{$item->name}}</b> <b>{{$item->surname}}</b>
                        </a>
                    </div>
                </div>
            </div>
        @empty
            <div class="col-md-8">Non sono presenti Networkers associate</div>
        @endforelse
</div>


@endsection

@push('custom_styles')
<style>
.image-flex img {
    max-width: 100%;
    height: 100px;
}
.card {
    margin-bottom:5px !important;
}
</style>
@endpush

@push('before_closing_body')

@endpush

@push('after_scripts')



@endpush

@endauth


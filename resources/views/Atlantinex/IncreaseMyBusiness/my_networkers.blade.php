@auth

@extends('admin_custom.top_left')
@section('content')



    <div class="row">
        <div class="col-md-12">
            <h1>I miei consumatori</h1>
        </div>
            @forelse ($networkers as $item)
                <div class="col-md-2 text-center">
                    <div class="card">
                        <div class="card-body">
                            <a href="\increase-my-business\{{$item->id}}\networker-revenue">
                                <b style="text-transform: capitalize">{{$item->name}}</b><br><b style="text-transform: capitalize">{{$item->surname}}</b>
                            </a>
                        </div>
                    </div>
                </div>
            @empty
                <div class="col-md-8">Non sono presenti consumatori associati</div>
            @endforelse
        </div>
    </div>


@endsection

@push('custom_styles')

@endpush

@push('before_closing_body')

@endpush

@push('after_scripts')



@endpush

@endauth


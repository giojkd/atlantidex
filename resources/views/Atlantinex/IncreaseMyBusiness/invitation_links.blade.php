@auth

@extends('admin_custom.top_left')
@section('content')



    <div class="row">
        <div class="col-md-8">
            <h1>Genera i tuoi link di invito</h1>
               <span>Hai a disposizione</span>
                <ul>
                    @if($user->company_invitation_links_count >0 )
                       <li>
                            <!--<b>{{ $user->networker_invitation_links_count  }}</b>--> I tuoi inviti per i marketer
                        </li>
                    @endif
                    @if($user->company_invitation_links_count >0 )
                        <li>
                            <!--<b>{{ $user->company_invitation_links_count }}</b>--> Gli inviti per affiliare le aziende
                        </li>
                    @endif
                    @if($user->company_booster_invitation_links_count >0 )
                        <li>
                            <b>{{ $user->company_booster_invitation_links_count }}</b> inviti di tipo booster per affiliare le aziende
                        </li>
                    @endif


                </ul>
            <div class="card">
                <div class="card-body">
                    Clicca sul tasto "Genera link" copia il link appena generato ed invialo
                </div>
            </div>
            @if($user->networker_invitation_links_count > 0 || $user->company_invitation_links_count > 0)
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('IncreaseMyBusiness.invitation-links.store') }}" method="POST" class="form">
                        @csrf
                        <div class="form-group">
                            <select name="data[link_type]" id="" class="form-control">
                                @if($user->networker_invitation_links_count > 0)
                                    <option value="networker">Invita un nuovo Consumatore</option>
                                @endif
                                @if($user->company_invitation_links_count > 0)
                                    <option value="company">Invita una nuova Azienda</option>
                                @endif
                                @if($user->company_booster_invitation_links_count > 0)
                                    <option value="company_booster">Invita una nuova Azienda con invito di tipo Booster</option>
                                @endif
                            </select>
                            <button type="submit" class="btn btn-primary btn-block mt-2">Genera link</button>
                        </div>
                    </form>
                </div>
            </div>
            @endif

             @if(isset($invitationLinks) && collect($invitationLinks)->count() > 0)
             <div class="table-responsive">
                <table class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Tipo</th>
                            <th>Creato il</th>
                            <th>Scade il</th>
                            {{-- <th>Riscattato</th>  --}}
                            <th>Azioni</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($invitationLinks as $link)
                            <tr>
                                <td>{{ $link['id'] }}</td>
                                <td>{{ $link['link_type'] }}</td>
                                <td>{{ $link['created_at'] }}</td>
                                <td>{{ $link['expires_at'] }}</td>
                                {{-- <td>{{ $link['is_redeemed'] }}</td>  --}}
                                <td>
                                    <a href="javascript:copyTextToClipboard('{{ $link['code'] }}');" class="btn btn-primary">Copia il link</a>
                                    <a target="_blank" href="https://wa.me/?text=Iscriviti subito ad Atlantidex, il nuovo mondo al prezzo che vuoi tu! Clicca sul link {{ $link['code'] }} e completa la registrazione!" class="btn btn-success"> <i class="fa fa-whatsapp"></i> </a>
                                    <a href="mailto:?&subject=Iscriviti ad Atlantidex!&body=Iscriviti subito ad Atlantidex, il nuovo mondo al prezzo che vuoi tu! Clicca sul link {{ $link['code'] }} e completa la registrazione!" class="btn btn-secondary text-white"> <i class="fa fa-envelope"></i> </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @endif

        </div>
    </div>


@endsection

@push('custom_styles')

@endpush

@push('before_closing_body')

@endpush

@push('after_scripts')

    <script>



        function fallbackCopyTextToClipboard(text) {
            var textArea = document.createElement("textarea");
            textArea.value = text;

            // Avoid scrolling to bottom
            textArea.style.top = "0";
            textArea.style.left = "0";
            textArea.style.position = "fixed";

            document.body.appendChild(textArea);
            textArea.focus();
            textArea.select();

            try {
                var successful = document.execCommand('copy');
                var msg = successful ? 'successful' : 'unsuccessful';
                console.log('Fallback: Copying text command was ' + msg);
                alert('Copiato con successo');
            } catch (err) {
                console.error('Fallback: Oops, unable to copy', err);
            }

            document.body.removeChild(textArea);
            }
            function copyTextToClipboard(text) {
            if (!navigator.clipboard) {
                fallbackCopyTextToClipboard(text);
                return;
            }
            navigator.clipboard.writeText(text).then(function() {
                console.log('Async: Copying to clipboard was successful!');
                alert('Copiato con successo');
            }, function(err) {
                console.error('Async: Could not copy text: ', err);
            });
        }



    </script>

@endpush

@endauth


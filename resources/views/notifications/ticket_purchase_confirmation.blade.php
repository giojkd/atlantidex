@extends('notifications.main')
@section('content')
<table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
      <tbody>
        <tr>
          <td class="o_bg-light o_px-xs" align="center">
            <!--[if mso]><table width="800" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td><![endif]-->
            <table class="o_block-lg" width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
              <tbody>
                <tr>
                  <td class="o_bg-primary o_px-md o_py-xl o_xs-py-md" align="center">
                    <!--[if mso]><table width="584" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td align="center"><![endif]-->
                    <div class="o_col-6s o_center o_sans o_text-md o_text-white">
                      <table class="o_center" cellspacing="0" cellpadding="0" border="0" role="presentation">
                        <tbody>
                          <tr>
                            <td class="o_sans o_text o_text-white o_b-white o_px o_py o_br-max" align="center">
                              <img editable src="{{ $resource_prefix }}shopping_cart-48-white.png" width="48" height="48" alt="" style="max-width: 48px;">
                            </td>
                          </tr>
                          <tr>
                            <td style="font-size: 24px; line-height: 24px; height: 24px;">&nbsp; </td>
                          </tr>
                        </tbody>
                      </table>
                      <h2 class="o_heading o_mb-xxs"><singleline label="Title">Ecco il tuo biglietto!</singleline></h2>
                      <multiline label="Content">
                        <p>Conserva questa email e mostra il codice all'ingresso dell'evento</p>
                      </multiline>
                    </div>
                    <!--[if mso]></td></tr></table><![endif]-->
                  </td>
                </tr>
              </tbody>
            </table>
            <!--[if mso]></td></tr></table><![endif]-->
          </td>
        </tr>
      </tbody>
    </table>
    <table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
      <tbody>
        <tr>
          <td class="o_bg-light o_px-xs" align="center">
            <!--[if mso]><table width="800" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td><![endif]-->
            <table class="o_block-lg" width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
              <tbody>
                <tr>
                  <td class="o_bg-white" style="font-size: 24px; line-height: 24px; height: 24px;">&nbsp; <singleline label="Blank">&nbsp; </singleline></td>
                </tr>
              </tbody>
            </table>
            <!--[if mso]></td></tr></table><![endif]-->
          </td>
        </tr>
      </tbody>
    </table>

<repeater>
    <layout label="order-summary">
      <table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
        <tbody>
          <tr>
            <td class="o_bg-light o_px-xs" align="center">
              <!--[if mso]><table width="800" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td><![endif]-->
              <table class="o_block-lg" width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
                <tbody>
                  <tr>
                    <td class="o_bg-white o_sans o_text-xs o_text-light o_px-md o_pt-xs" align="center">
                      <p><singleline label="Content">Riepilogo</singleline></p>
                      <p><singleline label="Content"><b>{{ $event->title }}</b></singleline></p>
                      <div>{!! $event->description !!}</div>
                      <table cellspacing="0" cellpadding="0" border="0" role="presentation">
                        <tbody>
                          <tr>
                            <td width="584" class="o_re o_bb-light" style="font-size: 8px; line-height: 8px; height: 8px;">&nbsp; </td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                </tbody>
              </table>
              <!--[if mso]></td></tr></table><![endif]-->
            </td>
          </tr>
        </tbody>
      </table>
    </layout>
    <layout label="product-track">
      <table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
        <tbody>
          <tr>
            <td class="o_bg-light o_px-xs" align="center">
              <!--[if mso]><table width="800" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td><![endif]-->
              <table class="o_block-lg" width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
                <tbody>
                  <tr>
                    <td class="o_re o_bg-white o_px o_pt" align="center">
                      <!--[if mso]><table cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td width="100" align="center" valign="top" style="padding: 0px 8px;"><![endif]-->
                      @if(!is_null($event->photos))
                        <div class="o_col o_col-1 o_col-full">
                            <div class="o_px-xs o_sans o_text o_center">
                            <p class="o_text-primary o_link">
                                <img editable src="{{ $url }}/{{ collect($event->photos)->first() }}" width="84" alt="" style="max-width: 84px;">
                                </p>
                            </div>
                        </div>
                      @endif
                      <!--[if mso]></td><td width="300" align="left" valign="top" style="padding: 0px 8px;"><![endif]-->
                      <div class="o_col o_col-3 o_col-full">
                        <div style="font-size: 10px; line-height: 10px; height: 10px;">&nbsp; </div>
                        <div class="o_px-xs o_sans o_text-xs o_text-secondary o_left o_xs-center">
                          <multiline label="Content">
                            <p class="o_text o_text-dark"><strong>{{ $ticket->name }}</strong></p>
                            <p class="o_mb-xxs">{{ $ticket->description }}</p>
                            <p class="o_text-primary"><strong>@fp($ticket->price)</strong></p>
                          </multiline>
                        </div>
                      </div>
                      <!--[if mso]></td><td width="200" align="right" valign="top" style="padding: 0px 8px;"><![endif]-->
                      <div class="o_col o_col-2 o_col-full">
                        <div style="font-size: 10px; line-height: 10px; height: 10px;">&nbsp; </div>
                        <div class="o_px-xs o_sans o_text-xxs o_text-light o_right o_xs-center">
                        @if (!$event->is_online)
                            <table class="o_right o_xs-center" cellspacing="0" cellpadding="0" border="0" role="presentation">
                                <tbody>
                                <tr>
                                    <td class="o_btn-xs o_bg-dark o_br o_heading o_text-xs" align="center">
                                    <a class="o_text-white" href="https://www.google.com/maps/place/{{ $event->address['value'] }}" target="_blank"><singleline label="Button">{{ $event->address['value']}}</singleline></a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                          @endif
                          <div style="font-size: 8px; line-height: 8px; height: 8px;">&nbsp; </div>
                          <p><singleline label="Code">Inizio: {{ date('d/m/Y H:i',strtotime($event->happens_at)) }} Fine: {{ date('d/m/Y H:i',strtotime($event->ends_at)) }}</singleline></p>
                        </div>
                      </div>
                      <!--[if mso]></td></tr><tr><td colspan="3" style="padding: 0px 8px;"><![endif]-->
                      <div class="o_px-xs">
                        <table cellspacing="0" cellpadding="0" border="0" role="presentation">
                          <tbody>
                            <tr>
                              <td width="584" class="o_re o_bb-light" style="font-size: 16px; line-height: 16px; height: 16px;">&nbsp; </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <!--[if mso]></td></tr></table><![endif]-->
                    </td>
                  </tr>
                </tbody>
              </table>
              <!--[if mso]></td></tr></table><![endif]-->
            </td>
          </tr>
        </tbody>
      </table>
    </layout>
    <layout label="invoice-total">
      <table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
        <tbody>
          <tr>
            <td class="o_bg-light o_px-xs" align="center">
              <!--[if mso]><table width="800" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td><![endif]-->
              <table class="o_block-lg" width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
                <tbody>
                  <tr>
                    <td class="o_re o_bg-white o_px-md o_py" align="center">
                      <!--[if mso]><table width="584" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td align="right"><![endif]-->
                      <div class="o_col-6s o_right">
                        <table class="o_right" role="presentation" cellspacing="0" cellpadding="0" border="0">
                          <tbody>
                            <tr>
                              <td width="284" align="left">
                                <table width="100%" role="presentation" cellspacing="0" cellpadding="0" border="0">
                                  <tbody>
                                    <tr>
                                      <td width="50%" class="o_pt-xs" align="left">
                                        <p class="o_sans o_text o_text-secondary"><singleline label="Subtotal">Parziale</singleline></p>
                                      </td>
                                      <td width="50%" class="o_pt-xs" align="right">
                                        <p class="o_sans o_text o_text-secondary"><singleline label="Subtotal">@fp($ticket->price)</singleline></p>
                                      </td>
                                    </tr>
                                    {{-- <tr>
                                      <td width="50%" class="o_pt-xs" align="left">
                                        <p class="o_sans o_text o_text-secondary"><singleline label="Tax">Tax</singleline></p>
                                      </td>
                                      <td width="50%" class="o_pt-xs" align="right">
                                        <p class="o_sans o_text o_text-secondary"><singleline label="Tax">$2,000 USD</singleline></p>
                                      </td>
                                    </tr> --}}
                                    <tr>
                                      <td class="o_pt o_bb-light">&nbsp; </td>
                                      <td class="o_pt o_bb-light">&nbsp; </td>
                                    </tr>
                                    <tr>
                                      <td width="50%" class="o_pt" align="left">
                                        <p class="o_sans o_text o_text-secondary"><strong><singleline label="Total">Totale</singleline></strong></p>
                                      </td>
                                      <td width="50%" class="o_pt" align="right">
                                        <p class="o_sans o_text o_text-primary"><strong><singleline label="Total">@fp($ticket->price)</singleline></strong></p>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <!--[if mso]></td></tr></table><![endif]-->
                    </td>
                  </tr>
                </tbody>
              </table>
              <!--[if mso]></td></tr></table><![endif]-->
            </td>
          </tr>
        </tbody>
      </table>
    </layout>
    <layout label="content">
      <table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
        <tbody>
          <tr>
            <td class="o_bg-light o_px-xs" align="center">
              <!--[if mso]><table width="800" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td><![endif]-->
              <table class="o_block-lg" width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
                <tbody>
                  <tr>
                    <td class="o_bg-white o_px-md o_py" align="center">
                      <!--[if mso]><table width="584" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td align="center"><![endif]-->
                      <div class="o_col-6s o_sans o_text o_text-secondary o_center">
                        <multiline label="Content">
                          <p>Utilizza il qr code qui sotto per accedere all'evento</p>
                        </multiline>
                      </div>
                      <!--[if mso]></td></tr></table><![endif]-->
                    </td>
                  </tr>
                </tbody>
              </table>
              <!--[if mso]></td></tr></table><![endif]-->
            </td>
          </tr>
        </tbody>
      </table>
    </layout>
    <layout label="button-success">
      <table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
        <tbody>
          <tr>
            <td class="o_bg-light o_px-xs" align="center">
              <!--[if mso]><table width="800" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td><![endif]-->
              <table class="o_block-lg" width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
                <tbody>
                  <tr>
                    <td class="o_bg-white o_px-md o_py-xs" align="center">
                      <table align="center" cellspacing="0" cellpadding="0" border="0" role="presentation">
                        <tbody>
                          <tr>

                            <td width="300" class="o_btn o_br o_heading o_text" align="center">
                              <img src="{{ $qr_code }}" alt="">
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                </tbody>
              </table>
              <!--[if mso]></td></tr></table><![endif]-->
            </td>
          </tr>
        </tbody>
      </table>
    </layout>
  </repeater>
@endsection

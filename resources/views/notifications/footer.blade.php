 <table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
      <tbody>
        <tr>
          <td class="o_bg-light o_px-xs" align="center">
            <!--[if mso]><table width="800" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td><![endif]-->
            <table class="o_block-lg" width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
              <tbody>
                <tr>
                  <td class="o_bg-white" style="font-size: 24px; line-height: 24px; height: 24px;">&nbsp; <singleline label="Blank">&nbsp; </singleline></td>
                </tr>
              </tbody>
            </table>
            <!--[if mso]></td></tr></table><![endif]-->
          </td>
        </tr>
      </tbody>
    </table>
    <table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
      <tbody>
        <tr>
          <td class="o_bg-light o_px-xs o_pb-lg o_xs-pb-xs" align="center">
            <!--[if mso]><table width="800" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td><![endif]-->
            <table class="o_block-lg" width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
              <tbody>
                <tr>
                  <td class="o_re o_bg-dark o_px o_pb-lg o_br-b" align="center">
                    <!--[if mso]><table cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td width="200" align="left" valign="top" style="padding:0px 8px;"><![endif]-->
                    <div class="o_col o_col-4">
                      <div style="font-size: 32px; line-height: 32px; height: 32px;">&nbsp; </div>
                      <div class="o_px-xs o_sans o_text-xs o_text-dark_light o_left o_xs-center">
                        <multiline label="Content">
                          <p class="o_mb-xs">©{{ date('Y') }} {{ $company_name }} Tutti i diritti sono riservati.</p>
                          <p class="o_mb-xs">{{ $company_address }}</p>
                          <p>
                            <a class="o_text-dark_light o_underline" href="{{ $help_center_link }}">Centro assistenza</a> <span class="o_hide-xs">&nbsp; • &nbsp;</span><br class="o_hide-lg">
                            <a class="o_text-dark_light o_underline" href="{{ $preferences_link }}">Preferenze</a> <span class="o_hide-xs">&nbsp; • &nbsp;</span><br class="o_hide-lg">
                            <a class="o_text-dark_light o_underline" href="{{ $unsubscribe_link }}">Disiscriviti</a>
                          </p>
                        </multiline>
                      </div>
                    </div>
                    <!--[if mso]></td><td width="400" align="right" valign="top" style="padding:0px 8px;"><![endif]-->
                    <div class="o_col o_col-2">
                      <div style="font-size: 32px; line-height: 32px; height: 32px;">&nbsp; </div>
                      <div class="o_px-xs o_sans o_text-xs o_text-dark_light o_right o_xs-center">
                        <p class="o_text-dark_light o_link">
                          <a href="{{ $facebook_link }}">
                            <img editable src="{{ $resource_prefix }}facebook-light.png" width="36" height="36" alt="fb" style="max-width: 36px;"><span> &nbsp;</span>
                          </a>
                          <a href="{{ $twitter_link }}">
                            <img editable src="{{ $resource_prefix }}twitter-light.png" width="36" height="36" alt="tw" style="max-width: 36px;"><span> &nbsp;</span>
                          </a>
                          <a href="{{ $instagram_link }}">
                            <img editable src="{{ $resource_prefix }}instagram-light.png" width="36" height="36" alt="ig" style="max-width: 36px;"><span> &nbsp;</span>
                          </a>
                        </p>
                      </div>
                    </div>
                    <!--[if mso]></td></tr></table><![endif]-->
                  </td>
                </tr>
              </tbody>
            </table>
            <!--[if mso]></td></tr></table><![endif]-->
            <div class="o_hide-xs" style="font-size: 64px; line-height: 64px; height: 64px;">&nbsp; </div>
          </td>
        </tr>
      </tbody>
    </table>
  </body>
</html>
